package com.foodridge.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.foodridge.R;
import com.foodridge.api.model.Meal;
import com.foodridge.api.model.Order;
import com.foodridge.application.AContext;
import com.foodridge.fragment.main.DishFragment;
import com.foodridge.fragment.main.ListDishesFragment;
import com.foodridge.fragment.main.OrdersFragment;
import com.foodridge.fragment.main.SideMenuFragment;
import com.foodridge.model.Profile;
import com.foodridge.type.OrderType;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class MainActivity extends BaseActivity {

    private static final String EXTRA_0 = "EXTRA_0";//Meal
    private static final String EXTRA_1 = "EXTRA_1";//Order

    public static void start(@NonNull Activity activity) {
        activity.startActivity(new Intent(activity, MainActivity.class));
    }

    @NonNull
    public static Intent newInstance(@NonNull Context context, @NonNull Meal meal) {
        return newInstance(context, EXTRA_0, meal);
    }

    @NonNull
    public static Intent newInstance(@NonNull Context context, @NonNull Order order) {
        return newInstance(context, EXTRA_1, order);
    }

    @NonNull
    private static Intent newInstance(@NonNull Context context, @NonNull String key, @NonNull Parcelable parcelable) {
        return new Intent(context, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(key, parcelable);
    }

    //VIEW's
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainer;
    private View mMenuContainer;
    private Toolbar mToolbar;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle b) {
        super.onCreate(b);
        super.setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mFragmentContainer = findViewById(R.id.fragment_container);
        mMenuContainer = findViewById(R.id.menu_container);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        {
            setSupportActionBar(mToolbar);
        }
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.content_Menu_open, R.string.content_application_logo) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AContext.hideInputKeyboard(drawerView);
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.setToolbarNavigationClickListener(mToolbarNavigationClickListener);

        getSupportFragmentManager().addOnBackStackChangedListener(mBackStackChangedListener);
        if (b == null) {
            startFragmentSimple(R.id.menu_container, SideMenuFragment.newInstance(), false);
            onIntent(getIntent());

        } else {
            setNavigationEnabled(getSupportFragmentManager().getBackStackEntryCount() == 0);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void onIntent(Intent intent) {
        if (intent.hasExtra(EXTRA_0)) {
            final Meal meal = intent.getParcelableExtra(EXTRA_0);
            startFragment(ListDishesFragment.newInstance(), false);
            startFragment(DishFragment.newInstance(null, meal), true);

        } else if (intent.hasExtra(EXTRA_1)) {
            final Order order = intent.getParcelableExtra(EXTRA_1);

            @OrderType final int type;
            if (!order.isActive()) {
                type = OrderType.COMPLETED;

            } else if (Profile.getInstance().getId().equals(order.getMeal().getChef().getId())) {
                type = OrderType.RECEIVED;

            } else {
                type = OrderType.PLACED;
            }
            startFragment(OrdersFragment.newInstance(type), false);
            SideMenuFragment.notifyMenuAboutSelection(R.id.nav_orders);

        } else {
            startFragment(ListDishesFragment.newInstance(), false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggleMenu();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getFragmentContainerId() {
        return R.id.fragment_container;
    }

    @Override
    public void setBlock(boolean block) {
    }

    @Override
    public void onCleanUp() throws Exception {
        mDrawerLayout.removeDrawerListener(mDrawerToggle);

        Utils.cleanUp(mDrawerLayout);
        Utils.cleanUp(mFragmentContainer);
        Utils.cleanUp(mMenuContainer);
        Utils.cleanUp(mToolbar);
    }

    private void toggleMenu() {
        if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);

        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    public boolean closeMenu() {
        final boolean isDrawerVisible = mDrawerLayout.isDrawerVisible(GravityCompat.START);
        if (isDrawerVisible) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        return isDrawerVisible;
    }

    @Override
    public void onBackPressed() {
        if (!popBackStack()) {
            super.onBackPressed();
        }
    }

    private boolean popBackStack() {
        final int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount > 0) {
            getSupportFragmentManager().popBackStack();
            return true;
        }
        return closeMenu();
    }

    @SuppressWarnings("ConstantConditions")
    public void setNavigationEnabled(boolean isEnabled) {
        mDrawerToggle.setDrawerIndicatorEnabled(isEnabled);
        getSupportActionBar().setDisplayHomeAsUpEnabled(!isEnabled);

        if (isEnabled) {
            mDrawerToggle.syncState();
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    private final OnClickListener mToolbarNavigationClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            popBackStack();
        }
    };

    private final OnBackStackChangedListener mBackStackChangedListener = new OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            final int count = getSupportFragmentManager().getBackStackEntryCount();
            switch (count) {
                case 0:
                    setNavigationEnabled(true);
                    break;

                case 1:
                default:
                    setNavigationEnabled(false);
                    break;
            }
        }
    };
}