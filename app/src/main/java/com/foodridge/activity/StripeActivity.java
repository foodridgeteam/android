package com.foodridge.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.foodridge.R;
import com.foodridge.api.request.ANetworkRequest;
import com.foodridge.api.request.GetProfileChefRequest;
import com.foodridge.application.AContext;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.fragment.stripe.StripeFragment;
import com.foodridge.utility.BaseUtils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 25/04/16
 */
public class StripeActivity extends AppCompatActivity {

    private static final String EXTRA_0 = "EXTRA_0";//tokenId
    private static final String EXTRA_1 = "EXTRA_1";//total

    public static void start(@NonNull Fragment f, int requestCode, float total) {
        f.startActivityForResult(new Intent(f.getActivity(), StripeActivity.class).putExtra(EXTRA_1, total), requestCode);
    }

    public static void sendResult(@NonNull Activity activity, @NonNull String tokenId) {
        activity.setResult(RESULT_OK, new Intent().putExtra(EXTRA_0, tokenId));
        activity.finish();
    }

    @NonNull
    public static String getToken(@NonNull Intent intent) {
        final String token = intent.getStringExtra(EXTRA_0);
        if (token == null) {
            throw new IllegalStateException();
        }
        return token;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(@Nullable Bundle b) {
        super.onCreate(b);

        getWindow().setBackgroundDrawable(null);
        Thread.setDefaultUncaughtExceptionHandler(AContext.getApp().getExceptionHandler());

        if (getIntent().getData() != null) {
            new ANetworkRequest(new GetProfileChefRequest()).oneShot(Thread.MAX_PRIORITY);
            finish();
            return;
        }
        setContentView(R.layout.activity_stripe);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.Pay_with_card);

        if (b == null) {
            final float total = getIntent().getFloatExtra(EXTRA_1, 0);
            startFragmentSimple(R.id.fragment_container, StripeFragment.newInstance(total), false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean startFragmentSimple(int containerId, @NonNull BaseFragment fragment, boolean addToBackStack) {
        final String tag = BaseUtils.getTag(fragment);
        final FragmentManager manager = getSupportFragmentManager();

        if (!addToBackStack) {
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        final FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(containerId, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();

        return true;
    }
}