package com.foodridge.activity;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import com.foodridge.R;
import com.foodridge.api.model.ServerError;
import com.foodridge.api.request.AMultipartNetworkRequest;
import com.foodridge.api.request.ANetworkRequest;
import com.foodridge.api.request.BaseMultipartRequestEntity;
import com.foodridge.api.request.BaseRequestEntity;
import com.foodridge.application.AContext;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.interfaces.CleaningEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.interfaces.OnBackPressedListener;
import com.foodridge.model.DbRequestEntity;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.RequestEntityContract;
import com.foodridge.request.BaseRequest;
import com.foodridge.request.RequestCenter;
import com.foodridge.service.ExecutorService;
import com.foodridge.type.RequestEntityStatus;
import com.foodridge.utility.BaseUtils;
import com.foodridge.utility.Log;
import com.foodridge.utility.Utils;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public abstract class BaseActivity extends AppCompatActivity
        implements ObjectsReceiver, CleaningEntity {

    protected static final Log LOG = Log.LOG;

    private final SpiceManager mSpiceManager = new SpiceManager(ExecutorService.class);

    @Override
    protected void onCreate(@Nullable Bundle b) {
        super.onCreate(b);

        getWindow().setBackgroundDrawable(null);
        Thread.setDefaultUncaughtExceptionHandler(AContext.getApp().getExceptionHandler());

        getSupportLoaderManager().initLoader(R.id.loader_request, null, mLoaderCallbacks);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSpiceManager.shouldStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            RequestCenter.cancel(this);
            onCleanUp();

        } catch (Exception ignore) {
        } finally {
            System.gc();
        }
    }

    public abstract int getFragmentContainerId();

    public abstract void setBlock(boolean block);

    @Nullable
    public final Fragment getFragmentFromContainer() {
        return getFragmentFromContainer(getFragmentContainerId());
    }

    @Nullable
    public final Fragment getFragmentFromContainer(@IdRes int containerId) {
        return getSupportFragmentManager().findFragmentById(containerId);
    }

    public boolean startFragment(@NonNull BaseFragment fragment, boolean addToBackStack, @NonNull Object... objects) {
        return startFragmentSimple(getFragmentContainerId(), fragment, addToBackStack);
    }

    protected boolean startFragmentSimple(int containerId, @NonNull BaseFragment fragment, boolean addToBackStack) {
        final String tag = BaseUtils.getTag(fragment);
        final FragmentManager manager = getSupportFragmentManager();

        if (!addToBackStack && containerId == getFragmentContainerId()) {
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        final FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(containerId, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();

        return true;
    }

    @Override
    public void onBackPressed() {
        final Fragment fragment = getFragmentFromContainer();
        final boolean pressBack = !(fragment instanceof OnBackPressedListener) || ((OnBackPressedListener) fragment).onBackPress();
        if (pressBack) {
            super.onBackPressed();
        }
    }

    @CallSuper
    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case R.id.code_block:
                final boolean block = (boolean) objects[0];
                setBlock(block);
                break;

            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                break;

            case RequestCenter.CODE_ERROR:
                setBlock(false);

                final ServerError error = (ServerError) objects[1];
//                Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
                Utils.showError(this, error.getMessage());
                break;

            default:
                break;
        }
    }

    public final <RESULT> boolean execute(@NonNull BaseRequest<RESULT> request, @Nullable RequestListener<RESULT> listener) {
        if (request.isNetworkRequired()) {
            final boolean isNetworkConnected = AContext.isNetworkConnected();
            if (isNetworkConnected) {
                mSpiceManager.execute(request, listener);

            } else if (listener != null) {
                Utils.showNoInternet(this);
            }
            return isNetworkConnected;

        } else {
            mSpiceManager.execute(request, listener);
            return true;
        }
    }

    private final LoaderCallbacks<Cursor> mLoaderCallbacks = new LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getApplicationContext(), RequestEntityContract.CONTENT_URI, null, null, null, null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if (cursor.moveToFirst()) {
                final ArrayList<ContentProviderOperation> operations = new ArrayList<>(cursor.getCount());
                final DbRequestEntity entity = new DbRequestEntity();

                final boolean isNetworkConnected = AContext.isNetworkConnected();
                boolean noInternet = false;

                do {
                    entity.fromCursor(cursor);

                    switch (entity.getStatus()) {
                        case RequestEntityStatus.PLACED_IN_DB:
                            if (isNetworkConnected) {
                                final BaseRequestEntity requestEntity = entity.getEntity();
                                final SpiceRequest<?> request;
                                if (requestEntity instanceof BaseMultipartRequestEntity) {
                                    request = new AMultipartNetworkRequest((BaseMultipartRequestEntity) entity.getEntity());

                                } else {
                                    request = new ANetworkRequest(entity.getEntity());
                                }
                                mSpiceManager.execute(request, null);

                                final ContentValues cv = new ContentValues(1);
                                cv.put(RequestEntityContract.STATUS, RequestEntityStatus.SENT_TO_EXECUTION);

                                operations.add(ContentProviderOperation
                                        .newUpdate(RequestEntityContract.CONTENT_URI)
                                        .withSelection(ContentHelper.eq(RequestEntityContract._ID, entity.getId()), null)
                                        .withValues(cv)
                                        .build());

                            } else {
                                RequestCenter.internetChanged(entity.getId(), false);
                                operations.add(ContentProviderOperation
                                        .newDelete(RequestEntityContract.CONTENT_URI)
                                        .withSelection(ContentHelper.eq(RequestEntityContract._ID, entity.getId()), null)
                                        .build());

                                if (!noInternet) {
                                    noInternet = true;
                                }
                            }
                            break;

                        case RequestEntityStatus.EXECUTED:
                            final ContentValues cv = new ContentValues(1);
                            cv.put(RequestEntityContract.STATUS, RequestCenter.receive(entity.getEntity()) ? RequestEntityStatus.RECEIVED : RequestEntityStatus.CANCELED);

                            operations.add(ContentProviderOperation
                                    .newUpdate(RequestEntityContract.CONTENT_URI)
                                    .withSelection(ContentHelper.eq(RequestEntityContract._ID, entity.getId()), null)
                                    .withValues(cv)
                                    .build());
                            break;

                        case RequestEntityStatus.RECEIVED:
                        case RequestEntityStatus.CANCELED:
                            operations.add(ContentProviderOperation
                                    .newDelete(RequestEntityContract.CONTENT_URI)
                                    .withSelection(ContentHelper.eq(RequestEntityContract._ID, entity.getId()), null)
                                    .build());
                            break;

                        case RequestEntityStatus.SENT_TO_EXECUTION:
                        default:
                            break;
                    }

                } while (cursor.moveToNext());

                if (operations.size() > 0) {
                    AContext.getApp().setApplyBatch(operations);
                }
                if (noInternet) {
                    Utils.showNoInternet(BaseActivity.this);
                }
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
        }
    };
}