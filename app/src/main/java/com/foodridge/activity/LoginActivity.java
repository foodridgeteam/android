package com.foodridge.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.foodridge.R;
import com.foodridge.api.request.GetProfileChefRequest;
import com.foodridge.api.request.GetProfileRequest;
import com.foodridge.fragment.login.LoginFragment;
import com.foodridge.model.Profile;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.Utils;

import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class LoginActivity extends BaseActivity {

    public static void start(@NonNull Activity activity) {
        activity.startActivity(new Intent(activity, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    private SwipeRefreshLayout mRefreshLayout;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);

        branchProcess(this);
        if (Profile.isAuthorized()) {
            RequestCenter.placeDelayed(new GetProfileRequest(), null, 500);
            RequestCenter.placeDelayed(new GetProfileChefRequest(), null, 500);

            MainActivity.start(this);
            finish();
            return;
        }
        super.setContentView(R.layout.activity_login);

        super.setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        //noinspection ConstantConditions
        Utils.setStyle(mRefreshLayout);
        mRefreshLayout.setEnabled(false);

        if (b == null) {
            startFragment(LoginFragment.newInstance(), false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getFragmentContainerId() {
        return R.id.fragment_container;
    }

    @Override
    public void setBlock(boolean block) {
        Utils.setRefreshing(mRefreshLayout, block);
    }

    @Override
    public void onCleanUp() throws Exception {
        Utils.cleanUp(mRefreshLayout);
    }

    private static void branchProcess(@NonNull Activity activity) {
        try {
            Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
                @Override
                public void onInitFinished(JSONObject referringParams, BranchError error) {
//                    LOG.toLog("referringParams: " + referringParams + "\n\terror: " + error);
                }
            }, activity.getIntent().getData(), activity);

        } catch (Throwable ignore) {
        }
    }
}