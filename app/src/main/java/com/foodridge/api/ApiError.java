package com.foodridge.api;

import android.support.annotation.IntDef;

import com.foodridge.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
@IntDef({
        ApiError.UNKNOWN, ApiError.DATA_NULL, ApiError.SERVER_UNAVAILABLE,
        ApiError.SOCIAL_AUTH_ERROR, ApiError.FORBIDDEN, ApiError.TERMS_CONDITIONS,
        ApiError.PARSING_EXCEPTION, ApiError.NO_INTERNET,
        ApiError.INVALID_EMAIL, ApiError.INVALID_PUSH_ID, ApiError.INVALID_USERNAME,
        ApiError.INVALID_LOGIN, ApiError.INVALID_DEVICE_TYPE, ApiError.INVALID_SOCIAL_TYPE,
        ApiError.INVALID_SOCIAL_USER_ID, ApiError.INVALID_SOCIAL_TOKEN, ApiError.INVALID_DEVICE_ID,
        ApiError.EMAIL_NOT_UNIQUE, ApiError.SOCIAL_LOGIN_FAILED, ApiError.INVALID_DELIVERY_INFO,
        ApiError.INVALID_FILE_TYPE, ApiError.UPLOAD_FAILED, ApiError.INVALID_PHONE,
        ApiError.INVALID_PUSH_TAG, ApiError.INCORRECT_TIME_FORMAT, ApiError.INVALID_ABOUT,
        ApiError.INVALID_BUSINESS_NAME, ApiError.INVALID_BUSINESS_SITE, ApiError.FILE_NOT_FOUND,
        ApiError.INVALID_REGISTRATION_ID, ApiError.INVALID_COUNTRY, ApiError.INVALID_CITY,
        ApiError.INVALID_STREET, ApiError.DB_QUERY_FAILED, ApiError.DELETE_FILE,
        ApiError.INCORRECT_DATA, ApiError.MISSED_MEAL_ID, ApiError.INCORRECT_CUISINE_ID,
        ApiError.SAVE_CHEF_PROFILE, ApiError.INVALID_JSON, ApiError.FOODRIDGE_TOKEN,
        ApiError.PAYMENT, ApiError.INVALID_ORDER_STATUS, ApiError.STRIPE_DEAUTHORIZED,
        ApiError.NOT_ENOUGH_PORTIONS, ApiError.TOTAL_SUM, ApiError.EMPTY_DELIVERY_ADDRESS,
        ApiError.REQUIRED_PARAMS, ApiError.INVALID_MEAL_NAME, ApiError.INVALID_DESCRIPTION,
        ApiError.INVALID_OTHER, ApiError.OPERATION_NOT_PERMITTED, ApiError.INVALID_INGREDIENT_ID,
        ApiError.REQUIRED_MEAL_ID, ApiError.ALREADY_FOLLOW, ApiError.PERMISSION,
        ApiError.CREATE_ORDER, ApiError.NOT_FOUND_ORDER, ApiError.NOT_PERMITTED_TO_ORDER,
        ApiError.INVALID_INPUT_PARAMS, ApiError.CREATE_MESSAGE, ApiError.MISSED_PARAMS,
        ApiError.MISSED_STATUS_OR_ID, ApiError.ALREADY_EXIST_COMMENT, ApiError.NO_COMMENT,
        ApiError.INVALID_DISH_ID, ApiError.REFUND, ApiError.SEND_EMAIL, ApiError.LIST_PARAM_WITHOUT_STRIPE,
        ApiError.REQUIRED_NAME_PHONE, ApiError.CLOSED_ORDER, ApiError.INVALID_DELIVERY_TYPE,
        ApiError.REQUIRED_ORDER_ID, ApiError.REQUIRED_PHONE, ApiError.REQUIRED_FEEDBACK, ApiError.FACEBOOK_LOGIN_FAILED
})
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiError {

    int UNKNOWN = R.string.error_Unknown;
    int DATA_NULL = R.string.error_Data_empty;
    int SERVER_UNAVAILABLE = R.string.error_Server_unavailable;
    int SOCIAL_AUTH_ERROR = R.string.error_Auth_failed;
    int FORBIDDEN = R.string.error_Session_expired;
    int TERMS_CONDITIONS = R.string.error_Terms_Conditions;
    int PARSING_EXCEPTION = R.string.error_Parsing;
    int NO_INTERNET = R.string.error_Internet_unavailable;

    int INVALID_EMAIL = 1;
    int INVALID_PUSH_ID = 2;
    int INVALID_USERNAME = 3;
    int INVALID_LOGIN = 4;
    int INVALID_DEVICE_TYPE = 5;
    int INVALID_SOCIAL_TYPE = 6;
    int INVALID_SOCIAL_USER_ID = 7;
    int INVALID_SOCIAL_TOKEN = 8;
    int EMAIL_NOT_UNIQUE = 9;
    int INVALID_DEVICE_ID = 10;
    int SOCIAL_LOGIN_FAILED = 11;
    int INVALID_DELIVERY_INFO = 12;
    int INVALID_FILE_TYPE = 13;
    int UPLOAD_FAILED = 14;
    int INVALID_PHONE = 15;
    int INVALID_PUSH_TAG = 16;
    int INCORRECT_TIME_FORMAT = 17;
    int INVALID_ABOUT = 18;
    int INVALID_BUSINESS_NAME = 19;
    int INVALID_BUSINESS_SITE = 20;
    int FILE_NOT_FOUND = 21;
    int INVALID_REGISTRATION_ID = 22;
    int INVALID_COUNTRY = 23;
    int INVALID_CITY = 24;
    int INVALID_STREET = 25;
    int DB_QUERY_FAILED = 26;
    int DELETE_FILE = 27;
    int INCORRECT_DATA = 28;
    int MISSED_MEAL_ID = 29;
    int INCORRECT_CUISINE_ID = 30;
    int SAVE_CHEF_PROFILE = 31;
    int INVALID_JSON = 32;
    int FOODRIDGE_TOKEN = 33;
    int PAYMENT = 34;
    int INVALID_ORDER_STATUS = 35;
    int STRIPE_DEAUTHORIZED = 36;
    int NOT_ENOUGH_PORTIONS = 37;
    int TOTAL_SUM = 38;
    int EMPTY_DELIVERY_ADDRESS = 39;
    int REQUIRED_PARAMS = 40;
    int INVALID_MEAL_NAME = 41;
    int INVALID_DESCRIPTION = 42;
    int INVALID_OTHER = 43;
    int OPERATION_NOT_PERMITTED = 44;
    int INVALID_INGREDIENT_ID = 45;
    int REQUIRED_MEAL_ID = 47;
    int ALREADY_FOLLOW = 48;
    int PERMISSION = 49;
    int CREATE_ORDER = 50;
    int NOT_FOUND_ORDER = 51;
    int NOT_PERMITTED_TO_ORDER = 52;
    int INVALID_INPUT_PARAMS = 53;
    int CREATE_MESSAGE = 54;
    int MISSED_PARAMS = 55;
    int MISSED_STATUS_OR_ID = 56;
    int ALREADY_EXIST_COMMENT = 57;
    int NO_COMMENT = 58;
    int INVALID_DISH_ID = 59;
    int REFUND = 60;
    int SEND_EMAIL = 61;
    int LIST_PARAM_WITHOUT_STRIPE = 62;
    int REQUIRED_NAME_PHONE = 63;
    int CLOSED_ORDER = 64;
    int INVALID_DELIVERY_TYPE = 65;
    int REQUIRED_ORDER_ID = 66;
    int REQUIRED_PHONE = 67;
    int REQUIRED_FEEDBACK = 68;
    int FACEBOOK_LOGIN_FAILED = 72;
}