package com.foodridge.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.foodridge.R;
import com.foodridge.api.model.ServerError;
import com.foodridge.application.AContext;
import com.google.android.gms.auth.UserRecoverableAuthException;

import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class ApiException extends RuntimeException {

    @SuppressWarnings("ThrowableInstanceNeverThrown")
    public static final ApiException SILENT = new ApiException(null, null, ApiError.UNKNOWN);

    @ApiError
    private final int mError;

    @NonNull
    public static ApiException create(@NonNull String message) {
        return new ApiException(null, message, ApiError.UNKNOWN);
    }

    @NonNull
    public static ApiException create(@ApiError int error) {
        return new ApiException(null, null, error);
    }

    @NonNull
    public static ApiException create(@NonNull ServerError error) {
        return new ApiException(null, error.getMessage(), error.getCode());
    }

    @NonNull
    public static ApiException create(@NonNull Throwable throwable) {
        return new ApiException(throwable, null, ApiError.UNKNOWN);
    }

    private ApiException(@Nullable Throwable throwable, @Nullable String message, @ApiError int error) {
        super(message, throwable);

        if (throwable instanceof SocketException) {
            mError = ApiError.SERVER_UNAVAILABLE;

        } else if (throwable instanceof UserRecoverableAuthException) {
            mError = ApiError.SOCIAL_AUTH_ERROR;

        } else if (throwable instanceof IllegalArgumentException) {
            final Throwable cause = throwable.getCause();
            if (cause instanceof JsonProcessingException) {
                mError = ApiError.PARSING_EXCEPTION;

            } else {
                mError = error;
            }

        } else if (throwable instanceof SocketTimeoutException) {
            mError = ApiError.SERVER_UNAVAILABLE;

        } else {
            mError = error;
        }
    }

    @ApiError
    public int getErrorCode() {
        return mError;
    }

    @NonNull
    @Override
    public String toString() {
        if (mError != ApiError.UNKNOWN && isKnown(mError)) {
            return getMessage(mError);
        }
        final String message = getMessage();
        if (!TextUtils.isEmpty(message)) {
            return message;
        }
        final Throwable throwable = getCause();
        if (throwable != null) {
            final String throwableMessage = getMessage();
            if (!TextUtils.isEmpty(throwableMessage)) {
                return throwableMessage;
            }
        }
        return getMessage(ApiError.UNKNOWN);
    }

    @NonNull
    public static String getMessage(@ApiError int error) {
        @StringRes final int stringRes;
        switch (error) {
            case ApiError.INVALID_EMAIL:
                stringRes = R.string.error_Invalid_email;
                break;

            case ApiError.INVALID_PUSH_ID:
                stringRes = R.string.error_Invalid_pushId;
                break;

            case ApiError.INVALID_USERNAME:
                stringRes = R.string.error_Invalid_username;
                break;

            case ApiError.INVALID_LOGIN:
                stringRes = R.string.error_Invalid_login;
                break;

            case ApiError.INVALID_DEVICE_TYPE:
                stringRes = R.string.error_Invalid_device_type;
                break;

            case ApiError.INVALID_SOCIAL_TYPE:
                stringRes = R.string.error_Invalid_social_type;
                break;

            case ApiError.INVALID_SOCIAL_USER_ID:
                stringRes = R.string.error_Invalid_social_userId;
                break;

            case ApiError.INVALID_SOCIAL_TOKEN:
                stringRes = R.string.error_Invalid_social_token;
                break;

            case ApiError.EMAIL_NOT_UNIQUE:
                stringRes = R.string.error_Email_not_unique;
                break;

            case ApiError.INVALID_DEVICE_ID:
                stringRes = R.string.error_Invalid_deviceId;
                break;

            case ApiError.SOCIAL_LOGIN_FAILED:
                stringRes = R.string.error_Social_login_failed;
                break;

            case ApiError.INVALID_DELIVERY_INFO:
                stringRes = R.string.error_Invalid_deliveryInfo;
                break;

            case ApiError.INVALID_FILE_TYPE:
                stringRes = R.string.error_Invalid_file_type;
                break;

            case ApiError.UPLOAD_FAILED:
                stringRes = R.string.error_Upload_failed;
                break;

            case ApiError.INVALID_PHONE:
                stringRes = R.string.error_Invalid_phone;
                break;

            case ApiError.INVALID_PUSH_TAG:
                stringRes = R.string.error_Invalid_push_tag;
                break;

            case ApiError.INCORRECT_TIME_FORMAT:
                stringRes = R.string.error_Incorrect_time_format;
                break;

            case ApiError.INVALID_ABOUT:
                stringRes = R.string.error_Invalid_about;
                break;

            case ApiError.INVALID_BUSINESS_NAME:
                stringRes = R.string.error_Invalid_business_name;
                break;

            case ApiError.INVALID_BUSINESS_SITE:
                stringRes = R.string.error_Invalid_business_site;
                break;

            case ApiError.FILE_NOT_FOUND:
                stringRes = R.string.error_File_not_found;
                break;

            case ApiError.INVALID_REGISTRATION_ID:
                stringRes = R.string.error_Invalid_registrationId;
                break;

            case ApiError.INVALID_COUNTRY:
                stringRes = R.string.error_Invalid_country;
                break;

            case ApiError.INVALID_CITY:
                stringRes = R.string.error_Invalid_city;
                break;

            case ApiError.INVALID_STREET:
                stringRes = R.string.error_Invalid_street;
                break;

            case ApiError.DB_QUERY_FAILED:
                stringRes = R.string.error_DB_query_failed;
                break;

            case ApiError.DELETE_FILE:
                stringRes = R.string.error_Delete_file;
                break;

            case ApiError.INCORRECT_DATA:
                stringRes = R.string.error_Incorrect_data;
                break;

            case ApiError.MISSED_MEAL_ID:
                stringRes = R.string.error_Missed_mealId;
                break;

            case ApiError.INCORRECT_CUISINE_ID:
                stringRes = R.string.error_Incorrect_cuisineId;
                break;

            case ApiError.SAVE_CHEF_PROFILE:
                stringRes = R.string.error_Save_chef_profile;
                break;

            case ApiError.INVALID_JSON:
                stringRes = R.string.error_Invalid_json;
                break;

            case ApiError.FOODRIDGE_TOKEN:
                stringRes = R.string.error_Foodridge_token;
                break;

            case ApiError.PAYMENT:
                stringRes = R.string.error_Payment;
                break;

            case ApiError.INVALID_ORDER_STATUS:
                stringRes = R.string.error_invalid_order_status;
                break;

            case ApiError.STRIPE_DEAUTHORIZED:
                stringRes = R.string.error_Stripe_deauthorized;
                break;

            case ApiError.NOT_ENOUGH_PORTIONS:
                stringRes = R.string.error_Not_enough_portions;
                break;

            case ApiError.TOTAL_SUM:
                stringRes = R.string.error_Total_sum;
                break;

            case ApiError.EMPTY_DELIVERY_ADDRESS:
                stringRes = R.string.error_Empty_delivery_address;
                break;

            case ApiError.REQUIRED_PARAMS:
                stringRes = R.string.error_Required_params;
                break;

            case ApiError.INVALID_MEAL_NAME:
                stringRes = R.string.error_Invalid_meal_name;
                break;

            case ApiError.INVALID_DESCRIPTION:
                stringRes = R.string.error_Invalid_description;
                break;

            case ApiError.INVALID_OTHER:
                stringRes = R.string.error_Invalid_other;
                break;

            case ApiError.OPERATION_NOT_PERMITTED:
                stringRes = R.string.error_Operation_not_permitted;
                break;

            case ApiError.INVALID_INGREDIENT_ID:
                stringRes = R.string.error_Invalid_ingredientId;
                break;

            case ApiError.REQUIRED_MEAL_ID:
                stringRes = R.string.error_Required_mealId;
                break;

            case ApiError.ALREADY_FOLLOW:
                stringRes = R.string.error_Already_follow;
                break;

            case ApiError.PERMISSION:
                stringRes = R.string.error_Permission;
                break;

            case ApiError.CREATE_ORDER:
                stringRes = R.string.error_Create_order;
                break;

            case ApiError.NOT_FOUND_ORDER:
                stringRes = R.string.error_Not_found_order;
                break;

            case ApiError.NOT_PERMITTED_TO_ORDER:
                stringRes = R.string.error_Not_permitted_to_order;
                break;

            case ApiError.INVALID_INPUT_PARAMS:
                stringRes = R.string.error_Invalid_input_params;
                break;

            case ApiError.CREATE_MESSAGE:
                stringRes = R.string.error_Create_message;
                break;

            case ApiError.MISSED_PARAMS:
                stringRes = R.string.error_Missed_params;
                break;

            case ApiError.MISSED_STATUS_OR_ID:
                stringRes = R.string.error_Missed_status_or_id;
                break;

            case ApiError.ALREADY_EXIST_COMMENT:
                stringRes = R.string.error_Already_exist_comment;
                break;

            case ApiError.NO_COMMENT:
                stringRes = R.string.error_No_comment;
                break;

            case ApiError.INVALID_DISH_ID:
                stringRes = R.string.error_Invalid_dishId;
                break;

            case ApiError.REFUND:
                stringRes = R.string.error_Refund;
                break;

            case ApiError.SEND_EMAIL:
                stringRes = R.string.error_Send_email;
                break;

            case ApiError.LIST_PARAM_WITHOUT_STRIPE:
                stringRes = R.string.error_List_param_wo_stripe;
                break;

            case ApiError.REQUIRED_NAME_PHONE:
                stringRes = R.string.error_Required_name_phone;
                break;

            case ApiError.CLOSED_ORDER:
                stringRes = R.string.error_Closed_order;
                break;

            case ApiError.INVALID_DELIVERY_TYPE:
                stringRes = R.string.error_Invalid_delivery_type;
                break;

            case ApiError.REQUIRED_ORDER_ID:
                stringRes = R.string.error_Required_orderId;
                break;

            case ApiError.REQUIRED_PHONE:
                stringRes = R.string.error_Required_phone;
                break;

            case ApiError.REQUIRED_FEEDBACK:
                stringRes = R.string.error_Required_feedback;
                break;

            case ApiError.FACEBOOK_LOGIN_FAILED:
                stringRes = R.string.error_Facebook_login_failed;
                break;

            case ApiError.SERVER_UNAVAILABLE:
            case ApiError.SOCIAL_AUTH_ERROR:
            case ApiError.PARSING_EXCEPTION:
            case ApiError.TERMS_CONDITIONS:
            case ApiError.NO_INTERNET:
            case ApiError.FORBIDDEN:
            case ApiError.DATA_NULL:
            case ApiError.UNKNOWN:
            default:
                stringRes = error;
                break;
        }
        return AContext.getString(stringRes);
    }

    private static boolean isKnown(int code) {
        for (int error : ERRORS) {
            if (code == error) {
                return true;
            }
        }
        return false;
    }

    private static final int[] ERRORS = {
            ApiError.UNKNOWN, ApiError.DATA_NULL, ApiError.SERVER_UNAVAILABLE,
            ApiError.SOCIAL_AUTH_ERROR, ApiError.FORBIDDEN, ApiError.TERMS_CONDITIONS,
            ApiError.PARSING_EXCEPTION, ApiError.NO_INTERNET,
            ApiError.INVALID_EMAIL, ApiError.INVALID_PUSH_ID, ApiError.INVALID_USERNAME,
            ApiError.INVALID_LOGIN, ApiError.INVALID_DEVICE_TYPE, ApiError.INVALID_SOCIAL_TYPE,
            ApiError.INVALID_SOCIAL_USER_ID, ApiError.INVALID_SOCIAL_TOKEN, ApiError.INVALID_DEVICE_ID,
            ApiError.EMAIL_NOT_UNIQUE, ApiError.SOCIAL_LOGIN_FAILED, ApiError.INVALID_DELIVERY_INFO,
            ApiError.INVALID_FILE_TYPE, ApiError.UPLOAD_FAILED, ApiError.INVALID_PHONE,
            ApiError.INVALID_PUSH_TAG, ApiError.INCORRECT_TIME_FORMAT, ApiError.INVALID_ABOUT,
            ApiError.INVALID_BUSINESS_NAME, ApiError.INVALID_BUSINESS_SITE, ApiError.FILE_NOT_FOUND,
            ApiError.INVALID_REGISTRATION_ID, ApiError.INVALID_COUNTRY, ApiError.INVALID_CITY,
            ApiError.INVALID_STREET, ApiError.DB_QUERY_FAILED, ApiError.DELETE_FILE,
            ApiError.INCORRECT_DATA, ApiError.MISSED_MEAL_ID, ApiError.INCORRECT_CUISINE_ID,
            ApiError.SAVE_CHEF_PROFILE, ApiError.INVALID_JSON, ApiError.FOODRIDGE_TOKEN,
            ApiError.PAYMENT, ApiError.INVALID_ORDER_STATUS, ApiError.STRIPE_DEAUTHORIZED,
            ApiError.NOT_ENOUGH_PORTIONS, ApiError.TOTAL_SUM, ApiError.EMPTY_DELIVERY_ADDRESS,
            ApiError.REQUIRED_PARAMS, ApiError.INVALID_MEAL_NAME, ApiError.INVALID_DESCRIPTION,
            ApiError.INVALID_OTHER, ApiError.OPERATION_NOT_PERMITTED, ApiError.INVALID_INGREDIENT_ID,
            ApiError.REQUIRED_MEAL_ID, ApiError.ALREADY_FOLLOW, ApiError.PERMISSION,
            ApiError.CREATE_ORDER, ApiError.NOT_FOUND_ORDER, ApiError.NOT_PERMITTED_TO_ORDER,
            ApiError.INVALID_INPUT_PARAMS, ApiError.CREATE_MESSAGE, ApiError.MISSED_PARAMS,
            ApiError.MISSED_STATUS_OR_ID, ApiError.ALREADY_EXIST_COMMENT, ApiError.NO_COMMENT,
            ApiError.INVALID_DISH_ID, ApiError.REFUND, ApiError.SEND_EMAIL, ApiError.LIST_PARAM_WITHOUT_STRIPE,
            ApiError.REQUIRED_NAME_PHONE, ApiError.CLOSED_ORDER, ApiError.INVALID_DELIVERY_TYPE,
            ApiError.REQUIRED_ORDER_ID, ApiError.REQUIRED_PHONE, ApiError.REQUIRED_FEEDBACK, ApiError.FACEBOOK_LOGIN_FAILED
    };
}