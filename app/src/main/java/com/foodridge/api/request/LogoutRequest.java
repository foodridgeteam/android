package com.foodridge.api.request;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.model.Profile;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class LogoutRequest extends BaseRequestEntity {

    public LogoutRequest() {
        super(R.id.code_logout, true, true, RequestType.GET, ApiConstant.LOGOUT, String.class);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        return null;
    }

    @Override
    public void doSomething() throws Exception {
        Profile.getInstance().makeLogout();
    }
}