package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.Meal;
import com.foodridge.provider.ContentHelper;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class MealRequest extends BaseRequestEntity {

    private final boolean mPlaceToDb;

    public MealRequest(@NonNull String mealId, boolean placeToDb) {
        super(R.id.code_meal, true, true, RequestType.GET, ApiConstant.SINGLE_MEAL + mealId, Meal.class);
        mPlaceToDb = placeToDb;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        return null;
    }

    @Override
    public void doSomething() throws Exception {
        if (mPlaceToDb) {
            final Meal meal = (Meal) getDataAndRemove();
            //noinspection ConstantConditions
            ContentHelper.insert(meal, true);
        }
    }
}