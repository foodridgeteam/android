package com.foodridge.api.request;

import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;

import com.foodridge.api.model.ServerError;
import com.foodridge.utility.SharedStrings;

import java.io.Serializable;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19.03.2016
 * <p/>
 * Container for requests using ContentProvider mechanism
 */
public abstract class BaseRequestEntity
        implements Serializable {

    public static final int NO_DB_ID = Integer.MIN_VALUE;

    /**
     * Must be set when added to DB
     */
    private int mDbId = NO_DB_ID;
    /**
     * Used when need to receive answer to listener
     */
    @IdRes
    private final int mReceiverCode;
    /**
     * If listener simply cancel all requests
     */
    private final boolean mCancelable;
    private final boolean mTokenRequired;
    @RequestType
    private final String mRequestType;
    @NonNull
    private final String mUrl;
    @NonNull
    private final Class<? extends Serializable> mDataClass;
    private Serializable mData;
    private ServerError mError;

    public BaseRequestEntity(@IdRes int receiverCode, boolean cancelable, boolean tokenRequired
            , @RequestType String requestType, @NonNull String url, @NonNull Class<? extends Serializable> dataClass) {
        mReceiverCode = receiverCode;
        mCancelable = cancelable;
        mTokenRequired = tokenRequired;
        mRequestType = requestType;
        mUrl = url;
        mDataClass = dataClass;
    }

    public int getDbId() {
        return mDbId;
    }

    public void setDbId(int dbId) {
        mDbId = dbId;
    }

    @IdRes
    public int getReceiverCode() {
        return mReceiverCode;
    }

    public boolean isCancelable() {
        return mCancelable;
    }

    public boolean isTokenRequired() {
        return mTokenRequired;
    }

    @RequestType
    public String getRequestType() {
        return mRequestType;
    }

    @NonNull
    public String getUrl() {
        return mUrl;
    }

    @NonNull
    public Class<? extends Serializable> getDataClass() {
        return mDataClass;
    }

    @Nullable
    public abstract ArrayMap<String, Object> getParameters() throws Exception;

    /**
     * Used when data must be wrapped in something
     */
    @Nullable
    public String getWrapperKey() {
        return null;
    }

    /**
     * Return data one time and remove it from entity
     */
    @Nullable
    public Serializable getDataAndRemove() {
        final Serializable data = mData;
        mData = null;

        return data;
    }

    @CallSuper
    public void setData(@Nullable Serializable data) {
        mData = data;
    }

    @Nullable
    public ServerError getError() {
        return mError;
    }

    @CallSuper
    public void setError(@NonNull ServerError error) {
        mError = error;
    }

    @WorkerThread
    public abstract void doSomething() throws Exception;

    public void finallyDoSomething() {
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append(SharedStrings.NEW_LINE_C).append(SharedStrings.TAB_C)
                .append(mRequestType).append(SharedStrings.SPACE_C).append(mUrl);
        sb.append(SharedStrings.NEW_LINE_C).append(SharedStrings.TAB_C)
                .append("tokenRequired: ").append(mTokenRequired);
        sb.append(SharedStrings.NEW_LINE_C).append(SharedStrings.TAB_C)
                .append("dbId: ").append(mDbId);
        sb.append(SharedStrings.NEW_LINE_C).append(SharedStrings.TAB_C)
                .append("receiverCode: ").append(mReceiverCode);
        sb.append(SharedStrings.NEW_LINE_C).append(SharedStrings.TAB_C)
                .append("cancelable: ").append(mCancelable);
        if (mData != null) {
            sb.append(SharedStrings.NEW_LINE_C).append(SharedStrings.TAB_C)
                    .append("answer: ").append(mData);
        }
        if (mError != null) {
            sb.append(SharedStrings.NEW_LINE_C).append(SharedStrings.TAB_C)
                    .append("error: ").append(mError);
        }
        return sb.toString();
    }
}