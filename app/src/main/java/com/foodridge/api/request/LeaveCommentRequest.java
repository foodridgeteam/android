package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Comment;
import com.foodridge.api.model.Order;
import com.foodridge.provider.ContentHelper;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 29.04.16
 */
public class LeaveCommentRequest extends BaseRequestEntity {

    private final String mOrderId;
    private final String mComment;
    private final int mRating;

    public LeaveCommentRequest(@NonNull Order order, int rating, @Nullable String comment) {
        super(R.id.code_leave_comment, true, true, RequestType.POST, ApiConstant.COMMENT, Comment.class);

        mOrderId = order.getId();
        mComment = comment;
        mRating = rating;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(3);

        map.put(ApiKeys.ORDER_ID, mOrderId);
        if (mComment != null) {
            map.put(ApiKeys.TEXT, mComment);
        }
        map.put(ApiKeys.RATE, mRating);

        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final Comment comment = (Comment) getDataAndRemove();
        ContentHelper.update(comment, mOrderId);
    }
}