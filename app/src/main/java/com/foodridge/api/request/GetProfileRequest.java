package com.foodridge.api.request;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.UserProfile;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class GetProfileRequest extends BaseRequestEntity {

    public GetProfileRequest() {
        super(R.id.code_profile, false, true, RequestType.GET, ApiConstant.USER_PROFILE, UserProfile.class);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final UserProfile userProfile = (UserProfile) getDataAndRemove();

        final Profile profile = Profile.getInstance();
        profile.set(userProfile);
        profile.commit();

        AContext.notifyChange(ProfileContract.CONTENT_URI);
    }
}