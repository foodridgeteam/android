package com.foodridge.api.request;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.ChefProfile;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class GetProfileChefRequest extends BaseRequestEntity {

    public GetProfileChefRequest() {
        super(R.id.code_profile_chef, false, true, RequestType.GET, ApiConstant.CHEF_PROFILE, ChefProfile.class);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final ChefProfile chefProfile = (ChefProfile) getDataAndRemove();

        final Profile profile = Profile.getInstance();
        profile.setChefProfile(chefProfile);
        profile.commit();

        AContext.notifyChange(ProfileContract.CONTENT_URI);
    }
}