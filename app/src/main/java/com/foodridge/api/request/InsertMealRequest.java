package com.foodridge.api.request;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Meal;
import com.foodridge.provider.ContentHelper;
import com.foodridge.utility.SharedStrings;
import com.foodridge.utility.Utils;

import java.util.List;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 20/04/16
 */
public class InsertMealRequest extends BaseRequestEntity {

    private final Meal mOriginal;
    private final Meal mMeal;
    private final String[] mImages;

    public InsertMealRequest(@Nullable Meal original, @NonNull Meal meal, @Nullable List<Uri> images) {
        super(R.id.code_insert_meal, true, true, original == null ? RequestType.POST : RequestType.PUT, ApiConstant.MEAL, Meal.class);

        mOriginal = original;
        mMeal = meal;

        if (images == null) {
            mImages = null;

        } else {
            mImages = new String[images.size()];
            int count = 0;

            for (Uri image : images) {
                mImages[count++] = image.toString();
            }
        }
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        if (mOriginal != null) {
            uploadPhotos(mOriginal.getId());
        }
        final ArrayMap<String, Object> map = new ArrayMap<>(13);
        final ObjectMapper mapper = new ObjectMapper();

        if (mOriginal == null) {
            map.put(ApiKeys.NAME, mMeal.getName());
            map.put(ApiKeys.DESCRIPTION, mMeal.getDescription());
            map.put(ApiKeys.CUISINES, Utils.toJSONArray(mMeal.getCuisines()));
            map.put(ApiKeys.INGREDIENTS, Utils.toJSONArray(mMeal.getIngredients()));
            map.put(ApiKeys.SPICY_RANK, mMeal.getSpicyRank());
            map.put(ApiKeys.FLAGS, Utils.toJSONObject(mapper, mMeal.getMealFlags()));
            map.put(ApiKeys.OTHER, mMeal.getOther());
            map.put(ApiKeys.DELIVERY_TYPE, mMeal.getDeliveryType());
            map.put(ApiKeys.PORTION_PRICE, mMeal.getPortionPrice());
            map.put(ApiKeys.DELIVERY_PRICE, mMeal.getDeliveryPrice());
            map.put(ApiKeys.PORTIONS_AVAILABLE, mMeal.getPortionsAvailable());
            map.put(ApiKeys.LISTED, mMeal.isListed());

        } else {
            map.put(ApiKeys.MEAL_ID, mOriginal.getId());
            if (!Utils.areEquals(mOriginal.getName(), mMeal.getName())) {
                map.put(ApiKeys.NAME, mMeal.getName());
            }
            if (!Utils.areEquals(mOriginal.getDescription(), mMeal.getDescription())) {
                map.put(ApiKeys.DESCRIPTION, mMeal.getDescription());
            }
            if (!Utils.areEquals(mOriginal.getCuisines(), mMeal.getCuisines())) {
                map.put(ApiKeys.CUISINES, Utils.toJSONArray(mMeal.getCuisines()));
            }
            if (!Utils.areEquals(mOriginal.getIngredients(), mMeal.getIngredients())) {
                map.put(ApiKeys.INGREDIENTS, Utils.toJSONArray(mMeal.getIngredients()));
            }
            if (mOriginal.getSpicyRank() != mMeal.getSpicyRank()) {
                map.put(ApiKeys.SPICY_RANK, mMeal.getSpicyRank());
            }
            if (!Utils.areEquals(mOriginal.getMealFlags(), mMeal.getMealFlags())) {
                map.put(ApiKeys.FLAGS, Utils.toJSONObject(mapper, mMeal.getMealFlags()));
            }
            if (!Utils.areEquals(mOriginal.getOther(), mMeal.getOther())) {
                map.put(ApiKeys.OTHER, mMeal.getOther());
            }
            if (mOriginal.getDeliveryType() != mMeal.getDeliveryType()) {
                map.put(ApiKeys.DELIVERY_TYPE, mMeal.getDeliveryType());
            }
            if (mOriginal.getPortionPrice() != mMeal.getPortionPrice()) {
                map.put(ApiKeys.PORTION_PRICE, mMeal.getPortionPrice());
            }
            if (mOriginal.getDeliveryPrice() != mMeal.getDeliveryPrice()) {
                map.put(ApiKeys.DELIVERY_PRICE, mMeal.getDeliveryPrice());
            }
            if (mOriginal.getPortionsAvailable() != mMeal.getPortionsAvailable()) {
                map.put(ApiKeys.PORTIONS_AVAILABLE, mMeal.getPortionsAvailable());
            }
            if (mOriginal.isListed() != mMeal.isListed()) {
                map.put(ApiKeys.LISTED, mMeal.isListed());
            }
        }
        return map;
    }

    @Override
    public void doSomething() throws Exception {
        final Meal meal = (Meal) getDataAndRemove();
        //noinspection ConstantConditions
        ContentHelper.insert(meal, mOriginal != null);

        if (mOriginal == null) {
            uploadPhotos(meal.getId());
        }
    }

    private void uploadPhotos(final @NonNull String id) {
        if (mImages != null) {
//            final Thread thread = new Thread(new Runnable() {
//                @Override
//                public void run() {
            int i = 1;
            if (mOriginal == null) {
                for (String image : mImages) {
                    new AMultipartNetworkRequest(new UploadMealImageRequest(id, i++, Uri.parse(image))).execute();
                }

            } else {
                final List<String> photos = Utils.toList(mOriginal.getPhotos());
                for (String image : mImages) {
                    if (image.startsWith(SharedStrings.FILE_PREFIX)) {
                        new AMultipartNetworkRequest(new UploadMealImageRequest(id, i++, Uri.parse(image))).execute();

                    } else {
                        photos.remove(image);
                    }
                }
                for (String uri : photos) {
                    new ANetworkRequest(new DeleteMealImageRequest(uri)).execute();
                }
            }
            new ANetworkRequest(new MealRequest(id, true)).execute();
        }
//            });
//            thread.setPriority(Thread.MAX_PRIORITY);
//            thread.start();
//        }
    }
}