package com.foodridge.api.request;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.OrderMessage;
import com.foodridge.application.AContext;
import com.foodridge.model.DbMessage;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.MessageContract;
import com.foodridge.utility.OrderMessageCache;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 02/05/16
 */
public class OrderMessagesRequest extends BaseRequestEntity {

    private final String mOrderId;
    private String mLastId;

    public OrderMessagesRequest(@NonNull String orderId) {
        super(R.id.code_order_messages, true, true, RequestType.GET, ApiConstant.MESSAGES, OrderMessage[].class);
        mOrderId = orderId;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(2);

        map.put(ApiKeys.ORDER_ID, mOrderId);

        Cursor c = null;
        try {
            c = AContext.getContentResolver().query(MessageContract.CONTENT_URI, null, ContentHelper.eq(MessageContract.ORDER_ID, mOrderId), null, null);
            if (c != null && c.moveToFirst()) {
                final String id = c.getString(c.getColumnIndex(MessageContract._ID));
                if (id != null) {
                    mLastId = id;
                    map.put(ApiKeys.LAST_ID, mLastId);
                }
            }

        } finally {
            Utils.closeSilently(c);
        }

        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final OrderMessage[] messages = (OrderMessage[]) getDataAndRemove();
        final OrderMessageCache cache = OrderMessageCache.getInstance();
        if (messages == null) {
            if (mLastId == null) {
                cache.put(mOrderId, (OrderMessage) null);
                AContext.getContentResolver().delete(MessageContract.CONTENT_URI, ContentHelper.eq(MessageContract.ORDER_ID, mOrderId), null);
            }
            return;
        }
        final ContentValues[] cvs = new ContentValues[messages.length];
        final DbMessage message = new DbMessage();
        int count = 0;

        for (OrderMessage m : messages) {
            cvs[count++] = message.toContentValues(mOrderId, m);
        }
        cache.put(mOrderId, messages);
        AContext.getContentResolver().bulkInsert(MessageContract.CONTENT_URI, cvs);
    }
}