package com.foodridge.api.request;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.IdName;
import com.foodridge.application.ASettings;

import java.util.Arrays;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class IngredientsRequest extends BaseRequestEntity {

    public IngredientsRequest() {
        super(R.id.code_ingredients, true, true, RequestType.GET, ApiConstant.INGREDIENTS, IdName[].class);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final IdName[] cuisines = (IdName[]) getDataAndRemove();
        ASettings.getInstance().setIngredients(Arrays.asList(cuisines));
    }
}