package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 26/04/16
 */
public class RestorePasswordRequest extends BaseRequestEntity {

    private final String mEmail;

    public RestorePasswordRequest(@NonNull String email) {
        super(R.id.code_restore, true, false, RequestType.POST, ApiConstant.FORGOT, String.class);
        mEmail = email;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        return Utils.singleArrayMap(ApiKeys.EMAIL, (Object) mEmail);
    }

    @Override
    public void doSomething() throws Exception {
        setData(mEmail);
    }
}