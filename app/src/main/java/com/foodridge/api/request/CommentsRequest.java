package com.foodridge.api.request;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.Comment;
import com.foodridge.application.AContext;
import com.foodridge.model.DbComment;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.CommentContract;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class CommentsRequest extends BaseRequestEntity {

    private static final Object LOCK = new Object();
    private final String mMealId;

    public CommentsRequest(@NonNull String mealId) {
        super(R.id.code_orders, true, true, RequestType.GET, ApiConstant.COMMENTS + mealId, Comment[].class);
        mMealId = mealId;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        return null;
    }

    @Override
    public void doSomething() throws Exception {
        synchronized (LOCK) {
            final Comment[] comments = (Comment[]) getDataAndRemove();
            final int offset = 0;
            if (comments == null) {
                AContext.getContentResolver().delete(CommentContract.CONTENT_URI, ContentHelper.eq(CommentContract.MEAL_ID, mMealId), null);
                setData(offset);
                return;
            }
            final ContentValues[] cvs = new ContentValues[comments.length];
            final DbComment dbComment = new DbComment();
            int count = 0;

            for (Comment comment : comments) {
                cvs[count] = dbComment.toContentValues(offset + count, comment);
                count++;
            }
            AContext.getContentResolver().bulkInsert(CommentContract.CONTENT_URI, cvs);
            setData(offset + count);
        }
    }
}