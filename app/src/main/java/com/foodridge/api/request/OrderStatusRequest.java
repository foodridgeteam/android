package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Order;
import com.foodridge.provider.ContentHelper;
import com.foodridge.type.OrderStatus;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 02/05/16
 */
public class OrderStatusRequest extends BaseRequestEntity {

    private final String mOrderId;
    @OrderStatus
    private final int mStatus;

    public OrderStatusRequest(@NonNull String orderId, @OrderStatus int status) {
        super(R.id.code_order, true, true, RequestType.PUT, ApiConstant.ORDER, Order.class);

        mOrderId = orderId;
        mStatus = status;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(2);

        map.put(ApiKeys.ORDER_ID, mOrderId);
        map.put(ApiKeys.ORDER_STATUS, mStatus);

        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final Order order = (Order) getDataAndRemove();

        if (order.isActive()) {
            new ANetworkRequest(new OrderMessagesRequest(order.getId())).execute();
        }
        ContentHelper.update(order);
    }
}