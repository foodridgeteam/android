package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.BusinessInfo;
import com.foodridge.api.model.ChefProfile;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.api.model.WeekWorkingTime;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;
import com.foodridge.utility.Utils;

import java.util.ArrayList;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class UpdateProfileChefRequest extends BaseRequestEntity {

    private final DeliveryInfo mDeliveryInfo;
    private final String mBio;
    private final ArrayList<Integer> mCuisines;
    private final BusinessInfo mBusinessInfo;
    private final WeekWorkingTime mWorkingTime;

    public UpdateProfileChefRequest(@NonNull DeliveryInfo deliveryInfo, @NonNull String bio
            , @NonNull ArrayList<Integer> cuisines, @NonNull BusinessInfo businessInfo
            , @NonNull WeekWorkingTime workingTime) {
        super(R.id.code_profile_chef, true, true, RequestType.PUT, ApiConstant.CHEF_PROFILE, ChefProfile.class);

        mDeliveryInfo = deliveryInfo;
        mBio = bio;
        mCuisines = cuisines;
        mBusinessInfo = businessInfo;
        mWorkingTime = workingTime;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(5);
        final ChefProfile profile = Profile.getInstance().getChefProfile();
        final ObjectMapper mapper = new ObjectMapper();

        if (!mDeliveryInfo.equals(profile.getDeliveryInfo())) {
            map.put(ApiKeys.DELIVERY_INFO, Utils.toJSONObject(mapper, mDeliveryInfo));
        }
        if (!Utils.areEquals(mBio, profile.getAbout())) {
            map.put(ApiKeys.ABOUT, mBio);
        }
        if (!profile.isCuisinesSame(mCuisines)) {
            map.put(ApiKeys.CUISINES, Utils.toJSONArray(mCuisines));
        }
        if (!mBusinessInfo.hasEmptyField() && !mBusinessInfo.equals(profile.getBusinessInfo())) {
            map.put(ApiKeys.BUSINESS_INFO, Utils.toJSONObject(mapper, mBusinessInfo));
        }
        if (!mWorkingTime.equals(profile.getWorkingTime())) {
            map.put(ApiKeys.WORKING_TIME, Utils.toJSONObject(mapper, mWorkingTime));
        }

        return map;
    }

    @Nullable
    @Override
    public String getWrapperKey() {
        return ApiKeys.CHEF_PROFILE;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final ChefProfile chefProfile = (ChefProfile) getDataAndRemove();

        final Profile profile = Profile.getInstance();
        profile.setChefProfile(chefProfile);
        profile.commit();

        AContext.notifyChange(ProfileContract.CONTENT_URI);
    }
}