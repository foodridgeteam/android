package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.model.Profile;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class AMultipartNetworkRequest extends ANetworkRequest {

    private static final MediaType MEDIA_TYPE = MediaType.parse("image/jpeg");

    public AMultipartNetworkRequest(@NonNull BaseMultipartRequestEntity entity) {
        super(entity);
    }

    @NonNull
    @Override
    protected Request prepareRequest() throws Exception {
        final MultipartBody.Builder body = new MultipartBody.Builder().setType(MultipartBody.FORM);

        final ArrayMap<String, Object> parameters = mEntity.getParameters();

        //noinspection ConstantConditions
        final String filePath = (String) parameters.remove(ApiKeys.FILE);
        final File file = new File(filePath);

        for (Entry<String, Object> entry : parameters.entrySet()) {
            body.addFormDataPart(entry.getKey(), entry.getValue().toString());
        }
        body.addFormDataPart(ApiKeys.FILE, file.getName(), RequestBody.create(MEDIA_TYPE, file));

        final Request.Builder request = new Request.Builder()
                .url(ApiConstant.SERVER_URL + mEntity.getUrl())
                .post(new ProgressRequestBody(body.build(), (BaseMultipartRequestEntity) mEntity));
        if (mEntity.isTokenRequired()) {
            final Profile profile = Profile.getInstance();
            request.addHeader(ApiKeys.HEADER_UID, profile.getId());
            request.addHeader(ApiKeys.HEADER_TOKEN, profile.getToken());
        }
        try {
            return request.build();

        } finally {
            LOG.toLog(">>> Q\tcode: " + getCode()
                    + "\n\tentity: " + mEntity
                    + "\n\tparameters: " + parameters
                    + "\n>>>");
        }
    }

    private static final class ProgressRequestBody extends RequestBody {

        private final RequestBody mDelegate;
        private final BaseMultipartRequestEntity mEntity;

        private ProgressSink mSink;

        public ProgressRequestBody(@NonNull RequestBody requestBody, @NonNull BaseMultipartRequestEntity entity) {
            mDelegate = requestBody;
            mEntity = entity;
        }

        @Override
        public MediaType contentType() {
            return mDelegate.contentType();
        }

        @Override
        public long contentLength() {
            try {
                return mDelegate.contentLength();

            } catch (IOException e) {
                LOG.toLog(e);
                return -1;
            }
        }

        @Override
        public void writeTo(BufferedSink sink) throws IOException {
            mSink = new ProgressSink(sink);

            final BufferedSink buffer = Okio.buffer(mSink);
            mDelegate.writeTo(buffer);
            buffer.flush();
        }

        private final class ProgressSink extends ForwardingSink {

            private float bytesWritten = 0;

            public ProgressSink(@NonNull Sink delegate) {
                super(delegate);
            }

            @Override
            public void write(Buffer source, long byteCount) throws IOException {
                super.write(source, byteCount);

                bytesWritten += byteCount;
                mEntity.onProgress(100F * (bytesWritten / contentLength()));
            }
        }
    }
}