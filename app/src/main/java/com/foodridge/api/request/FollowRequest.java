package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Chef;
import com.foodridge.provider.ContentHelper;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class FollowRequest extends BaseRequestEntity {

    private final String mCheId;
    private final boolean mFollow;

    public FollowRequest(@NonNull Chef chef) {
        super(R.id.code_follow, true, true, RequestType.POST, ApiConstant.FOLLOW_CHEF, Chef.class);

        mCheId = chef.getId();
        mFollow = !chef.isFollow();
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        final ArrayMap<String, Object> map = new ArrayMap<>(2);

        map.put(ApiKeys.CHEF_ID, mCheId);
        map.put(ApiKeys.FOLLOW, mFollow);

        return map;
    }

    @Override
    public void doSomething() throws Exception {
        final Chef chef = (Chef) getDataAndRemove();
        //noinspection ConstantConditions
        ContentHelper.insert(chef);
    }
}