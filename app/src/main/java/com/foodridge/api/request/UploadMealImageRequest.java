package com.foodridge.api.request;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.application.AContext;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
public class UploadMealImageRequest extends BaseMultipartRequestEntity {

    private final String mMealId;

    public UploadMealImageRequest(@NonNull String mealId, int i, @NonNull Uri fileUri) {
        super(R.id.code_meal_image, ApiConstant.MEAL_MEDIA, String.class
                , fileUri, AContext.getStringArgs(R.string.format_Meal_image_d, i));
        mMealId = mealId;
    }

    @NonNull
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = super.getParameters();
        map.put(ApiKeys.ID, mMealId);

        return map;
    }

    @Override
    public void doSomething() throws Exception {
    }
}