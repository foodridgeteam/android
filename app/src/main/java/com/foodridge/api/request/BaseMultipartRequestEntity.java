package com.foodridge.api.request;

import android.net.Uri;
import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.foodridge.api.ApiKeys;
import com.foodridge.utility.NotifyCenter;

import java.io.Serializable;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
public abstract class BaseMultipartRequestEntity extends BaseRequestEntity {

    protected final String mFilePath;
    private final String mFileName;

    private int mLastPercent = Integer.MIN_VALUE;

    public BaseMultipartRequestEntity(@IdRes int receiverCode, @NonNull String url, @NonNull Class<? extends Serializable> dataClass
            , @NonNull Uri fileUri, @NonNull String fileName) {
        super(receiverCode, false, true, RequestType.POST, url, dataClass);

        mFilePath = fileUri.toString().replaceFirst("file://", "");
        mFileName = fileName;
    }

    @CallSuper
    @NonNull
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>();

        map.put(ApiKeys.FILE, mFilePath);

        return map;
    }

    public void onProgress(float uploadedPercent) {
        final int percent = (int) uploadedPercent;
        if (percent != mLastPercent) {
            mLastPercent = percent;
            NotifyCenter.getInstance().showProgress(mFileName, uploadedPercent);
        }
    }

    @Override
    public void finallyDoSomething() {
        NotifyCenter.getInstance().removeProgress(mFileName);
    }
}