package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.BuildConfig;
import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 11/05/16
 */
public class FeedbackRequest extends BaseRequestEntity {

    private final String mFeedback;

    public FeedbackRequest(@NonNull String feedback) {
        super(R.id.code_common, true, true, RequestType.POST, ApiConstant.FEEDBACK, String.class);
        mFeedback = feedback;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final Object object = mFeedback
                + "\n\n\n" + Utils.getDeviceInfo()
                + "\nApp version: " + BuildConfig.VERSION_CODE + ", " + BuildConfig.VERSION_NAME;

        return Utils.singleArrayMap(ApiKeys.FEEDBACK, object);
    }

    @Override
    public void doSomething() throws Exception {
    }
}