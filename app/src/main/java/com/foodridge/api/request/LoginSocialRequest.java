package com.foodridge.api.request;

import android.accounts.Account;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Auth;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.type.SocialType;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import static com.google.android.gms.auth.api.Auth.GOOGLE_SIGN_IN_API;
import static com.google.android.gms.auth.api.Auth.GoogleSignInApi;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class LoginSocialRequest extends BaseRequestEntity {

    @SocialType
    private final int mType;
    @NonNull
    private final String mToken;
    @NonNull
    private final String mUserId;

    public LoginSocialRequest(@SocialType int type, @NonNull String token, @NonNull String userId) {
        super(R.id.code_login, true, false, RequestType.POST, ApiConstant.LOGIN, Auth.class);

        mType = type;
        mToken = token;
        mUserId = userId;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(5);

        map.put(ApiKeys.DEVICE_TYPE, ApiConstant.DEVICE_TYPE);
        map.put(ApiKeys.DEVICE_ID, Secure.getString(AContext.getContentResolver(), Secure.ANDROID_ID));
        map.put(ApiKeys.SOCIAL_TYPE, mType);
        if (mType == SocialType.GOOGLE_PLUS) {
            map.put(ApiKeys.SOCIAL_TOKEN, GoogleAuthUtil.getToken(AContext.getApp(), new Account(mUserId, "com.google"), "oauth2:profile email"));

        } else {
            map.put(ApiKeys.SOCIAL_TOKEN, mToken);
            map.put(ApiKeys.SOCIAL_USER_ID, mUserId);
        }
        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final Auth auth = (Auth) getDataAndRemove();
        Profile.login(auth);
    }

    @Override
    public void finallyDoSomething() {
        if (mType == SocialType.GOOGLE_PLUS) {
            final GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(AContext.getString(R.string.google_client_id))
                    .requestEmail()
                    .build();

            final GoogleApiClient client = new GoogleApiClient.Builder(AContext.getApp())
                    .addApi(GOOGLE_SIGN_IN_API, options)
                    .build();

            final ConnectionResult result = client.blockingConnect();
            if (result.isSuccess()) {
                GoogleSignInApi.signOut(client).await();
            }
        }
    }
}