package com.foodridge.api.request;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.ChefProfile;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData;

    /**
     * Created by kkxmshu on 06/07/16.
     */
public class DisconnectStripeRequest extends BaseRequestEntity {

    public DisconnectStripeRequest() {
        super(R.id.code_disconnect_stripe, true, true, RequestType.DELETE, ApiConstant.DISCONNECT_STRIPE, ChefProfile.class);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final ChefProfile chefProfile = (ChefProfile) getDataAndRemove();

        final Profile profile = Profile.getInstance();
        profile.setChefProfile(chefProfile);
        profile.commit();

        AContext.notifyChange(FoodridgeMetaData.ProfileContract.CONTENT_URI);
    }
}