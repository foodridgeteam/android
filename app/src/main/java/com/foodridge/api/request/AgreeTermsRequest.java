package com.foodridge.api.request;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.model.Profile;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 28/04/16
 */
public class AgreeTermsRequest extends BaseRequestEntity {

    public AgreeTermsRequest() {
        super(R.id.code_terms_policy, true, true, RequestType.PUT, ApiConstant.TERMS, String.class);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        return Utils.singleArrayMap(ApiKeys.AGREE_TERMS, (Object) true);
    }

    @Override
    public void doSomething() throws Exception {
        Profile.getInstance().agreeTerms();
    }
}