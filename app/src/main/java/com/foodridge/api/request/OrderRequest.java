package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.Order;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class OrderRequest extends BaseRequestEntity {

    public OrderRequest(@NonNull String orderId) {
        super(R.id.code_order, true, true, RequestType.GET, ApiConstant.SINGLE_ORDER + orderId, Order.class);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        return null;
    }

    @Override
    public void doSomething() throws Exception {
    }
}