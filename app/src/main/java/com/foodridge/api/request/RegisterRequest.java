package com.foodridge.api.request;

import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Auth;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 05/04/16
 */
public class RegisterRequest extends BaseRequestEntity {

    private final String mEmail;
    private final String mPassword;
    private final String mName;

    public RegisterRequest(@NonNull String email, @NonNull String password, @NonNull String name) {
        super(R.id.code_registration, true, false, RequestType.POST, ApiConstant.REGISTER, Auth.class);

        mEmail = email;
        mPassword = password;
        mName = name;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        final ArrayMap<String, Object> map = new ArrayMap<>(5);

        map.put(ApiKeys.DEVICE_TYPE, ApiConstant.DEVICE_TYPE);
        map.put(ApiKeys.DEVICE_ID, Secure.getString(AContext.getContentResolver(), Secure.ANDROID_ID));
        map.put(ApiKeys.EMAIL, mEmail);
        map.put(ApiKeys.PASSWORD, Utils.createMD5(mPassword));
        map.put(ApiKeys.USERNAME, mName);

        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        Profile.login((Auth) getDataAndRemove());
        new ANetworkRequest(new AgreeTermsRequest()).loadDataFromNetwork();
    }
}