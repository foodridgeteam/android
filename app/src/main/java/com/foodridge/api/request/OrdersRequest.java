package com.foodridge.api.request;

import android.content.ContentValues;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Order;
import com.foodridge.application.AContext;
import com.foodridge.model.DbOrder;
import com.foodridge.provider.FoodridgeMetaData.OrderContract;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class OrdersRequest extends BaseRequestEntity {

    private static final Object LOCK = new Object();

    private final Boolean mActive;

    public OrdersRequest(@Nullable Boolean active) {
        super(R.id.code_orders, true, true, RequestType.GET, ApiConstant.ORDERS, Order[].class);
        mActive = active;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        final ArrayMap<String, Object> map = new ArrayMap<>(1);
        if (mActive != null) {
            map.put(ApiKeys.ACTIVE, mActive);
        }
        return map;
    }

    @Override
    public void doSomething() throws Exception {
        synchronized (LOCK) {
            final Order[] orders = (Order[]) getDataAndRemove();
            final int offset = 0;
            if (orders == null) {
                if (mActive == null) {
                    AContext.getContentResolver().delete(OrderContract.CONTENT_URI, null, null);
                }
                setData(offset);
                return;
            }
            final ContentValues[] cvs = new ContentValues[orders.length];
            final DbOrder dbOrder = new DbOrder();
            int count = 0;

            for (Order order : orders) {
                cvs[count] = dbOrder.toContentValues(offset + count, order);
                count++;
            }
            AContext.getContentResolver().bulkInsert(OrderContract.CONTENT_URI, cvs);

            setData(offset + count);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (Order order : orders) {
                        if (order.isActive()) {
                            new ANetworkRequest(new OrderMessagesRequest(order.getId())).execute();
                        }
                    }
                    AContext.notifyChange(OrderContract.CONTENT_URI);
                }
            }).start();
        }
    }
}