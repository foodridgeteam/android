package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.application.AConstant;
import com.foodridge.application.ASettings;
import com.foodridge.model.Profile;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 08/04/16
 */
public class PushIdRequest extends BaseRequestEntity {

    private final String mToken;

    private PushIdRequest(@NonNull String token) {
        super(R.id.code_common, true, true, RequestType.POST, ApiConstant.SET_PUSHID, String.class);
        mToken = token;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(2);

        map.put(ApiKeys.PUSH_ID, mToken);
        map.put(ApiKeys.PUSH_FLAG, AConstant.PUSH_FLAG);

        return map;
    }

    @Override
    public void doSomething() throws Exception {
        ASettings.getInstance().setGcmTokenAccepted(true);
    }

    @WorkerThread
    public static void send() {
        if (Profile.isAuthorized()) {
            final ASettings settings = ASettings.getInstance();
            final String token = settings.getGcmToken();
            if (token != null && !settings.isGcmTokenAccepted()) {
                new ANetworkRequest(new PushIdRequest(token)).execute();
            }
        }
    }
}