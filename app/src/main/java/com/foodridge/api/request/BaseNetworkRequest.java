package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.foodridge.request.BaseRequest;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
abstract class BaseNetworkRequest<RESULT> extends BaseRequest<RESULT> {

    private final long mCode;
    private static long sCode;

    protected BaseNetworkRequest(@NonNull Class<RESULT> result) {
        super(result);
        mCode = sCode++;
    }

    @Nullable
    @Override
    public final RESULT loadDataFromNetwork() throws Exception {
        preLoad();
        boolean errorOccurred = true;
        try {
            final Request request = prepareRequest();
            final Response response = new OkHttpClient.Builder()
//                    .cache(AContext.getCache())
                    .build().newCall(request).execute();
            final RESULT rawResult = handleResponse(response);
            final RESULT checkedResult = checkResult(rawResult);

            errorOccurred = false;
            return checkedResult;

        } catch (Throwable t) {
            return handleException(t);

        } finally {
            postLoad(errorOccurred);
        }
    }

    @Override
    public final boolean isNetworkRequired() {
        return true;
    }

    public final long getCode() {
        return mCode;
    }

    protected void preLoad() {
    }

    protected void postLoad(boolean errorOccurred) {
    }

    @NonNull
    protected abstract Request prepareRequest() throws Exception;

    @Nullable
    protected abstract RESULT handleResponse(@NonNull Response response) throws Exception;

    @Nullable
    protected abstract RESULT checkResult(@Nullable RESULT result) throws Exception;

    @Nullable
    protected abstract RESULT handleException(@NonNull Throwable t);
}