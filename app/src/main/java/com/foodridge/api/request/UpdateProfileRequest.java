package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.api.model.UserProfile;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class UpdateProfileRequest extends BaseRequestEntity {

    private final String mUsername;
    private final String mPhone;
    private final DeliveryInfo mInfo;

    public UpdateProfileRequest(@NonNull String username, @NonNull String phone, @NonNull DeliveryInfo info) {
        super(R.id.code_profile, true, true, RequestType.PUT, ApiConstant.USER_PROFILE, UserProfile.class);

        mUsername = username;
        mPhone = phone;
        mInfo = info;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(1);

        final Profile profile = Profile.getInstance();

        if (!Utils.areEquals(mUsername, profile.getName())) {
            map.put(ApiKeys.NAME, mUsername);
        }
        if (!Utils.areEquals(mPhone, profile.getPhone())) {
            map.put(ApiKeys.PHONE, mPhone);
        }
        if (!mInfo.equals(profile.getDeliveryInfo())) {
            map.put(ApiKeys.DELIVERY_INFO, Utils.toJSONObject(new ObjectMapper(), mInfo));
        }
        return map;
    }

    @Nullable
    @Override
    public String getWrapperKey() {
        return ApiKeys.USER_PROFILE;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final UserProfile userProfile =  (UserProfile) getDataAndRemove();

        final Profile profile = Profile.getInstance();
        profile.set(userProfile);
        profile.commit();

        AContext.notifyChange(ProfileContract.CONTENT_URI);
    }
}