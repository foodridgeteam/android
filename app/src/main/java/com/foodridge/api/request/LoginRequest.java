package com.foodridge.api.request;

import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Auth;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class LoginRequest extends BaseRequestEntity {

    private final String mEmail;
    private final String mPassword;

    public LoginRequest(@NonNull String email, @NonNull String password) {
        super(R.id.code_login, true, false, RequestType.POST, ApiConstant.LOGIN, Auth.class);

        mEmail = email;
        mPassword = password;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        final ArrayMap<String, Object> map = new ArrayMap<>(4);

        map.put(ApiKeys.DEVICE_TYPE, ApiConstant.DEVICE_TYPE);
        map.put(ApiKeys.DEVICE_ID, Secure.getString(AContext.getContentResolver(), Secure.ANDROID_ID));
        map.put(ApiKeys.EMAIL, mEmail);
        map.put(ApiKeys.PASSWORD, Utils.createMD5(mPassword));

        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final Auth auth = (Auth) getDataAndRemove();
        Profile.login(auth);
    }
}