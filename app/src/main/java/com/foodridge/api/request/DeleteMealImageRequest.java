package com.foodridge.api.request;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
public class DeleteMealImageRequest extends BaseRequestEntity {

    public DeleteMealImageRequest(@NonNull String imageUri) {
        super(R.id.code_meal_image, false, true, RequestType.DELETE
                , ApiConstant.MEAL_MEDIA + "/" + Uri.parse(imageUri).getLastPathSegment(), String.class);
    }

    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        return null;
    }

    @Override
    public void doSomething() throws Exception {
    }
}