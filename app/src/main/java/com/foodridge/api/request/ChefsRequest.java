package com.foodridge.api.request;

import android.content.ContentProviderOperation;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Chef;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.model.DbChef;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.ChefContract;
import com.foodridge.utility.Utils;

import java.util.ArrayList;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class ChefsRequest extends BaseRequestEntity {

    private static final Object LOCK = new Object();

    private final int mOffset;
    private final int mLimit;

    private final String mSearch;

    public ChefsRequest() {
        this(0, AConstant.LIMIT, DbChef.SEARCH_SELF);
    }

    public ChefsRequest(int offset, int limit, @Nullable String search) {
        super(R.id.code_chefs, true, true, RequestType.GET, ApiConstant.CHEFS, Chef[].class);

        mLimit = Math.max(limit, AConstant.LIMIT);
        mOffset = offset / mLimit;
        mSearch = Utils.toLowerCase(search);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        final ArrayMap<String, Object> map = new ArrayMap<>(3);

        if (mSearch != null) {
            map.put(ApiKeys.SEARCH, mSearch);
        }
        map.put(ApiKeys.OFFSET, mOffset);
        map.put(ApiKeys.LIMIT, mLimit);

        return map;
    }

    @Override
    public void doSomething() throws Exception {
        synchronized (LOCK) {
            final Chef[] chefs = (Chef[]) getDataAndRemove();
            final int offset = mOffset * mLimit;
            if (chefs == null) {
                setData(offset);
                if (mLimit == AConstant.LIMIT) {
                    AContext.getContentResolver().delete(ChefContract.CONTENT_URI
                            , ContentHelper.and(ContentHelper.eq(ChefContract.SEARCH, mSearch), ContentHelper.moreEq(ChefContract.ORDER, true, offset)), null);
                }

            } else {
                final ArrayList<ContentProviderOperation> operations = new ArrayList<>(chefs.length * 3);
                final DbChef dbChef = new DbChef();
                int count = 0;

                for (Chef chef : chefs) {
                    operations.addAll(ContentHelper.insertOperations(dbChef, offset + count, chef, mSearch));
                    count++;
                }
                setData(offset + count);
                AContext.getContentResolver().applyBatch(FoodridgeMetaData.AUTHORITY, operations);
            }
        }
    }
}