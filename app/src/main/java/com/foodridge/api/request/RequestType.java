package com.foodridge.api.request;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19.03.2016
 */

@StringDef({RequestType.GET, RequestType.PUT, RequestType.POST, RequestType.HEAD, RequestType.DELETE})
@Retention(RetentionPolicy.RUNTIME)
@interface RequestType {

    String GET = "GET";
    String PUT = "PUT";
    String POST = "POST";
    String HEAD = "HEAD";
    String DELETE = "DELETE";
}