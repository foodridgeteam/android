package com.foodridge.api.request;

import android.content.ContentProviderOperation;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Meal;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.model.DbMeal;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.MealContract;
import com.foodridge.utility.Utils;

import java.util.ArrayList;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class MealsRequest extends BaseRequestEntity {

    private static final Object LOCK = new Object();

    private final int mOffset;
    private final int mLimit;

    private final String mChefId;
    private final String mSearch;

    public MealsRequest(int offset, @Nullable String chefId, @Nullable String search) {
        this(offset, AConstant.LIMIT, chefId, search);
    }

    public MealsRequest(int offset, int limit, @Nullable String chefId, @Nullable String search) {
        super(R.id.code_meals, true, true, RequestType.GET, ApiConstant.MEAL, Meal[].class);

        mLimit = Math.max(limit, AConstant.LIMIT);
        mOffset = offset / mLimit;
        mChefId = chefId;
        mSearch = Utils.toLowerCase(search);
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() {
        final ArrayMap<String, Object> map = new ArrayMap<>(4);

        if (mChefId != null) {
            map.put(ApiKeys.CHEF_ID, mChefId);

        } else if (mSearch != null) {
            map.put(ApiKeys.SEARCH, mSearch);
        }
        map.put(ApiKeys.OFFSET, mOffset);
        map.put(ApiKeys.LIMIT, mLimit);

        return map;
    }

    @Override
    public void doSomething() throws Exception {
        synchronized (LOCK) {
            final Meal[] meals = (Meal[]) getDataAndRemove();
            final int offset = mOffset * mLimit;

            if (meals == null) {
                setData(offset);
                if (mLimit == AConstant.LIMIT) {
                    AContext.getContentResolver().delete(MealContract.CONTENT_URI
                            , ContentHelper.and(ContentHelper.eq(MealContract.SEARCH, mSearch), ContentHelper.moreEq(MealContract.ORDER, true, offset)), null);
                }

            } else {
                final ArrayList<ContentProviderOperation> operations = new ArrayList<>(meals.length * 2);
                final DbMeal dbMeal = new DbMeal();
                int count = 0;

                for (Meal meal : meals) {
                    operations.addAll(ContentHelper.insertOperations(dbMeal, offset + count, meal, mSearch, false));
                    count++;
                }
                setData(offset + count);
                AContext.getContentResolver().applyBatch(FoodridgeMetaData.AUTHORITY, operations);
            }
        }
    }
}