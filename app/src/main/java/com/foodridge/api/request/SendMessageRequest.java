package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Order;
import com.foodridge.api.model.OrderMessage;
import com.foodridge.application.AContext;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.OrderContract;
import com.foodridge.utility.OrderMessageCache;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 29.04.16
 */
public class SendMessageRequest extends BaseRequestEntity {

    private final String mOrderId;
    private final String mMessage;

    public SendMessageRequest(@NonNull Order order, @NonNull String message) {
        super(R.id.code_send_message, true, true, RequestType.POST, ApiConstant.MESSAGES, OrderMessage.class);

        mOrderId = order.getId();
        mMessage = message;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(2);

        map.put(ApiKeys.ORDER_ID, mOrderId);
        map.put(ApiKeys.TEXT, mMessage);

        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final OrderMessage message = (OrderMessage) getDataAndRemove();

        ContentHelper.insert(mOrderId, message);
        OrderMessageCache.getInstance().put(mOrderId, message);
        AContext.notifyChange(OrderContract.CONTENT_URI);
    }
}