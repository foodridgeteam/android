package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Order;
import com.foodridge.provider.ContentHelper;
import com.foodridge.type.DeliveryType;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 27/04/16
 */
public class InsertOrderRequest extends BaseRequestEntity {

    private final String mMealId;
    private final String mStripeToken;
    private final int mQuantity;
    @DeliveryType
    private final int mDeliveryType;
    private final float mTotal;

    public InsertOrderRequest(@NonNull String mealId, @NonNull String stripeToken, int quantity, @DeliveryType int deliveryType, float total) {
        super(R.id.code_insert_order, true, true, RequestType.POST, ApiConstant.ORDER, Order.class);

        mMealId = mealId;
        mStripeToken = stripeToken;
        mQuantity = quantity;
        mDeliveryType = deliveryType;
        mTotal = total;
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(5);

        map.put(ApiKeys.DISH_ID, mMealId);
        map.put(ApiKeys.STRIPE_TOKEN, mStripeToken);
        map.put(ApiKeys.QUANTITY, mQuantity);
        map.put(ApiKeys.DELIVERY_TYPE, mDeliveryType);
        map.put(ApiKeys.TOTAL_SUM, mTotal);

        return map;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final Order order = (Order) getDataAndRemove();
        ContentHelper.insert(order);
    }
}