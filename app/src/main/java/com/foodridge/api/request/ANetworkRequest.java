package com.foodridge.api.request;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiError;
import com.foodridge.api.ApiException;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.ServerAnswer;
import com.foodridge.api.model.ServerError;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.RequestEntityContract;
import com.foodridge.type.RequestEntityStatus;
import com.foodridge.utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Map.Entry;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class ANetworkRequest extends BaseNetworkRequest<BaseRequestEntity> {

    protected static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    protected final BaseRequestEntity mEntity;
    private boolean mSilentError;

    public ANetworkRequest(@NonNull BaseRequestEntity entity) {
        super(BaseRequestEntity.class);
        mEntity = entity;
    }

    @NonNull
    @Override
    protected Request prepareRequest() throws Exception {
        final Builder builder = new Builder();
        if (mEntity.isTokenRequired()) {
            final Profile profile = Profile.getInstance();
            final String token = profile.getToken();

            if (token == null) {
                throw ApiException.SILENT;
            }
            builder.addHeader(ApiKeys.HEADER_UID, profile.getId());
            builder.addHeader(ApiKeys.HEADER_TOKEN, token);
        }
        final ArrayMap<String, Object> parameters = mEntity.getParameters();
        String url = ApiConstant.SERVER_URL + mEntity.getUrl();
        switch (mEntity.getRequestType()) {
            case RequestType.GET:
                if (parameters != null) {
                    final HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
                    for (Entry<String, Object> entry : parameters.entrySet()) {
                        urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
                    }
                    url = urlBuilder.toString();
                }
                builder.get();
                break;

            case RequestType.PUT:
                builder.put(toRequestBody(parameters));
                break;

            case RequestType.POST:
                builder.post(toRequestBody(parameters));
                break;

            case RequestType.DELETE:
                builder.delete(toRequestBody(parameters));
                break;

            default:
                throw new IllegalStateException("Unknown HttpType");
        }
        builder.url(url);

        try {
            return builder.build();

        } finally {
            LOG.toLog(">>> Q\tcode: " + getCode()
                    + "\n\tentity: " + mEntity
                    + "\n\tparameters: " + (parameters == null ? null : toString(parameters))
                    + "\n>>>");
        }
    }

    @NonNull
    private String toString(@NonNull ArrayMap<String, Object> parameters) throws JSONException {
        final JSONObject data = new JSONObject();
        Object value;
        for (Entry<String, Object> entry : parameters.entrySet()) {
            value = entry.getValue();
            data.put(entry.getKey(), value == null ? JSONObject.NULL : value);
        }
        final String wrapperKey = mEntity.getWrapperKey();
        if (wrapperKey != null) {
            final JSONObject wrapper = new JSONObject();
            wrapper.put(wrapperKey, data);

            return wrapper.toString();
        }
        return data.toString();
    }

    @Nullable
    protected RequestBody toRequestBody(@Nullable ArrayMap<String, Object> parameters) throws JSONException {
        return parameters == null ? null : RequestBody.create(MEDIA_TYPE_JSON, toString(parameters));
    }

    @Nullable
    @Override
    protected BaseRequestEntity handleResponse(@NonNull Response response) throws Exception {
        final int responseCode = response.code();
        InputStream is = null;
        Object raw = null;

        try {
            switch (responseCode) {
                case HttpURLConnection.HTTP_FORBIDDEN:
                    Profile.getInstance().makeLogout();
                    throw ApiException.create(ApiError.FORBIDDEN);

                case 424://Failed Dependency
                    Profile.getInstance().showTermsConditions();
                    throw ApiException.create(ApiError.TERMS_CONDITIONS);

                default:
                    break;
            }
            final ResponseBody body = response.body();
            is = body.byteStream();

            response.code();
            final String string = Utils.toString(is, false);
            raw = string;

            if (string.length() == 0) {
                throw ApiException.create(ApiError.PARSING_EXCEPTION);
            }

            final JsonParser parser = new JsonFactory().createParser(string);
            final ObjectMapper mapper = new ObjectMapper();

            final ServerAnswer answer = mapper.readValue(parser, ServerAnswer.class);
            if (answer.getError() != null) {
                throw ApiException.create(answer.getError());
            }
            final JsonNode data = answer.getData();
            if (data == null) {
                throw ApiException.create(ApiError.DATA_NULL);
            }
            mEntity.setData(mapper.convertValue(data, mEntity.getDataClass()));
            mEntity.doSomething();

        } finally {
            Utils.closeSilently(is);
            LOG.toLog("<<< A\tcode: " + getCode()
                    + "\n\tHttpCode: " + responseCode
                    + "\n\traw: " + raw
                    + "\n\tentity: " + mEntity
                    + "\n<<<");
        }
        return mEntity;
    }

    @Nullable
    @Override
    protected BaseRequestEntity checkResult(@Nullable BaseRequestEntity requestEntity) throws Exception {
        return mEntity;
    }

    @Nullable
    @Override
    protected BaseRequestEntity handleException(@NonNull Throwable t) {
        LOG.toLog(t);

        if (t == ApiException.SILENT) {
            mSilentError = true;
            return null;
        }
        //noinspection ThrowableResultOfMethodCallIgnored
        final ApiException exception = t instanceof ApiException ? (ApiException) t : ApiException.create(t);
        mEntity.setError(new ServerError(exception.getErrorCode(), exception.toString()));

        throw exception;
    }

    @Override
    protected void postLoad(boolean errorOccurred) {
        mEntity.finallyDoSomething();

        final int dbId = mEntity.getDbId();
        if (dbId != BaseRequestEntity.NO_DB_ID) {
            final ContentValues cv = new ContentValues(2);
            cv.put(RequestEntityContract.STATUS, RequestEntityStatus.EXECUTED);
            cv.put(RequestEntityContract.ENTITY, mSilentError ? null : Utils.serialize(mEntity));

            AContext.getContentResolver().update(RequestEntityContract.CONTENT_URI, cv
                    , ContentHelper.eq(RequestEntityContract._ID, dbId), null);
        }
    }
}