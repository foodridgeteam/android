package com.foodridge.api.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Meal;
import com.foodridge.provider.ContentHelper;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 20/04/16
 */
public class ListedMealRequest extends BaseRequestEntity {

    private final String mMealId;
    private final boolean mListed;

    public ListedMealRequest(@NonNull Meal meal) {
        super(R.id.code_listed_meal, true, true, RequestType.PUT, ApiConstant.MEAL, Meal.class);

        mMealId = meal.getId();
        mListed = !meal.isListed();
    }

    @Nullable
    @Override
    public ArrayMap<String, Object> getParameters() throws Exception {
        final ArrayMap<String, Object> map = new ArrayMap<>(2);

        map.put(ApiKeys.MEAL_ID, mMealId);
        map.put(ApiKeys.LISTED, mListed);

        return map;
    }

    @Override
    public void doSomething() throws Exception {
        final Meal meal = (Meal) getDataAndRemove();
        //noinspection ConstantConditions
        ContentHelper.insert(meal, true);
    }
}