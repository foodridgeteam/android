package com.foodridge.api.request;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.api.model.UserProfile;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
public class UploadAvatarRequest extends BaseMultipartRequestEntity {

    public UploadAvatarRequest(@NonNull Uri fileUri) {
        super(R.id.code_avatar, ApiConstant.UPLOAD_AVATAR, UserProfile.class
                , fileUri, AContext.getString(R.string.User_avatar));
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void doSomething() throws Exception {
        final UserProfile userProfile = (UserProfile) getDataAndRemove();

        final Profile profile = Profile.getInstance();
        profile.set(userProfile);
        profile.commit();

        AContext.notifyChange(ProfileContract.CONTENT_URI);
    }
}