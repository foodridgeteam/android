package com.foodridge.api;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public @interface ApiKeys {

    String HEADER_UID = "uid";
    String HEADER_TOKEN = "token";

    String DATA = "data";
    String ERROR = "error";

    String CODE = "code";
    String MESSAGE = "message";

    String DEVICE_TYPE = "deviceType";
    String DEVICE_ID = "deviceId";
    String EMAIL = "email";
    String USERNAME = "username";
    String PASSWORD = "password";
    String TOKEN = "token";
    String USER_PROFILE = "userProfile";
    String ID = "id";
    String AVATAR = "avatar";
    String NAME = "name";
    String ABOUT = "about";
    String PAYMENT_INFO = "paymentInfo";
    String DELIVERY_INFO = "deliveryInfo";
    String BUSINESS_INFO = "businessInfo";
    String WORKING_TIME = "workingTime";
    String CUISINES = "cuisines";
    String PHONE = "phone";
    String COUNTRY = "country";
    String CITY = "city";
    String STREET = "street";
    String MORE = "more";
    String REGISTRATION_ID = "registrationId";
    String SITE = "site";
    String START = "start";
    String END = "end";
    String MON = "mon";
    String TUE = "tue";
    String WED = "wed";
    String THU = "thu";
    String FRI = "fri";
    String SAT = "sat";
    String SUN = "sun";
    String RATING = "rating";
    String VOTES_COUNT = "votesCount";
    String FOLLOWERS = "followers";
    String LAST_VISIT = "lastVisit";
    String FOLLOW = "follow";
    String CHEF = "chef";
    String DESCRIPTION = "description";
    String INGREDIENTS = "ingredients";
    String PHOTO = "photo";
    String PORTIONS_AVAILABLE = "portionsAvailable";
    String PORTIONS_SOLD = "portionsSold";
    String SPICY_RANK = "spicyRank";
    String FLAGS = "flags";
    String OTHER = "other";
    String PORTION_PRICE = "portionPrice";
    String DELIVERY_PRICE = "deliveryPrice";
    String LISTED = "listed";
    String DELIVERY_TYPE = "deliveryType";
    String BREAKFAST = "breakfast";
    String LUNCH = "lunch";
    String DINNER = "dinner";
    String BAKERY = "bakery";
    String VEGETARIAN = "vegetarian";
    String EGG = "egg";
    String GLUTEN_FREE = "glutenFree";
    String TRACES_NUTS = "tracesNuts";
    String GARLIC = "garlic";
    String GINGER = "ginger";
    String CLIENT = "client";
    String MEAL = "meal";
    String QUANTITY = "quantity";
    String STATUS = "status";
    String CREATED = "created";
    String CLIENT_DELIVERY_INFO = "clientDeliveryInfo";
    String ORDER_ID = "orderId";
    String MEAL_ID = "mealId";
    String USER = "user";
    String SYSTEM = "system";
    String SOCIAL_TYPE = "socialType";
    String SOCIAL_USER_ID = "socialUserId";
    String SOCIAL_TOKEN = "socialToken";
    String TITLE = "title";
    String BODY = "body";
    String PUSH_ID = "pushId";
    String PUSH_FLAG = "push_flag";
    String FILE = "file";
    String CHEF_PROFILE = "chefProfile";
    String CHEF_ID = "chefId";
    String SEARCH = "search";
    String OFFSET = "offset";
    String LIMIT = "limit";
    String STRIPE_ID = "stripeId";
    String DISH_ID = "dishId";
    String TOTAL_SUM = "totalSum";
    String STRIPE_TOKEN = "stripeToken";
    String AGREE_TERMS = "agreeTerms";
    String TEXT = "text";
    String RATE = "rate";
    String ACTIVE = "active";
    String COMMENT = "comment";
    String TYPE = "type";
    String ENTITY = "entity";
    String ORDER_STATUS = "orderStatus";
    String LAST_ID = "lastId";
    String USER_ID = "userId";
    String FEEDBACK = "feedBack";
}