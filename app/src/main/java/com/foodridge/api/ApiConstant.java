package com.foodridge.api;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public @interface ApiConstant {

    int DEVICE_TYPE = 1;

//    String SERVER_URL = "http://foodridge.dev.rollncode.com/api/";
    String SERVER_URL = "https://www.foodridge.com/api/";

    String HTML_TERMS_CONDITION_USER = SERVER_URL + "html/tandc.html";

    String REGISTER = "register";
    String LOGIN = "login";
    String LOGOUT = "logout";
    String SET_PUSHID = "set_pushid";
    String USER_PROFILE = "user_profile";
    String UPLOAD_AVATAR = "upload_avatar";
    String CHEF_PROFILE = "chef_profile";
    String CUISINES = "cuisines";
    String INGREDIENTS = "ingredients";
    String MEAL = "meal";
    String FOLLOW_CHEF = "follow_chef";
    String CHEFS = "chefs";
    String MEAL_MEDIA = "meal_media";
    String SINGLE_MEAL = "single_meal/";
    String FORGOT = "forgot";
    String ORDER = "order";
    String SINGLE_ORDER = "order/";
    String TERMS = "terms";
    String COMMENT = "comment";
    String ORDERS = "orders";
    String COMMENTS = "comments/";
    String MESSAGES = "messages";
    String FEEDBACK = "feedback";
    String DISCONNECT_STRIPE = "stripe_disconnect";
}