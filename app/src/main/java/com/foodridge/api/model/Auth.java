package com.foodridge.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 05/04/16
 */
public class Auth
        implements ApiModel {

    @JsonProperty(ApiKeys.TOKEN)
    private String mToken;

    @JsonProperty(ApiKeys.USER_PROFILE)
    private UserProfile mProfile;

    public Auth() {
    }

    public String getToken() {
        return mToken;
    }

    public UserProfile getProfile() {
        return mProfile;
    }
}