package com.foodridge.api.model;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class ChefProfile
        implements ApiModel {

    @JsonProperty(value = ApiKeys.ABOUT)
    private String mAbout;

    @JsonProperty(ApiKeys.PAYMENT_INFO)
    private PaymentInfo mPaymentInfo;

    @JsonProperty(ApiKeys.DELIVERY_INFO)
    private DeliveryInfo mDeliveryInfo;

    @JsonProperty(ApiKeys.BUSINESS_INFO)
    private BusinessInfo mBusinessInfo;

    @JsonProperty(value = ApiKeys.WORKING_TIME)
    private WeekWorkingTime mWorkingTime;

    @JsonProperty(ApiKeys.CUISINES)
    private List<Integer> mCuisines;

    public ChefProfile() {
    }

    @NonNull
    public String getAbout() {
        return mAbout;
    }

    public PaymentInfo getPaymentInfo() {
        return mPaymentInfo;
    }

    public DeliveryInfo getDeliveryInfo() {
        return mDeliveryInfo;
    }

    public BusinessInfo getBusinessInfo() {
        return mBusinessInfo;
    }

    public WeekWorkingTime getWorkingTime() {
        return mWorkingTime;
    }

    public List<Integer> getCuisines() {
        return mCuisines;
    }

    @JsonIgnore
    public boolean isCuisinesSame(@NonNull ArrayList<Integer> cuisines) {
        if (mCuisines == null) {
            mCuisines = new ArrayList<>(0);
        }
        if (mCuisines.size() == 0 && cuisines.size() == 0) {
            return true;
        }
        if (mCuisines.size() == 0 || cuisines.size() == 0 || (mCuisines.size() != cuisines.size())) {
            return false;
        }
        final ArrayList<Integer> integers = new ArrayList<>(cuisines);
        for (int cuisine : mCuisines) {
            for (Iterator<Integer> iterator = integers.iterator(); iterator.hasNext(); ) {
                if (iterator.next() == cuisine) {
                    iterator.remove();
                    break;
                }
            }
        }
        return integers.size() == 0;
    }
}