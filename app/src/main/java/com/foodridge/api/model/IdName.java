package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 14/04/16
 */
public class IdName
        implements ApiModel, Parcelable, Comparable<IdName> {

    public static final IdName CUISINES[] = {new IdName(0, "Italian"), new IdName(1, "Ukrainian"), new IdName(2, "French")};
    public static final IdName INGREDIENTS[] = {new IdName(0, "Bread"), new IdName(1, "Paper"), new IdName(2, "Cheese")};

    @JsonProperty(ApiKeys.ID)
    private int mId;

    @JsonProperty(ApiKeys.NAME)
    private String mName;

    public IdName() {
    }

    private IdName(int id, String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeString(this.mName);
    }

    protected IdName(Parcel in) {
        this.mId = in.readInt();
        this.mName = in.readString();
    }

    public static final Creator<IdName> CREATOR = new Creator<IdName>() {
        @Override
        public IdName createFromParcel(Parcel source) {
            return new IdName(source);
        }

        @Override
        public IdName[] newArray(int size) {
            return new IdName[size];
        }
    };

    @Override
    public String toString() {
        return mName;
    }

    @Override
    public int hashCode() {
        return mId;
    }

    @Override
    public int compareTo(@NonNull IdName idName) {
        return mName.compareTo(idName.mName);
    }
}