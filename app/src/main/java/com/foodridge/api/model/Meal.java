package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.type.DeliveryType;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class Meal
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.ID)
    private String mId;

    @JsonProperty(ApiKeys.CHEF)
    private Chef mChef;

    @JsonProperty(ApiKeys.NAME)
    private String mName;

    @JsonProperty(ApiKeys.DESCRIPTION)
    private String mDescription;

    @JsonProperty(ApiKeys.CUISINES)
    private int[] mCuisines;

    @JsonProperty(ApiKeys.INGREDIENTS)
    private int[] mIngredients;

    @JsonProperty(ApiKeys.PHOTO)
    private String[] mPhotos;

    @JsonProperty(ApiKeys.PORTIONS_AVAILABLE)
    private int mPortionsAvailable;

    @JsonProperty(ApiKeys.PORTIONS_SOLD)
    private int mPortionsSold;

    @JsonProperty(ApiKeys.RATING)
    @FloatRange(from = 0F, to = 5F)
    private float mRating;

    @JsonProperty(ApiKeys.VOTES_COUNT)
    private int mVotesCount;

    @JsonProperty(ApiKeys.SPICY_RANK)
    @FloatRange(from = 0F, to = 5F)
    private float mSpicyRank;

    @JsonProperty(ApiKeys.FLAGS)
    private MealFlags mMealFlags;

    @JsonProperty(ApiKeys.OTHER)
    private String mOther;

    @JsonProperty(ApiKeys.PORTION_PRICE)
    private float mPortionPrice;

    @JsonProperty(ApiKeys.DELIVERY_PRICE)
    private float mDeliveryPrice;

    @JsonProperty(ApiKeys.LISTED)
    private boolean mListed;

    @DeliveryType
    @JsonProperty(ApiKeys.DELIVERY_TYPE)
    private int mDeliveryType;

    public Meal() {
    }

    public Meal(String name, String description, int[] cuisines, int[] ingredients
            , int portionsAvailable, float spicyRank, MealFlags mealFlags, String other
            , float portionPrice, float deliveryPrice, int deliveryType, boolean listed) {
        mName = name;
        mDescription = Utils.checkEmpty(description);
        mCuisines = cuisines;
        mIngredients = ingredients;
        mPortionsAvailable = portionsAvailable;
        mSpicyRank = spicyRank;
        mMealFlags = mealFlags;
        mOther = Utils.checkEmpty(other);
        mPortionPrice = portionPrice;
        mDeliveryPrice = deliveryPrice;
        mDeliveryType = deliveryType;
        mListed = listed;
    }

    public String getId() {
        return mId;
    }

    public Chef getChef() {
        return mChef;
    }

    public void setChef(Chef chef) {
        mChef = chef;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public int[] getCuisines() {
        return mCuisines;
    }

    public int[] getIngredients() {
        return mIngredients;
    }

    public String[] getPhotos() {
        return mPhotos;
    }

    @Nullable
    public String getFirstPhoto() {
        return mPhotos == null || mPhotos.length < 1 ? null : mPhotos[0];
    }

    public int getPortionsAvailable() {
        return mPortionsAvailable;
    }

    public int getPortionsSold() {
        return mPortionsSold;
    }

    @FloatRange(from = 0F, to = 5F)
    public float getRating() {
        return mRating;
    }

    public int getVotesCount() {
        return mVotesCount;
    }

    @FloatRange(from = 0F, to = 5F)
    public float getSpicyRank() {
        return mSpicyRank;
    }

    public MealFlags getMealFlags() {
        return mMealFlags;
    }

    public String getOther() {
        return mOther;
    }

    public float getPortionPrice() {
        return mPortionPrice;
    }

    public float getDeliveryPrice() {
        return mDeliveryPrice;
    }

    public boolean isListed() {
        return mListed;
    }

    @DeliveryType
    public int getDeliveryType() {
        return mDeliveryType;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Meal && ((Meal) o).mId.equals(mId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeParcelable(this.mChef, flags);
        dest.writeString(this.mName);
        dest.writeString(this.mDescription);
        dest.writeIntArray(this.mCuisines);
        dest.writeIntArray(this.mIngredients);
        dest.writeStringArray(this.mPhotos);
        dest.writeInt(this.mPortionsAvailable);
        dest.writeInt(this.mPortionsSold);
        dest.writeFloat(this.mRating);
        dest.writeInt(this.mVotesCount);
        dest.writeFloat(this.mSpicyRank);
        dest.writeParcelable(this.mMealFlags, flags);
        dest.writeString(this.mOther);
        dest.writeFloat(this.mPortionPrice);
        dest.writeFloat(this.mDeliveryPrice);
        dest.writeByte(mListed ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mDeliveryType);
    }

    protected Meal(Parcel in) {
        this.mId = in.readString();
        this.mChef = in.readParcelable(Chef.class.getClassLoader());
        this.mName = in.readString();
        this.mDescription = in.readString();
        this.mCuisines = in.createIntArray();
        this.mIngredients = in.createIntArray();
        this.mPhotos = in.createStringArray();
        this.mPortionsAvailable = in.readInt();
        this.mPortionsSold = in.readInt();
        this.mRating = in.readFloat();
        this.mVotesCount = in.readInt();
        this.mSpicyRank = in.readFloat();
        this.mMealFlags = in.readParcelable(MealFlags.class.getClassLoader());
        this.mOther = in.readString();
        this.mPortionPrice = in.readFloat();
        this.mDeliveryPrice = in.readFloat();
        this.mListed = in.readByte() != 0;
        //noinspection WrongConstant
        this.mDeliveryType = in.readInt();
    }

    public static final Creator<Meal> CREATOR = new Creator<Meal>() {
        @Override
        public Meal createFromParcel(Parcel source) {
            return new Meal(source);
        }

        @Override
        public Meal[] newArray(int size) {
            return new Meal[size];
        }
    };
}