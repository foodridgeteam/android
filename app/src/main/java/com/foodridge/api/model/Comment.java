package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntRange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class Comment
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.ORDER_ID)
    private String mOrderId;

    @JsonProperty(ApiKeys.MEAL_ID)
    private String mMealId;

    @JsonProperty(ApiKeys.CREATED)
    private String mCreated;

    @JsonProperty(ApiKeys.MESSAGE)
    private String mMessage;

    @JsonProperty(ApiKeys.RATING)
    @IntRange(from = 0, to = 5)
    private int mRating;

    @JsonProperty(ApiKeys.USER)
    private User mUser;

    public Comment() {
    }

    public String getOrderId() {
        return mOrderId;
    }

    public String getMealId() {
        return mMealId;
    }

    public String getCreated() {
        return mCreated;
    }

    public String getMessage() {
        return mMessage;
    }

    @IntRange(from = 0, to = 5)
    public int getRating() {
        return mRating;
    }

    public User getUser() {
        return mUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mOrderId);
        dest.writeString(this.mMealId);
        dest.writeString(this.mCreated);
        dest.writeString(this.mMessage);
        dest.writeInt(this.mRating);
        dest.writeParcelable(this.mUser, flags);
    }

    protected Comment(Parcel in) {
        this.mOrderId = in.readString();
        this.mMealId = in.readString();
        this.mCreated = in.readString();
        this.mMessage = in.readString();
        this.mRating = in.readInt();
        this.mUser = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}