package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.interfaces.UserInterface;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class Chef
        implements ApiModel, Parcelable, UserInterface {

    @JsonProperty(ApiKeys.ID)
    private String mId;

    @JsonProperty(ApiKeys.AVATAR)
    private String mAvatar;

    @JsonProperty(ApiKeys.NAME)
    private String mName;

    @JsonProperty(ApiKeys.ABOUT)
    private String mAbout;

    @JsonProperty(ApiKeys.RATING)
    private float mRating;

    @JsonProperty(ApiKeys.VOTES_COUNT)
    private int mVotesCount;

    @JsonProperty(ApiKeys.FOLLOWERS)
    private int mFollowers;

    @JsonProperty(ApiKeys.LAST_VISIT)
    private String mLastVisit;

    @JsonProperty(ApiKeys.DELIVERY_INFO)
    private DeliveryInfo mDeliveryInfo;

    @JsonProperty(ApiKeys.BUSINESS_INFO)
    private BusinessInfo mBusinessInfo;

    @JsonProperty(ApiKeys.WORKING_TIME)
    private WeekWorkingTime mWorkingTime;

    @JsonProperty(ApiKeys.CUISINES)
    private int[] mCuisines;

    @JsonProperty(ApiKeys.FOLLOW)
    private boolean mFollow;

    public Chef() {
    }

    public String getId() {
        return mId;
    }

    @Override
    public String getAvatar() {
        return mAvatar;
    }

    @Override
    public String getName() {
        return mName;
    }

    public String getAbout() {
        return mAbout;
    }

    public float getRating() {
        return mRating;
    }

    public int getVotesCount() {
        return mVotesCount;
    }

    public int getFollowers() {
        return mFollowers;
    }

    public String getLastVisit() {
        return mLastVisit;
    }

    public DeliveryInfo getDeliveryInfo() {
        return mDeliveryInfo;
    }

    public BusinessInfo getBusinessInfo() {
        return mBusinessInfo;
    }

    public WeekWorkingTime getWorkingTime() {
        return mWorkingTime;
    }

    public int[] getCuisines() {
        return mCuisines;
    }

    public boolean isFollow() {
        return mFollow;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mAvatar);
        dest.writeString(this.mName);
        dest.writeString(this.mAbout);
        dest.writeFloat(this.mRating);
        dest.writeInt(this.mVotesCount);
        dest.writeInt(this.mFollowers);
        dest.writeString(this.mLastVisit);
        dest.writeParcelable(this.mDeliveryInfo, flags);
        dest.writeParcelable(this.mBusinessInfo, flags);
        dest.writeParcelable(this.mWorkingTime, flags);
        dest.writeIntArray(this.mCuisines);
        dest.writeByte(mFollow ? (byte) 1 : (byte) 0);
    }

    protected Chef(Parcel in) {
        this.mId = in.readString();
        this.mAvatar = in.readString();
        this.mName = in.readString();
        this.mAbout = in.readString();
        this.mRating = in.readFloat();
        this.mVotesCount = in.readInt();
        this.mFollowers = in.readInt();
        this.mLastVisit = in.readString();
        this.mDeliveryInfo = in.readParcelable(DeliveryInfo.class.getClassLoader());
        this.mBusinessInfo = in.readParcelable(BusinessInfo.class.getClassLoader());
        this.mWorkingTime = in.readParcelable(WeekWorkingTime.class.getClassLoader());
        this.mCuisines = in.createIntArray();
        this.mFollow = in.readByte() != 0;
    }

    public static final Creator<Chef> CREATOR = new Creator<Chef>() {
        @Override
        public Chef createFromParcel(Parcel source) {
            return new Chef(source);
        }

        @Override
        public Chef[] newArray(int size) {
            return new Chef[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        return o instanceof Chef && ((Chef) o).mId.equals(mId);
    }
}