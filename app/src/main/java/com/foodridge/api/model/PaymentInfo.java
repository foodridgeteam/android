package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class PaymentInfo
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.STRIPE_ID)
    private String mId;

    public PaymentInfo() {
    }

    public String getId() {
        return mId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
    }

    protected PaymentInfo(Parcel in) {
        this.mId = in.readString();
    }

    public static final Creator<PaymentInfo> CREATOR = new Creator<PaymentInfo>() {
        @Override
        public PaymentInfo createFromParcel(Parcel source) {
            return new PaymentInfo(source);
        }

        @Override
        public PaymentInfo[] newArray(int size) {
            return new PaymentInfo[size];
        }
    };
}