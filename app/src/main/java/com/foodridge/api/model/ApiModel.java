package com.foodridge.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 06/04/16
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public interface ApiModel extends Serializable {
}