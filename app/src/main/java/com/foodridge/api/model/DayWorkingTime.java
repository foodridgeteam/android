package com.foodridge.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class DayWorkingTime
        implements ApiModel {

    @JsonProperty(ApiKeys.START)
    private String mStart;

    @JsonProperty(ApiKeys.END)
    private String mEnd;

    public DayWorkingTime() {
    }

    public String getStart() {
        return mStart;
    }

    public String getEnd() {
        return mEnd;
    }
}