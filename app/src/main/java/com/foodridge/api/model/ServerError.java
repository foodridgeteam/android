package com.foodridge.api.model;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiError;
import com.foodridge.api.ApiKeys;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class ServerError
        implements ApiModel {

    @JsonProperty(ApiKeys.CODE)
    @ApiError
    private int mCode;

    @JsonProperty(ApiKeys.MESSAGE)
    private String mMessage;

    public ServerError() {
    }

    public ServerError(@ApiError int code, @NonNull String message) {
        mCode = code;
        mMessage = message;
    }

    @ApiError
    public int getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }
}