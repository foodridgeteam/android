package com.foodridge.api.model;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.type.OrderStatus;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class OrderMessage
        implements ApiModel, Comparable<OrderMessage> {

    @JsonProperty(ApiKeys.ID)
    private String mId;

    @JsonProperty(ApiKeys.CREATED)
    private String mCreated;

    @JsonProperty(ApiKeys.MESSAGE)
    private String mMessage;

    @JsonProperty(ApiKeys.SYSTEM)
    private int mStatus;

    @JsonProperty(ApiKeys.USER_ID)
    private String mUserId;

    @JsonIgnore
    private long mCreatedMillis;

    public OrderMessage() {
    }

    public String getId() {
        return mId;
    }

    public String getCreated() {
        return mCreated;
    }

    public String getMessage() {
        return mMessage;
    }

    @OrderStatus
    public int getStatus() {
        return mStatus;
    }

    public String getUserId() {
        return mUserId;
    }

    @JsonIgnore
    public long getCreatedMillis() {
        return mCreatedMillis;
    }

    @JsonIgnore
    public void setCreatedMillis(long createdMillis) {
        mCreatedMillis = createdMillis;
    }

    @Override
    public int compareTo(@NonNull OrderMessage message) {
        if (message.mCreatedMillis == mCreatedMillis) {
            return 0;
        }
        return message.mCreatedMillis > mCreatedMillis ? -1 : 1;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return this == o || mId.equals(((OrderMessage) o).mId);
    }
}