package com.foodridge.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.foodridge.api.ApiKeys;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class ServerAnswer
        implements ApiModel {

    @JsonProperty(ApiKeys.DATA)
    private JsonNode mData;

    @JsonProperty(ApiKeys.ERROR)
    private ServerError mError;

    public ServerAnswer() {
    }

    public JsonNode getData() {
        return mData;
    }

    public ServerError getError() {
        return mError;
    }
}