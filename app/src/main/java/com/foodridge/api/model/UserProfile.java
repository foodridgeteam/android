package com.foodridge.api.model;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 05/04/16
 */
public class UserProfile
        implements ApiModel {

    @JsonProperty(ApiKeys.ID)
    protected String mId;

    @JsonProperty(ApiKeys.AVATAR)
    protected String mAvatar;

    @JsonProperty(ApiKeys.NAME)
    protected String mName;

    @JsonProperty(ApiKeys.EMAIL)
    protected String mEmail;

    @JsonProperty(ApiKeys.PHONE)
    private String mPhone;

    @JsonProperty(ApiKeys.DELIVERY_INFO)
    protected DeliveryInfo mDeliveryInfo;

    @JsonProperty(ApiKeys.AGREE_TERMS)
    protected boolean mAgreeTerms;

    public UserProfile() {
    }

    public String getId() {
        return mId;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public String getName() {
        return mName;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public DeliveryInfo getDeliveryInfo() {
        return mDeliveryInfo;
    }

    public boolean isAgreeTerms() {
        return mAgreeTerms;
    }

    public void set(@NonNull UserProfile profile) {
        mId = profile.mId;
        mAvatar = profile.mAvatar;
        mName = profile.mName;
        mEmail = profile.mEmail;
        mPhone = profile.mPhone;
        mDeliveryInfo = profile.mDeliveryInfo;
        mAgreeTerms = profile.mAgreeTerms;
    }
}