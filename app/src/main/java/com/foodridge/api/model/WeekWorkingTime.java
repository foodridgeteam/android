package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class WeekWorkingTime
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.MON)
    private WorkingTime mMon;

    @JsonProperty(ApiKeys.TUE)
    private WorkingTime mTue;

    @JsonProperty(ApiKeys.WED)
    private WorkingTime mWed;

    @JsonProperty(ApiKeys.THU)
    private WorkingTime mThu;

    @JsonProperty(ApiKeys.FRI)
    private WorkingTime mFri;

    @JsonProperty(ApiKeys.SAT)
    private WorkingTime mSat;

    @JsonProperty(ApiKeys.SUN)
    private WorkingTime mSun;

    public WeekWorkingTime() {
    }

    public WeekWorkingTime(@Nullable WeekWorkingTime time) {
        if (time != null) {
            mMon = WorkingTime.check(time.mMon);
            mTue = WorkingTime.check(time.mTue);
            mWed = WorkingTime.check(time.mWed);
            mThu = WorkingTime.check(time.mThu);
            mFri = WorkingTime.check(time.mFri);
            mSat = WorkingTime.check(time.mSat);
            mSun = WorkingTime.check(time.mSun);
        }
    }

    public WorkingTime getMon() {
        return mMon;
    }

    public void setMon(WorkingTime mon) {
        mMon = mon;
    }

    public WorkingTime getTue() {
        return mTue;
    }

    public void setTue(WorkingTime tue) {
        mTue = tue;
    }

    public WorkingTime getWed() {
        return mWed;
    }

    public void setWed(WorkingTime wed) {
        mWed = wed;
    }

    public WorkingTime getThu() {
        return mThu;
    }

    public void setThu(WorkingTime thu) {
        mThu = thu;
    }

    public WorkingTime getFri() {
        return mFri;
    }

    public void setFri(WorkingTime fri) {
        mFri = fri;
    }

    public WorkingTime getSat() {
        return mSat;
    }

    public void setSat(WorkingTime sat) {
        mSat = sat;
    }

    public WorkingTime getSun() {
        return mSun;
    }

    public void setSun(WorkingTime sun) {
        mSun = sun;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @JsonIgnore
    public boolean isAllEmpty() {
        return mMon == null
                && mTue == null
                && mWed == null
                && mThu == null
                && mFri == null
                && mSat == null
                && mSun == null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mMon, flags);
        dest.writeParcelable(this.mTue, flags);
        dest.writeParcelable(this.mWed, flags);
        dest.writeParcelable(this.mThu, flags);
        dest.writeParcelable(this.mFri, flags);
        dest.writeParcelable(this.mSat, flags);
        dest.writeParcelable(this.mSun, flags);
    }

    protected WeekWorkingTime(Parcel in) {
        this.mMon = in.readParcelable(WorkingTime.class.getClassLoader());
        this.mTue = in.readParcelable(WorkingTime.class.getClassLoader());
        this.mWed = in.readParcelable(WorkingTime.class.getClassLoader());
        this.mThu = in.readParcelable(WorkingTime.class.getClassLoader());
        this.mFri = in.readParcelable(WorkingTime.class.getClassLoader());
        this.mSat = in.readParcelable(WorkingTime.class.getClassLoader());
        this.mSun = in.readParcelable(WorkingTime.class.getClassLoader());
    }

    public static final Creator<WeekWorkingTime> CREATOR = new Creator<WeekWorkingTime>() {
        @Override
        public WeekWorkingTime createFromParcel(Parcel source) {
            return new WeekWorkingTime(source);
        }

        @Override
        public WeekWorkingTime[] newArray(int size) {
            return new WeekWorkingTime[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (o instanceof WeekWorkingTime) {
            final WeekWorkingTime time = (WeekWorkingTime) o;
            return Utils.areEquals(mMon, time.mMon)
                    && Utils.areEquals(mTue, time.mTue)
                    && Utils.areEquals(mWed, time.mWed)
                    && Utils.areEquals(mThu, time.mThu)
                    && Utils.areEquals(mFri, time.mFri)
                    && Utils.areEquals(mSat, time.mSat)
                    && Utils.areEquals(mSun, time.mSun);
        }
        return isAllEmpty();
    }
}