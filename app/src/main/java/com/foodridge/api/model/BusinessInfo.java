package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class BusinessInfo
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.NAME)
    private String mName;

    @JsonProperty(ApiKeys.REGISTRATION_ID)
    private String mRegistrationId;

    @JsonProperty(ApiKeys.PHONE)
    private String mPhone;

    @JsonProperty(ApiKeys.SITE)
    private String mSite;

    public BusinessInfo() {
    }

    public BusinessInfo(@Nullable BusinessInfo info) {
        if (info != null) {
            mName = info.mName;
            mRegistrationId = info.mRegistrationId;
            mPhone = info.mPhone;
            mSite = info.mSite;
        }
    }

    public String getName() {
        return mName;
    }

    public String getRegistrationId() {
        return mRegistrationId;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getSite() {
        return mSite;
    }

    public void set(@NonNull String name, @NonNull String registration, @NonNull String phone, @NonNull String site) {
        mName = name;
        mRegistrationId = registration;
        mPhone = phone;
        mSite = site;
    }

    public boolean hasEmptyField() {
        return mName == null || mPhone == null;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BusinessInfo) {
            final BusinessInfo info = (BusinessInfo) o;
            return Utils.areEquals(mName, info.mName)
                    && Utils.areEquals(mRegistrationId, info.mRegistrationId)
                    && Utils.areEquals(mPhone, info.mPhone)
                    && Utils.areEquals(mSite, info.mSite);
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mName);
        dest.writeString(this.mRegistrationId);
        dest.writeString(this.mPhone);
        dest.writeString(this.mSite);
    }

    protected BusinessInfo(Parcel in) {
        this.mName = in.readString();
        this.mRegistrationId = in.readString();
        this.mPhone = in.readString();
        this.mSite = in.readString();
    }

    public static final Creator<BusinessInfo> CREATOR = new Creator<BusinessInfo>() {
        @Override
        public BusinessInfo createFromParcel(Parcel source) {
            return new BusinessInfo(source);
        }

        @Override
        public BusinessInfo[] newArray(int size) {
            return new BusinessInfo[size];
        }
    };
}