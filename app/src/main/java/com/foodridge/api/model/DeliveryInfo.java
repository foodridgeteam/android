package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class DeliveryInfo
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.COUNTRY)
    private String mCountry;

    @JsonProperty(ApiKeys.CITY)
    private String mCity;

    @JsonProperty(ApiKeys.STREET)
    private String mStreet;

    @JsonProperty(ApiKeys.MORE)
    private String mMore;

    public DeliveryInfo() {
    }

    public DeliveryInfo(@Nullable DeliveryInfo info) {
        if (info != null) {
            mCountry = info.mCountry;
            mCity = info.mCity;
            mStreet = info.mStreet;
            mMore = info.mMore;
        }
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public String getStreet() {
        return mStreet;
    }

    public String getMore() {
        return mMore;
    }

    public void set(String street, String city, String country, String more) {
        mStreet = street;
        mCity = city;
        mCountry = country;
        mMore = more;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DeliveryInfo) {
            final DeliveryInfo info = (DeliveryInfo) o;
            return Utils.areEquals(mCountry, info.mCountry)
                    && Utils.areEquals(mCity, info.mCity)
                    && Utils.areEquals(mStreet, info.mStreet);
        }
        return mStreet == null && mCity == null && mCountry == null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCountry);
        dest.writeString(mCity);
        dest.writeString(mStreet);
        dest.writeString(mMore);
    }

    protected DeliveryInfo(Parcel in) {
        mCountry = in.readString();
        mCity = in.readString();
        mStreet = in.readString();
        mMore = in.readString();
    }

    public static final Creator<DeliveryInfo> CREATOR = new Creator<DeliveryInfo>() {
        @Override
        public DeliveryInfo createFromParcel(Parcel source) {
            return new DeliveryInfo(source);
        }

        @Override
        public DeliveryInfo[] newArray(int size) {
            return new DeliveryInfo[size];
        }
    };

    @Override
    public String toString() {
        return "country: " + mCountry
                + "\tcity: " + mCity
                + "\tstreet: " + mStreet
                + "\tmore: " + mMore;
    }
}