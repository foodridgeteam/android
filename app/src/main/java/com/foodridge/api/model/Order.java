package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.type.DeliveryType;
import com.foodridge.type.OrderStatus;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class Order
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.ID)
    private String mId;

    @JsonProperty(ApiKeys.CLIENT)
    private User mClient;

    @JsonProperty(ApiKeys.MEAL)
    private Meal mMeal;

    @JsonProperty(ApiKeys.QUANTITY)
    private int mQuantity;

    @OrderStatus
    @JsonProperty(ApiKeys.STATUS)
    private int mStatus;

    @JsonProperty(ApiKeys.CREATED)
    private String mCreated;

    @DeliveryType
    @JsonProperty(ApiKeys.DELIVERY_TYPE)
    private int mDeliveryType;

    @JsonProperty(ApiKeys.CLIENT_DELIVERY_INFO)
    private DeliveryInfo mClientDeliveryInfo;

    @JsonProperty(ApiKeys.PHONE)
    private String mPhone;

    @JsonProperty(ApiKeys.TOTAL_SUM)
    private float mTotalSum;

    @JsonProperty(ApiKeys.COMMENT)
    private Comment mComment;

    public Order() {
    }

    public String getId() {
        return mId;
    }

    public User getClient() {
        return mClient;
    }

    public Meal getMeal() {
        return mMeal;
    }

    public int getQuantity() {
        return mQuantity;
    }

    @OrderStatus
    public int getStatus() {
        return mStatus;
    }

    public String getCreated() {
        return mCreated;
    }

    @DeliveryType
    public int getDeliveryType() {
        return mDeliveryType;
    }

    public DeliveryInfo getClientDeliveryInfo() {
        return mClientDeliveryInfo;
    }

    public String getPhone() {
        return mPhone;
    }

    public float getTotalSum() {
        return mTotalSum;
    }

    public Comment getComment() {
        return mComment;
    }

    public void setComment(Comment comment) {
        mComment = comment;
    }

    @JsonIgnore
    public boolean isActive() {
        switch (mStatus) {
            case OrderStatus.CREATED:
            case OrderStatus.ACCEPTED:
            case OrderStatus.PREPARED:
            case OrderStatus.SENT:
            case OrderStatus.RECEIVED:
            case OrderStatus.PAYED:
                return true;

            case OrderStatus.USER_CANCEL:
            case OrderStatus.CHEF_CANCEL:
            case OrderStatus.SYSTEM_CANCEL:
            case OrderStatus.COMPLETED:
            default:
                return false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeParcelable(this.mClient, flags);
        dest.writeParcelable(this.mMeal, flags);
        dest.writeInt(this.mQuantity);
        dest.writeInt(this.mStatus);
        dest.writeString(this.mCreated);
        dest.writeInt(this.mDeliveryType);
        dest.writeParcelable(this.mClientDeliveryInfo, flags);
        dest.writeString(this.mPhone);
        dest.writeFloat(this.mTotalSum);
        dest.writeParcelable(this.mComment, flags);
    }

    protected Order(Parcel in) {
        this.mId = in.readString();
        this.mClient = in.readParcelable(User.class.getClassLoader());
        this.mMeal = in.readParcelable(Meal.class.getClassLoader());
        this.mQuantity = in.readInt();
        //noinspection WrongConstant
        this.mStatus = in.readInt();
        this.mCreated = in.readString();
        //noinspection WrongConstant
        this.mDeliveryType = in.readInt();
        this.mClientDeliveryInfo = in.readParcelable(DeliveryInfo.class.getClassLoader());
        this.mPhone = in.readString();
        this.mTotalSum = in.readFloat();
        this.mComment = in.readParcelable(Comment.class.getClassLoader());
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}