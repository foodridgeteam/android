package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class MealFlags
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.BREAKFAST)
    private boolean mBreakfast;

    @JsonProperty(ApiKeys.LUNCH)
    private boolean mLunch;

    @JsonProperty(ApiKeys.DINNER)
    private boolean mDinner;

    @JsonProperty(ApiKeys.BAKERY)
    private boolean mBakery;

    @JsonProperty(ApiKeys.VEGETARIAN)
    private boolean mVegetarian;

    @JsonProperty(ApiKeys.EGG)
    private boolean mEgg;

    @JsonProperty(ApiKeys.GLUTEN_FREE)
    private boolean mGlutenFree;

    @JsonProperty(ApiKeys.TRACES_NUTS)
    private boolean mTracesNuts;

    @JsonProperty(ApiKeys.GARLIC)
    private boolean mGarlic;

    @JsonProperty(ApiKeys.GINGER)
    private boolean mGinger;

    public MealFlags() {
    }

    public MealFlags(@Nullable MealFlags flags) {
        if (flags != null) {
            mBreakfast = flags.mBreakfast;
            mLunch = flags.mLunch;
            mDinner = flags.mDinner;
            mBakery = flags.mBakery;
            mVegetarian = flags.mVegetarian;
            mEgg = flags.mEgg;
            mGlutenFree = flags.mGlutenFree;
            mTracesNuts = flags.mTracesNuts;
            mGarlic = flags.mGarlic;
            mGinger = flags.mGinger;
        }
    }

    public void set(boolean breakfast, boolean lunch, boolean dinner, boolean bakery, boolean vegetarian
            , boolean egg, boolean glutenFree, boolean tracesNuts, boolean garlic, boolean ginger) {
        mBreakfast = breakfast;
        mLunch = lunch;
        mDinner = dinner;
        mBakery = bakery;
        mVegetarian = vegetarian;
        mEgg = egg;
        mGlutenFree = glutenFree;
        mTracesNuts = tracesNuts;
        mGarlic = garlic;
        mGinger = ginger;
    }

    public boolean isBreakfast() {
        return mBreakfast;
    }

    public boolean isLunch() {
        return mLunch;
    }

    public boolean isDinner() {
        return mDinner;
    }

    public boolean isBakery() {
        return mBakery;
    }

    public boolean isVegetarian() {
        return mVegetarian;
    }

    public boolean isEgg() {
        return mEgg;
    }

    public boolean isGlutenFree() {
        return mGlutenFree;
    }

    public boolean isTracesNuts() {
        return mTracesNuts;
    }

    public boolean isGarlic() {
        return mGarlic;
    }

    public boolean isGinger() {
        return mGinger;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MealFlags) {
            final MealFlags flags = (MealFlags) o;

            return mBreakfast == flags.mBreakfast
                    && mLunch == flags.mLunch
                    && mDinner == flags.mDinner
                    && mBakery == flags.mBakery
                    && mVegetarian == flags.mVegetarian
                    && mEgg == flags.mEgg
                    && mGlutenFree == flags.mGlutenFree
                    && mTracesNuts == flags.mTracesNuts
                    && mGarlic == flags.mGarlic
                    && mGinger == flags.mGinger;
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(mBreakfast ? (byte) 1 : (byte) 0);
        dest.writeByte(mLunch ? (byte) 1 : (byte) 0);
        dest.writeByte(mDinner ? (byte) 1 : (byte) 0);
        dest.writeByte(mBakery ? (byte) 1 : (byte) 0);
        dest.writeByte(mVegetarian ? (byte) 1 : (byte) 0);
        dest.writeByte(mEgg ? (byte) 1 : (byte) 0);
        dest.writeByte(mGlutenFree ? (byte) 1 : (byte) 0);
        dest.writeByte(mTracesNuts ? (byte) 1 : (byte) 0);
        dest.writeByte(mGarlic ? (byte) 1 : (byte) 0);
        dest.writeByte(mGinger ? (byte) 1 : (byte) 0);
    }

    protected MealFlags(Parcel in) {
        this.mBreakfast = in.readByte() != 0;
        this.mLunch = in.readByte() != 0;
        this.mDinner = in.readByte() != 0;
        this.mBakery = in.readByte() != 0;
        this.mVegetarian = in.readByte() != 0;
        this.mEgg = in.readByte() != 0;
        this.mGlutenFree = in.readByte() != 0;
        this.mTracesNuts = in.readByte() != 0;
        this.mGarlic = in.readByte() != 0;
        this.mGinger = in.readByte() != 0;
    }

    public static final Creator<MealFlags> CREATOR = new Creator<MealFlags>() {
        @Override
        public MealFlags createFromParcel(Parcel source) {
            return new MealFlags(source);
        }

        @Override
        public MealFlags[] newArray(int size) {
            return new MealFlags[size];
        }
    };
}