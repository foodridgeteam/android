package com.foodridge.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 08/04/16
 */
public class PageContent
        implements ApiModel {

    @JsonProperty(ApiKeys.TITLE)
    private String mTitle;

    @JsonProperty(ApiKeys.BODY)
    private String mBody;

    public PageContent() {
    }

    public String getTitle() {
        return mTitle;
    }

    public String getBody() {
        return mBody;
    }
}