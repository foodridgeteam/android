package com.foodridge.api.model;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.utility.SharedStrings;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class WorkingTime
        implements ApiModel, Parcelable {

    @JsonProperty(ApiKeys.START)
    private String mStart;

    @JsonProperty(ApiKeys.END)
    private String mEnd;

    private transient Point mStartTime;
    private transient Point mEndTime;

    public WorkingTime() {
    }

    public WorkingTime(String start, String end) {
        mStart = start;
        mEnd = end;
    }

    public WorkingTime(@Nullable WorkingTime time) {
        if (time != null) {
            mStart = time.mStart;
            mEnd = time.mEnd;
        }
    }

    public String getStart() {
        return mStart;
    }

    public void setStart(String start) {
        mStart = start;
    }

    public String getEnd() {
        return mEnd;
    }

    public void setEnd(String end) {
        mEnd = end;
    }

    @JsonIgnore
    @NonNull
    public Point getStartTime() {
        if (mStartTime == null) {
            mStartTime = new Point();
        }
        if (mStart != null) {
            final String[] strings = mStart.split(SharedStrings.COLON);
            mStartTime.x = Integer.parseInt(strings[0]);
            mStartTime.y = Integer.parseInt(strings[1]);
        }
        return mStartTime;
    }

    @JsonIgnore
    @NonNull
    public Point getEndTime() {
        if (mEndTime == null) {
            mEndTime = new Point();
        }
        if (mEnd != null) {
            final String[] strings = mEnd.split(SharedStrings.COLON);
            mEndTime.x = Integer.parseInt(strings[0]);
            mEndTime.y = Integer.parseInt(strings[1]);
        }
        return mEndTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mStart);
        dest.writeString(this.mEnd);
    }

    protected WorkingTime(Parcel in) {
        this.mStart = in.readString();
        this.mEnd = in.readString();
    }

    public static final Creator<WorkingTime> CREATOR = new Creator<WorkingTime>() {
        @Override
        public WorkingTime createFromParcel(Parcel source) {
            return new WorkingTime(source);
        }

        @Override
        public WorkingTime[] newArray(int size) {
            return new WorkingTime[size];
        }
    };

    @Override
    public String toString() {
        return mStart + " - " + mEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof WorkingTime) {
            final WorkingTime time = (WorkingTime) o;
            return Utils.areEquals(mStart, time.mStart) && Utils.areEquals(mEnd, time.mEnd);
        }
        return false;
    }

    @Nullable
    public static WorkingTime check(@Nullable WorkingTime workingTime) {
        if (workingTime == null) {
            return null;
        }
        return workingTime.mStart == null || workingTime.mEnd == null ? null : new WorkingTime(workingTime);
    }
}