package com.foodridge.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodridge.api.ApiKeys;
import com.foodridge.interfaces.UserInterface;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
public class User
        implements ApiModel, UserInterface, Parcelable {

    @JsonProperty(ApiKeys.ID)
    private String mId;

    @JsonProperty(ApiKeys.AVATAR)
    private String mAvatar;

    @JsonProperty(ApiKeys.NAME)
    private String mName;

    public User() {
    }

    public String getId() {
        return mId;
    }

    @Override
    public String getAvatar() {
        return mAvatar;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mAvatar);
        dest.writeString(this.mName);
    }

    protected User(Parcel in) {
        this.mId = in.readString();
        this.mAvatar = in.readString();
        this.mName = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}