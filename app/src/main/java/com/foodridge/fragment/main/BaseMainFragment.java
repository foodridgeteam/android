package com.foodridge.fragment.main;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.foodridge.R;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
abstract class BaseMainFragment extends BaseFragment {

    private SwipeRefreshLayout mRefreshLayout;

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.refresh_layout);
        if (mRefreshLayout != null) {
            Utils.setStyle(mRefreshLayout);
            final OnRefreshListener refreshListener = this instanceof OnRefreshListener ? (OnRefreshListener) this : null;
            if (refreshListener == null) {
                mRefreshLayout.setEnabled(false);

            } else {
                mRefreshLayout.setOnRefreshListener(refreshListener);
            }
        }
    }

    @Override
    protected final void onActionBarReady(@NonNull ActionBar bar) {
        final CharSequence title = getToolbarTitle();
        if (title != null) {
            bar.setTitle(title);
        }
        if (supportToolbarSubtitleChange()) {
            bar.setSubtitle(getToolbarSubTitle());
        }
    }

    @Nullable
    protected CharSequence getToolbarTitle() {
        @StringRes final int stringRes = getToolbarTitleRes();
        if (stringRes > 0) {
            return getString(stringRes);
        }
        return null;
    }

    @StringRes
    protected int getToolbarTitleRes() {
        return 0;
    }

    /**
     * Used to trigger if the fragment should affect on toolbar subtitle (e.g. if enabled subtitle
     * would set or cleared if {@link #getToolbarSubTitle()} returns null, or doesn't affect on
     * toolbar subtitle if disabled).
     */
    protected boolean supportToolbarSubtitleChange() {
        return true;
    }

    @Nullable
    protected CharSequence getToolbarSubTitle() {
        @StringRes final int stringRes = getToolbarSubTitleRes();
        if (stringRes > 0) {
            return getString(stringRes);
        }
        return null;
    }

    @StringRes
    protected int getToolbarSubTitleRes() {
        return 0;
    }

    @CallSuper
    @Override
    public void setBlock(boolean block) {
        if (mRefreshLayout != null
                && mRefreshLayout.isRefreshing() != block) {
            Utils.setRefreshing(mRefreshLayout, block);
        }
    }

    @CallSuper
    @Override
    public void onCleanUp() throws Exception {
        if (mRefreshLayout != null) {
            Utils.cleanUp(mRefreshLayout);
        }
    }
}