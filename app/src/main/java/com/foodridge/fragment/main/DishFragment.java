package com.foodridge.fragment.main;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.adapter.DishCommentsOffsetAdapter;
import com.foodridge.adapter.PagerImagesAdapter;
import com.foodridge.api.model.Meal;
import com.foodridge.api.request.CommentsRequest;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.model.DbMeal;
import com.foodridge.model.Profile;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.CommentContract;
import com.foodridge.provider.FoodridgeMetaData.MealContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.type.DeliveryType;
import com.foodridge.utility.Utils;
import com.foodridge.view.ChefShortView;
import com.foodridge.view.IngredientsView;
import com.foodridge.view.MealFlagsView;
import com.foodridge.view.PriceOrderView;
import com.foodridge.view.RatingView;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 11.04.16
 */
public class DishFragment extends BaseMainFragment
        implements LoaderCallbacks<Cursor> {

    private static final String EXTRA_1 = "EXTRA_1";//mMeal

    // VIEW's
    private View mHeaderView;
    private ListView mListView;

    private ViewPager mViewPager;

    private PriceOrderView mDishPriceView;

    private ImageView mIvPickup;
    private ImageView mIvDelivery;
    private ImageView mIvServings;
    private ImageView mIvReviews;

    //    private TextView mTvPickup;
//    private TextView mTvDelivery;
    private TextView mTvServings;
    private TextView mTvReviews;
    private RatingView mRatingReview;

    private ChefShortView mChefShortView;

    private TextView mTvCuisines;
    private TextView mTvDescription;

    private IngredientsView mIngredientsView;

    private RatingView mRatingSpicy;
    private MealFlagsView mMealFlagsView;
    private View mLayoutOtherFlags;
    private TextView mTvOtherFlags;

    private View mLayoutOrderBig;
    private View mBtnOrderBig;
    private TextView mTvOrderBig;

    private RatingView mRatingAverage;
    private TextView mTvSold;

    private View mLayoutReviews;
    private TextView mTvComments;

    //VALUE's
    private String mProfileId;
    private Meal mMeal;

    // ADAPTER's
    private PagerImagesAdapter mImagesAdapter;
    private DishCommentsOffsetAdapter mCommentsAdapter;

    public static DishFragment newInstance(@Nullable ChefFragment fragment, @NonNull Meal meal) {
        final Bundle b = new Bundle(1);
        b.putParcelable(EXTRA_1, meal);

        final DishFragment f = new DishFragment();
        f.setTargetFragment(fragment, 0);
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        final Bundle bundle = b != null ? b : getArguments();
        mMeal = bundle.getParcelable(EXTRA_1);

        mProfileId = Profile.getInstance().getId();
        //noinspection ConstantConditions
        mImagesAdapter = new PagerImagesAdapter(mMeal.getPhotos());
        mCommentsAdapter = new DishCommentsOffsetAdapter();

        getLoaderManager().initLoader(R.id.loader_meals, null, this);
        getLoaderManager().initLoader(R.id.loader_comments, null, this);
        if (AContext.isNetworkConnected()) {
            RequestCenter.place(new CommentsRequest(mMeal.getId()), this);
        }
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        mHeaderView = inflater.inflate(R.layout.view_dish_details, null);
        return inflater.inflate(R.layout.fragment_refresh_list, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mListView = (ListView) v.findViewById(R.id.list_view);

        mViewPager = (ViewPager) mHeaderView.findViewById(R.id.view_pager);
        mViewPager.setAdapter(mImagesAdapter);

        mViewPager.getLayoutParams().height = (int) (AContext.getScreenSize().x / 1.333F);
        mViewPager.requestLayout();

        mDishPriceView = (PriceOrderView) mHeaderView.findViewById(R.id.view_dish_price);

        mIvPickup = (ImageView) mHeaderView.findViewById(R.id.iv_pick_up);
        mIvDelivery = (ImageView) mHeaderView.findViewById(R.id.iv_delivery);
        mIvServings = (ImageView) mHeaderView.findViewById(R.id.iv_servings);
        mIvReviews = (ImageView) mHeaderView.findViewById(R.id.iv_review);

//        mTvPickup = (TextView) mHeaderView.findViewById(R.id.tv_pick_up);
//        mTvDelivery = (TextView) mHeaderView.findViewById(R.id.tv_delivery);
        mTvServings = (TextView) mHeaderView.findViewById(R.id.tv_servings);
        mTvReviews = (TextView) mHeaderView.findViewById(R.id.tv_review);
        mRatingReview = (RatingView) mHeaderView.findViewById(R.id.review_rating);

        mChefShortView = (ChefShortView) mHeaderView.findViewById(R.id.view_chef_short);

        mTvCuisines = (TextView) mHeaderView.findViewById(R.id.tv_cuisines);
        mTvDescription = (TextView) mHeaderView.findViewById(R.id.tv_description);

        mIngredientsView = (IngredientsView) mHeaderView.findViewById(R.id.ingredients);

        mRatingSpicy = (RatingView) mHeaderView.findViewById(R.id.spicy_rating);
        mMealFlagsView = (MealFlagsView) mHeaderView.findViewById(R.id.meal_flags);
        mLayoutOtherFlags = mHeaderView.findViewById(R.id.layout_other);
        mTvOtherFlags = (TextView) mHeaderView.findViewById(R.id.tv_other_flags);

        mLayoutOrderBig = mHeaderView.findViewById(R.id.layout_order_big);
        mBtnOrderBig = mHeaderView.findViewById(R.id.btn_order_big);
        mTvOrderBig = (TextView) mHeaderView.findViewById(R.id.tv_order_big);

        mRatingAverage = (RatingView) mHeaderView.findViewById(R.id.average_rating);
        mTvSold = (TextView) mHeaderView.findViewById(R.id.tv_total_sold);

        mLayoutReviews = mHeaderView.findViewById(R.id.layout_reviews);
        mTvComments = (TextView) mHeaderView.findViewById(R.id.tv_comments);

        mListView.setPadding(0, 0, 0, 0);

        mListView.addHeaderView(mHeaderView);
        mListView.setAdapter(mCommentsAdapter);

        mChefShortView.setOnClickListener(mClickListener);
        mDishPriceView.setOnClickListener(mClickListener);
        mBtnOrderBig.setOnClickListener(mClickListener);

        updateData();
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();

        Utils.cleanUp(mListView);
        Utils.cleanUp(mViewPager);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putParcelable(EXTRA_1, mMeal);
    }

    @Nullable
    @Override
    protected CharSequence getToolbarTitle() {
        return mMeal.getName();
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.view_chef_short:
                    if (getTargetFragment() instanceof ChefFragment) {
                        popBackStack();

                    } else {
                        startFragment(ChefFragment.newInstance(mMeal.getChef()), true);
                    }
                    break;

                case R.id.btn_order:
                case R.id.btn_order_big:
                    startFragment(OrderFragment.newInstance(mMeal), true);
                    break;

                default:
                    break;
            }
        }
    };

    private void updateData() {
        mImagesAdapter.setData(mMeal.getPhotos());
        mDishPriceView.setData(mMeal);

        final int availableToOrder = mMeal.getPortionsAvailable();
        final boolean currentUser = mProfileId.equals(mMeal.getChef().getId());
        final boolean orderEnabled = availableToOrder > 0;

        if (currentUser) {
            Utils.changeVisibility(mLayoutOrderBig, View.GONE);

        } else {
            Utils.changeVisibility(mLayoutOrderBig, View.VISIBLE);
            mTvOrderBig.setText(orderEnabled ? R.string.Order_now : R.string.Sold_out);
        }
        mBtnOrderBig.setEnabled(orderEnabled || currentUser);

        boolean pickupAvailable;
        boolean deliveryAvailable;
        switch (mMeal.getDeliveryType()) {
            case DeliveryType.PICKUP_AND_DELIVERY:
                pickupAvailable = true;
                deliveryAvailable = true;
                break;

            case DeliveryType.DELIVERY:
                pickupAvailable = false;
                deliveryAvailable = true;
                break;

            default:
            case DeliveryType.PICKUP:
                pickupAvailable = true;
                deliveryAvailable = false;
                break;
        }
        Utils.setChecked(mIvPickup, pickupAvailable);
        Utils.setChecked(mIvDelivery, deliveryAvailable);

//        mTvPickup.setText(pickupAvailable ? R.string.Pick_up_available : R.string.Pick_up_unavailable);
//        mTvDelivery.setText(deliveryAvailable ? R.string.Delivery_available : R.string.Delivery_unavailable);
        mTvServings.setText(Html.fromHtml(getString(R.string.format_servings_left, availableToOrder)));
        mTvReviews.setText(mMeal.getVotesCount() <= 0 ? null : getResources().getQuantityString(R.plurals.format_Review, mMeal.getVotesCount(), mMeal.getVotesCount()));
        mRatingReview.setRating(mMeal.getRating());

        Utils.setChecked(mIvServings, mMeal.getPortionsAvailable() > 0);
        Utils.setChecked(mIvReviews, mMeal.getVotesCount() > 0);

        mChefShortView.setData(mMeal.getChef());

        mTvCuisines.setText(Utils.findAll(mMeal.getCuisines(), ASettings.getInstance().getCuisines(), true));
        mTvDescription.setText(mMeal.getDescription());

        mIngredientsView.setData(mMeal.getIngredients());

        mTvSold.setText(Html.fromHtml(getString(R.string.format_total_portions_sold, mMeal.getPortionsSold())));

        mRatingSpicy.setRating(mMeal.getSpicyRank());
        mMealFlagsView.setData(mMeal.getMealFlags(), false);

        if (TextUtils.isEmpty(mMeal.getOther())) {
            Utils.changeVisibility(mLayoutOtherFlags, View.GONE);

        } else {
            Utils.changeVisibility(mLayoutOtherFlags, View.VISIBLE);
            mTvOtherFlags.setText(mMeal.getOther());
        }

        mRatingAverage.setRating(mMeal.getRating());
        if (mMeal.getVotesCount() > 0) {
            Utils.changeVisibility(mLayoutReviews, View.VISIBLE);
            mTvComments.setText(Html.fromHtml(getString(R.string.format_comments_count, mMeal.getVotesCount())));

        } else {
            Utils.changeVisibility(mLayoutReviews, View.GONE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.loader_meals:
                return new CursorLoader(getActivity(), MealContract.CONTENT_URI, null, ContentHelper.eq(MealContract.ENTITY_ID, mMeal.getId()), null, null);

            case R.id.loader_comments:
                return new CursorLoader(getActivity(), CommentContract.CONTENT_URI, null, ContentHelper.eq(CommentContract.MEAL_ID, mMeal.getId()), null, null);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case R.id.loader_meals:
                if (data.moveToFirst()) {
                    final DbMeal meal = new DbMeal();
                    meal.fromCursor(data);
                    mMeal = meal.getEntity();

                    updateData();
                }
                break;

            case R.id.loader_comments:
                mCommentsAdapter.swapCursor(data, true);
                break;

            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}