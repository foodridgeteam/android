package com.foodridge.fragment.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;

import com.foodridge.R;
import com.foodridge.adapter.BaseOffsetCursorAdapter;
import com.foodridge.adapter.MyDishesOffsetAdapter;
import com.foodridge.api.model.Meal;
import com.foodridge.api.request.ListedMealRequest;
import com.foodridge.api.request.MealsRequest;
import com.foodridge.model.DbMeal;
import com.foodridge.model.Profile;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.MealContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.LimitOffsetHelper;
import com.foodridge.utility.LimitOffsetHelper.OnLimitOffsetCallback;
import com.foodridge.utility.SharedStrings;
import com.foodridge.utility.Utils;
import com.foodridge.view.item.MyDishItemView;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class MyDishesFragment extends BaseMainFragment
        implements OnRefreshListener, LoaderCallbacks<Cursor>, OnLimitOffsetCallback {

    // VIEW's
    private ListView mListView;
    private View mHeaderView;

    //VALUE
    private String mChefId;
    private LimitOffsetHelper mHelper;

    // ADAPTER
    private MyDishesOffsetAdapter mAdapter;

    @NonNull
    public static MyDishesFragment newInstance() {
        return new MyDishesFragment();
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        mChefId = Profile.getInstance().getId();
        mAdapter = new MyDishesOffsetAdapter(this);
        mHelper = new LimitOffsetHelper(this, R.id.code_meals);

        getLoaderManager().initLoader(R.id.loader_meals, null, this);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        mHeaderView = inflater.inflate(R.layout.view_my_dishes_header, null);
        return inflater.inflate(R.layout.fragment_refresh_list, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mListView = (ListView) v.findViewById(R.id.list_view);

        mListView.setPadding(0, 0, 0, mListView.getPaddingBottom());

        mListView.addHeaderView(mHeaderView);
        mAdapter.setListView(mListView);

        mHeaderView.findViewById(R.id.btn_add).setOnClickListener(mAddListener);
        mHeaderView.findViewById(R.id.view_background).setOnClickListener(mAddListener);
    }

    @Override

    protected int getToolbarTitleRes() {
        return R.string.My_Menu;
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();

        Utils.cleanUp(mListView);
        Utils.cleanUp(mHeaderView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHelper.onDestroy();
    }

    @Override
    public void onRefresh() {
        mHelper.onRefreshAll();
        RequestCenter.place(new MealsRequest(0, mAdapter.getCount(), mChefId, DbMeal.SEARCH_SELF), this);
    }

    private final OnClickListener mAddListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            startFragment(MyDishFragment.newInstance(MyDishesFragment.this, null), true);
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case MyDishItemView.CODE:
                final Meal meal = (Meal) objects[0];
                final Boolean edit = (Boolean) objects[1];
                if (edit == null) {
                    if (Profile.getInstance().getChefProfile().getPaymentInfo() == null) {
                        mAdapter.notifyDataSetChanged();
                        Utils.showError(getActivity(), getString(R.string.Dish_cant_listed));

                    } else {
                        setBlock(true);
                        RequestCenter.place(new ListedMealRequest(meal), MyDishesFragment.this);
                    }

                } else if (edit) {
                    startFragment(MyDishFragment.newInstance(this, meal), true);

                } else {
                    final Intent intent = new Intent(Intent.ACTION_SEND)
                            .setType(SharedStrings.MIME_PLAIN)
                            .putExtra(Intent.EXTRA_SUBJECT, getString(R.string.format_Chef_s_Share_dish_s, meal.getChef().getName(), meal.getName()))
                            .putExtra(Intent.EXTRA_TEXT, Utils.toString(meal));

                    Utils.startIntent(getActivity(), intent);
                }
                break;

            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                mHelper.onRequestAnswer(objects);
                break;

            case BaseOffsetCursorAdapter.CODE:
                if (mHelper.loadMoreData()) {
                    setBlock(true);
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.loader_meals:
                return new CursorLoader(getActivity(), MealContract.CONTENT_URI, null
                        , ContentHelper.and(ContentHelper.eq(MealContract.SEARCH, DbMeal.SEARCH_SELF), ContentHelper.eq(MealContract.CHEF_ID, mChefId)), null, null);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case R.id.loader_meals:
                mAdapter.swapCursor(data);
                break;

            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void placeRequest(int offset) {
        RequestCenter.place(new MealsRequest(offset, mChefId, DbMeal.SEARCH_SELF), this);
    }

    @Override
    public void setOffset(int offset) {
        mAdapter.setOffset(offset);
    }

    void resetData() {
        mHelper.reset();
    }
}