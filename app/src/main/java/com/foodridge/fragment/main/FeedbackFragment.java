package com.foodridge.fragment.main;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.api.request.FeedbackRequest;
import com.foodridge.application.AContext;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 07.04.16.
 */
@SuppressWarnings("unused")
public class FeedbackFragment extends BaseMainFragment {

    //VIEW's
    private EditText mEditText;
    private View mButton;

    @NonNull
    public static FeedbackFragment newInstance() {
        return new FeedbackFragment();
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mEditText = (EditText) v.findViewById(R.id.edit_text);
        mButton = v.findViewById(R.id.btn_send_feedback);

        mButton.setOnClickListener(mClickListener);

        AContext.showInputKeyboard(mEditText);
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.Feedback;
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mEditText);
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();
        Utils.cleanUp(mEditText);
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final String feedback = mEditText.getText().toString().trim();
            if (feedback.length() == 0) {
                mEditText.setError(getString(R.string.Field_required));

            } else {
                AContext.hideInputKeyboard(mEditText);

                setBlock(true);
                RequestCenter.place(new FeedbackRequest(feedback), FeedbackFragment.this);
            }
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                mEditText.setText(null);
                showSnackBar(R.string.Feedback_sent);
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @Override
    public void setBlock(boolean block) {
        super.setBlock(block);

        mEditText.setEnabled(!block);
        mButton.setEnabled(!block);
    }
}