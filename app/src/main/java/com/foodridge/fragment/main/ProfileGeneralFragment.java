package com.foodridge.fragment.main;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.foodridge.R;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.api.request.UpdateProfileRequest;
import com.foodridge.api.request.UploadAvatarRequest;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.dialog.DeliveryAddressDialog;
import com.foodridge.interfaces.OnBackPressedListener;
import com.foodridge.model.Profile;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.ImagePickHelper;
import com.foodridge.utility.Utils;
import com.foodridge.utility.Utils.SymbolsFilter;
import com.foodridge.view.DeliveryAddressView;

import java.io.File;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 07.04.16
 */
@SuppressWarnings("FieldCanBeLocal")
public class ProfileGeneralFragment extends BaseMainFragment
        implements OnBackPressedListener {

    private static final String EXTRA_1 = "EXTRA_1";//mDeliveryInfo
    private static final String EXTRA_2 = "EXTRA_2";//mAvatarUri

    //VIEW's
    private ImageView mIvAvatar;
    private View mProgressAvatar;

    private DeliveryAddressView mAddressView;

    private EditText mEtName;
    private EditText mEtEmail;
    private EditText mEtPhone;

    private ViewGroup mBtnSave;
    private View mProgressSave;

    private DeliveryAddressDialog mAddressDialog;

    //VALUE's
    private DeliveryInfo mDeliveryInfo;
    private Uri mAvatarUri;

    private RequestManager mGlide;
    private ImagePickHelper mImagePickHelper;

    @NonNull
    public static ProfileGeneralFragment newInstance() {
        return new ProfileGeneralFragment();
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        if (b == null) {
            final Profile profile = Profile.getInstance();

            mDeliveryInfo = new DeliveryInfo(profile.getDeliveryInfo());
            mAvatarUri = profile.getAvatar() == null ? null : Uri.parse(profile.getAvatar());

        } else {
            mDeliveryInfo = b.getParcelable(EXTRA_1);
            mAvatarUri = b.getParcelable(EXTRA_2);
        }
        mGlide = Glide.with(this);
        mImagePickHelper = new ImagePickHelper(this, b);

        mAddressDialog = new DeliveryAddressDialog(getActivity(), R.string.Delivery_Address, this);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_profile_general, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mGlide.load(R.drawable.background_profile).into((ImageView) v.findViewById(R.id.iv_background));

        mIvAvatar = (ImageView) v.findViewById(R.id.iv_avatar);
        mProgressAvatar = v.findViewById(R.id.progress_avatar);

        mAddressView = (DeliveryAddressView) v.findViewById(R.id.view_delivery_address);

        mEtName = (EditText) v.findViewById(R.id.et_name);
        mEtEmail = (EditText) v.findViewById(R.id.et_email);
        mEtPhone = (EditText) v.findViewById(R.id.et_phone);

        mBtnSave = (ViewGroup) v.findViewById(R.id.btn_save);
        mProgressSave = v.findViewById(R.id.progress_save);

        mEtEmail.setKeyListener(null);
        mGlide.load(mAvatarUri).into(mIvAvatar);

        mAddressView.setTitle(R.string.Delivery_Address, Color.BLACK);
        mAddressView.setData(mDeliveryInfo, true);

        if (b == null) {
            final Profile profile = Profile.getInstance();

            mEtName.setText(profile.getName());
            mEtEmail.setText(profile.getEmail());
            mEtPhone.setText(profile.getPhone());

            mEtName.setSelection(mEtName.length());

            if (mEtPhone.length() == 0) {
                mEtPhone.setText(ASettings.getInstance().getCountryCode());
            }
        }
        mEtName.addTextChangedListener(mTextWatcher);
        mEtPhone.addTextChangedListener(mTextWatcher);

        Utils.addFilter(mEtPhone, new SymbolsFilter(AConstant.PHONE_NOT_ALLOWED));

        checkChanges();
        Utils.setOnClickListener(mClickListener, mIvAvatar, mAddressView, mBtnSave);
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mEtName);
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();

        mEtName.removeTextChangedListener(mTextWatcher);
        mEtPhone.removeTextChangedListener(mTextWatcher);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);

        b.putParcelable(EXTRA_1, mDeliveryInfo);
        b.putParcelable(EXTRA_2, mAvatarUri);
        mImagePickHelper.onSaveInstanceState(b);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImagePickHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImagePickHelper.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_avatar:
                    mImagePickHelper.start(ProfileGeneralFragment.this);
                    break;

                case R.id.btn_delivery_address:
                    mAddressDialog.show(mDeliveryInfo);
                    break;

                case R.id.btn_save:
                    final String name = mEtName.getText().toString().trim();
                    final String phone = mEtPhone.getText().toString().trim();

                    if (checkChanges()) {
                        showSnackBar(R.string.Nothing_changed);

                    } else if (name.length() == 0) {
                        mEtName.setError(getString(R.string.Field_required));

                    } else if (phone.length() == 0) {
                        mEtPhone.setError(getString(R.string.Field_required));

                    } else {
                        if (!Utils.isPhoneNumberValid(phone)) {
                            mEtPhone.setError(getString(R.string.Phone_number_wrong));
                        }
                        mBtnSave.setEnabled(false);

                        setBlock(true);
                        RequestCenter.place(new UpdateProfileRequest(name, phone, mDeliveryInfo), ProfileGeneralFragment.this);
                    }
                    break;

                default:
                    break;
            }
        }
    };

    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case DeliveryAddressDialog.CODE:
                final String street = (String) objects[0];
                final String city = (String) objects[1];
                final String country = (String) objects[2];
                final String more = (String) objects[3];

                mDeliveryInfo.set(street, city, country, more);
                mAddressView.setData(mDeliveryInfo, true);

                checkChanges();
                break;

            case ImagePickHelper.CODE:
                final File file = (File) objects[0];
                if (file == null) {
                    showSnackBar(R.string.Access_denied);

                } else {
                    mAvatarUri = Uri.fromFile(file);
                    mGlide.load(mAvatarUri).into(mIvAvatar);

                    Profile profile = Profile.getInstance();
                    if (mAvatarUri != null && !profile.isSameAvatar(mAvatarUri)) {
                        mIvAvatar.setEnabled(false);
                        Utils.changeVisibility(mProgressAvatar, View.VISIBLE);

                        RequestCenter.place(new UploadAvatarRequest(mAvatarUri), ProfileGeneralFragment.this);
                    }
                }
                break;

            case RequestCenter.CODE_ERROR: {
                final int receiverCode = (int) objects[0];
                showError(objects);

                if (receiverCode == R.id.code_avatar) {
                    final Profile profile = Profile.getInstance();
                    profile.setFileAvatar(null);

                    mAvatarUri = profile.getAvatar() == null ? null : Uri.parse(profile.getAvatar());
                    mGlide.load(mAvatarUri).into(mIvAvatar);

                    mIvAvatar.setEnabled(true);
                    Utils.changeVisibility(mProgressAvatar, View.GONE);

                } else {
                    setBlock(false);
                    mBtnSave.setEnabled(true);
                }
            }
            break;

            case RequestCenter.CODE_ANSWER: {
                final int receiverCode = (int) objects[0];
                if (receiverCode == R.id.code_avatar) {
                    Profile.getInstance().setFileAvatar(mAvatarUri.toString());

                    mIvAvatar.setEnabled(true);
                    Utils.changeVisibility(mProgressAvatar, View.GONE);

                } else {
                    setBlock(false);
                    showSnackBar(R.string.Changes_saved);
                }
            }
            break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @Override
    public void setBlock(boolean block) {
        super.setBlock(block);
        Utils.changeVisibility(mProgressSave, block ? View.VISIBLE : View.INVISIBLE);
    }

    private boolean checkChanges() {
        final Profile profile = Profile.getInstance();
        final String name = mEtName.getText().toString().trim();
        final String phone = mEtPhone.getText().toString().trim();

        final boolean nothingChanged = name.equals(profile.getName())
                && phone.equals(profile.getPhone())
                && mDeliveryInfo.equals(profile.getDeliveryInfo());

        mBtnSave.setEnabled(!nothingChanged);

        return nothingChanged;
    }

    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            checkChanges();
        }
    };

    @Override
    public boolean onBackPress() {
        if (mBtnSave.isEnabled() && mBtnSave.getTag() == null) {
            mBtnSave.setTag(true);
            showSnackBar(R.string.Available_unsaved_data);
            return false;
        }
        return true;
    }
}