package com.foodridge.fragment.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.adapter.MyDishPhotoAdapter;
import com.foodridge.api.model.Meal;
import com.foodridge.api.model.MealFlags;
import com.foodridge.api.request.InsertMealRequest;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.model.Profile;
import com.foodridge.request.RequestCenter;
import com.foodridge.type.DeliveryType;
import com.foodridge.utility.HorizontalLayoutDecorator;
import com.foodridge.utility.ImagePickHelper;
import com.foodridge.utility.SharedStrings;
import com.foodridge.utility.Utils;
import com.foodridge.view.AutoCompleteView;
import com.foodridge.view.MealFlagsView;
import com.foodridge.view.RatingView;
import com.foodridge.view.item.MyDishPhotoItemView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 13.04.16
 */
public class MyDishFragment extends BaseMainFragment {

    private static final String EXTRA_0 = "EXTRA_0";//mMealFlags
    private static final String EXTRA_1 = "EXTRA_1";//mMealFlags
    private static final String EXTRA_2 = "EXTRA_2";//mCuisines
    private static final String EXTRA_3 = "EXTRA_3";//mIngredients
    private static final String EXTRA_4 = "EXTRA_4";//mAdapter
    private static final String EXTRA_5 = "EXTRA_5";//mSpicyLevel

    private static final int PRICE_MAX_LENGTH = 8;
    private static final int SERVINGS_MAX_LENGTH = 4;

    //VIEW's
    private EditText mEtName;
    private EditText mEtDescription;
    private AutoCompleteView mEtCuisines;

    private RecyclerView mRvPhotos;
    private View mLayoutInfoAddPhotos;

    private AutoCompleteView mEtIngredients;

    private RatingView mRatingSpicy;
    private MealFlagsView mMealFlagsView;
    private EditText mEtOther;

    private SwitchCompat mScPickUp;
    private EditText mEtPrice;

    private SwitchCompat mScDelivery;
    private View mLayoutDelivery;
    private EditText mEtDelivery;

    private EditText mEtServings;

    private SwitchCompat mScDisclaimer;

    private View mBtnSave;
    private View mProgressSave;

    //VALUE's
    private Meal mMeal;
    private MealFlags mMealFlags;
    private ArrayList<Integer> mCuisines;
    private ArrayList<Integer> mIngredients;
    private float mSpicyLevel;

    private ASettings mSettings;
    private ImagePickHelper mImagePickHelper;
    private HorizontalLayoutDecorator mLayoutDecorator;

    //ADAPTER
    private MyDishPhotoAdapter mAdapter;

    public static MyDishFragment newInstance(@NonNull MyDishesFragment target, @Nullable Meal meal) {
        final Bundle b = new Bundle(1);
        b.putParcelable(EXTRA_0, meal);

        final MyDishFragment f = new MyDishFragment();
        if (meal == null) {
            f.setTargetFragment(target, 0);
        }
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        final ArrayList<Uri> photos;
        if (b == null) {
            mMeal = getArguments().getParcelable(EXTRA_0);
            if (mMeal == null) {
                mMealFlags = new MealFlags();
                mCuisines = new ArrayList<>();
                mIngredients = new ArrayList<>();
                photos = new ArrayList<>(0);
                mSpicyLevel = 0;

            } else {
                mMealFlags = new MealFlags(mMeal.getMealFlags());
                mCuisines = Utils.toList(mMeal.getCuisines());
                mIngredients = Utils.toList(mMeal.getIngredients());
                final String[] photosServer = mMeal.getPhotos();
                if (photosServer == null) {
                    photos = new ArrayList<>(0);

                } else {
                    photos = new ArrayList<>(photosServer.length);
                    for (String uri : photosServer) {
                        photos.add(Uri.parse(uri));
                    }
                }
                mSpicyLevel = mMeal.getSpicyRank();
            }

        } else {
            mMeal = b.getParcelable(EXTRA_0);
            mMealFlags = b.getParcelable(EXTRA_1);
            mCuisines = b.getIntegerArrayList(EXTRA_2);
            mIngredients = b.getIntegerArrayList(EXTRA_3);
            photos = b.getParcelableArrayList(EXTRA_4);
            mSpicyLevel = b.getFloat(EXTRA_5);
        }
        mSettings = ASettings.getInstance();
        mImagePickHelper = new ImagePickHelper(this, b);
        mLayoutDecorator = new HorizontalLayoutDecorator(R.dimen.stroke_x3);

        //noinspection ConstantConditions
        mAdapter = new MyDishPhotoAdapter(photos, this);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_add_dish, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mEtName = (EditText) v.findViewById(R.id.et_name);
        mEtDescription = (EditText) v.findViewById(R.id.et_description);
        mEtCuisines = (AutoCompleteView) v.findViewById(R.id.et_cuisines);

        mRvPhotos = (RecyclerView) v.findViewById(R.id.rv_photos);
        mLayoutInfoAddPhotos = v.findViewById(R.id.layout_info_add_photos);

        mEtIngredients = (AutoCompleteView) v.findViewById(R.id.et_ingredients);

        mRatingSpicy = (RatingView) v.findViewById(R.id.rating_spicy);
        mMealFlagsView = (MealFlagsView) v.findViewById(R.id.meal_flags);
        mEtOther = (EditText) v.findViewById(R.id.et_other);

        mScPickUp = (SwitchCompat) v.findViewById(R.id.switch_pickup);
        mEtPrice = (EditText) v.findViewById(R.id.et_price);

        mScDelivery = (SwitchCompat) v.findViewById(R.id.switch_delivery);
        mLayoutDelivery = v.findViewById(R.id.layout_delivery_price);
        mEtDelivery = (EditText) v.findViewById(R.id.et_delivery_price);

        mEtServings = (EditText) v.findViewById(R.id.et_servings);

        mScDisclaimer = (SwitchCompat) v.findViewById(R.id.switch_disclaimer);

        mBtnSave = v.findViewById(R.id.btn_save);
        mProgressSave = v.findViewById(R.id.progress_save);

        final ASettings settings = ASettings.getInstance();
        mEtCuisines.setAvailableOptions(settings.getCuisines());
        mEtIngredients.setAvailableOptions(settings.getIngredients());

        if (AConstant.DEBUG && b == null && mMeal == null) {
            final Random random = new Random();

            mEtName.setText(Utils.randomWords(random, 2));
            mEtDescription.setText(Utils.randomWords(random, 12));
            mEtCuisines.setSelectedOptions(Arrays.asList(1, 4));
            mEtIngredients.setSelectedOptions(Arrays.asList(1, 2, 3, 4, 5, 6));
            mEtOther.setText(Utils.randomWords(random, 3));
            mScPickUp.setChecked(true);
            mEtPrice.setText((1 + random.nextInt(9)) + "." + random.nextInt(100));
            if (random.nextBoolean()) {
                mScDelivery.setChecked(true);
                mEtDelivery.setText((1 + random.nextInt(9)) + "." + random.nextInt(100));
            }
            mEtServings.setText(String.valueOf(10 + random.nextInt(100)));
            mScDisclaimer.setChecked(true);
        }
        if (b == null && mMeal != null) {
            mEtName.setText(mMeal.getName());
            mEtDescription.setText(mMeal.getDescription());
            mEtOther.setText(mMeal.getOther());

            final boolean pickupAvailable;
            final boolean deliveryAvailable;
            switch (mMeal.getDeliveryType()) {
                case DeliveryType.DELIVERY:
                    pickupAvailable = false;
                    deliveryAvailable = true;
                    break;

                case DeliveryType.PICKUP:
                    pickupAvailable = true;
                    deliveryAvailable = false;
                    break;

                case DeliveryType.PICKUP_AND_DELIVERY:
                default:
                    pickupAvailable = true;
                    deliveryAvailable = true;
                    break;
            }
            mScPickUp.setChecked(pickupAvailable);
            mScDelivery.setChecked(deliveryAvailable);

            mEtPrice.setText(String.valueOf(mMeal.getPortionPrice()));
            mEtDelivery.setText(String.valueOf(mMeal.getDeliveryPrice()));
            mEtServings.setText(String.valueOf(mMeal.getPortionsAvailable()));
        }
        mEtCuisines.setSelectedOptions(mCuisines);

        if (Profile.getInstance().getChefProfile().getDeliveryInfo() == null) {
            mScPickUp.setChecked(false);
            mScPickUp.setEnabled(false);
        }
        mRvPhotos.setAdapter(mAdapter);
        mRvPhotos.setHasFixedSize(true);
        mRvPhotos.addItemDecoration(mLayoutDecorator);
        mRvPhotos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        mEtName.setSelection(mEtName.length());

        Utils.addFilter(mEtPrice, new LengthFilter(PRICE_MAX_LENGTH), new PriceFilter(true));
        Utils.addFilter(mEtDelivery, new LengthFilter(PRICE_MAX_LENGTH), new PriceFilter(true));
        Utils.addFilter(mEtServings, new LengthFilter(SERVINGS_MAX_LENGTH), new PriceFilter(false));

        mEtPrice.setLongClickable(false);
        mEtDelivery.setLongClickable(false);
        mEtServings.setLongClickable(false);

        mEtIngredients.setSelectedOptions(mIngredients);

        mRatingSpicy.setRating(mSpicyLevel);
        mMealFlagsView.setData(mMealFlags, true);

        mLayoutDelivery.setVisibility(mScDelivery.isChecked() ? View.VISIBLE : View.GONE);

        changeInfoPhotosVisibility();
        Utils.setOnClickListener(mClickListener, v, R.id.btn_add_photo, R.id.btn_save);
        mScDelivery.setOnCheckedChangeListener(mDeliveryCheckedChangeListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mEtName);
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();

        saveWhenViewAvailable();
        mRvPhotos.removeItemDecoration(mLayoutDecorator);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);

        if (getView() != null) {
            saveWhenViewAvailable();
        }
        b.putParcelable(EXTRA_0, mMeal);
        b.putParcelable(EXTRA_1, mMealFlags);
        b.putIntegerArrayList(EXTRA_2, mCuisines);
        b.putIntegerArrayList(EXTRA_3, mIngredients);
        b.putParcelableArrayList(EXTRA_4, mAdapter.getUris());
        b.putFloat(EXTRA_5, mSpicyLevel);
        mImagePickHelper.onSaveInstanceState(b);
    }

    private void saveWhenViewAvailable() {
        mMealFlagsView.fillData(mMealFlags);
        mEtCuisines.getObjects(mCuisines, mSettings.getCuisinesMap());
        mEtIngredients.getObjects(mIngredients, mSettings.getIngredientsMap());
        mSpicyLevel = (int) mRatingSpicy.getRating();
    }

    @Override
    protected int getToolbarTitleRes() {
        return mMeal == null ? R.string.Add_Dish : R.string.Update_Dish;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImagePickHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImagePickHelper.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public void setBlock(boolean block) {
        if (mProgressSave == null) {
            super.setBlock(block);

        } else {
            mBtnSave.setEnabled(!block);
            Utils.changeVisibility(mProgressSave, block ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AContext.hideInputKeyboard(v);
            switch (v.getId()) {
                case R.id.btn_add_photo:
                    if (AConstant.DEBUG || mAdapter.getItemCount() == 0) {
                        mImagePickHelper.start(MyDishFragment.this);
                    }
                    break;

                case R.id.btn_save:
                    saveWhenViewAvailable();
                    if (!mScDisclaimer.isChecked()) {
                        showSnackBar(R.string.Disclaimer_error);
                        return;
                    }
                    final boolean pickupAvailable = mScPickUp.isChecked();
                    final boolean deliveryAvailable = mScDelivery.isChecked();
                    if (!pickupAvailable && !deliveryAvailable) {
                        showSnackBar(R.string.Delivery_type_not_selected);
                        return;
                    }
                    final String name = mEtName.getText().toString().trim();
                    if (name.length() == 0) {
                        mEtName.setError(getString(R.string.Field_required));
                        return;
                    }
                    final String dishPrice = mEtPrice.getText().toString().trim();
                    if (dishPrice.length() == 0) {
                        mEtPrice.setError(getString(R.string.Field_required));
                        return;
                    }
                    final String servingsCount = mEtServings.getText().toString().trim();
                    if (servingsCount.length() == 0) {
                        mEtServings.setError(getString(R.string.Field_required));
                        return;
                    }
                    if (mCuisines.size() == 0) {
                        showSnackBar(R.string.No_cuisine);
                        return;
                    }
//                    if (mAdapter.getItemCount() == 0) {
//                        showSnackBar(R.string.No_dish_photo);
//                        return;
//                    }
                    if (mIngredients.size() == 0) {
                        showSnackBar(R.string.No_ingredients);
                        return;
                    }
                    final int servings = Integer.parseInt(servingsCount);
                    final String deliveryPrice = mEtDelivery.getText().toString().trim();
                    final float delivery = !deliveryAvailable || deliveryPrice.length() == 0 ? 0 : Float.parseFloat(deliveryPrice);

                    @DeliveryType final int deliveryType;
                    if (pickupAvailable && deliveryAvailable) {
                        deliveryType = DeliveryType.PICKUP_AND_DELIVERY;

                    } else if (pickupAvailable) {
                        deliveryType = DeliveryType.PICKUP;

                    } else {
                        deliveryType = DeliveryType.DELIVERY;
                    }
                    final Meal meal = new Meal(name, mEtDescription.getText().toString()
                            , Utils.toIntArray(mCuisines), Utils.toIntArray(mIngredients), servings
                            , mSpicyLevel, mMealFlags
                            , mEtOther.getText().toString(), Float.parseFloat(dishPrice), delivery
                            , deliveryType, mMeal != null && mMeal.isListed());

                    final ObjectsReceiver receiver = MyDishFragment.this;
                    setBlock(true);
                    RequestCenter.place(new InsertMealRequest(mMeal, meal, mAdapter.getUris()), receiver);
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case ImagePickHelper.CODE:
                final File file = (File) objects[0];
                if (file == null) {
                    showSnackBar(R.string.Access_denied);

                } else if (!mAdapter.add(Uri.fromFile(file))) {
                    showSnackBar(R.string.Image_already_added);
                }
                changeInfoPhotosVisibility();
                break;

            case MyDishPhotoItemView.CODE:
                mAdapter.remove((Uri) objects[0]);
                changeInfoPhotosVisibility();
                break;

            case RequestCenter.CODE_ERROR: {
                setBlock(false);
                showError(objects);
            }
            break;

            case RequestCenter.CODE_ANSWER:
                final Fragment target = getTargetFragment();
                if (target instanceof MyDishesFragment) {
                    ((MyDishesFragment) target).resetData();
                }
                postPopBackStack();
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    private void changeInfoPhotosVisibility() {
        final int visibility = mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE;
        Utils.changeVisibility(mLayoutInfoAddPhotos, visibility);
    }

    private static final class PriceFilter implements InputFilter {

        private final boolean mDotAvailable;

        private final StringBuilder mSb;

        public PriceFilter(boolean dotAvailable) {
            mDotAvailable = dotAvailable;
            mSb = new StringBuilder();
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            mSb
                    .delete(0, mSb.length())
                    .append(dest)
                    .insert(dstart, source);

            int dotsCount = 0;
            int digitsAfterDot = 0;
            char c;

            final int size = mSb.length();
            for (int i = 0; i < size; i++) {
                c = mSb.charAt(i);
                if (Character.isDigit(c)) {
                    if (dotsCount > 0) {
                        if (++digitsAfterDot > 2) {
                            return SharedStrings.EMPTY;
                        }
                    }

                } else {
                    if (mDotAvailable && c == '.') {
                        if (dotsCount++ > 0) {
                            return SharedStrings.EMPTY;
                        }

                    } else {
                        return SharedStrings.EMPTY;
                    }
                }
            }
            if (size > 0) {
                final char first = mSb.charAt(0);
                return first == '0' || first == '.' ? SharedStrings.EMPTY : null;
            }
            return null;
        }
    }

    private final OnCheckedChangeListener mDeliveryCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                mLayoutDelivery.setVisibility(View.VISIBLE);
                mLayoutDelivery.setAlpha(0F);
                mLayoutDelivery.animate().alpha(1F).start();

            } else {
                if (mEtDelivery.isFocused()) {
                    AContext.hideInputKeyboard(mEtDelivery);
                }
                mLayoutDelivery.setVisibility(View.GONE);
            }
        }
    };
}