package com.foodridge.fragment.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout.LayoutParams;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.application.AContext;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 07.04.16.
 */
public class TermsFragment extends BaseMainFragment {

    //VIEW's
    private WebView mWebView;

    //VALUE
    private boolean mLoaded;

    @NonNull
    public static TermsFragment newInstance() {
        return new TermsFragment();
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        final RefreshLayout layout = new RefreshLayout(new ContextThemeWrapper(inflater.getContext(), R.style.AppTheme_AlertDialogLight));
        layout.setId(R.id.refresh_layout);

        return layout;
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        v.setEnabled(true);
        mWebView = new WebView(v.getContext());

        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.setWebViewClient(new Client());
        mWebView.loadUrl(ApiConstant.HTML_TERMS_CONDITION_USER);
        {
            final int padding = getResources().getDimensionPixelSize(R.dimen.spacing);
            mWebView.setPadding(padding, padding, padding, padding);
        }
        ((ViewGroup) v).addView(mWebView, new RefreshLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.Terms_Conditions;
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();
        Utils.cleanUp(mWebView);
    }

    private final class Client extends WebViewClient {

        private Object mError;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            setBlock(true);
            mError = null;
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            mError = error;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            setBlock(false);
            mLoaded = mError == null;
        }
    }

    private final class RefreshLayout extends SwipeRefreshLayout
            implements OnRefreshListener {

        public RefreshLayout(Context context) {
            super(context);
            super.setOnRefreshListener(this);
        }

        @Override
        public boolean canChildScrollUp() {
            return mWebView.getScrollY() > 0;
        }

        @Override
        public void onRefresh() {
            if (mLoaded && !AContext.isNetworkConnected()) {
                setBlock(false);

            } else {
                mWebView.loadUrl(ApiConstant.HTML_TERMS_CONDITION_USER);
            }
        }
    }
}