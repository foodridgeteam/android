package com.foodridge.fragment.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.BusinessInfo;
import com.foodridge.api.model.ChefProfile;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.api.model.WeekWorkingTime;
import com.foodridge.api.request.DisconnectStripeRequest;
import com.foodridge.api.request.UpdateProfileChefRequest;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.dialog.BusinessDetailsDialog;
import com.foodridge.dialog.DeliveryAddressDialog;
import com.foodridge.interfaces.OnBackPressedListener;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.SharedStrings;
import com.foodridge.utility.Utils;
import com.foodridge.view.AutoCompleteView;
import com.foodridge.view.BusinessDetailsView;
import com.foodridge.view.DeliveryAddressView;
import com.foodridge.view.WeekWorkingTimeView;

import java.util.ArrayList;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 07.04.16
 */
@SuppressWarnings("FieldCanBeLocal")
public class ProfileChefFragment extends BaseMainFragment
        implements LoaderCallbacks<Cursor>, OnBackPressedListener {

    private static final String EXTRA_1 = "EXTRA_1";//mWorkingTime
    private static final String EXTRA_2 = "EXTRA_2";//mDeliveryInfo
    private static final String EXTRA_3 = "EXTRA_3";//mBusinessInfo
    private static final String EXTRA_4 = "EXTRA_4";//mCuisines

    //VIEW's
    private DeliveryAddressView mAddressView;
    private BusinessDetailsView mBusinessView;

    private EditText mEtBio;
    private AutoCompleteView mEtCuisine;

    private View mBtnBusinessDetails;

    private WeekWorkingTimeView mWorkingTimeView;

    private TextView mBtnStripe;

    private View mBtnSave;
    private View mProgressSave;

    private View mBtnDeleteAccount;
    @SuppressWarnings("unused")
    private View mProgressDelete;

    private DeliveryAddressDialog mAddressDialog;
    private BusinessDetailsDialog mBusinessDetailsDialog;

    //VALUE's
    private WeekWorkingTime mWorkingTime;
    private DeliveryInfo mDeliveryInfo;
    private BusinessInfo mBusinessInfo;
    private ArrayList<Integer> mCuisines;
    private boolean mStripeNotConnected;

    private ASettings mSettings;

    @NonNull
    public static ProfileChefFragment newInstance() {
        return new ProfileChefFragment();
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        if (b == null) {
            final ChefProfile chefProfile = Profile.getInstance().getChefProfile();

            mWorkingTime = new WeekWorkingTime(chefProfile.getWorkingTime());
            mDeliveryInfo = new DeliveryInfo(chefProfile.getDeliveryInfo());
            mBusinessInfo = new BusinessInfo(chefProfile.getBusinessInfo());
            mCuisines = chefProfile.getCuisines() == null ? new ArrayList<Integer>(0) : new ArrayList<>(chefProfile.getCuisines());

        } else {
            mWorkingTime = b.getParcelable(EXTRA_1);
            mDeliveryInfo = b.getParcelable(EXTRA_2);
            mBusinessInfo = b.getParcelable(EXTRA_3);
            mCuisines = b.getIntegerArrayList(EXTRA_4);
        }
        mAddressDialog = new DeliveryAddressDialog(getActivity(), R.string.Pickup_Address, this);
        mBusinessDetailsDialog = new BusinessDetailsDialog(getActivity(), this);

        mSettings = ASettings.getInstance();

        getLoaderManager().initLoader(0, null, this);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_profile_chef, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mAddressView = (DeliveryAddressView) v.findViewById(R.id.view_delivery_address);
        mBusinessView = (BusinessDetailsView) v.findViewById(R.id.view_business_details);

        mEtBio = (EditText) v.findViewById(R.id.et_bio);
        mEtCuisine = (AutoCompleteView) v.findViewById(R.id.et_cuisines);

        mBtnBusinessDetails = v.findViewById(R.id.btn_business_details);

        mWorkingTimeView = (WeekWorkingTimeView) v.findViewById(R.id.view_working_time);

        mBtnStripe = (TextView) v.findViewById(R.id.btn_stripe);

        mBtnSave = v.findViewById(R.id.btn_save);
        mProgressSave = v.findViewById(R.id.progress_save);

        mBtnDeleteAccount = v.findViewById(R.id.btn_delete);
        mProgressDelete = v.findViewById(R.id.progress_delete);

        mAddressView.setTitle(R.string.Pickup_Address, Color.BLACK);
        mAddressView.setData(mDeliveryInfo, true);

        mBusinessView.setTitle(R.string.Business_details, Color.BLACK);
        mBusinessView.setData(mBusinessInfo, true);

        if (b == null) {
            mEtBio.setText(Profile.getInstance().getChefProfile().getAbout());
        }
        mEtBio.setSelection(mEtBio.length());
        mEtCuisine.setAvailableOptions(mSettings.getCuisines());

        mWorkingTimeView.setData(mWorkingTime, true);
        mWorkingTimeView.setReceiver(this);

        mEtCuisine.setSelectedOptions(mCuisines);

        mEtBio.addTextChangedListener(mTextWatcher);
        mEtCuisine.addTextChangedListener(mTextWatcher);

        if (b == null || Profile.getInstance().isChef()) {
            mEtCuisine.post(mCheckChangesRun);

        } else {
            mBtnSave.setEnabled(false);
        }
        Utils.setOnClickListener(mClickListener, mAddressView, mBusinessView, mBtnBusinessDetails, v.findViewById(R.id.btn_share), mBtnStripe, mBtnSave, mBtnDeleteAccount);
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mEtBio);
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();

        mEtBio.removeTextChangedListener(mTextWatcher);
        mEtCuisine.removeTextChangedListener(mTextWatcher);

        saveWhenViewAvailable();
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);

        if (getView() != null) {
            saveWhenViewAvailable();
        }
        b.putParcelable(EXTRA_1, mWorkingTime);
        b.putParcelable(EXTRA_2, mDeliveryInfo);
        b.putParcelable(EXTRA_3, mBusinessInfo);
        b.putIntegerArrayList(EXTRA_4, mCuisines);
    }

    private void saveWhenViewAvailable() {
        mWorkingTime = mWorkingTimeView.getEditedWorkingTime();
        mEtCuisine.getObjects(mCuisines, mSettings.getCuisinesMap());
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AContext.hideInputKeyboard(v);
            switch (v.getId()) {
                case R.id.btn_delivery_address:
                    mAddressDialog.show(mDeliveryInfo);
                    break;

                case R.id.btn_business_details:
                    mBusinessDetailsDialog.show(mBusinessInfo);
                    break;

                case R.id.btn_save:
                    if (checkChanges()) {
                        showSnackBar(R.string.Nothing_changed);

                    } else if (mBusinessInfo.hasEmptyField()) {
                        showSnackBar(R.string.Business_details_required);

                    } else {
                        mBtnSave.setEnabled(false);

                        setBlock(true);
                        RequestCenter.place(new UpdateProfileChefRequest(mDeliveryInfo, mEtBio.getText().toString().trim(), mCuisines, mBusinessInfo, mWorkingTime), ProfileChefFragment.this);
                    }
                    break;

                case R.id.btn_delete:
                    showSnackBar("Not ready yet");
                    break;

                case R.id.btn_stripe:
                    if (!mStripeNotConnected) {
                        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setBlock(true);
                                RequestCenter.place(new DisconnectStripeRequest(), ProfileChefFragment.this);
                            }
                        };
                        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme_AlertDialogLight))
                                .setTitle(R.string.Confirm_Action)
                                .setMessage(R.string.Disconnect_Stripe_account)
                                .setPositiveButton(R.string.Yes, listener)
                                .setNegativeButton(R.string.Cancel, null)
                                .show();

                    } else if (Profile.getInstance().isChef()) {
                        Utils.startIntent(getActivity(), new Intent(Intent.ACTION_VIEW, Uri.parse(Utils.getStripeAuthorizeUri())));

                    } else {
                        showSnackBar(R.string.Unavailable_until_chief);
                    }
                    break;

                case R.id.btn_share:
                    final Intent intent = new Intent(Intent.ACTION_SEND)
                            .setType(SharedStrings.MIME_PLAIN)
                            .putExtra(Intent.EXTRA_SUBJECT, getString(R.string.Chef_share_self_subject))
                            .putExtra(Intent.EXTRA_TEXT, getString(R.string.Chef_share_self_text));

                    Utils.startIntent(getActivity(), intent);
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                showSnackBar(R.string.Changes_saved);
                break;

            case RequestCenter.CODE_ERROR:
                mBtnSave.setEnabled(true);

                setBlock(false);
                showError(objects);
                break;

            case DeliveryAddressDialog.CODE:
                final String street = (String) objects[0];
                final String city = (String) objects[1];
                final String country = (String) objects[2];
                final String more = (String) objects[3];

                mDeliveryInfo.set(street, city, country, more);
                mAddressView.setData(mDeliveryInfo, true);

                checkChanges();
                break;

            case BusinessDetailsDialog.CODE:
                final String name = (String) objects[0];
                final String registration = (String) objects[1];
                final String phone = (String) objects[2];
                final String site = (String) objects[3];

                if (!TextUtils.isEmpty(phone) && !Utils.isPhoneNumberValid(phone)) {
                    showSnackBar(R.string.Phone_number_wrong);
                }
                mBusinessInfo.set(name, registration, phone, site);
                mBusinessView.setData(mBusinessInfo, true);

                checkChanges();
                break;

            case WeekWorkingTimeView.CODE:
                checkChanges();
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @Override
    public void setBlock(boolean block) {
        super.setBlock(block);

        Utils.changeVisibility(mProgressSave, block ? View.VISIBLE : View.INVISIBLE);
        mBtnStripe.setEnabled(!block);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ProfileContract.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mStripeNotConnected = Profile.getInstance().getChefProfile().getPaymentInfo() == null;

        //noinspection deprecation
        mBtnStripe.setTextColor(getResources().getColorStateList(mStripeNotConnected ? R.color.colorAccent : R.color.sel_blue));
        mBtnStripe.setBackgroundResource(mStripeNotConnected ? R.drawable.sel_facebook : R.drawable.sel_stroke_blue);
        mBtnStripe.setText(mStripeNotConnected ? R.string.Connect_Stripe : R.string.Stripe_connected);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private boolean checkChanges() {
        saveWhenViewAvailable();

        final ChefProfile profile = Profile.getInstance().getChefProfile();
        final String bio = mEtBio.getText().toString().trim();

        final boolean nothingChanged = mDeliveryInfo.equals(profile.getDeliveryInfo())
                && Utils.areEquals(bio, profile.getAbout())
                && profile.isCuisinesSame(mCuisines)
                && mBusinessInfo.equals(profile.getBusinessInfo())
                && mWorkingTime.equals(profile.getWorkingTime());

        mBtnSave.setEnabled(!nothingChanged);

        return nothingChanged;
    }

    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (AConstant.DEBUG) {
                mEtCuisine.removeCallbacks(mCheckChangesRun);
                mEtCuisine.postDelayed(mCheckChangesRun, 100);

            } else {
                mBtnSave.setEnabled(true);
            }
        }
    };

    @Override
    public boolean onBackPress() {
        if (mBtnSave.isEnabled() && mBtnSave.getTag() == null) {
            mBtnSave.setTag(true);
            showSnackBar(R.string.Available_unsaved_data);
            return false;
        }
        return true;
    }

    private final Runnable mCheckChangesRun = new Runnable() {
        @Override
        public void run() {
            checkChanges();
        }
    };
}