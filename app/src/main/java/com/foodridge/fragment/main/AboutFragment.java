package com.foodridge.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.foodridge.BuildConfig;
import com.foodridge.R;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 07.04.16.
 */
public class AboutFragment extends BaseMainFragment {

    @NonNull
    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);
        ((TextView) v.findViewById(R.id.tv_version)).setText(getString(R.string.format_version, BuildConfig.VERSION_NAME));
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.About;
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();
    }
}