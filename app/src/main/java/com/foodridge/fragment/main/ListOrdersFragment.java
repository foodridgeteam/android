package com.foodridge.fragment.main;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.foodridge.R;
import com.foodridge.activity.BaseActivity;
import com.foodridge.adapter.OrdersOffsetAdapter;
import com.foodridge.api.model.Order;
import com.foodridge.api.request.ANetworkRequest;
import com.foodridge.api.request.LeaveCommentRequest;
import com.foodridge.api.request.MealRequest;
import com.foodridge.api.request.OrderStatusRequest;
import com.foodridge.api.request.OrdersRequest;
import com.foodridge.application.AContext;
import com.foodridge.dialog.OrderMessage2Dialog;
import com.foodridge.dialog.RateChefDialog;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.model.Profile;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.OrderContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.type.OrderStatus;
import com.foodridge.type.OrderType;
import com.foodridge.utility.Utils;
import com.foodridge.view.ListEmptyView;
import com.foodridge.view.item.order.BaseOrderItemView;
import com.foodridge.view.item.order.BaseOrderItemView.OrderItemClick;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 11.04.16.
 */
public class ListOrdersFragment extends BaseMainFragment
        implements LoaderCallbacks<Cursor>, OnRefreshListener {

    private static final String KEY_TYPE = "KEY_TYPE";

    // VIEW's
    private ListView mListView;

    private RateChefDialog mRateChefDialog;
    private OrderMessage2Dialog mOrderMessageDialog;

    //VALUE's
    private String mProfileId;

    // ADAPTER
    private OrdersOffsetAdapter mAdapter;

    // VALUE
    @OrderType
    private int mType;

    @NonNull
    public static ListOrdersFragment newInstance(@OrderType int type) {
        final Bundle args = new Bundle();
        args.putInt(KEY_TYPE, type);

        final ListOrdersFragment f = new ListOrdersFragment();
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        final Bundle bundle = b != null ? b : getArguments();
        //noinspection WrongConstant
        mType = bundle.getInt(KEY_TYPE);

        mProfileId = Profile.getInstance().getId();
        mAdapter = new OrdersOffsetAdapter(this, mType);

        getLoaderManager().initLoader(R.id.loader_orders, null, this);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_refresh_list_with_empty, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mListView = (ListView) v.findViewById(R.id.list_view);
        mListView.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.spacing));

        final ListEmptyView emptyView = ListEmptyView.attach((ViewGroup) v.findViewById(R.id.frame_layout), mListView);
        switch (mType) {
            case OrderType.COMPLETED:
                emptyView.setData(0, R.string.empty_order_completed, 0, null);
                break;

            case OrderType.PLACED:
                emptyView.setData(R.drawable.ic_no_order, R.string.empty_order_placed, R.string.Check_the_menu, mGoDishesClickListener);
                break;

            case OrderType.RECEIVED:
                emptyView.setData(0, R.string.empty_order_received, 0, null);
                break;

            default:
                throw new IllegalStateException();
        }

        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();
        Utils.cleanUp(mListView);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putInt(KEY_TYPE, mType);
    }

    @Override
    protected int getToolbarTitleRes() {
        return 0;
    }

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case BaseOrderItemView.CODE: {
                @OrderItemClick final int click = (int) objects[0];
                final Order order = (Order) objects[1];
                final boolean isClient = mProfileId.equals(order.getClient().getId());
                switch (click) {
                    case OrderItemClick.ACCEPT_EDIT:
                        @OrderStatus final int acceptOperation = (int) objects[2];
                        if (acceptOperation == OrderStatus.ACCEPTED) {
                            showConfirmDialog(order, OrderStatus.ACCEPTED, R.string.format_Accept_order_s);

                        } else {
                            mAdapter.blockOrder(order, true);

                            setBlock(true);
                            RequestCenter.place(new OrderStatusRequest(order.getId(), acceptOperation), this);
                        }
                        break;

                    case OrderItemClick.CANCEL:
                        showConfirmDialog(order, isClient ? OrderStatus.USER_CANCEL : OrderStatus.CHEF_CANCEL, R.string.format_Cancel_order_s);
                        break;

                    case OrderItemClick.DELIVERED_RECEIVED:
                        mAdapter.blockOrder(order, true);

                        @OrderStatus final int deliveryOperation = (int) objects[2];
                        setBlock(true);
                        RequestCenter.place(new OrderStatusRequest(order.getId(), deliveryOperation), this);
                        break;

                    case OrderItemClick.MESSAGE:
                        if (mOrderMessageDialog == null) {
                            mOrderMessageDialog = new OrderMessage2Dialog(getActivity(), this);
                        }
                        mOrderMessageDialog.show(order);
                        break;

                    case OrderItemClick.RATE:
                        mAdapter.blockOrder(order, true);

                        if (mRateChefDialog == null) {
                            mRateChefDialog = new RateChefDialog(getActivity(), this);
                        }
                        mRateChefDialog.show(order);
                        break;

                    case OrderItemClick.USER:
                        if (order.isActive()
                                && !Utils.callPhone(getActivity(), isClient ? order.getMeal().getChef().getBusinessInfo().getPhone() : order.getPhone())) {
                            showSnackBar(R.string.cant_handle_intent);
                        }
                        break;

                    case OrderItemClick.MEAL:
                        new ANetworkRequest(new MealRequest(order.getMeal().getId(), true)).oneShot(Thread.MAX_PRIORITY);
                        startFragment(DishFragment.newInstance(null, order.getMeal()), true);
                        break;

                    case OrderItemClick.ORDER:
                    default:
                        break;
                }
            }
            break;

            case RateChefDialog.CODE:
                final Order order = (Order) objects[0];
                final int rating = (int) objects[1];
                final String comment = (String) objects[2];

                if (order != null) {
                    setBlock(true);
                    RequestCenter.place(new LeaveCommentRequest(order, rating, comment), this);
                }
                if (order == null || !AContext.isNetworkConnected()) {
                    mAdapter.clearAllBlocked();
                    mAdapter.notifyDataSetChanged();
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @Override
    public boolean startFragment(@NonNull BaseFragment fragment, boolean addToBackStack, @NonNull Object... objects) {
        final BaseActivity activity = (BaseActivity) getActivity();
        final boolean activityAvailable = activity != null;
        if (activity != null) {
            activity.startFragment(fragment, addToBackStack);
        }
        return activityAvailable;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.loader_orders:
                final String selection;
                switch (mType) {
                    case OrderType.RECEIVED:
                        selection = ContentHelper.and(ContentHelper.eq(OrderContract.CHEF_ID, mProfileId),
                                ContentHelper.orderStatus(true));
                        break;

                    case OrderType.PLACED:
                        selection = ContentHelper.and(ContentHelper.eq(OrderContract.CLIENT_ID, mProfileId),
                                ContentHelper.orderStatus(true));
                        break;

                    case OrderType.COMPLETED:
                        selection = ContentHelper.orderStatus(false);
                        break;

                    default:
                        throw new IllegalStateException();

                }
                return new CursorLoader(getActivity(), OrderContract.CONTENT_URI, null, selection, null, null);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case R.id.loader_orders:
                mAdapter.clearAllBlocked();
                mAdapter.swapCursor(data, true);
                break;

            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onRefresh() {
        RequestCenter.place(new OrdersRequest(mType != OrderType.COMPLETED), this);
    }

    private void showConfirmDialog(@NonNull final Order order, @OrderStatus final int newStatus, @StringRes int formatMessage) {
        final OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAdapter.blockOrder(order, true);

                setBlock(true);
                RequestCenter.place(new OrderStatusRequest(order.getId(), newStatus), ListOrdersFragment.this);
            }
        };
        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme_AlertDialogLight))
                .setTitle(R.string.Confirm_Action)
                .setMessage(getString(formatMessage, order.getId()))
                .setPositiveButton(R.string.Confirm, listener)
                .setNegativeButton(R.string.Cancel, null)
                .show();
    }

    private final View.OnClickListener mGoDishesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startFragment(ListDishesFragment.newInstance(), false);
            SideMenuFragment.notifyMenuAboutSelection(R.id.nav_menu);
        }
    };
}