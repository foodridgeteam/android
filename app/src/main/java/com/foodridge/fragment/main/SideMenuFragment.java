package com.foodridge.fragment.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.IdRes;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.foodridge.R;
import com.foodridge.activity.LoginActivity;
import com.foodridge.activity.MainActivity;
import com.foodridge.api.request.AgreeTermsRequest;
import com.foodridge.api.request.LogoutRequest;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.dialog.TermsPolicyDialog;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.model.Profile;
import com.foodridge.provider.FoodridgeMetaData.MessageContract;
import com.foodridge.provider.FoodridgeMetaData.OrderContract;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;
import com.foodridge.provider.FoodridgeMetaData.RequestEntityContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.service.GCMInstanceService;
import com.foodridge.utility.Log;
import com.foodridge.utility.OrderMessageCache;
import com.foodridge.utility.Utils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class SideMenuFragment extends BaseMainFragment
        implements OnNavigationItemSelectedListener, LoaderCallbacks<Cursor> {

    private static final String ACTION_2 = "ACTION_2";
    private static final String EXTRA_1 = "EXTRA_1";//mLastItemId
    private static final String EXTRA_20 = "EXTRA_20";//menuItemId

    //VIEW's
    private Menu mMenu;
    private ImageView mIvAvatar;
    private TextView mTvUsername;
    private TextView mTvEmail;

    private TermsPolicyDialog mTermsPolicyDialog;
    private ProgressDialog mProgressDialog;

    //VALUE's
    private RequestManager mGlide;
    private int mLastItemId;
    private boolean mLogoutOnCallback;

    @NonNull
    public static SideMenuFragment newInstance() {
        return new SideMenuFragment();
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        mGlide = Glide.with(this);

        mProgressDialog = new ProgressDialog(new ContextThemeWrapper(getActivity(), R.style.AppTheme_AlertDialogLight));
        mProgressDialog.setMessage(getString(R.string.Logout_in_progress));
        mProgressDialog.setCancelable(false);

        getLoaderManager().initLoader(0, null, this);

        if (b != null) {
            mLastItemId = b.getInt(EXTRA_1);
        }
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_side_menu, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        final NavigationView navigation = (NavigationView) v.findViewById(R.id.navigation_view);
        final View header = navigation.inflateHeaderView(R.layout.view_menu_header);

        mMenu = navigation.getMenu();

        mIvAvatar = (ImageView) header.findViewById(R.id.iv_profile_image);
        mTvUsername = (TextView) header.findViewById(R.id.tv_username);
        mTvEmail = (TextView) header.findViewById(R.id.tv_user_email);

        final NavigationMenuView menuView = (NavigationMenuView) navigation.getChildAt(0);
        if (menuView != null) {
            menuView.setVerticalScrollBarEnabled(false);
        }
        header.setOnClickListener(mHeaderClickListener);
        navigation.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        AContext.getLocalBroadcastManager().registerReceiver(mReceiver, new IntentFilter(ACTION_2));
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.getLocalBroadcastManager().unregisterReceiver(mReceiver);
    }

    @Override
    protected boolean supportToolbarSubtitleChange() {
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return onNavigationItemSelected(item, null);
    }

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case RequestCenter.CODE_ERROR:
                final int receiverCode = (int) objects[0];
                switch (receiverCode) {
                    case R.id.code_terms_policy:
                        showTermsDialog();
                        break;

                    case R.id.code_logout:
                        final Activity activity = getActivity();
                        if (activity == null) {
                            return;
                        }
                        if (Profile.isAuthorized()) {
                            logoutAnyway(activity, false);

                        } else {
                            logout(activity);
                        }
                        break;

                    default:
                        break;
                }
                break;

            case TermsPolicyDialog.CODE:
                final boolean accept = (boolean) objects[0];
                setBlock(true);
                if (accept) {
                    RequestCenter.place(new AgreeTermsRequest(), this);

                } else {
                    if (AContext.isNetworkConnected()) {
                        RequestCenter.place(new LogoutRequest(), this);

                    } else {
                        logout(getActivity());
                    }
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    private void logoutAnyway(@NonNull final Activity activity, boolean noInternet) {
        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout(activity);
            }
        };
        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme_AlertDialogLight))
                .setTitle(noInternet ? R.string.error_Internet_unavailable : R.string.Something_went_wrong)
                .setMessage(R.string.Make_log_out_anyway)
                .setPositiveButton(R.string.Yes, listener)
                .setNegativeButton(R.string.Cancel, null)
                .show();
    }

    private final OnClickListener mHeaderClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            onNavigationItemSelected(R.id.nav_profile, null);
        }
    };
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final int itemId = intent.getIntExtra(EXTRA_20, mLastItemId);
            final MenuItem item = mMenu == null ? null : mMenu.findItem(itemId);
            if (item != null) {
                mLastItemId = itemId;
                item.setChecked(true);
            }
        }
    };

    private void onNavigationItemSelected(@IdRes int itemId, @Nullable BaseFragment exactFragment) {
        final MenuItem item = mMenu == null ? null : mMenu.findItem(itemId);
        if (item != null) {
            item.setChecked(true);
            onNavigationItemSelected(item, exactFragment);
        }
    }

    private boolean onNavigationItemSelected(@NonNull MenuItem item, @Nullable BaseFragment exactFragment) {
        final Activity activity = getActivity();
        if (!(activity instanceof MainActivity)) {
            return false;
        }
        final MainActivity mainActivity = (MainActivity) activity;
        mainActivity.closeMenu();

        final int itemId = item.getItemId();
        if (mLastItemId == itemId) {
            return false;
        }
        switch (itemId) {
//            case R.id.nav_feedback:
//                final Intent intent = new Intent(Intent.ACTION_SENDTO)
//                        .setType(SharedStrings.MIME_PLAIN)
//                        .setData(Uri.parse("mailto:info@foodridge.com"))
//                        .putExtra(Intent.EXTRA_SUBJECT, getString(R.string.Feedback))
//                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//                if (!Utils.startIntent(getContext(), intent)) {
//                    showSnackBar(R.string.cant_handle_intent);
//                }
//                return false;

            case R.id.nav_log_out:
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme_AlertDialogLight))
                        .setTitle(R.string.Confirm_Action)
                        .setMessage(R.string.Log_out)
                        .setPositiveButton(R.string.Yes, mLogoutClickListener)
                        .setNegativeButton(R.string.Cancel, null)
                        .show();
                return false;

            default:
                if (exactFragment == null) {
                    switch (itemId) {
                        case R.id.nav_menu:
                            exactFragment = ListDishesFragment.newInstance();
                            break;

                        case R.id.nav_chefs:
                            exactFragment = ListChefsFragment.newInstance();
                            break;

                        case R.id.nav_orders:
                            exactFragment = OrdersFragment.newInstance();
                            break;

                        case R.id.nav_profile:
                            exactFragment = ProfileFragment.newInstance();
                            break;

                        case R.id.nav_my_menu:
                            exactFragment = MyDishesFragment.newInstance();
                            break;

                        case R.id.nav_about:
                            exactFragment = AboutFragment.newInstance();
                            break;

                        case R.id.nav_feedback:
                            exactFragment = FeedbackFragment.newInstance();
                            break;

                        case R.id.nav_terms:
                            exactFragment = TermsFragment.newInstance();
                            break;

                        default:
                            throw new IllegalStateException();
                    }
                }
                mLastItemId = itemId;
                mainActivity.startFragment(exactFragment, false);
                return true;

        }
    }

    private final DialogInterface.OnClickListener mLogoutClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (AContext.isNetworkConnected()) {
                setBlock(true);
                RequestCenter.place(new LogoutRequest(), SideMenuFragment.this);

            } else {
                final Activity activity = getActivity();
                if (activity != null) {
                    logoutAnyway(activity, true);
                }
            }
        }
    };

    @Override
    public void setBlock(boolean block) {
        super.setBlock(block);

        if (block) {
            mProgressDialog.show();

        } else {
            mProgressDialog.dismiss();
        }
    }

    private void showTermsDialog() {
        if (mTermsPolicyDialog == null) {
            mTermsPolicyDialog = new TermsPolicyDialog(getActivity(), this);
        }
        if (!mTermsPolicyDialog.isShowing()) {
            mTermsPolicyDialog.show();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ProfileContract.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        final Profile profile = Profile.getInstance();

        if (profile.isMakeLogout()) {
            if (!mLogoutOnCallback) {
                mLogoutOnCallback = true;
                logout(getActivity());
            }

        } else if (!profile.isAgreeTerms()) {
            showTermsDialog();

        } else {
            mTvUsername.setText(profile.getName());
            mTvEmail.setText(profile.getEmail());
            Utils.loadInto(mGlide, profile.getAvatar(), null, mIvAvatar);

            final MenuItem item = mMenu.findItem(R.id.nav_my_menu);
            if (item != null) {
                item.setVisible(profile.isChef());
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public static void notifyMenuAboutSelection(@IdRes int id) {
        final Intent intent = new Intent(ACTION_2).putExtra(EXTRA_20, id);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AContext.sendLocalBroadcast(intent);
            }
        }, 1_000);
    }

    @MainThread
    public static void logout(@Nullable Activity activity) {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Profile.getInstance().logout();
                ASettings.logout();
                AContext.getContentResolver().delete(RequestEntityContract.CONTENT_URI, null, null);
                AContext.getContentResolver().delete(MessageContract.CONTENT_URI, null, null);
                AContext.getContentResolver().delete(OrderContract.CONTENT_URI, null, null);
                OrderMessageCache.getInstance().clear();

                final Context context = AContext.getApp();
                try {
                    InstanceID.getInstance(context).deleteToken(AContext.getString(R.string.google_cloud_message_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE);

                } catch (Exception e) {
                    Log.LOG.toLog(e);
                }
                context.startService(new Intent(context, GCMInstanceService.class));
            }
        });
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();

        if (activity instanceof LoginActivity) {
            activity.recreate();

        } else if (activity != null) {
            LoginActivity.start(activity);
            activity.finish();

        } else {
            Process.killProcess(Process.myPid());
        }
    }
}