package com.foodridge.fragment.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.activity.BaseActivity;
import com.foodridge.adapter.BaseOffsetCursorAdapter;
import com.foodridge.adapter.ChefDishesOffsetAdapter;
import com.foodridge.api.model.Chef;
import com.foodridge.api.model.Meal;
import com.foodridge.api.model.WeekWorkingTime;
import com.foodridge.api.request.FollowRequest;
import com.foodridge.api.request.MealsRequest;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.model.DbChef;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.ChefContract;
import com.foodridge.provider.FoodridgeMetaData.MealContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.LimitOffsetHelper;
import com.foodridge.utility.LimitOffsetHelper.OnLimitOffsetCallback;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;
import com.foodridge.view.BusinessDetailsView;
import com.foodridge.view.DeliveryAddressView;
import com.foodridge.view.FollowersFollowView;
import com.foodridge.view.ProgressImageView;
import com.foodridge.view.RatingView;
import com.foodridge.view.WeekWorkingTimeView;
import com.foodridge.view.item.ChefDishItemView;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 13.04.16.
 */
public class ChefFragment extends BaseMainFragment
        implements LoaderCallbacks<Cursor>, OnLimitOffsetCallback {

    private static final String EXTRA_1 = "EXTRA_1";//mChef

    // VIEW
    private View mHeaderView;
    private ListView mListView;

    private ProgressImageView mIvChefAvatar;
    private FollowersFollowView mFollowersView;

    private TextView mTvCuisines;
    private TextView mTvReviews;
    private RatingView mViewRating;

    private TextView mTvBio;
    private DeliveryAddressView mAddressView;

//    private CompoundButton mCbPhoto;
//    private CompoundButton mCbLocation;
//    private CompoundButton mCbDelivery;
//    private CompoundButton mCbMail;
//    private CompoundButton mCbPhone;
//    private CompoundButton mCbPayment;

    private BusinessDetailsView mBusinessView;

    private View mTvWorkingTime;
    private WeekWorkingTimeView mWorkingTimeView;

    private View mTvCheMenu;

    //VALUE's
    private Chef mChef;
    private String mChefSearch;
    private LimitOffsetHelper mHelper;

    // ADAPTER
    private ChefDishesOffsetAdapter mAdapter;

    @NonNull
    public static ChefFragment newInstance(@NonNull Chef chef) {
        final Bundle b = new Bundle(1);
        b.putParcelable(EXTRA_1, chef);

        final ChefFragment f = new ChefFragment();
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        final Bundle bundle = b != null ? b : getArguments();
        mChef = bundle.getParcelable(EXTRA_1);
        if (mChef == null) {
            throw new IllegalStateException();
        }
        mChefSearch = ChefContract.CONTENT_URI + "/" + mChef.getId();

        mAdapter = new ChefDishesOffsetAdapter(this);
        mHelper = new LimitOffsetHelper(this, R.id.code_meals);

        getLoaderManager().initLoader(R.id.loader_chef, null, this);
        getLoaderManager().initLoader(R.id.loader_meals, null, this);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        mHeaderView = inflater.inflate(R.layout.view_chef, null);
        return inflater.inflate(R.layout.fragment_refresh_list, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mListView = (ListView) v.findViewById(R.id.list_view);

        mIvChefAvatar = (ProgressImageView) mHeaderView.findViewById(R.id.iv_chef_avatar);
        mFollowersView = (FollowersFollowView) mHeaderView.findViewById(R.id.view_followers);

        mIvChefAvatar.getLayoutParams().height = (int) (AContext.getScreenSize().x / 1.333F);
        mIvChefAvatar.requestLayout();

        mTvCuisines = (TextView) mHeaderView.findViewById(R.id.tv_cuisines);
        mTvReviews = (TextView) mHeaderView.findViewById(R.id.tv_reviews);
        mViewRating = (RatingView) mHeaderView.findViewById(R.id.rating_chef);

        mTvBio = (TextView) mHeaderView.findViewById(R.id.tv_bio);
        mAddressView = (DeliveryAddressView) mHeaderView.findViewById(R.id.view_delivery_address);

//        mCbPhoto = (CompoundButton) mHeaderView.findViewById(R.id.cb_photo);
//        mCbLocation = (CompoundButton) mHeaderView.findViewById(R.id.cb_location);
//        mCbDelivery = (CompoundButton) mHeaderView.findViewById(R.id.iv_delivery);
//        mCbMail = (CompoundButton) mHeaderView.findViewById(R.id.cb_mail);
//        mCbPhone = (CompoundButton) mHeaderView.findViewById(R.id.cb_phone);
//        mCbPayment = (CompoundButton) mHeaderView.findViewById(R.id.cb_payment);

        mBusinessView = (BusinessDetailsView) mHeaderView.findViewById(R.id.view_business_details);

        mTvWorkingTime = mHeaderView.findViewById(R.id.tv_working_time);
        mWorkingTimeView = (WeekWorkingTimeView) mHeaderView.findViewById(R.id.view_working_time);

        mTvCheMenu = mHeaderView.findViewById(R.id.tv_chef_menu);

        mListView.setPadding(0, 0, 0, 0);
        mListView.addHeaderView(mHeaderView, null, false);
        mAdapter.setListView(mListView);

        mIvChefAvatar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.black));
        mIvChefAvatar.setPlaceholder(R.drawable.svg_logo_placeholder);

        mAddressView.setTitle(R.string.Address, ContextCompat.getColor(getActivity(), R.color.gray_hint));
        mBusinessView.setTitle(R.string.Business_details, ContextCompat.getColor(getActivity(), R.color.gray_hint));

        Utils.changeVisibility(mTvCheMenu, mAdapter.getCount() == 0 ? View.GONE : View.VISIBLE);

        updateData();

        mBusinessView.setOnClickListener(mOnClickListener);
        mFollowersView.setOnClickListener(mOnClickListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHelper.onDestroy();
    }

    @Nullable
    @Override
    protected String getToolbarTitle() {
        return mChef.getName();
    }

    @Nullable
    @Override
    protected String getToolbarSubTitle() {
        return TimeCache.getInstance().getVisitedAt(mChef.getLastVisit());
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();
        Utils.cleanUp(mListView);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putParcelable(EXTRA_1, mChef);
    }

    private void updateData() {
        mIvChefAvatar.show(mChef.getAvatar());
        mFollowersView.setData(mChef);

        mTvCuisines.setText(Utils.findAll(mChef.getCuisines(), ASettings.getInstance().getCuisines(), true));
        mTvReviews.setText(mChef.getVotesCount() <= 0 ? null : getResources().getQuantityString(R.plurals.format_Review, mChef.getVotesCount(), mChef.getVotesCount()));
        mViewRating.setRating(mChef.getRating());

        mTvBio.setText(mChef.getAbout());
        mAddressView.setData(mChef.getDeliveryInfo(), false);

//        mCbPhoto.setChecked(mChef.getAvatar() != null);
//        mCbLocation.setChecked(mChef.getDeliveryInfo() != null);
//        mCbDelivery.setChecked(mChef.getDeliveryInfo() != null);
//        mCbMail.setChecked(true);
//        mCbPhone.setChecked(mChef.getBusinessInfo() != null && mChef.getBusinessInfo().getPhone() != null);
//        mCbPayment.setChecked(true);

        mBusinessView.setData(mChef.getBusinessInfo(), false);

        final WeekWorkingTime workingTime = mChef.getWorkingTime();
        if (workingTime == null || workingTime.isAllEmpty()) {
            mTvWorkingTime.setVisibility(View.GONE);
            mWorkingTimeView.setVisibility(View.GONE);

        } else {
            mTvWorkingTime.setVisibility(View.VISIBLE);
            mWorkingTimeView.setVisibility(View.VISIBLE);

            mWorkingTimeView.setData(workingTime, false);
        }
        final Activity activity = getActivity();
        final ActionBar bar = activity instanceof BaseActivity ? ((BaseActivity) activity).getSupportActionBar() : null;
        if (bar != null) {
            onActionBarReady(bar);
        }
    }

    private final OnClickListener mOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_follow:
                    setBlock(true);
                    RequestCenter.place(new FollowRequest(mChef), ChefFragment.this);
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case ChefDishItemView.CODE:
                final boolean order = (boolean) objects[0];
                final Meal meal = (Meal) objects[1];
                if (order) {
                    startFragment(OrderFragment.newInstance(meal), true);

                } else {
                    startFragment(DishFragment.newInstance(this, meal), true);
                }
                break;

            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                mHelper.onRequestAnswer(objects);
                break;

            case BaseOffsetCursorAdapter.CODE:
                if (mHelper.loadMoreData()) {
                    setBlock(true);
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.loader_chef:
                return new CursorLoader(getActivity(), ChefContract.CONTENT_URI, null
                        , ContentHelper.and(ContentHelper.eq(ChefContract.SEARCH, DbChef.SEARCH_SELF), ContentHelper.eq(ChefContract.ENTITY_ID, mChef.getId())), null, null);

            case R.id.loader_meals:
                return new CursorLoader(getActivity(), MealContract.CONTENT_URI, null
                        , ContentHelper.and(ContentHelper.eq(MealContract.SEARCH, null), ContentHelper.eq(MealContract.CHEF_ID, mChef.getId())), null, null);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case R.id.loader_meals:
                mAdapter.swapCursor(data);
                Utils.changeVisibility(mTvCheMenu, mAdapter.getCount() == 0 ? View.GONE : View.VISIBLE);
                break;

            case R.id.loader_chef:
                if (mFollowersView != null && data.moveToFirst()) {
                    final Chef chef = new DbChef(data).getEntity();
                    mChef = chef;

                    mFollowersView.setData(chef);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void placeRequest(int offset) {
        RequestCenter.place(new MealsRequest(offset, mChef.getId(), mChefSearch), this);
    }

    @Override
    public void setOffset(int offset) {
        mAdapter.setOffset(offset);
        if (mTvCheMenu != null) {
            Utils.changeVisibility(mTvCheMenu, mAdapter.getCount() == 0 ? View.GONE : View.VISIBLE);
        }
    }
}