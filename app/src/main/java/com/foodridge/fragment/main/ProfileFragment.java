package com.foodridge.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodridge.R;
import com.foodridge.adapter.ProfilePagerAdapter;
import com.foodridge.utility.Utils;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 07.04.16
 */
public class ProfileFragment extends BaseMainFragment {

    //VIEW's
    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    //ADAPTER
    private ProfilePagerAdapter mAdapter;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        mAdapter = new ProfilePagerAdapter(getChildFragmentManager());
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mTabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) v.findViewById(R.id.view_pager);

        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.Profile;
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();

        Utils.cleanUp(mTabLayout);
        Utils.cleanUp(mViewPager);
    }
}