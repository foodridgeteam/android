package com.foodridge.fragment.main;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.foodridge.R;
import com.foodridge.adapter.BaseOffsetCursorAdapter;
import com.foodridge.api.request.BaseRequestEntity;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.OnBackPressedListener;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.SearchColumns;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.LimitOffsetHelper;
import com.foodridge.utility.LimitOffsetHelper.OnLimitOffsetCallback;
import com.foodridge.utility.LimitOffsetHelper.OnSearchCallback;
import com.foodridge.utility.Utils;
import com.foodridge.view.ListEmptyView;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
abstract class BaseMainSearchFragment extends BaseMainFragment
        implements OnRefreshListener, LoaderCallbacks<Cursor>, OnLimitOffsetCallback
        , OnBackPressedListener, OnSearchCallback {

    private static final String EXTRA_01 = "EXTRA_01";//mSearch

    //VIEW's
    private ListView mListView;
    private SearchView mSearchView;

    //VALUE's
    private LimitOffsetHelper mHelper;
    private String mSearch;

    //ADAPTER
    private BaseOffsetCursorAdapter mAdapter;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        super.setHasOptionsMenu(true);

        if (b != null) {
            mSearch = b.getString(EXTRA_01);
        }
        mAdapter = newAdapter();
        mHelper = new LimitOffsetHelper(this, getReceiverCode());
        mHelper.setSearchCallback(this);

        getLoaderManager().initLoader(mSearch == null ? getLoaderId() : R.id.loader_search, null, this);
    }

    @Nullable
    @Override
    protected final View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_refresh_list_with_empty, container, false);
    }

    @CallSuper
    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mListView = (ListView) v.findViewById(R.id.list_view);
        mListView.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.spacing));

        final ListEmptyView emptyView = ListEmptyView.attach((ViewGroup) v.findViewById(R.id.frame_layout), mListView);
        onPrepareEmptyView(emptyView);

        mAdapter.setListView(mListView);
    }

    protected abstract void onPrepareEmptyView(@NonNull ListEmptyView view);

    @CallSuper
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        if (item != null) {
            mSearchView = (SearchView) item.getActionView();
            if (mSearchView != null) {
                mSearchView.setOnQueryTextListener(mQueryTextListener);
                mSearchView.setOnCloseListener(mCloseListener);

                if (mSearch != null) {
                    mSearchView.setIconified(false);
                    mSearchView.setQuery(mSearch, false);
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mListView);
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();
        Utils.cleanUp(mListView);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putString(EXTRA_01, mSearch);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHelper.onDestroy();
    }

    @NonNull
    protected abstract BaseOffsetCursorAdapter newAdapter();

    @NonNull
    protected abstract Uri getLoaderContentUri();

    @IdRes
    protected abstract int getReceiverCode();

    @IdRes
    protected abstract int getLoaderId();

    @NonNull
    protected abstract BaseRequestEntity newRequest(int offset, int limit, @Nullable String search);

    @CallSuper
    @Override
    public final void onRefresh() {
        mHelper.onRefreshAll();
        RequestCenter.place(newRequest(0, mAdapter.getCount(), mSearch), this);
    }

    @CallSuper
    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                mHelper.onRequestAnswer(objects);
                break;

            case BaseOffsetCursorAdapter.CODE:
                if (mHelper.loadMoreData()) {
                    setBlock(true);
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @CallSuper
    @Override
    public boolean onBackPress() {
        if (mSearchView.isIconified()) {
            return true;
        }
        mSearchView.setIconified(true);
        return false;
    }

    private final OnQueryTextListener mQueryTextListener = new OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
//            makeSearch(query.trim());
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            mHelper.startSearch(newText.trim());
            return false;
        }
    };

    private final OnCloseListener mCloseListener = new OnCloseListener() {
        @Override
        public boolean onClose() {
            makeSearch(null);
            return false;
        }
    };

    @Override
    public void makeSearch(@Nullable String search) {
        if (Utils.areEquals(mSearch, search)) {
            return;
        }
        search = Utils.checkEmpty(search);
        mSearch = search;

        setBlock(true);
        mHelper.reset();

        final LoaderManager manager = getLoaderManager();
        if (search == null) {
            manager.destroyLoader(R.id.loader_search);
            manager.restartLoader(getLoaderId(), null, this);

        } else {
            manager.destroyLoader(getLoaderId());
            manager.restartLoader(R.id.loader_search, null, this);
        }
    }

    @CallSuper
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == R.id.loader_search) {
            return new CursorLoader(getActivity(), getLoaderContentUri(), null, ContentHelper.eq(SearchColumns.SEARCH, Utils.toLowerCase(mSearch)), null, null);

        } else if (id == getLoaderId()) {
            return new CursorLoader(getActivity(), getLoaderContentUri(), null, ContentHelper.eq(SearchColumns.SEARCH, null), null, null);
        }
        return null;
    }

    @CallSuper
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        final int id = loader.getId();
        if (id == getLoaderId() || id == R.id.loader_search) {
            mAdapter.swapCursor(data);
        }
    }

    @CallSuper
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public final void setOffset(int offset) {
        mAdapter.setOffset(offset);
    }

    @Override
    public final void placeRequest(int offset) {
        RequestCenter.place(newRequest(offset, AConstant.LIMIT, mSearch), this);
    }
}