package com.foodridge.fragment.main;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;

import com.foodridge.R;
import com.foodridge.adapter.BaseOffsetCursorAdapter;
import com.foodridge.adapter.ChefsOffsetAdapter;
import com.foodridge.api.model.Chef;
import com.foodridge.api.request.BaseRequestEntity;
import com.foodridge.api.request.ChefsRequest;
import com.foodridge.api.request.FollowRequest;
import com.foodridge.provider.FoodridgeMetaData.ChefContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.SharedStrings;
import com.foodridge.utility.Utils;
import com.foodridge.view.ListEmptyView;
import com.foodridge.view.item.ChefItemView;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 07.04.16.
 */
public class ListChefsFragment extends BaseMainSearchFragment {

    @NonNull
    public static ListChefsFragment newInstance() {
        return new ListChefsFragment();
    }

    @Override
    protected void onPrepareEmptyView(@NonNull ListEmptyView view) {
        view.setData(R.drawable.ic_no_food, R.string.empty_search_dish, R.string.Invite, mInviteClickListener);
    }

    @NonNull
    @Override
    protected BaseOffsetCursorAdapter newAdapter() {
        return new ChefsOffsetAdapter(this);
    }

    @NonNull
    @Override
    protected Uri getLoaderContentUri() {
        return ChefContract.CONTENT_URI;
    }

    @Override
    protected int getReceiverCode() {
        return R.id.code_chefs;
    }

    @Override
    protected int getLoaderId() {
        return R.id.loader_chefs;
    }

    @NonNull
    @Override
    protected BaseRequestEntity newRequest(int offset, int limit, @Nullable String search) {
        return new ChefsRequest(offset, limit, search);
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.Chefs;
    }

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case ChefItemView.CODE:
                final boolean follow = (boolean) objects[0];
                final Chef chef = (Chef) objects[1];

                if (follow) {
                    setBlock(true);
                    RequestCenter.place(new FollowRequest(chef), ListChefsFragment.this);

                } else {
                    startFragment(ChefFragment.newInstance(chef), true);
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
        }
    }

    private final OnClickListener mInviteClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final Intent intent = new Intent(Intent.ACTION_SEND)
                    .setType(SharedStrings.MIME_PLAIN)
                    .putExtra(Intent.EXTRA_TEXT, getString(R.string.Invite_user));

            Utils.startIntent(getActivity(), intent);
        }
    };
}