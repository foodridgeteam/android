package com.foodridge.fragment.main;

import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.foodridge.R;
import com.foodridge.adapter.BaseOffsetCursorAdapter;
import com.foodridge.adapter.DishesOffsetAdapter;
import com.foodridge.api.model.Meal;
import com.foodridge.api.request.BaseRequestEntity;
import com.foodridge.api.request.MealsRequest;
import com.foodridge.provider.FoodridgeMetaData.MealContract;
import com.foodridge.view.ListEmptyView;
import com.foodridge.view.item.DishItemView;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class ListDishesFragment extends BaseMainSearchFragment {

    @NonNull
    public static ListDishesFragment newInstance() {
        return new ListDishesFragment();
    }

    @Override
    protected void onPrepareEmptyView(@NonNull ListEmptyView view) {
        view.setData(R.drawable.ic_no_chef, R.string.empty_search_chef, 0, null);
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.Dishes;
    }

    @NonNull
    @Override
    protected BaseOffsetCursorAdapter newAdapter() {
        return new DishesOffsetAdapter(this);
    }

    @NonNull
    @Override
    protected Uri getLoaderContentUri() {
        return MealContract.CONTENT_URI;
    }

    @Override
    protected int getReceiverCode() {
        return R.id.code_meals;
    }

    @Override
    protected int getLoaderId() {
        return R.id.loader_meals;
    }

    @NonNull
    @Override
    protected BaseRequestEntity newRequest(int offset, int limit, @Nullable String search) {
        return new MealsRequest(offset, limit, null, search);
    }

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case DishItemView.CODE:
                final Boolean order = (Boolean) objects[0];
                final Meal meal = (Meal) objects[1];
                if (order == null) {
                    startFragment(DishFragment.newInstance(null, meal), true);

                } else if (order) {
                    startFragment(OrderFragment.newInstance(meal), true);

                } else {
                    startFragment(ChefFragment.newInstance(meal.getChef()), true);
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }
}