package com.foodridge.fragment.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.activity.StripeActivity;
import com.foodridge.api.model.Meal;
import com.foodridge.api.request.InsertOrderRequest;
import com.foodridge.dialog.QuantityDialog;
import com.foodridge.model.Profile;
import com.foodridge.request.RequestCenter;
import com.foodridge.type.DeliveryType;
import com.foodridge.type.OrderType;
import com.foodridge.utility.Utils;
import com.foodridge.view.DeliveryAddressView;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 20/04/16
 */
public class OrderFragment extends BaseMainFragment {

    private static final int REQUEST_STRIPE_TOKEN = 0x1;

    private static final String EXTRA_0 = "EXTRA_0";//mMeal
    private static final String EXTRA_1 = "EXTRA_1";//mDelivery
    private static final String EXTRA_2 = "EXTRA_2";//mQuantity

    //VIEW's
    private TextView mTvDelivery;
    private TextView mTvPickup;

    private DeliveryAddressView mAddressView;
    private TextView mTvCounter;

    private TextView mTvNameCounter;
    private TextView mTvPriceMeal;
    private TextView mTvPriceDelivery;
    private TextView mTvPriceTotal;

    private View mBtnOrder;

    private QuantityDialog mDialog;

    //VALUE's
    private Meal mMeal;
    private boolean mDelivery;
    private int mQuantity;

    private int mMaxAvailable;
    private boolean mPickupAvailable;
    private boolean mDeliveryAvailable;

    private float mTotal;

    @NonNull
    public static OrderFragment newInstance(@NonNull Meal meal) {
        final Bundle b = new Bundle(1);
        b.putParcelable(EXTRA_0, meal);

        final OrderFragment f = new OrderFragment();
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        final Bundle bundle = b != null ? b : getArguments();
        mMeal = bundle.getParcelable(EXTRA_0);

        if (mMeal == null) {
            throw new IllegalStateException();
        }
        mMaxAvailable = mMeal.getPortionsAvailable();
        mPickupAvailable = false;
        mDeliveryAvailable = false;
        switch (mMeal.getDeliveryType()) {
            case DeliveryType.PICKUP_AND_DELIVERY:
                mPickupAvailable = true;
                mDeliveryAvailable = true;
                break;

            case DeliveryType.DELIVERY:
                mPickupAvailable = false;
                mDeliveryAvailable = true;
                break;

            default:
            case DeliveryType.PICKUP:
                mPickupAvailable = true;
                mDeliveryAvailable = false;
                break;
        }
        mDelivery = bundle.getBoolean(EXTRA_1, mPickupAvailable && mDeliveryAvailable || mDeliveryAvailable);
        mQuantity = bundle.getInt(EXTRA_2, 1);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        final TextView tvName = (TextView) v.findViewById(R.id.tv_name);
        final TextView tvPrice = (TextView) v.findViewById(R.id.tv_price);

        mTvDelivery = (TextView) v.findViewById(R.id.tv_delivery);
        mTvPickup = (TextView) v.findViewById(R.id.tv_pick_up);

        mAddressView = (DeliveryAddressView) v.findViewById(R.id.view_delivery_address);

        final View tvMinus = v.findViewById(R.id.tv_minus);
        final View tvPlus = v.findViewById(R.id.tv_plus);
        mTvCounter = (TextView) v.findViewById(R.id.tv_counter);

        mTvNameCounter = (TextView) v.findViewById(R.id.tv_name_with_counter);
        mTvPriceMeal = (TextView) v.findViewById(R.id.tv_price_meal);
        mTvPriceDelivery = (TextView) v.findViewById(R.id.tv_price_delivery);
        mTvPriceTotal = (TextView) v.findViewById(R.id.tv_price_total);

        mBtnOrder = v.findViewById(R.id.btn_order);

        tvName.setText(mMeal.getName());
        tvPrice.setText(getString(R.string.format_dollar_2f, mMeal.getPortionPrice()));

        mTvPickup.setEnabled(mPickupAvailable);
        mTvDelivery.setEnabled(mDeliveryAvailable);
        mTvPickup.setActivated(!mDelivery);
        mTvDelivery.setActivated(mDelivery);

        tvMinus.setActivated(true);
        tvPlus.setActivated(true);

        mAddressView.setPadding(0, 0, 0, 0);
        mAddressView.setData(mMeal.getChef().getDeliveryInfo(), false);
        mAddressView.setTitle(R.string.Pickup_Address, ContextCompat.getColor(getActivity(), R.color.gray));

        updateTotal();

        mTvDelivery.setOnClickListener(mClickListener);
        mTvPickup.setOnClickListener(mClickListener);

        mTvCounter.setOnClickListener(mClickListener);
        tvMinus.setOnClickListener(mClickListener);
        tvPlus.setOnClickListener(mClickListener);

        mBtnOrder.setOnClickListener(mClickListener);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);

        b.putParcelable(EXTRA_0, mMeal);
        b.putBoolean(EXTRA_1, mDelivery);
        b.putInt(EXTRA_2, mQuantity);
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.Order;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        switch (requestCode) {
            case REQUEST_STRIPE_TOKEN:
                if (resultCode == StripeActivity.RESULT_OK) {
                    setBlock(true);
                    RequestCenter.place(new InsertOrderRequest(mMeal.getId(), StripeActivity.getToken(data), mQuantity
                            , mDelivery ? DeliveryType.DELIVERY : DeliveryType.PICKUP, mTotal), OrderFragment.this);
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void updateTotal() {
        final float single = mMeal.getPortionPrice();
        final float delivery = mMeal.getDeliveryPrice();
        final float total = mTotal = mQuantity * single + (mDelivery ? delivery : 0);

        if (Utils.changeVisibility(mAddressView, mDelivery ? View.GONE : View.VISIBLE) && !mDelivery) {
            mAddressView.setAlpha(0F);
            mAddressView.animate().alpha(1F).start();
        }
        mTvCounter.setText(String.valueOf(mQuantity));
        mTvNameCounter.setText(Html.fromHtml(getString(R.string.format_html_s_xd, mMeal.getName(), mQuantity)));
        mTvPriceMeal.setText(getString(R.string.format_dollar_2f, mQuantity * single));
        mTvPriceDelivery.setText(getString(R.string.format_dollar_2f, mDelivery ? delivery : 0));
        mTvPriceTotal.setText(getString(R.string.format_dollar_2f, total));
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_delivery:
                    mDelivery = true;

                    mTvDelivery.setActivated(true);
                    mTvPickup.setActivated(false);

                    updateTotal();
                    break;

                case R.id.tv_pick_up:
                    mDelivery = false;

                    mTvPickup.setActivated(true);
                    mTvDelivery.setActivated(false);

                    updateTotal();
                    break;

                case R.id.tv_minus:
                    if (mQuantity > 1) {
                        mQuantity--;
                        updateTotal();
                    }
                    break;

                case R.id.tv_counter:
                    if (mDialog == null) {
                        mDialog = new QuantityDialog(getActivity(), OrderFragment.this);
                    }
                    mDialog.show(mQuantity, mMaxAvailable);
                    break;

                case R.id.tv_plus:
                    if (mQuantity < mMaxAvailable) {
                        mQuantity++;
                        updateTotal();
                    }
                    break;

                case R.id.btn_order:
                    final Profile profile = Profile.getInstance();
                    if (profile.getPhone() == null) {
                        showSnackBar(R.string.Your_phone_number_required);

                    } else if (profile.getDeliveryInfo() == null && mDelivery) {
                        showSnackBar(R.string.Your_delivery_address_required);

                    } else {
                        StripeActivity.start(OrderFragment.this, REQUEST_STRIPE_TOKEN, mTotal);
                    }
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case QuantityDialog.CODE:
                mQuantity = (int) objects[0];
                updateTotal();
                break;

            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                mBtnOrder.setEnabled(false);

                mBtnOrder.post(new Runnable() {
                    @Override
                    public void run() {
                        final View view = getView();
                        if (view != null) {
                            startFragment(OrdersFragment.newInstance(OrderType.PLACED), false);
                            SideMenuFragment.notifyMenuAboutSelection(R.id.nav_orders);
                        }
                    }
                });
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    @Override
    public void setBlock(boolean block) {
        super.setBlock(block);
        mBtnOrder.setEnabled(!block);
    }
}