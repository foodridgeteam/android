package com.foodridge.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodridge.R;
import com.foodridge.adapter.OrdersPagerAdapter;
import com.foodridge.api.request.OrdersRequest;
import com.foodridge.application.AContext;
import com.foodridge.model.Profile;
import com.foodridge.request.RequestCenter;
import com.foodridge.type.OrderType;
import com.foodridge.utility.Utils;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 07.04.16.
 */
public class OrdersFragment extends BaseMainFragment {

    private static final String EXTRA_1 = "EXTRA_1";

    //VIEW's
    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    //ADAPTER
    private OrdersPagerAdapter mAdapter;

    @NonNull
    public static OrdersFragment newInstance() {
        return newInstance(Profile.getInstance().isChef() ? OrderType.RECEIVED : OrderType.PLACED);
    }

    @NonNull
    public static OrdersFragment newInstance(@OrderType int selectedPage) {
        final Bundle b = new Bundle(1);
        b.putInt(EXTRA_1, selectedPage);

        final OrdersFragment f = new OrdersFragment();
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        mAdapter = new OrdersPagerAdapter(getChildFragmentManager());
        if (AContext.isNetworkConnected()) {
            RequestCenter.place(new OrdersRequest(null), null);
        }
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_refresh_pages, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mTabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) v.findViewById(R.id.view_pager);

        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        if (b == null) {
            @OrderType final int orderType = getArguments().getInt(EXTRA_1);
            final boolean isChef = Profile.getInstance().isChef();
            mViewPager.setOffscreenPageLimit(isChef ? 3 : 2);

            final int currentItem;
            switch (orderType) {
                case OrderType.COMPLETED:
                    currentItem = isChef ? 2 : 1;
                    break;

                case OrderType.PLACED:
                    currentItem = isChef ? 1 : 0;
                    break;

                case OrderType.RECEIVED:
                default:
                    currentItem = 0;
                    break;
            }
            mViewPager.setCurrentItem(currentItem);
        }
    }

    @Override
    protected int getToolbarTitleRes() {
        return R.string.Orders;
    }

    @Override
    public void onCleanUp() throws Exception {
        super.onCleanUp();

        Utils.cleanUp(mTabLayout);
        Utils.cleanUp(mViewPager);
    }
}