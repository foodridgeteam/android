package com.foodridge.fragment.login;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;

import com.foodridge.activity.LoginActivity;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.utility.BaseUtils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 06/04/16
 */
abstract class BaseLoginFragment extends BaseFragment {

    @CallSuper
    @Override
    public void setBlock(boolean block) {
        final Activity activity = getActivity();
        if (activity instanceof LoginActivity) {
            ((LoginActivity) activity).setBlock(block);
        }
    }

    @Override
    public boolean startFragment(@NonNull BaseFragment fragment, boolean addToBackStack, @NonNull Object... objects) {
        final String tag = BaseUtils.getTag(fragment);
        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        transaction.replace(getFragmentContainerId(), fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();

        return true;
    }
}