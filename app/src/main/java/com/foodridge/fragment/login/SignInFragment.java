package com.foodridge.fragment.login;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.foodridge.R;
import com.foodridge.activity.MainActivity;
import com.foodridge.api.request.LoginRequest;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.dialog.ForgotPasswordDialog;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 06/04/16
 */
public class SignInFragment extends BaseLoginFragment {

    //VIEW's
    private EditText mEtEmail;
    private EditText mEtPassword;
    private View mBtnSignIn;
    private View mBtnForgotPassword;
    private ImageView mIvPassword;

    private ForgotPasswordDialog mDialog;

    @NonNull
    public static SignInFragment newInstance() {
        return new SignInFragment();
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        mDialog = new ForgotPasswordDialog(getActivity(), this);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mEtEmail = (EditText) v.findViewById(R.id.et_email);
        mEtPassword = (EditText) v.findViewById(R.id.et_password);
        mBtnSignIn = v.findViewById(R.id.btn_sign_in);
        mBtnForgotPassword = v.findViewById(R.id.btn_forgot_password);
        mIvPassword = (ImageView) v.findViewById(R.id.iv_password);

        mBtnSignIn.setOnClickListener(mClickListener);
        mBtnForgotPassword.setOnClickListener(mClickListener);
        mIvPassword.setOnTouchListener(mPasswordTouchListener);

        if (AConstant.DEBUG) {
            mEtEmail.setText(AConstant.DEFAULT_EMAIL);
            mEtPassword.setText(AConstant.DEFAULT_PASSWORD);
        }
        AContext.showInputKeyboard(mEtEmail);
    }

    @Override
    protected void onActionBarReady(@NonNull ActionBar bar) {
        bar.show();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setDefaultDisplayHomeAsUpEnabled(true);

        bar.setTitle(R.string.empty);
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mEtEmail);
    }

    @Override
    public void onCleanUp() throws Exception {
        Utils.cleanUp(mEtEmail);
        Utils.cleanUp(mEtPassword);
        Utils.cleanUp(mBtnSignIn);
        Utils.cleanUp(mBtnForgotPassword);
        Utils.cleanUp(mIvPassword);
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final String email = mEtEmail.getText().toString().trim();
            if (v.getId() == R.id.btn_forgot_password) {
                mDialog.show(email);

            } else {
                if (email.length() == 0 || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mEtEmail.setError(getString(TextUtils.isEmpty(email) ? R.string.Field_required : R.string.Email_wrong));
                    return;
                }
                final String password = mEtPassword.getText().toString().trim();
                if (password.length() < 6) {
                    mEtPassword.setError(getString(R.string.Password_short));
                    return;
                }
                setBlock(true);
                AContext.hideInputKeyboard(mEtEmail);
                RequestCenter.place(new LoginRequest(email, password), SignInFragment.this);
            }
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                switch ((int) objects[0]) {
                    case R.id.code_login:
                        MainActivity.start(getActivity());
                        getActivity().finish();
                        break;

                    case R.id.code_restore:
                        final String email = (String) objects[1];
                        showSnackBar(getString(R.string.format_Check_email_s_restore, email));

                    default:
                        setBlock(false);
                        break;
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    private final OnTouchListener mPasswordTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    break;

                default:
                    mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        }
    };
}