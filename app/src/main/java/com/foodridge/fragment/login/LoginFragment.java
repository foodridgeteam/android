package com.foodridge.fragment.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.foodridge.R;
import com.foodridge.activity.MainActivity;
import com.foodridge.api.request.AgreeTermsRequest;
import com.foodridge.api.request.LoginSocialRequest;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.dialog.TermsPolicyDialog;
import com.foodridge.fragment.main.SideMenuFragment;
import com.foodridge.model.Profile;
import com.foodridge.request.RequestCenter;
import com.foodridge.type.SocialType;
import com.foodridge.utility.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

import java.util.Collections;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 06/04/16
 */
public class LoginFragment extends BaseLoginFragment {

    private static final String EXTRA_AGREE_TERMS_POLICY = "EXTRA_AGREE_TERMS_POLICY";
    private static final int CODE_GOOGLE = 7;

    //VIEW's
    private ViewGroup mBtnFacebook;
    private ViewGroup mBtnGoogle;
    private ViewGroup mBtnEmail;
    private ViewGroup mBtnSigUp;

    private TermsPolicyDialog mDialog;

    //VALUE's
    private CallbackManager mFacebookManager;
    private GoogleApiClient mGoogleClient;
    private boolean mAgreeTermsPolicy;

    @NonNull
    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        mAgreeTermsPolicy = b != null && b.getBoolean(EXTRA_AGREE_TERMS_POLICY);
        mDialog = new TermsPolicyDialog(getActivity(), this);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mBtnFacebook = (ViewGroup) v.findViewById(R.id.btn_facebook);
        mBtnGoogle = (ViewGroup) v.findViewById(R.id.btn_google_plus);
        mBtnEmail = (ViewGroup) v.findViewById(R.id.btn_email);
        mBtnSigUp = (ViewGroup) v.findViewById(R.id.btn_sign_up);

        Utils.setOnClickListener(mClickListener, mBtnFacebook, mBtnGoogle, mBtnEmail, mBtnSigUp);
    }

    @Override
    protected void onActionBarReady(@NonNull ActionBar bar) {
        bar.hide();
    }

    @Override
    public void onCleanUp() throws Exception {
        Utils.cleanUp(mBtnFacebook);
        Utils.cleanUp(mBtnGoogle);
        Utils.cleanUp(mBtnEmail);
        Utils.cleanUp(mBtnSigUp);
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putBoolean(EXTRA_AGREE_TERMS_POLICY, mAgreeTermsPolicy);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_GOOGLE) {
            final GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                setBlock(true);
                final GoogleSignInAccount account = result.getSignInAccount();
                //noinspection ConstantConditions
                RequestCenter.place(new LoginSocialRequest(SocialType.GOOGLE_PLUS, account.getId(), account.getEmail()), LoginFragment.this);
            }

        } else if (mFacebookManager == null) {
            super.onActivityResult(requestCode, resultCode, data);

        } else {
            mFacebookManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private final OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_facebook:
                    if (AContext.isNetworkConnected()) {
                        if (!FacebookSdk.isInitialized()) {
                            FacebookSdk.sdkInitialize(AContext.getApp());

                            if (AConstant.DEBUG) {
                                FacebookSdk.setIsDebugEnabled(true);
                                FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
                            }
                        }
                        final LoginManager manager = LoginManager.getInstance();
                        if (mFacebookManager == null) {
                            mFacebookManager = CallbackManager.Factory.create();
                            manager.registerCallback(mFacebookManager, newFacebookCallback());
                        }
                        manager.logOut();
//                        manager.setLoginBehavior(LoginBehavior.WEB_ONLY);
                        manager.logInWithReadPermissions(LoginFragment.this, Collections.singletonList("public_profile"));

                    } else {
                        Utils.showNoInternet(getActivity());
                    }
                    break;

                case R.id.btn_google_plus:
                    if (AContext.isNetworkConnected()) {
                        if (mGoogleClient == null) {
                            final GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                    .requestIdToken(getString(R.string.google_client_id))
                                    .requestEmail()
                                    .build();

                            mGoogleClient = new GoogleApiClient.Builder(getActivity())
                                    .enableAutoManage(getActivity(), newGoogleFailedListener())
                                    .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                                    .build();
                        }
                        startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(mGoogleClient), CODE_GOOGLE);

                    } else {
                        Utils.showNoInternet(getActivity());
                    }
                    break;

                case R.id.btn_email:
                    startFragment(SignInFragment.newInstance(), true);
                    break;

                case R.id.btn_sign_up:
                    if (mAgreeTermsPolicy) {
                        startFragment(SignUpFragment.newInstance(), true);

                    } else {
                        mDialog.show(mBtnSigUp);
                    }
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case RequestCenter.CODE_ANSWER: {
                setBlock(false);
                final int receiverCode = (int) objects[0];
                final Activity activity = getActivity();
                if (activity == null) {
                    return;
                }
                switch (receiverCode) {
                    case R.id.code_logout:
                        SideMenuFragment.logout(activity);
                        break;

                    case R.id.code_login:
                        if (!Profile.getInstance().isAgreeTerms()) {
                            mDialog.show();
                            break;
                        }

                    case R.id.code_terms_policy:
                        MainActivity.start(activity);
                        activity.finish();
                        return;

                    default:
                        break;
                }
            }
            break;

            case RequestCenter.CODE_ERROR:
                setBlock(false);
                showError(objects);
                final int receiverCode = (int) objects[0];

                switch (receiverCode) {
                    case R.id.code_terms_policy:
                        mDialog.show();
                        return;

                    case R.id.code_logout:
                        SideMenuFragment.logout(getActivity());
                        break;

                    case R.id.code_login:
                        if (Profile.isAuthorized() && !Profile.getInstance().isAgreeTerms()) {
                            mDialog.show();
                        }
                        break;

                    default:
                        break;
                }
                break;

            case TermsPolicyDialog.CODE:
                final boolean agreeTerms = (boolean) objects[0];
                if (Profile.isAuthorized()) {
                    setBlock(true);
                    if (agreeTerms) {
                        RequestCenter.place(new AgreeTermsRequest(), this);

                    } else {
//                        if (AContext.isNetworkConnected()) {
//                            RequestCenter.place(new LogoutRequest(), this);
//
//                        } else {
                        SideMenuFragment.logout(getActivity());
//                        }
                    }

                } else if (mAgreeTermsPolicy = agreeTerms) {
                    mClickListener.onClick(mBtnSigUp);
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }

    }

    @NonNull
    private FacebookCallback<LoginResult> newFacebookCallback() {
        return new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult result) {
                LOG.toLog(result.getAccessToken());

                setBlock(true);
                final AccessToken token = AccessToken.getCurrentAccessToken();
                RequestCenter.place(new LoginSocialRequest(SocialType.FACEBOOK, token.getToken(), token.getUserId()), LoginFragment.this);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
                Utils.showError(getActivity(), e.getMessage());
            }
        };
    }

    @NonNull
    private OnConnectionFailedListener newGoogleFailedListener() {
        return new OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult result) {
                Utils.showError(getActivity(), result.getErrorMessage());
            }
        };
    }
}