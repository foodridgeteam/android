package com.foodridge.fragment.login;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.foodridge.R;
import com.foodridge.activity.MainActivity;
import com.foodridge.api.request.RegisterRequest;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.Utils;

import java.util.UUID;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 06/04/16
 */
public class SignUpFragment extends BaseLoginFragment {

    //VIEW's
    private EditText mEtEmail;
    private EditText mEtPassword;
    private EditText mEtName;
    private View mBtnSignUp;
    private ImageView mIvPassword;

    @NonNull
    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle b) {
        super.onViewCreated(v, b);

        mEtEmail = (EditText) v.findViewById(R.id.et_email);
        mEtPassword = (EditText) v.findViewById(R.id.et_password);
        mEtName = (EditText) v.findViewById(R.id.et_name);
        mBtnSignUp = v.findViewById(R.id.btn_sign_up);
        mIvPassword = (ImageView) v.findViewById(R.id.iv_password);

        mBtnSignUp.setOnClickListener(mSignUpClickListener);
        mIvPassword.setOnTouchListener(mPasswordTouchListener);

        if (AConstant.DEBUG) {
            final String email = UUID.randomUUID().toString().substring(0, 8) + "@mail.ru";//AConstant.DEFAULT_EMAIL;

            mEtEmail.setText(email);
            mEtPassword.setText(AConstant.DEFAULT_PASSWORD);
            mEtName.setText(email);
        }
        AContext.showInputKeyboard(mEtEmail);
    }

    @Override
    protected void onActionBarReady(@NonNull ActionBar bar) {
        bar.show();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setDefaultDisplayHomeAsUpEnabled(true);

        bar.setTitle(R.string.empty);
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mEtEmail);
    }

    @Override
    public void onCleanUp() throws Exception {
        Utils.cleanUp(mEtEmail);
        Utils.cleanUp(mEtPassword);
        Utils.cleanUp(mEtName);
        Utils.cleanUp(mBtnSignUp);
        Utils.cleanUp(mIvPassword);
    }

    private final OnClickListener mSignUpClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final String email = mEtEmail.getText().toString().trim();
            if (email.length() == 0 || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                mEtEmail.setError(getString(TextUtils.isEmpty(email) ? R.string.Field_required : R.string.Email_wrong));
                return;
            }
            final String password = mEtPassword.getText().toString().trim();
            if (password.length() < 6) {
                mEtPassword.setError(getString(R.string.Password_short));
                return;
            }
            final String name = mEtName.getText().toString().trim();
            if (name.length() == 0) {
                mEtName.setError(getString(R.string.Field_required));
                return;
            }
            setBlock(true);
            AContext.hideInputKeyboard(mEtEmail);
            RequestCenter.place(new RegisterRequest(email, password, name), SignUpFragment.this);
        }
    };

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case RequestCenter.CODE_ANSWER:
                @IdRes final int receiverCode = (int) objects[0];
                switch (receiverCode) {
                    case R.id.code_registration:
                        MainActivity.start(getActivity());
                        getActivity().finish();
                        break;

                    case R.id.code_restore:
                        setBlock(false);
                        break;

                    default:
                        break;
                }
                break;

            default:
                super.onObjectsReceive(code, objects);
                break;
        }
    }

    private final OnTouchListener mPasswordTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    break;

                default:
                    mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        }
    };
}