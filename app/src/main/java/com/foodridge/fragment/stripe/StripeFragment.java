package com.foodridge.fragment.stripe;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.activity.StripeActivity;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.model.Profile;
import com.foodridge.utility.Utils;
import com.foodridge.view.creditcard.CreditCardEditText;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.util.Locale;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 27.04.16.
 */
public class StripeFragment extends BaseFragment {

    private static final String EXTRA_0 = "EXTRA_0";//mTotalSum

    //VIEW's
    private CreditCardEditText mEtCardNumber;
    private EditText mEtExpMonth;
    private EditText mEtExpYear;
    private EditText mEtCvc;
    private View mBtnPay;

    private ProgressDialog mProgressDialog;

    //VALUE's
    private float mTotalSum;

    @NonNull
    public static StripeFragment newInstance(float total) {
        final Bundle b = new Bundle(1);
        b.putFloat(EXTRA_0, total);

        final StripeFragment f = new StripeFragment();
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        final Bundle bundle = b != null ? b : getArguments();
        mTotalSum = bundle.getFloat(EXTRA_0);
    }

    @Nullable
    @Override
    protected View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        return inflater.inflate(R.layout.fragment_stripe, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEtCardNumber = (CreditCardEditText) view.findViewById(R.id.et_card_number);
        mEtExpMonth = (EditText) view.findViewById(R.id.et_exp_month);
        mEtExpYear = (EditText) view.findViewById(R.id.et_exp_year);
        mEtCvc = (EditText) view.findViewById(R.id.et_cvc);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mProgressDialog.setMessage(getString(R.string.Loading));

        final TextView btnPay = (TextView) view.findViewById(R.id.btn_pay);
        btnPay.setText(String.format(Locale.ENGLISH, getString(R.string.format_Pay), mTotalSum));
        btnPay.setOnClickListener(mPayClickListener);
        mBtnPay = btnPay;

        if (AConstant.DEBUG) {
            mEtCardNumber.setText("4242424242424242");
            mEtExpMonth.setText("12");
            mEtExpYear.setText("16");
            mEtCvc.setText("123");
        }
    }

    @Override
    protected void onActionBarReady(@NonNull ActionBar bar) {
        super.onActionBarReady(bar);
        bar.setTitle(R.string.Pay_with_card);
    }

    @Override
    public void onPause() {
        super.onPause();
        AContext.hideInputKeyboard(mEtCardNumber);
    }

    @Override
    public void onCleanUp() throws Exception {
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putFloat(EXTRA_0, mTotalSum);
    }

    @Override
    public void setBlock(boolean block) {
        try {
            if (block) {
                mProgressDialog.show();

            } else {
                mProgressDialog.dismiss();
            }

        } catch (Exception ignore) {
        } finally {
            mBtnPay.setEnabled(!block);
        }
    }

    private final OnClickListener mPayClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final String cardNumber = mEtCardNumber.getCreditCardNumber();
            final String expMonth = mEtExpMonth.getText().toString();
            final String expYear = mEtExpYear.getText().toString();
            final String cvc = mEtCvc.getText().toString();

            final int expMonthInt = TextUtils.isEmpty(expMonth) ? 0 : Integer.parseInt(expMonth);
            final int expYearInt = TextUtils.isEmpty(expYear) ? 0 : Integer.parseInt(expYear);

            final Card card = new Card(cardNumber, expMonthInt, expYearInt, cvc);
            if (AConstant.DEBUG) {
                final Profile profile = Profile.getInstance();
                final DeliveryInfo deliveryInfo = Profile.getInstance().getDeliveryInfo();
                if (deliveryInfo != null) {
                    card.setAddressCountry(deliveryInfo.getCountry());
                    card.setAddressState(deliveryInfo.getCity());
                    card.setAddressLine1(deliveryInfo.getStreet());
                    card.setName(profile.getName());
                }
            }
            EditText focusView = null;

            mEtCardNumber.setError(null);
            mEtExpMonth.setError(null);
            mEtExpYear.setError(null);
            mEtCvc.setError(null);

            if (!card.validateNumber()) {
                mEtCardNumber.setError(getString(R.string.Wrong_card_number));
                focusView = mEtCardNumber;
            }
            if (!card.validateExpMonth()) {
                mEtExpMonth.setError(getString(R.string.Enter_month));
                focusView = Utils.coalesce(focusView, mEtExpMonth);
            }
            if (!card.validateExpYear()) {
                mEtExpYear.setError(getString(R.string.Enter_year));
                focusView = Utils.coalesce(focusView, mEtExpYear);
            }
            if (!card.validateCVC()) {
                mEtCvc.setError(getString(R.string.Enter_cvc));
                focusView = Utils.coalesce(focusView, mEtCvc);
            }
            if (focusView == null) {
                new Stripe().createToken(card, getString(R.string.stripe_test_pk), mTokenCallback);

                setBlock(true);
                AContext.hideInputKeyboard(mEtCardNumber);

            } else {
                focusView.setSelection(focusView.length());
                focusView.requestFocus();
            }
        }
    };

    private final TokenCallback mTokenCallback = new TokenCallback() {
        @Override
        public void onError(Exception error) {
            setBlock(false);
            Utils.showError(getActivity(), error.getLocalizedMessage());
        }

        @Override
        public void onSuccess(Token token) {
            setBlock(false);
            StripeActivity.sendResult(getActivity(), token.getId());
        }
    };
}