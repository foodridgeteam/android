package com.foodridge.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodridge.R;
import com.foodridge.activity.BaseActivity;
import com.foodridge.api.model.ServerError;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.CleaningEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.BaseUtils;
import com.foodridge.utility.Log;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public abstract class BaseFragment extends Fragment
        implements ObjectsReceiver, CleaningEntity {

    protected static final Log LOG = Log.LOG;

    private int mFragmentContainerId;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        final Activity activity = getActivity();
        if (activity instanceof BaseActivity) {
            mFragmentContainerId = ((BaseActivity) activity).getFragmentContainerId();
        }
    }

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b) {
        //        inflater=LayoutInflater.from(getActivity());
        return onInheritorCreateView(inflater, container, b);
    }

    @Nullable
    protected abstract View onInheritorCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle b);

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Activity activity = getActivity();
        final ActionBar bar = activity instanceof AppCompatActivity ? ((AppCompatActivity) activity).getSupportActionBar() : null;
        if (bar != null) {
            onActionBarReady(bar);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            RequestCenter.cancel(this);
            onCleanUp();

        } catch (Exception ignore) {
        } finally {
            System.gc();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
    }

    protected void onActionBarReady(@NonNull ActionBar bar) {
    }

    public abstract void setBlock(boolean block);

    protected final int getFragmentContainerId() {
        return mFragmentContainerId;
    }

    public boolean startFragment(@NonNull BaseFragment fragment, boolean addToBackStack, @NonNull Object... objects) {
        return startFragmentSimple(fragment, addToBackStack);
    }

    protected final boolean startFragmentSimple(@NonNull BaseFragment fragment, boolean addToBackStack) {
//        final BaseActivity activity = (BaseActivity) getActivity();
//        final boolean activityAvailable = activity != null;
//        if (activity != null) {
//            activity.startFragment(fragment, addToBackStack);
//        }
//        return activityAvailable;

        final String tag = BaseUtils.getTag(fragment);

        final FragmentManager manager = getFragmentManager();
        if (!addToBackStack) {
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        final FragmentTransaction transaction = manager.beginTransaction();
//        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        transaction.replace(getFragmentContainerId(), fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commitAllowingStateLoss();

        return true;
    }

    protected final void receiveObjects(int code, @NonNull Object... objects) {
        final Fragment fragment = getTargetFragment();
        if (fragment instanceof ObjectsReceiver) {
            ((ObjectsReceiver) fragment).onObjectsReceive(code, objects);

        } else {
            final Activity activity = getActivity();
            if (activity instanceof ObjectsReceiver) {
                ((ObjectsReceiver) activity).onObjectsReceive(code, objects);
            }
        }
    }

    protected final void popBackStack() {
        final Activity activity = getActivity();
        if (activity != null) {
            activity.onBackPressed();
        }
    }

    protected final void postPopBackStack() {
        final View view = getView();
        if (view == null) {
            return;
        }
        view.post(new Runnable() {
            @Override
            public void run() {
                popBackStack();
            }
        });
    }

    @CallSuper
    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case R.id.code_block:
                final boolean block = (boolean) objects[0];
                setBlock(block);
                break;

            case RequestCenter.CODE_ANSWER:
                setBlock(false);
                break;

            case RequestCenter.CODE_ERROR:
                setBlock(false);
                showError(objects);
                break;

            default:
                break;
        }
    }

    protected final void showError(@NonNull Object... objects) {
        final ServerError error = (ServerError) objects[1];
        Utils.showError(getActivity(), error.getMessage());

//        final String message = error.getMessage();
//        if (!TextUtils.isEmpty(message)) {
//            Toast.makeText(AContext.getApp(), message, Toast.LENGTH_LONG).show();
//        }
    }

    protected void showSnackBar(@NonNull String message) {
        final View view = getView();
        if (view != null && view.getParent() != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
        }
    }

    protected void showSnackBar(@StringRes int stringRes) {
        showSnackBar(AContext.getString(stringRes));
    }
}