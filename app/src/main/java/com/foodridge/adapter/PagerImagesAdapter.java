package com.foodridge.adapter;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.foodridge.R;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.view.ProgressImageView;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 12.04.16
 */
public class PagerImagesAdapter extends PagerAdapter {

    private final Drawable mPlaceholder;
    private String[] mImages;

    public PagerImagesAdapter(@Nullable String... images) {
        mImages = images == null || images.length == 0 ? new String[1] : images;
        mPlaceholder = AContext.getDrawable(R.drawable.svg_logo_placeholder);
    }

    @WorkerThread
    public void setData(@Nullable String... images) {
        mImages = images == null || images.length == 0 ? new String[1]
                : (AConstant.DEBUG ? images : new String[]{images[0]});
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ProgressImageView view = new ProgressImageView(container.getContext());
        view.setPlaceholder(mPlaceholder);
        view.show(mImages[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}