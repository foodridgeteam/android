package com.foodridge.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.foodridge.R;
import com.foodridge.application.AContext;
import com.foodridge.fragment.main.ProfileChefFragment;
import com.foodridge.fragment.main.ProfileGeneralFragment;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 07.04.16
 */
public class ProfilePagerAdapter extends FragmentPagerAdapter {

    private final String[] mTabArray;

    public ProfilePagerAdapter(@NonNull FragmentManager manager) {
        super(manager);
        mTabArray = AContext.getResources().getStringArray(R.array.profile_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        return position == 0 ? ProfileGeneralFragment.newInstance() : ProfileChefFragment.newInstance();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabArray[position];
    }

    @Override
    public int getCount() {
        return mTabArray.length;
    }
}