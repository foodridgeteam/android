package com.foodridge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.foodridge.api.model.Comment;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.model.DbComment;
import com.foodridge.view.item.DishCommentItemView;

import java.lang.ref.WeakReference;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 03/05/16
 */
public class DishCommentsOffsetAdapter extends BaseOffsetCursorAdapter<Comment, DbComment, DishCommentItemView> {

    public DishCommentsOffsetAdapter() {
        super(null, DbComment.class);
    }

    @NonNull
    @Override
    protected DishCommentItemView obtain(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        return new DishCommentItemView(context);
    }
}