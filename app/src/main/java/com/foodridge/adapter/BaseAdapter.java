package com.foodridge.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.foodridge.interfaces.DataEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 08/12/15
 */
public abstract class BaseAdapter<DATA, VIEW extends View & DataEntity<VIEW, DATA>> extends android.widget.BaseAdapter {

    private final List<DATA> mData;

    public BaseAdapter(@Nullable List<DATA> data) {
        mData = data != null ? data : new ArrayList<DATA>(0);
    }

    public void setData(@Nullable List<DATA> data) {
        mData.clear();
        if (data != null && data.size() > 0) {
            mData.addAll(data);
        }
    }

    public void setData(@Nullable DATA[] data) {
        mData.clear();
        if (data != null && data.length > 0) {
            mData.addAll(Arrays.asList(data));
        }
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @NonNull
    protected final List<DATA> getData() {
        return mData;
    }

    @Override
    public final DATA getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final View getView(int position, View cV, ViewGroup parent) {
        final VIEW v = cV != null ? (VIEW) cV : obtain(parent, getItemViewType(position));

        setData(v, position);

        return v;
    }

    protected abstract VIEW obtain(@NonNull View parent, int viewType);

    protected void setData(@NonNull VIEW v, int position) {
        v.setData(getItem(position), position);
    }
}