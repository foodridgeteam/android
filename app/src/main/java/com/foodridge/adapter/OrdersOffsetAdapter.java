package com.foodridge.adapter;

import android.content.Context;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v4.util.SimpleArrayMap;

import com.foodridge.api.model.Order;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.model.DbOrder;
import com.foodridge.type.OrderType;
import com.foodridge.view.item.order.BaseOrderItemView;
import com.foodridge.view.item.order.CompletedOrderItemView;
import com.foodridge.view.item.order.OrdinalOrderItemView;

import java.lang.ref.WeakReference;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 11.04.16.
 */
public class OrdersOffsetAdapter extends BaseOffsetCursorAdapter<Order, DbOrder, BaseOrderItemView> {

    @OrderType
    private final int mType;
    private final SimpleArrayMap<String, Object> mBlockedOrders;

    public OrdersOffsetAdapter(@NonNull ObjectsReceiver receiver, @OrderType int type) {
        super(receiver, DbOrder.class);

        mType = type;
        mBlockedOrders = new SimpleArrayMap<>();
    }

    @MainThread
    public void blockOrder(@NonNull Order order, boolean block) {
        if (block) {
            if (AContext.isNetworkConnected()) {
                mBlockedOrders.put(order.getId(), Boolean.TRUE);
                super.notifyDataSetChanged();
            }

        } else {
            mBlockedOrders.remove(order.getId());
            super.notifyDataSetChanged();
        }
    }

    public void clearAllBlocked() {
        mBlockedOrders.clear();
    }

    @NonNull
    @Override
    protected BaseOrderItemView obtain(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        switch (mType) {
            case OrderType.RECEIVED:
            case OrderType.PLACED:
                return new OrdinalOrderItemView(context, receiver, mType);

            case OrderType.COMPLETED:
                return new CompletedOrderItemView(context, receiver, mType);

            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    protected void setData2View(@NonNull BaseOrderItemView view, @NonNull Order order) {
        view.setData(order, mBlockedOrders.get(order.getId()) != null);
    }
}