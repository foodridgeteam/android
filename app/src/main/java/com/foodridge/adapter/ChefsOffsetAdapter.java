package com.foodridge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.foodridge.api.model.Chef;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.model.DbChef;
import com.foodridge.view.item.ChefItemView;

import java.lang.ref.WeakReference;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 11.04.16.
 */
public class ChefsOffsetAdapter extends BaseOffsetCursorAdapter<Chef, DbChef, ChefItemView> {

    public ChefsOffsetAdapter(@NonNull ObjectsReceiver receiver) {
        super(receiver, DbChef.class);
    }

    @NonNull
    @Override
    protected ChefItemView obtain(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        return new ChefItemView(context, receiver);
    }
}