package com.foodridge.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.SparseArrayCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.foodridge.R;
import com.foodridge.application.AConstant;
import com.foodridge.interfaces.AdapterCursorEntity;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Log;
import com.foodridge.utility.Utils;

import java.lang.ref.WeakReference;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 18/04/16
 */
public abstract class BaseOffsetCursorAdapter<DATA, ENTITY extends AdapterCursorEntity<DATA>, VIEW extends View & DataEntity<VIEW, DATA>> extends android.widget.BaseAdapter {

    public static final int CODE = R.id.code_offset_adapter;

    private final WeakReference<ObjectsReceiver> mReceiver;
    private final EntityCache<DATA, ENTITY> mCache;

    private Cursor mCursor;
    private int mOffset;

    private int mAdditionalOffset;

    public BaseOffsetCursorAdapter(@Nullable ObjectsReceiver receiver, @NonNull Class<ENTITY> cls) {
        mReceiver = receiver == null ? null : new WeakReference<>(receiver);
        mCache = new EntityCache<>(cls);
    }

    public void setListView(@NonNull ListView view) {
        view.setOnScrollListener(mScrollListener);
        view.setAdapter(this);
        mAdditionalOffset = view.getHeaderViewsCount() + view.getFooterViewsCount();
    }

    private final OnScrollListener mScrollListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            final int position = firstVisibleItem + visibleItemCount;
            if (position + 2 >= mOffset + mAdditionalOffset) {
                Utils.receiveObjects(mReceiver, CODE);
            }
        }
    };

    public void swapCursor(@Nullable Cursor newCursor, boolean noOffset) {
        if (newCursor == mCursor) {
            return;
        }
        final Cursor oldCursor = mCursor;
        final boolean setFirstOffset = mCursor == null;
        mCursor = newCursor;
        if (newCursor == null) {
            notifyDataSetInvalidated();

        } else {
            final int count = mCursor.getCount();
            if (noOffset) {
                mOffset = count;

            } else if (setFirstOffset) {
                int offset = AConstant.LIMIT;
                if (count < offset) {
                    offset = count;
                }
                mOffset = offset;
            }
            mCursor.moveToFirst();
            notifyDataSetChanged();
        }
        if (oldCursor != null && !oldCursor.isClosed()) {
            oldCursor.close();
        }
    }

    public void swapCursor(@Nullable Cursor newCursor) {
        this.swapCursor(newCursor, false);
    }

    @MainThread
    public void setOffset(int offset) {
        final int count = mCursor == null ? 0 : mCursor.getCount();
        if (count < offset) {
            offset = count;
        }
        mOffset = offset;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mOffset;
    }

    @Nullable
    public Cursor getItem(int position) {
        return mCursor != null && !mCursor.isClosed() && mCursor.moveToPosition(position) ? mCursor : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final View getView(int position, View cV, ViewGroup parent) {
        final VIEW view;
        if (cV == null) {
            view = obtain(parent.getContext(), mReceiver);

        } else {
            view = (VIEW) cV;
        }
        final Cursor cursor = getItem(position);
        if (cursor != null) {
            final DATA data = mCache.getEntity(view, cursor);
            if (data != null) {
                setData2View(view, data);
            }
        }
        return view;
    }

    @NonNull
    protected abstract VIEW obtain(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver);

    protected void setData2View(@NonNull VIEW view, @NonNull DATA data) {
        view.setData(data);
    }

    protected static final class EntityCache<ENTITY, DB_ENTITY extends AdapterCursorEntity<ENTITY>> {

        private final Class<DB_ENTITY> mClass;
        private final SparseArrayCompat<AdapterCursorEntity<ENTITY>> mCache;

        public EntityCache(@NonNull Class<DB_ENTITY> cls) {
            mClass = cls;
            mCache = new SparseArrayCompat<>();
        }

        @Nullable
        public ENTITY getEntity(@NonNull View view, @NonNull Cursor cursor) {
            final int key = view.hashCode();
            AdapterCursorEntity<ENTITY> entity = mCache.get(key);
            if (entity == null) {
                try {
                    entity = mClass.newInstance();
                    mCache.put(key, entity);

                } catch (Exception e) {
                    throw new IllegalStateException();
                }
            }
            try {
                entity.fromCursor(cursor);

            } catch (Exception e) {
                Log.LOG.toLog(e);
            }
            return entity.getEntity();
        }
    }
}