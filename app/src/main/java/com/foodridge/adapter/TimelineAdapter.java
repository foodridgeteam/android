package com.foodridge.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.foodridge.api.model.Order;
import com.foodridge.api.model.OrderMessage;
import com.foodridge.view.item.order.OrderTimelineItemView;

import java.util.List;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 19.04.16.
 */
public class TimelineAdapter extends BaseRecyclerLinearAdapter<OrderMessage, OrderTimelineItemView> {

    private Order mOrder;

    public TimelineAdapter() {
        super(null);
    }

    @Override
    protected OrderTimelineItemView obtain(@NonNull View parent) {
        return new OrderTimelineItemView(parent.getContext());
    }

    public void setData(@NonNull Order order, @Nullable List<OrderMessage> data) {
        mOrder = order;
        super.setData(data);
    }

    @Override
    protected void setData2View(@NonNull OrderTimelineItemView view, int position) {
        view.setData(getItem(position), mOrder);
    }
}