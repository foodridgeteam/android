package com.foodridge.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;

import com.foodridge.adapter.MyDishPhotoAdapter.Holder;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.view.item.MyDishPhotoItemView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 20/04/16
 */
public class MyDishPhotoAdapter extends RecyclerView.Adapter<Holder> {

    private final WeakReference<ObjectsReceiver> mReceiver;
    private final ArrayList<Uri> mUris;

    public MyDishPhotoAdapter(@NonNull ArrayList<Uri> list, @NonNull ObjectsReceiver receiver) {
        mReceiver = new WeakReference<>(receiver);
        mUris = list;
    }

    @WorkerThread
    public boolean add(@NonNull Uri uri) {
        final boolean add = !mUris.contains(uri);
        if (add) {
            mUris.add(uri);
            notifyItemInserted(mUris.size() - 1);
        }
        return add;
    }

    @WorkerThread
    public void remove(@NonNull Uri uri) {
        final int position = mUris.indexOf(uri);
        if (position >= 0) {
            mUris.remove(position);
            notifyItemRemoved(position);
        }
    }

    @NonNull
    public ArrayList<Uri> getUris() {
        return mUris;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(new MyDishPhotoItemView(parent.getContext(), mReceiver));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ((MyDishPhotoItemView) holder.itemView).setData(mUris.get(position));
    }

    @Override
    public int getItemCount() {
        return mUris.size();
    }

    public static final class Holder extends ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }
    }
}