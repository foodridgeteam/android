package com.foodridge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.foodridge.api.model.Meal;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.model.DbMeal;
import com.foodridge.view.item.DishItemView;

import java.lang.ref.WeakReference;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 11.04.16.
 */
public class DishesOffsetAdapter extends BaseOffsetCursorAdapter<Meal, DbMeal, DishItemView> {

    public DishesOffsetAdapter(@NonNull ObjectsReceiver receiver) {
        super(receiver, DbMeal.class);
    }

    @NonNull
    @Override
    protected DishItemView obtain(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        return new DishItemView(context, receiver);
    }
}