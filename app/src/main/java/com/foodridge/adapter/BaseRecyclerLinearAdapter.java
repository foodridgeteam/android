package com.foodridge.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.foodridge.interfaces.DataEntity;
import com.foodridge.view.RecyclerLinearLayout.Adapter;
import com.foodridge.view.RecyclerLinearLayout.ViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 19.04.16.
 */
public abstract class BaseRecyclerLinearAdapter<DATA, VIEW extends View & DataEntity<VIEW, DATA>>
        extends Adapter<ViewHolder<VIEW>> {

    private final List<DATA> mData;

    public BaseRecyclerLinearAdapter(@Nullable List<DATA> data) {
        mData = data != null ? data : new ArrayList<DATA>(0);
    }

    public void setData(@Nullable List<DATA> data) {
        mData.clear();
        if (data != null && data.size() > 0) {
            mData.addAll(data);
        }
    }

    public void setData(@Nullable DATA[] data) {
        mData.clear();
        if (data != null && data.length > 0) {
            mData.addAll(Arrays.asList(data));
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public final DATA getItem(int position) {
        return mData.get(position);
    }

    @Override
    public ViewHolder<VIEW> onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder<VIEW>(obtain(parent)) {
        };
    }

    protected abstract VIEW obtain(@NonNull View parent);

    @Override
    public final void onBindViewHolder(ViewHolder<VIEW> viewHolder, int position) {
        setData2View(viewHolder.mItemView, position);
    }

    protected void setData2View(@NonNull VIEW view, int position) {
        view.setData(getItem(position), position);
    }
}