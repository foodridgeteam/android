package com.foodridge.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.foodridge.application.AContext;
import com.foodridge.fragment.main.ListOrdersFragment;
import com.foodridge.model.Profile;
import com.foodridge.type.OrderType;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 11.04.16.
 */
public class OrdersPagerAdapter extends FragmentPagerAdapter {

    private static final int[] ORDINAL = {OrderType.PLACED, OrderType.COMPLETED};
    private static final int[] CHEF = {OrderType.RECEIVED, OrderType.PLACED, OrderType.COMPLETED};

    private final int[] mTypes;
    private final String[] mTitles;

    public OrdersPagerAdapter(@NonNull FragmentManager manager) {
        super(manager);

        mTypes = Profile.getInstance().isChef() ? CHEF : ORDINAL;
        mTitles = new String[mTypes.length];
        for (int i = 0; i < mTypes.length; i++) {
            mTitles[i] = AContext.getString(mTypes[i]);
        }
    }

    @Override
    public Fragment getItem(int position) {
        return ListOrdersFragment.newInstance(mTypes[position]);
    }

    @Override
    public int getCount() {
        return mTypes.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}