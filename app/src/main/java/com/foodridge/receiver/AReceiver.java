package com.foodridge.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.foodridge.application.AContext;
import com.foodridge.provider.FoodridgeMetaData.RequestEntityContract;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class AReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case ConnectivityManager.CONNECTIVITY_ACTION:
                if (AContext.isNetworkConnected()) {
                    AContext.notifyChange(RequestEntityContract.CONTENT_URI);
                }
                break;

            default:
                break;
        }
    }
}