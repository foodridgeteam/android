package com.foodridge.service;

import android.app.IntentService;
import android.content.Intent;

import com.foodridge.R;
import com.foodridge.api.request.PushIdRequest;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.utility.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 08/04/16
 */
public class GCMInstanceService extends IntentService {

    public GCMInstanceService() {
        super("GCMInstanceService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ASettings settings = ASettings.getInstance();
        try {
            if (settings.getGcmToken() == null && AContext.isNetworkConnected()) {
                final String token = InstanceID.getInstance(this).getToken(getString(R.string.google_cloud_message_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                settings.setGcmToken(token);
            }

        } catch (Exception e) {
            Log.LOG.toLog(e);
        }
        PushIdRequest.send();
    }
}