package com.foodridge.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;

import com.foodridge.R;
import com.foodridge.activity.LoginActivity;
import com.foodridge.activity.MainActivity;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Meal;
import com.foodridge.api.model.Order;
import com.foodridge.api.request.ANetworkRequest;
import com.foodridge.api.request.BaseRequestEntity;
import com.foodridge.api.request.MealRequest;
import com.foodridge.api.request.MealsRequest;
import com.foodridge.api.request.OrderRequest;
import com.foodridge.api.request.OrdersRequest;
import com.foodridge.application.AConstant;
import com.foodridge.application.App;
import com.foodridge.utility.Log;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 08/04/16
 */
public class GCMHandleService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        Log.LOG.toLog("PUSH\tdata: " + data);

        if (data == null) {
            return;
        }
        final String message = data.getString(ApiKeys.MESSAGE);
        final String type = data.getString(ApiKeys.TYPE);
        final String id = data.getString(ApiKeys.ENTITY);
        if (TextUtils.isEmpty(message) || type == null) {
            Log.LOG.toLog(" !!! PUSH validation error\tdata: " + data);
            return;
        }
        final String appName = getString(R.string.app_name);
        Intent intent = null;
        if (id != null) {
            switch (type) {
                case "101": {//new dish of chef that current user follow
                    BaseRequestEntity entity = new ANetworkRequest(new MealRequest(id, false)).execute();
                    if (entity != null) {
                        final Meal meal = (Meal) entity.getDataAndRemove();
                        if (meal != null && id.equals(meal.getId())) {
                            intent = MainActivity.newInstance(this, meal);
                            new ANetworkRequest(new MealsRequest(0, null, null)).oneShot(Thread.MIN_PRIORITY);
                        }
                    }
                }
                break;

                case "102"://new order
                case "103"://new message
                case "104"://new comment
                default:
                    BaseRequestEntity entity = new ANetworkRequest(new OrderRequest(id)).execute();
                    if (entity != null) {
                        final Order order = (Order) entity.getDataAndRemove();
                        if (order != null && id.equals(order.getId())) {
                            intent = MainActivity.newInstance(this, order);
                            new ANetworkRequest(new OrdersRequest(order.isActive())).oneShot(Thread.MIN_PRIORITY);
                        }
                    }
                    break;
            }
        }
        final int code = message.hashCode();
        if (intent == null) {
            intent = new Intent(this, LoginActivity.class).setAction(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_LAUNCHER);
        }
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, code, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (!AConstant.DEBUG && ((App) getApplication()).isAppVisible()) {
            return;
        }
        NotificationManagerCompat.from(this)
                .notify(code, new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(appName)
                        .setContentText(message)
                        .setContentIntent(pendingIntent)

                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setCategory(NotificationCompat.CATEGORY_EVENT)
//                        .setGroup(appName)
                        .setStyle(new BigTextStyle()
                                .bigText(message))

                        .build());
    }
}