package com.foodridge.service;

import android.app.Application;
import android.app.Notification;
import android.content.Context;

import com.foodridge.application.AConstant;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.networkstate.NetworkStateChecker;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.memory.LruCacheStringObjectPersister;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class ExecutorService extends SpiceService {

    private static final NetworkStateChecker CHECKER = new NetworkStateChecker() {
        @Override
        public boolean isNetworkAvailable(Context context) {
            return true;
        }

        @Override
        public void checkPermissions(Context context) {
        }
    };

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        final CacheManager cacheManager = new CacheManager();
        cacheManager.addPersister(new LruCacheStringObjectPersister(1_048_576));

        return cacheManager;
    }

    @Override
    public int getThreadCount() {
        return AConstant.AVAILABLE_PROCESSORS;
    }

    @Override
    public Notification createDefaultNotification() {
        return null;
    }

    @Override
    protected NetworkStateChecker getNetworkStateChecker() {
        return CHECKER;
    }
}