package com.foodridge.application;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.CheckResult;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateFormat;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.foodridge.R;

import okhttp3.Cache;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class AContext {

    //SINGLETON
    private static AContext sInstance;

    private final App mApp;

    private final NotificationManagerCompat mNotificationManager;
    private final LocalBroadcastManager mLocalBroadcastManager;
    private final ConnectivityManager mConnectivityManager;
    private final InputMethodManager mInputMethodManager;
    private final ContentResolver mContentResolver;
    private final Resources mResources;
    private final Cache mCache;

    private final Point mScreenSize;
    private final int mStatusBarHeight;
    private final int mActionBarHeight;

    static void init(@NonNull App app) {
        sInstance = new AContext(app);
    }

    @SuppressLint("PrivateResource")
    private AContext(@NonNull App app) {
        mApp = app;

        mConnectivityManager = (ConnectivityManager) mApp.getSystemService(Context.CONNECTIVITY_SERVICE);
        mInputMethodManager = (InputMethodManager) mApp.getSystemService(Context.INPUT_METHOD_SERVICE);
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(app);
        mNotificationManager = NotificationManagerCompat.from(mApp);
        mCache = new Cache(mApp.getCacheDir(), 1024 * 1024);
        mContentResolver = app.getContentResolver();
        mResources = mApp.getResources();
        {
            final WindowManager windowManager = (WindowManager) mApp.getSystemService(Context.WINDOW_SERVICE);
            mScreenSize = new Point();
            windowManager.getDefaultDisplay().getSize(mScreenSize);
        }
        {
            final int resourceId = mResources.getIdentifier("status_bar_height", "dimen", "android");
            mStatusBarHeight = resourceId > 0 ? mResources.getDimensionPixelSize(resourceId) : 0;
        }
        final TypedValue value = new TypedValue();
        if (app.getTheme().resolveAttribute(R.attr.actionBarSize, value, true)) {
            mActionBarHeight = TypedValue.complexToDimensionPixelSize(value.data, mResources.getDisplayMetrics());

        } else {
            mActionBarHeight = mResources.getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
        }
    }

    @NonNull
    public static Cache getCache() {
        return sInstance.mCache;
    }

    public boolean isActiveNetworkConnected() {
        final NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    public int checkSelfPermission(@NonNull String permission) {
        return ContextCompat.checkSelfPermission(mApp, permission);
    }

    @CheckResult
    @NonNull
    public static App getApp() {
        return sInstance.mApp;
    }

    @CheckResult
    @NonNull
    public static ConnectivityManager getConnectivityManager() {
        return sInstance.mConnectivityManager;
    }

    @CheckResult
    @NonNull
    public static Resources getResources() {
        return sInstance.mResources;
    }

    @CheckResult
    @NonNull
    public static Point getScreenSize() {
        return sInstance.mScreenSize;
    }

    @CheckResult
    public static int getStatusBarHeight() {
        return sInstance.mStatusBarHeight;
    }

    @CheckResult
    public static int getActionBarHeight() {
        return sInstance.mActionBarHeight;
    }

    public static boolean isNetworkConnected() {
        return sInstance.isActiveNetworkConnected();
    }

    public static boolean isPermissionGranted(@NonNull String permission) {
        return sInstance.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static void hideInputKeyboard(@NonNull View view) {
        sInstance.mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showInputKeyboard(@NonNull EditText view) {
        view.setSelection(view.length());
        view.requestFocus();

        sInstance.mInputMethodManager.showSoftInput(view, 0);
    }

    public static String getString(@StringRes int stringRes) {
        return sInstance.mResources.getString(stringRes);
    }

    public static String getStringArgs(@StringRes int stringRes, @NonNull Object... objects) {
        return sInstance.mResources.getString(stringRes, objects);
    }

    @Nullable
    public static Drawable getDrawable(@DrawableRes int drawableRes) {
        return ContextCompat.getDrawable(sInstance.mApp, drawableRes);
    }

    public static void sendLocalBroadcast(@NonNull Intent intent) {
        sInstance.mLocalBroadcastManager.sendBroadcast(intent);
    }

    @NonNull
    public static LocalBroadcastManager getLocalBroadcastManager() {
        return sInstance.mLocalBroadcastManager;
    }

    @NonNull
    public static ContentResolver getContentResolver() {
        return sInstance.mContentResolver;
    }

    public static int getColor(@ColorRes int colorRes) {
        return ContextCompat.getColor(sInstance.mApp, colorRes);
    }

    public static void notifyChange(@NonNull Uri contentUri) {
        sInstance.mContentResolver.notifyChange(contentUri, null);
    }

    @NonNull
    public static NotificationManagerCompat getNotificationManager() {
        return sInstance.mNotificationManager;
    }

    public static boolean is24HourFormat() {
        return DateFormat.is24HourFormat(sInstance.mApp);
    }
}