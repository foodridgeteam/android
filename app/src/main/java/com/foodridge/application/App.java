package com.foodridge.application;

import android.app.Activity;
import android.app.Application;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.foodridge.model.Profile;
import com.foodridge.provider.DbHelper;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.RequestEntityContract;
import com.foodridge.request.RequestCenter;
import com.foodridge.service.GCMInstanceService;
import com.foodridge.utility.BaseUtils;
import com.foodridge.utility.Log;
import com.foodridge.utility.OrderMessageCache;
import com.foodridge.utility.Utils;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;

import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class App extends Application {

    private static final int WHAT_INIT = 0;
    private static final int WHAT_SAVE = 1;
    private static final int WHAT_APPLY_BATCH = 2;

    private Looper mWorkerLooper;
    private WorkerHandler mWorkerHandler;
    private UncaughtExceptionHandler mDefaultExceptionHandler;

    @Override
    public final void onCreate() {
        super.onCreate();
        {
            ASettings.init(this);
            AContext.init(this);
            Profile.init(this);

            Branch.getAutoInstance(this);
            Fabric.with(this, new Crashlytics());
            mDefaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        }
        mWorkerLooper = Utils.newLooper(getPackageName(), Thread.MAX_PRIORITY);
        mWorkerHandler = new WorkerHandler(mWorkerLooper);

        super.registerActivityLifecycleCallbacks(mWorkerHandler);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        mWorkerHandler.sendEmptyMessage(WHAT_SAVE);
    }

    private final UncaughtExceptionHandler mExceptionHandler = new UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {
            Log.LOG.toLog(throwable);

            final String email = Profile.getInstance().getEmail();
            Answers.getInstance().logCustom(new CustomEvent("appFell")
                    .putCustomAttribute("type", throwable.getClass().getName())
                    .putCustomAttribute("who", TextUtils.isEmpty(email) ? Secure.getString(AContext.getContentResolver(), Secure.ANDROID_ID) : email)
            );

            mWorkerHandler.sendEmptyMessage(WHAT_SAVE);
            mDefaultExceptionHandler.uncaughtException(thread, throwable);
        }
    };

    @NonNull
    public final UncaughtExceptionHandler getExceptionHandler() {
        return mExceptionHandler;
    }

    @NonNull
    public Looper getWorkerLooper() {
        return mWorkerLooper;
    }

    public void setApplyBatch(@NonNull ArrayList<ContentProviderOperation> operations) {
        final Message message = mWorkerHandler.obtainMessage();
        message.what = WHAT_APPLY_BATCH;
        message.obj = operations;

        mWorkerHandler.sendMessage(message);
    }

    public boolean isAppVisible() {
        return mWorkerHandler.mResumePause.size() > 0;
    }

    private final class WorkerHandler extends Handler
            implements ActivityLifecycleCallbacks {

        private final SimpleArrayMap<String, Object> mCreateDestroy;
        private final SimpleArrayMap<String, Object> mResumePause;

        public WorkerHandler(@NonNull Looper looper) {
            super(looper);

            mCreateDestroy = new ArrayMap<>();
            mResumePause = new ArrayMap<>();

            sendEmptyMessage(WHAT_INIT);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_INIT:
                    RequestCenter.init(mWorkerLooper);
                    DbHelper.getInstance(getApplicationContext());
                    getContentResolver().delete(RequestEntityContract.CONTENT_URI, null, null);
                    startService(new Intent(getApplicationContext(), GCMInstanceService.class));

                    if (Profile.isAuthorized()) {
                        ASettings.loadStatic();
                        OrderMessageCache.getInstance().getFromDb();
                    }
                    break;

                case WHAT_SAVE:
                    Profile.getInstance().commit();
                    break;

                case WHAT_APPLY_BATCH:
                    final ArrayList<ContentProviderOperation> operations = (ArrayList<ContentProviderOperation>) msg.obj;
                    try {
                        getContentResolver().applyBatch(FoodridgeMetaData.AUTHORITY, operations);

                    } catch (Exception e) {
                        Log.LOG.toLog(e);
                    }
                    break;

                default:
                    super.handleMessage(msg);
                    break;
            }
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            mCreateDestroy.put(BaseUtils.getTag(activity), true);
        }

        @Override
        public void onActivityStarted(Activity activity) {
        }

        @Override
        public void onActivityResumed(Activity activity) {
            mResumePause.put(BaseUtils.getTag(activity), true);
        }

        @Override
        public void onActivityPaused(Activity activity) {
            mResumePause.remove(BaseUtils.getTag(activity));
            sendEmptyMessage(WHAT_SAVE);
        }

        @Override
        public void onActivityStopped(Activity activity) {
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            mCreateDestroy.remove(BaseUtils.getTag(activity));
            if (mCreateDestroy.size() == 0) {
                sendEmptyMessage(WHAT_SAVE);
            }
        }
    }
}