package com.foodridge.application;

import com.foodridge.BuildConfig;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class AConstant {

    public static final boolean DEBUG = BuildConfig.DEBUG;
//    public static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();
    public static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();

    //    public static final String DEFAULT_EMAIL = "8ccb69@mail.ru";
    public static final String DEFAULT_EMAIL = "6380d4cc@mail.ru";
    public static final String DEFAULT_PASSWORD = "qwerty";

    public static final int PUSH_FLAG = 1;
    public static final int LIMIT = DEBUG ? 3 : 30;

    public static final char[] PHONE_NOT_ALLOWED = {'*', '#', '/', ',', '.', ';', 'N'};
    public static final int ANIMATION_DURATION = 333;
}