package com.foodridge.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodridge.api.model.IdName;
import com.foodridge.api.request.ANetworkRequest;
import com.foodridge.api.request.CuisinesRequest;
import com.foodridge.api.request.IngredientsRequest;
import com.foodridge.utility.Log;
import com.foodridge.utility.Utils;

import java.util.Collection;
import java.util.List;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 08/04/16
 */
public class ASettings {

    private static final long STATIC_UPDATE_TIMEOUT = 24 * 60 * 60 * 1_000;

    private static final String KEY_1 = "KEY_1";//mGcmToken
    private static final String KEY_2 = "KEY_2";//mGcmTokenAccepted
    private static final String KEY_3 = "KEY_3";//mCuisines
    private static final String KEY_4 = "KEY_4";//mIngredients
    private static final String KEY_5 = "KEY_5";//mLastStaticTime
    private static final String KEY_6 = "KEY_6";//mCountryCode

    //SINGLETON
    private static ASettings sInstance;

    //VALUE's
    private final SharedPreferences mPreferences;

    //SETTINGS
    private String mGcmToken;
    private boolean mGcmTokenAccepted;
    private long mLastStaticTime;
    private String mCountryCode;

    //OTHER
    private final ArrayMap<String, IdName> mCuisinesMap;
    private final ArrayMap<String, IdName> mIngredientsMap;

    static void init(@NonNull Context context) {
        sInstance = new ASettings(context);
    }

    @NonNull
    public static ASettings getInstance() {
        return sInstance;
    }

    private ASettings(@NonNull Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        mCuisinesMap = new ArrayMap<>();
        mIngredientsMap = new ArrayMap<>();

        restore();
    }

    private void restore() {
        mGcmToken = mPreferences.getString(KEY_1, null);
        mGcmTokenAccepted = mPreferences.getBoolean(KEY_2, false);
        if (mCuisinesMap.size() == 0 || mIngredientsMap.size() == 0) {
            final JsonFactory factory = new JsonFactory();
            final ObjectMapper mapper = new ObjectMapper();

            IdName[] cuisines = null;
            final String cuisinesRaw = mPreferences.getString(KEY_3, null);
            if (cuisinesRaw != null) {
                try {
                    cuisines = mapper.readValue(factory.createParser(cuisinesRaw), IdName[].class);

                } catch (Exception e) {
                    Log.LOG.toLog(e);
                }
            }
            if (cuisines == null) {
                cuisines = IdName.CUISINES;
            }
            mCuisinesMap.clear();
            for (IdName cuisine : cuisines) {
                mCuisinesMap.put(cuisine.getName(), cuisine);
            }

            IdName[] ingredients = null;
            final String ingredientsRaw = mPreferences.getString(KEY_4, null);
            if (ingredientsRaw != null) {
                try {
                    ingredients = mapper.readValue(factory.createParser(ingredientsRaw), IdName[].class);

                } catch (Exception e) {
                    Log.LOG.toLog(e);
                }
            }
            if (ingredients == null) {
                ingredients = IdName.INGREDIENTS;
            }
            mIngredientsMap.clear();
            for (IdName cuisine : ingredients) {
                mIngredientsMap.put(cuisine.getName(), cuisine);
            }
        }
        mLastStaticTime = mPreferences.getLong(KEY_5, 0);
        mCountryCode = mPreferences.getString(KEY_6, null);
    }

    @WorkerThread
    public static void logout() {
        sInstance.mPreferences.edit()
                .remove(KEY_1)
                .remove(KEY_2)
                .apply();

        sInstance.restore();
    }

    @Nullable
    public String getGcmToken() {
        return mGcmToken;
    }

    public void setGcmToken(@NonNull String token) {
        mGcmToken = token;
        mPreferences.edit().putString(KEY_1, mGcmToken).apply();
    }

    public boolean isGcmTokenAccepted() {
        return mGcmTokenAccepted;
    }

    public void setGcmTokenAccepted(boolean tokenAccepted) {
        mGcmTokenAccepted = tokenAccepted;
        mPreferences.edit().putBoolean(KEY_2, mGcmTokenAccepted).apply();
    }

    @NonNull
    public Collection<IdName> getCuisines() {
        return mCuisinesMap.values();
    }

    @NonNull
    public ArrayMap<String, IdName> getCuisinesMap() {
        return mCuisinesMap;
    }

    @WorkerThread
    public void setCuisines(@NonNull List<IdName> cuisines) {
        mCuisinesMap.clear();
        for (IdName value : cuisines) {
            mCuisinesMap.put(value.getName(), value);
        }
        try {
            final String raw = new ObjectMapper().writeValueAsString(cuisines);
            mPreferences.edit().putString(KEY_3, raw).apply();

        } catch (Exception e) {
            Log.LOG.toLog(e);
        }
    }

    @Nullable
    public IdName findCuisine(@Nullable Object object) {
        return mCuisinesMap.get(object);
    }

    @NonNull
    public Collection<IdName> getIngredients() {
        return mIngredientsMap.values();
    }

    @NonNull
    public ArrayMap<String, IdName> getIngredientsMap() {
        return mIngredientsMap;
    }

    @WorkerThread
    public void setIngredients(List<IdName> ingredients) {
        mIngredientsMap.clear();
        for (IdName value : ingredients) {
            mIngredientsMap.put(value.getName(), value);
        }
        try {
            final String raw = new ObjectMapper().writeValueAsString(ingredients);
            mPreferences.edit().putString(KEY_4, raw).apply();

        } catch (Exception e) {
            Log.LOG.toLog(e);
        }
    }

    public void setLastStaticTime(long lastStaticTime) {
        mLastStaticTime = lastStaticTime;
        mPreferences.edit().putLong(KEY_5, mLastStaticTime).apply();
    }

    public static void loadStatic() {
        final long lastStaticTime = sInstance.mLastStaticTime;
        if (System.currentTimeMillis() - lastStaticTime > STATIC_UPDATE_TIMEOUT) {
            final Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Utils.getCountryCode();
                    final Object cuisines = new ANetworkRequest(new CuisinesRequest()).execute();
                    final Object ingredients = new ANetworkRequest(new IngredientsRequest()).execute();

                    if (cuisines != null && ingredients != null) {
                        sInstance.setLastStaticTime(System.currentTimeMillis());
                    }
                }
            });
            thread.setPriority(Thread.MIN_PRIORITY);
            thread.start();
        }
    }

    @Nullable
    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(@Nullable String countryCode) {
        mCountryCode = countryCode;
        mPreferences.edit().putString(KEY_6, mCountryCode).apply();
    }
}