package com.foodridge.request;

import android.content.ContentUris;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v4.util.SparseArrayCompat;

import com.foodridge.R;
import com.foodridge.api.model.ServerError;
import com.foodridge.api.request.BaseRequestEntity;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.model.DbRequestEntity;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData.RequestEntityContract;
import com.foodridge.type.RequestEntityStatus;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19.03.2016
 */
public class RequestCenter {

    public static final int CODE_ANSWER = R.id.code_request_answer;
    public static final int CODE_ERROR = R.id.code_request_error;

    //BASE
    private static RequestCenter sInstance;

    //VALUE's
    private final InnerHandler mHandler;
    private final SparseArrayCompat<Boolean> mCancelable;
    private final SparseArrayCompat<WeakReference<ObjectsReceiver>> mStack;
    private final SparseArrayCompat<WeakReference<ObjectsReceiver>> mListeners;

    public static void init(@NonNull Looper looper) {
        sInstance = new RequestCenter(looper);
    }

    private RequestCenter(@NonNull Looper looper) {
        mHandler = new InnerHandler(looper);
        mStack = new SparseArrayCompat<>();
        mListeners = new SparseArrayCompat<>();
        mCancelable = new SparseArrayCompat<>();
    }

    public static void place(@NonNull BaseRequestEntity entity, @Nullable ObjectsReceiver receiver) {
        placeDelayed(entity, receiver, 0);
    }

    public static void placeDelayed(@NonNull BaseRequestEntity entity, @Nullable ObjectsReceiver receiver, long delayMillis) {
        sInstance.placeInner(entity, receiver, delayMillis);
    }

    public static void cancel(@NonNull ObjectsReceiver receiver) {
        sInstance.cancelInner(receiver);
    }

    public static boolean receive(@Nullable BaseRequestEntity entity) {
        return entity != null && sInstance.receiveInner(entity);
    }

    public static void internetChanged(int dbId, boolean internetAvailable) {
        sInstance.internetChangedInner(dbId, internetAvailable);
    }

    private void placeInner(@NonNull BaseRequestEntity entity, @Nullable ObjectsReceiver receiver, long delayMillis) {
        WeakReference<ObjectsReceiver> weakReceiver = null;
        if (receiver != null) {
            final int key = receiver.hashCode();
            weakReceiver = mStack.get(key);
            if (weakReceiver == null) {
                weakReceiver = new WeakReference<>(receiver);
                mStack.put(key, weakReceiver);
            }
        }
        mHandler.place(entity, weakReceiver, delayMillis);
    }

    private void cancelInner(@NonNull ObjectsReceiver receiver) {
        mHandler.cancel(receiver);
    }

    private boolean receiveInner(@NonNull BaseRequestEntity entity) {
        final WeakReference<ObjectsReceiver> weakReceiver = mListeners.get(entity.getDbId());
        final ObjectsReceiver receiver = weakReceiver == null ? null : weakReceiver.get();
        if (receiver == null) {
            return false;

        } else {
            final ServerError error = entity.getError();
            if (error == null) {
                receiver.onObjectsReceive(CODE_ANSWER, entity.getReceiverCode(), entity.getDataAndRemove());

            } else {
                receiver.onObjectsReceive(CODE_ERROR, entity.getReceiverCode(), error);
            }
            return true;
        }
    }

    private void internetChangedInner(int dbId, boolean internetAvailable) {
        final WeakReference<ObjectsReceiver> weakReference = mListeners.get(dbId);
        final ObjectsReceiver receiver = weakReference == null ? null : weakReference.get();
        if (receiver != null) {
            receiver.onObjectsReceive(R.id.code_block, internetAvailable);
        }
    }

    private static final class InnerHandler extends Handler {

        private static final int WHAT_PLACE = 0;
        private static final int WHAT_CANCEL = 1;

        public InnerHandler(@NonNull Looper looper) {
            super(looper);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_PLACE:
                    final Pair pair = Pair.class.cast(msg.obj);
                    final BaseRequestEntity entity = BaseRequestEntity.class.cast(pair.first);
                    final WeakReference reference = WeakReference.class.cast(pair.second);

                    final Uri uri = AContext.getContentResolver().insert(RequestEntityContract.CONTENT_URI, DbRequestEntity.convert(entity));
                    final int id = (int) ContentUris.parseId(uri);

                    sInstance.mCancelable.put(id, entity.isCancelable());
                    if (pair.second != null) {
                        sInstance.mListeners.put(id, reference);
                    }
                    break;

                case WHAT_CANCEL:
                    final SparseArrayCompat<WeakReference<ObjectsReceiver>> listeners = sInstance.mListeners;
                    final SparseArrayCompat<Boolean> cancelable = sInstance.mCancelable;
                    final ObjectsReceiver receiver = (ObjectsReceiver) msg.obj;

                    final List<Integer> ids2Cancel = new ArrayList<>();
                    WeakReference<ObjectsReceiver> weakReference;
                    ObjectsReceiver weakReceiver;
                    int entityId;

                    for (int i = 0, size = listeners.size(); i < size; i++) {
                        weakReference = listeners.valueAt(i);
                        if (weakReference != null) {
                            weakReceiver = weakReference.get();
                            if (weakReceiver != null && weakReceiver.equals(receiver)) {
                                entityId = listeners.keyAt(i);
                                if (Boolean.TRUE.equals(cancelable.get(entityId))) {
                                    ids2Cancel.add(entityId);
                                    listeners.removeAt(i);
                                }
                            }
                        }
                    }
                    if (ids2Cancel.size() > 0) {
                        final ContentValues cv = new ContentValues(1);
                        cv.put(RequestEntityContract.STATUS, RequestEntityStatus.CANCELED);

                        AContext.getContentResolver().update(RequestEntityContract.CONTENT_URI, cv
                                , ContentHelper.in(RequestEntityContract._ID, ids2Cancel), null);
                    }
                    break;

                default:
                    super.handleMessage(msg);
                    break;
            }
        }

        public void place(@NonNull BaseRequestEntity entity, @Nullable WeakReference<ObjectsReceiver> receiver, long delayMillis) {
            final Message message = obtainMessage();
            message.obj = new Pair<>(entity, receiver);
            message.what = WHAT_PLACE;

            super.sendMessageDelayed(message, delayMillis);
        }

        public void cancel(@NonNull ObjectsReceiver receiver) {
            final Message message = obtainMessage();
            message.what = WHAT_CANCEL;
            message.obj = receiver;

            super.sendMessage(message);
        }
    }
}