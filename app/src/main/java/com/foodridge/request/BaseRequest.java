package com.foodridge.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.foodridge.utility.Log;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 24/12/15
 */
public abstract class BaseRequest<RESULT> extends SpiceRequest<RESULT> {

    protected static final Log LOG = new Log("requestLog");

    private static final DefaultRetryPolicy RETRY_POLICY = new DefaultRetryPolicy(1, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    protected BaseRequest(@NonNull Class<RESULT> result) {
        super(result);
        super.setRetryPolicy(RETRY_POLICY);
    }

    @Nullable
    @Override
    public abstract RESULT loadDataFromNetwork() throws Exception;

    public abstract boolean isNetworkRequired();

    @WorkerThread
    @Nullable
    public final RESULT execute() {
        try {
            return loadDataFromNetwork();

        } catch (Throwable e) {
            LOG.toLog(e);
        }
        return null;
    }

    /**
     * {@link java.lang.Thread#MAX_PRIORITY}, {@link java.lang.Thread#NORM_PRIORITY}, {@link java.lang.Thread#MIN_PRIORITY}
     */
    public final void oneShot(int priority) {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                execute();
            }
        });
        thread.setPriority(priority);
        thread.start();
    }
}