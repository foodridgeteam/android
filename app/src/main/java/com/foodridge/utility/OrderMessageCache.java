package com.foodridge.utility;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.SimpleArrayMap;

import com.foodridge.api.model.OrderMessage;
import com.foodridge.application.AContext;
import com.foodridge.model.DbMessage;
import com.foodridge.provider.FoodridgeMetaData.MessageContract;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 03/05/16
 */
public class OrderMessageCache {

    private static OrderMessageCache sInstance;

    private final SimpleArrayMap<String, ArrayList<OrderMessage>> mCache;

    @NonNull
    public static OrderMessageCache getInstance() {
        if (sInstance == null) {
            synchronized (OrderMessageCache.class) {
                if (sInstance == null) {
                    sInstance = new OrderMessageCache();
                }
            }
        }
        return sInstance;
    }

    private OrderMessageCache() {
        mCache = new SimpleArrayMap<>();
    }

    public synchronized void put(@NonNull String orderId, @Nullable OrderMessage message) {
        ArrayList<OrderMessage> messages = mCache.get(orderId);
        if (message == null) {
            mCache.remove(orderId);
            return;

        } else if (messages == null) {
            messages = new ArrayList<>();
            mCache.put(orderId, messages);
        }
        boolean add = true;
        for (OrderMessage m : messages) {
            if (m.equals(message)) {
                add = false;
                break;
            }
        }
        if (add) {
            messages.add(message);
        }
        Collections.sort(messages);
    }

    @WorkerThread
    public synchronized void put(@NonNull String orderId, @NonNull OrderMessage[] messagesArray) {
        ArrayList<OrderMessage> messages = mCache.get(orderId);
        if (messages == null) {
            messages = new ArrayList<>(messagesArray.length);
            mCache.put(orderId, messages);
        }
        boolean add;
        for (OrderMessage m : messagesArray) {
            add = true;
            for (OrderMessage om : messages) {
                if (om.equals(m)) {
                    add = false;
                    break;
                }
            }
            if (add) {
                messages.add(m);
            }
        }
        Collections.sort(messages);
    }

    @Nullable
    public ArrayList<OrderMessage> find(@NonNull String orderId) {
        return mCache.get(orderId);
    }

    @WorkerThread
    public void getFromDb() {
        Cursor c = null;
        try {
            c = AContext.getContentResolver().query(MessageContract.CONTENT_URI, null, null, null, null, null);
            if (c != null && c.moveToFirst()) {
                final DbMessage dbMessage = new DbMessage();
                do {
                    dbMessage.fromCursor(c);

                    final String orderId = dbMessage.getOrderId();

                    ArrayList<OrderMessage> messages = mCache.get(orderId);
                    if (messages == null) {
                        messages = new ArrayList<>();
                        mCache.put(orderId, messages);
                    }
                    messages.add(dbMessage.getEntity());

                } while (c.moveToNext());

                for (int i = 0, size = mCache.size(); i < size; i++) {
                    Collections.sort(mCache.valueAt(i));
                }
            }

        } finally {
            Utils.closeSilently(c);
        }
    }

    public void clear() {
        mCache.clear();
    }
}