package com.foodridge.utility;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;

import com.foodridge.R;
import com.foodridge.application.AContext;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19/04/16
 */
public class TimeCache {

    private static final SimpleDateFormat SERVER_UTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    private static final SimpleDateFormat EEE_DD_MMM_HH_MM = new SimpleDateFormat("EEE, dd MMM, hh:mma", Locale.ENGLISH);
    private static final SimpleDateFormat EEE_DD_MMM_HH_MM24 = new SimpleDateFormat("EEE, dd MMM, HH:mm", Locale.ENGLISH);

    public static final DateFormat DD_MMM_HH_MM = new SimpleDateFormat("dd MMM, '<b>'hh:mma'</b>'", Locale.ENGLISH);
    public static final DateFormat DD_MMM_HH_MM24 = new SimpleDateFormat("dd MMM, '<b>'HH:mm'</b>'", Locale.ENGLISH);

    public static final DateFormat HH_MM = new SimpleDateFormat("hh:mma", Locale.ENGLISH);
    public static final DateFormat HH_MM24 = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

    public static final DateFormat HTML_HH_MM = new SimpleDateFormat("'<b>'hh:mm'</b>' a", Locale.US);
    public static final DateFormat HTML_HH_MM24 = new SimpleDateFormat("'<b>'HH:mm'</b>'", Locale.US);

    public static final DateFormat DD_MMM_YYYY = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);

    static {
        SERVER_UTC.setTimeZone(TimeZone.getTimeZone("SERVER_UTC"));
    }

    private static final Object LOCK = new Object();
    private static final long SOME_TIME = 946_728_000_000L;//Sat, 01 Jan 2000 12:00:00 GMT
    private static final long REQUEST_TIMEOUT = 60_000;

    private static TimeCache sInstance;
    private final SimpleArrayMap<String, Long> mTimeMillis;
    private final Calendar mCalendar;
    private final Date mDate;

    private boolean m24Hours;
    private long mLast24HoursRequest;

    @NonNull
    public static TimeCache getInstance() {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = new TimeCache();
                }
            }
        }
        return sInstance;
    }

    private TimeCache() {
        mTimeMillis = new SimpleArrayMap<>();
        mCalendar = Calendar.getInstance();
        mDate = new Date();

        mLast24HoursRequest = System.currentTimeMillis();
        m24Hours = AContext.is24HourFormat();
    }

    public long getTime(@NonNull String timeString) {
        synchronized (LOCK) {
            Long time = mTimeMillis.get(timeString);
            if (time == null) {
                try {
                    time = SERVER_UTC.parse(timeString).getTime();
                    mTimeMillis.put(timeString, time);

                } catch (ParseException e) {
                    throw new IllegalStateException();
                }
            }
            return time;
        }
    }

    @NonNull
    public String format(@NonNull DateFormat format, int hourOfDay, int minute) {
        synchronized (LOCK) {
            mCalendar.setTimeInMillis(0);

            mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            mCalendar.set(Calendar.MINUTE, minute);

            mDate.setTime(mCalendar.getTimeInMillis());

            return format.format(mDate);
        }
    }

    @NonNull
    public String getCuteTime(@Nullable String timeString) {
        return getCuteTime(getCuteFormat(), timeString);
    }

    @NonNull
    public String getCuteTime(@NonNull DateFormat format, @Nullable String timeString) {
        final long key = timeString == null ? SOME_TIME : getTime(timeString);

        synchronized (LOCK) {
            mDate.setTime(key);
            return format.format(mDate);
        }
    }

    @NonNull
    public String getVisitedAt(@Nullable String timeString) {
        return timeString == null ? AContext.getString(R.string.Offline) : AContext.getStringArgs(R.string.format_Visited_at_s, getCuteTime(timeString));
    }

    @NonNull
    private DateFormat getCuteFormat() {
        return is24HourCached() ? EEE_DD_MMM_HH_MM24 : EEE_DD_MMM_HH_MM;
    }

    public boolean is24HourCached() {
        final long currentTimeMillis = System.currentTimeMillis();
        final long difference = currentTimeMillis - mLast24HoursRequest;
        if (difference > REQUEST_TIMEOUT) {
            mLast24HoursRequest = currentTimeMillis;
            m24Hours = AContext.is24HourFormat();
        }
        return m24Hours;
    }
}