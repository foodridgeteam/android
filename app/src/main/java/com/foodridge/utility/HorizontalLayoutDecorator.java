package com.foodridge.utility;

import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

import com.foodridge.application.AContext;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 28/12/15
 */
public class HorizontalLayoutDecorator extends ItemDecoration {

    private final int mSpacing;

    public HorizontalLayoutDecorator(@DimenRes int dimenRes) {
        mSpacing = AContext.getResources().getDimensionPixelSize(dimenRes);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
        final int position = parent.getChildAdapterPosition(view);

        outRect.top = mSpacing;
        outRect.bottom = mSpacing;
        if (position == 0) {
            outRect.left = mSpacing;
            outRect.right = mSpacing / 2;

        } else if (position + 1 == parent.getChildCount()) {
            outRect.left = mSpacing / 2;
            outRect.right = mSpacing;

        } else {
            outRect.left = outRect.right = mSpacing / 2;
        }
    }
}