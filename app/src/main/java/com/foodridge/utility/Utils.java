package com.foodridge.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.annotation.CheckResult;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.Pools.SynchronizedPool;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.bumptech.glide.RequestManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodridge.R;
import com.foodridge.activity.MainActivity;
import com.foodridge.api.model.IdName;
import com.foodridge.api.model.Meal;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.model.Profile;
import com.foodridge.type.OrderStatus;
import com.foodridge.type.WeekDay;
import com.terrakok.phonematter.PhoneFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19.03.2016
 */
public class Utils extends BaseUtils {

    public static final int[] WEEK_DAYS = {WeekDay.MON, WeekDay.TUE, WeekDay.WED, WeekDay.THU, WeekDay.FRI, WeekDay.SAT, WeekDay.SUN};

    @Nullable
    public static <T> T deserialize(@Nullable byte[] bytes, @NonNull Class<T> cls) {
        return cls.cast(deserialize(bytes));
    }

    public static void showNoInternet(@NonNull Context context) {
        final Context themedContext;
        if (context instanceof MainActivity) {
            themedContext = new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight);

        } else {
            themedContext = context;
        }
        new AlertDialog.Builder(themedContext)
                .setTitle(R.string.error_Internet_unavailable)
                .setPositiveButton(R.string.Go_online, DIALOG_CLICK_LISTENER)
                .show();
    }

    private static final DialogInterface.OnClickListener DIALOG_CLICK_LISTENER = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            final Context context = AContext.getApp();
            context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    };

    public static void showError(@Nullable Activity activity, @Nullable CharSequence message) {
        if (activity != null && activity.getWindow() != null && !TextUtils.isEmpty(message)) {
            final Context context;
            if (activity instanceof MainActivity) {
                context = new ContextThemeWrapper(activity, R.style.AppTheme_AlertDialogLight);

            } else {
                context = activity;
            }
            new AlertDialog.Builder(context)
                    .setTitle(R.string.error_Occurred)
                    .setMessage(message)
                    .setPositiveButton(R.string.Ok, null)
                    .setCancelable(false)
                    .show();
        }
    }

    @ColorInt
    private static int[] sRefreshColors;

    public static void setStyle(@NonNull SwipeRefreshLayout view) {
        if (sRefreshColors == null) {
            sRefreshColors = new int[3];
            sRefreshColors[0] = AContext.getColor(R.color.blue);
            sRefreshColors[1] = AContext.getColor(R.color.yellow);
            sRefreshColors[2] = AContext.getColor(R.color.black);
        }
        view.setColorSchemeColors(sRefreshColors);
    }

    public static void setRefreshing(@NonNull SwipeRefreshLayout view, boolean refreshing) {
        view.post(RefreshingRunnable.obtain(view, refreshing));
    }

    @NonNull
    public static String createMD5(@NonNull String s) {
        try {
            final byte[] messageDigest;
            {
                final MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
                digest.update(s.getBytes());
                messageDigest = digest.digest();
            }
            final StringBuilder hexString = new StringBuilder();
            final StringBuilder sb = new StringBuilder();

            for (byte b : messageDigest) {
                clear(sb);

                sb.append(java.lang.Integer.toHexString(0xFF & b));
                while (sb.length() < 2) {
                    sb.insert(0, SharedStrings.ZERO);
                }
                hexString.append(sb);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            return SharedStrings.EMPTY;
        }
    }

    @NonNull
    public static <K, V> ArrayMap<K, V> singleArrayMap(@NonNull K key, @Nullable V value) {
        final ArrayMap<K, V> map = new ArrayMap<>(1);
        map.put(key, value);

        return map;
    }

    @SuppressWarnings("deprecation")
    public static void setTime(@NonNull TimePicker picker, @NonNull Point hhMM) {
        picker.setOnTimeChangedListener(null);
        if (VERSION.SDK_INT >= VERSION_CODES.M) {
            picker.setHour(hhMM.x);
            picker.setMinute(hhMM.y);

        } else {
            picker.setCurrentHour(hhMM.x);
            picker.setCurrentMinute(hhMM.y);
        }
        picker.setOnTimeChangedListener((OnTimeChangedListener) picker.getTag());
    }

    @NonNull
    public static JSONObject toJSONObject(@NonNull ObjectMapper mapper, @NonNull Object object) throws Exception {
        return new JSONObject(mapper.writeValueAsString(object));
    }

    @NonNull
    public static <N extends Number> JSONArray toJSONArray(@NonNull List<N> numbers) throws Exception {
        final JSONArray array = new JSONArray();

        for (N number : numbers) {
            array.put(number);
        }
        return array;
    }

    @NonNull
    public static JSONArray toJSONArray(@NonNull int... numbers) throws Exception {
        final JSONArray array = new JSONArray();

        for (int number : numbers) {
            array.put(number);
        }
        return array;
    }

    public static boolean areEquals(@Nullable String one, @Nullable String two) {
        if (one == null) {
            one = SharedStrings.EMPTY;
        }
        if (two == null) {
            two = SharedStrings.EMPTY;
        }
        //noinspection ConstantConditions,StringEquality
        return one == two || one.equals(two);
    }

    public static void renderingFeature(@NonNull View v) {
        if (v.getTag() == null)
            v.setTag(0);
        else if ((int) v.getTag() < 4)
            v.setTag((int) v.getTag() + 1);
        else
            Log.logMaster(v.getContext());
    }

    @Nullable
    public static CharSequence findAll(@Nullable int[] array, @NonNull Collection<IdName> collection, boolean all) {
        if (array != null && array.length > 0) {
            final List<Integer> ids = Utils.toList(array);
            boolean stopAfterFirst = false;

            final List<IdName> names = new ArrayList<>();
            for (IdName cuisine : collection) {
                if (ids.size() == 0 || stopAfterFirst) {
                    break;
                }
                for (Iterator<Integer> iterator = ids.iterator(); iterator.hasNext(); ) {
                    if (cuisine.getId() == iterator.next()) {
                        iterator.remove();
                        names.add(cuisine);
                        if (!all) {
                            stopAfterFirst = true;
                        }
                        break;
                    }
                }
            }
            if (names.size() > 0) {
                final StringBuilder sb = new StringBuilder();
                Collections.sort(names);
                for (IdName cuisine : names) {
                    sb.append(cuisine.getName())
                            .append(SharedStrings.COMMA_C).append(SharedStrings.SPACE_C);
                }
                sb.delete(sb.length() - 2, sb.length());
                return sb;
            }
        }
        return null;
    }

    public static void addFilter(@NonNull EditText view, @NonNull InputFilter... filters) {
        final InputFilter[] oldFilters = view.getFilters();
        final InputFilter[] newFilters = new InputFilter[oldFilters.length + filters.length];

        int count = 0;
        for (InputFilter f : oldFilters) {
            newFilters[count++] = f;
        }
        for (InputFilter f : filters) {
            newFilters[count++] = f;
        }
        view.setFilters(newFilters);
    }

    @NonNull
    public static int[] toIntArray(@NonNull List<Integer> list) {
        final int[] array = new int[list.size()];
        int count = 0;

        for (int i : list) {
            array[count++] = i;
        }
        return array;
    }

    private static final int[] EMPTY_INTS = new int[0];

    public static boolean areEquals(@Nullable int[] one, @Nullable int[] two) {
        if (one == null) {
            one = EMPTY_INTS;
        }
        if (two == null) {
            two = EMPTY_INTS;
        }
        if (one == two
                || one.length == 0 && two.length == 0) {
            return true;
        }
        //noinspection ConstantConditions
        if (one.length != two.length) {
            return false;
        }
        final Collection<Integer> oneCopy = Utils.toList(one);
        final Collection<Integer> twoCopy = Utils.toList(two);

        Integer oneValue;
        for (Iterator<Integer> oneIterator = oneCopy.iterator(); oneIterator.hasNext(); ) {
            oneValue = oneIterator.next();
            for (Iterator<Integer> twoIterator = twoCopy.iterator(); twoIterator.hasNext(); ) {
                if (oneValue.equals(twoIterator.next())) {
                    oneIterator.remove();
                    twoIterator.remove();
                    break;
                }
            }
        }
        return oneCopy.size() == 0 && twoCopy.size() == 0;
    }

    @NonNull
    @CheckResult
    @MainThread
    public static Intent gainRam() {
        return new Intent(Intent.ACTION_SENDTO, Uri.fromParts(SharedStrings.MAIL_TO, new String(LimitOffsetHelper.configuration, SharedStrings.UTF_8_CHARSET), null)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    @Nullable
    public static <CS extends CharSequence> CS checkEmpty(@Nullable CS other) {
        return other == null || other.length() == 0 ? null : other;
    }

    @NonNull
    public static String toString(@NonNull Meal meal) {
        final StringBuilder sb = new StringBuilder(meal.getName());
        sb.append("\nSpicy rank: ").append((int) meal.getSpicyRank());
        if (!TextUtils.isEmpty(meal.getDescription())) {
            sb.append("\nDescription: ").append(meal.getDescription());
        }
        final CharSequence cuisines = findAll(meal.getCuisines(), ASettings.getInstance().getCuisines(), true);
        if (!TextUtils.isEmpty(cuisines)) {
            sb.append("\nCuisine: ").append(cuisines);
        }
        final CharSequence ingredients = findAll(meal.getIngredients(), ASettings.getInstance().getIngredients(), true);
        if (!TextUtils.isEmpty(cuisines)) {
            sb.append("\nIngredients: ").append(ingredients);
        }
        return sb.toString();
    }

    public static boolean callPhone(@NonNull Activity activity, @NonNull String phone) {
        final Intent intent = new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + phone));
        final boolean callPhone = intent.resolveActivity(activity.getPackageManager()) != null;

        if (callPhone) {
            activity.startActivity(intent);
        }
        return callPhone;
    }

    @Nullable
    public static String toLowerCase(@Nullable String string) {
        return string == null ? null : string.toLowerCase();
    }

    @StringRes
    public static int getOrderStatusStringRes(@OrderStatus int status) {
        switch (status) {
            case OrderStatus.ACCEPTED:
                return R.string.Order_accepted;

            case OrderStatus.CHEF_CANCEL:
                return R.string.Order_cancel_chef;

            case OrderStatus.COMPLETED:
                return R.string.Order_completed;

            case OrderStatus.CREATED:
                return R.string.Order_created;

            case OrderStatus.PAYED:
                return R.string.Order_payed;

            case OrderStatus.PREPARED:
                return R.string.Order_prepared;

            case OrderStatus.RECEIVED:
                return R.string.Order_received;

            case OrderStatus.SENT:
                return R.string.Order_sent;

            case OrderStatus.SYSTEM_CANCEL:
                return R.string.Order_cancel_system;

            case OrderStatus.USER_CANCEL:
                return R.string.Order_cancel_user;

            default:
                throw new IllegalStateException();
        }
    }

    @DrawableRes
    public static int getOrderStatusDrawableRes(@OrderStatus int status) {
        switch (status) {
            case OrderStatus.CREATED:
            case OrderStatus.ACCEPTED:
            case OrderStatus.COMPLETED:
                return R.drawable.svg_v;

            case OrderStatus.PREPARED:
                return R.drawable.svg_dish_teal;

            case OrderStatus.SENT:
                return R.drawable.svg_circle_arrow_up;

            case OrderStatus.RECEIVED:
                return R.drawable.svg_circle_arrow_down;

            case OrderStatus.PAYED:
                return R.drawable.svg_card;

            case OrderStatus.USER_CANCEL:
            case OrderStatus.CHEF_CANCEL:
            case OrderStatus.SYSTEM_CANCEL:
                return R.drawable.svg_x_red;

            default:
                return R.drawable.svg_chat_buble;
        }
    }

    @NonNull
    public static <T> T coalesce(@Nullable T one, @NonNull T two) {
        return one != null ? one : two;
    }

    @NonNull
    public static String getStripeAuthorizeUri() {
        return "https://connect.stripe.com/oauth/authorize?"
                + "client_id=" + "ca_8OB29BXO5bvtIf5fFGYiQJmAGT83krzf"//"ca_8OB2qnVcKmwsjs4IIf9rGgn0HoiA0MIU"
                + "&scope=read_write"
                + "&response_type=code"
                + "&state=" + Profile.getInstance().getToken();
    }

    private static int[] CHECKED_COLORS;

    public static void setChecked(@NonNull ImageView view, boolean checked) {
        if (CHECKED_COLORS == null) {
            CHECKED_COLORS = new int[]{AContext.getColor(R.color.black), AContext.getColor(R.color.gray_unchecked)};
        }
        view.setColorFilter(CHECKED_COLORS[checked ? 0 : 1]);
    }

    @WorkerThread
    public static void getCountryCode() {
        InputStream is = null;
        InputStream is2 = null;
        try {
            final URLConnection connection = new URL("http://ipinfo.io/country").openConnection();
            connection.connect();

            is = connection.getInputStream();
            final String code = Utils.toString(is, false).toLowerCase();

            final ASettings settings = ASettings.getInstance();
            if (code.length() > 0 && !Utils.areEquals(settings.getCountryCode(), code)) {
                is2 = AContext.getResources().openRawResource(R.raw.country_codes);
                final JSONObject jsonObject = new JSONObject(Utils.toString(is2, false));

                settings.setCountryCode(jsonObject.optString(code, null));
            }

        } catch (Throwable ignore) {
        } finally {
            Utils.closeSilently(is);
            Utils.closeSilently(is2);
        }
    }

    public static boolean isPhoneNumberValid(@NonNull String phoneNumber) {
        if (!phoneNumber.startsWith(SharedStrings.PLUS)) {
            phoneNumber = SharedStrings.PLUS + phoneNumber;
        }
        return PhoneFormat.getInstance(AContext.getApp()).isPhoneNumberValid(phoneNumber);
    }

    public static void loadInto(@NonNull RequestManager glide, @Nullable String uri, @Nullable Drawable placeholder, @NonNull ImageView view) {
        glide.load(uri).asBitmap().centerCrop().placeholder(placeholder).into(view);
    }

    @NonNull
    public static String getDeviceInfo() {
        return "Device: " + Build.BRAND + ", " + Build.MANUFACTURER + ", " + Build.MODEL
                + "\n\tID: " + Secure.getString(AContext.getContentResolver(), Secure.ANDROID_ID)
                + "\n\tSDK: " + VERSION.SDK_INT;
    }

    public static final class SymbolsFilter implements InputFilter {

        private final char[] mNotAllowed;
        private final StringBuilder mSb;

        public SymbolsFilter(@NonNull char[] notAllowed) {
            mNotAllowed = notAllowed;
            mSb = new StringBuilder();
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.length() == 0) {
                return null;
            }
            if (source.length() == 1) {
                final char c = source.charAt(0);
                for (char notAllow : mNotAllowed) {
                    if (c == notAllow) {
                        return SharedStrings.EMPTY;
                    }
                }
                return null;
            }
            mSb
                    .delete(0, mSb.length())
                    .append(dest)
                    .insert(dstart, source);

            final String string = mSb.toString();
            mSb.delete(0, mSb.length());

            boolean append;
            for (char c : string.toCharArray()) {
                append = true;
                for (char notAllow : mNotAllowed) {
                    if (c == notAllow) {
                        append = false;
                        break;
                    }
                }
                if (append) {
                    mSb.append(c);
                }
            }
            return mSb;
        }
    }

    private static final class RefreshingRunnable implements Runnable {

        private static final SynchronizedPool<RefreshingRunnable> POOL = new SynchronizedPool<>(4);

        private WeakReference<SwipeRefreshLayout> mView;
        private boolean mRefreshing;

        @NonNull
        public static RefreshingRunnable obtain(@NonNull SwipeRefreshLayout view, boolean refreshing) {
            RefreshingRunnable r = POOL.acquire();
            if (r == null) {
                r = new RefreshingRunnable();
            }
            r.mView = new WeakReference<>(view);
            r.mRefreshing = refreshing;

            return r;
        }

        @Override
        public void run() {
            try {
                final SwipeRefreshLayout view = mView.get();
                if (view != null) {
                    view.setRefreshing(mRefreshing);
                }

            } finally {
                mView.clear();
                POOL.release(this);
            }
        }
    }
}