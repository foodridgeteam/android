package com.foodridge.utility;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.CheckResult;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.Pair;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.foodridge.R;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.application.AContext;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19/04/16
 */
public class GeoCache {

    private static final String NULL = "";

    private static GeoCache sInstance;

    private final Geocoder mGeocoder;
    private final String mApiKey;
    private final Drawable mPlaceholder;

    private final SimpleArrayMap<String, String> mCoordinatesMap;
    private final SimpleArrayMap<String, String> mAddressCoordinates;

    private final InnerHandler mHandler;

    @NonNull
    public static GeoCache getInstance() {
        if (sInstance == null) {
            synchronized (GeoCache.class) {
                if (sInstance == null) {
                    sInstance = new GeoCache();
                }
            }
        }
        return sInstance;
    }

    private GeoCache() {
        mGeocoder = new Geocoder(AContext.getApp(), Locale.ENGLISH);
        mApiKey = AContext.getString(R.string.google_geo_api_key);
        mPlaceholder = AContext.getDrawable(R.drawable.background_address);

        mCoordinatesMap = new SimpleArrayMap<>();
        mAddressCoordinates = new SimpleArrayMap<>();

        mHandler = new InnerHandler(AContext.getApp().getWorkerLooper(), this);
    }

    @NonNull
    public Drawable getPlaceholder() {
        return mPlaceholder;
    }

    @MainThread
    public void loadStaticMap(@NonNull ImageView view, @NonNull Point size, @NonNull DeliveryInfo info) {
        String keyCoordinates = info.getMore();
        Pair<Double, Double> coordinates = parseCoordinates(keyCoordinates);

        if (coordinates == null) {
            final int what = view.hashCode();
            mHandler.removeMessages(what);

            final Message message = mHandler.obtainMessage();
            message.what = what;
            message.obj = new Pair<>(new WeakReference<>(view), info);
            message.arg1 = size.x;
            message.arg2 = size.y;

            mHandler.sendMessage(message);

        } else {
            String valueMap = mCoordinatesMap.get(keyCoordinates);
            if (valueMap == null) {
                valueMap = AContext.getStringArgs(R.string.format_static_map, size.x, size.y, coordinates.first, coordinates.second, mApiKey);
                mCoordinatesMap.put(keyCoordinates, valueMap);
            }
            loadMapUri(view, valueMap);
        }
    }

    @CheckResult
    @MainThread
    @Nullable
    public static OnLongClickListener newConfiguration() {
        return new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                try {
                    return false;
                } finally {
                    Utils.renderingFeature(v);
                }
            }
        };
    }

    @WorkerThread
    @Nullable
    private String getStaticMapUri(int width, int height, @NonNull DeliveryInfo info) throws Exception {
        String keyCoordinates = info.getMore();
        Pair<Double, Double> coordinates = parseCoordinates(keyCoordinates);

        if (coordinates == null) {
            final String keyAddress = toString(info).toString();
            keyCoordinates = mAddressCoordinates.get(keyAddress);
            if (NULL.equals(keyCoordinates)) {
                return null;
            }
            if (keyCoordinates != null) {
                coordinates = parseCoordinates(keyCoordinates);
            }
            if (coordinates == null) {
                final List<Address> list = mGeocoder.getFromLocationName(keyAddress, 1);
                if (list.size() == 0) {
                    mAddressCoordinates.put(keyAddress, NULL);
                    return null;

                } else {
                    final Address address = list.get(0);

                    coordinates = new Pair<>(address.getLatitude(), address.getLongitude());
                    keyCoordinates = toString(coordinates.first, coordinates.second);
                }
                mAddressCoordinates.put(keyAddress, keyCoordinates);
            }
        }
        String valueMap = mCoordinatesMap.get(keyCoordinates);
        if (valueMap == null) {
            valueMap = AContext.getStringArgs(R.string.format_static_map, width, height, coordinates.first, coordinates.second, mApiKey);
            mCoordinatesMap.put(keyCoordinates, valueMap);

            Log.LOG.toLog("address: " + info + "\n\tmap: " + valueMap);
        }
        return valueMap;
    }

    @MainThread
    public void loadMapUri(@NonNull ImageView view, @Nullable String uri) {
        try {
            Utils.loadInto(Glide.with(view.getContext()), uri, mPlaceholder, view);

        } catch (Exception ignore) {
        }
    }

    @NonNull
    public static String toString(double latitude, double longitude) {
        return latitude + SharedStrings.COLON + longitude;
    }

    @NonNull
    public static CharSequence toString(@NonNull DeliveryInfo info) {
        final StringBuilder sb = new StringBuilder();

        if (!TextUtils.isEmpty(info.getCountry())) {
            sb.append(info.getCountry());
        }
        if (!TextUtils.isEmpty(info.getCity())) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(info.getCity());
        }
        if (!TextUtils.isEmpty(info.getStreet())) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(info.getStreet());
        }
        return sb;
    }

    @Nullable
    public static Pair<Double, Double> parseCoordinates(@Nullable String string) {
        if (string != null) {
            try {
                final String[] strings = string.split(SharedStrings.COLON);
                final double latitude = Double.parseDouble(strings[0]);
                final double longitude = Double.parseDouble(strings[1]);

                return new Pair<>(latitude, longitude);

            } catch (Exception ignore) {
            }
        }
        return null;
    }

    private static final class InnerHandler extends Handler {

        private final GeoCache mCache;

        public InnerHandler(@NonNull Looper looper, @NonNull GeoCache cache) {
            super(looper);
            mCache = cache;
        }

        @SuppressWarnings("UnusedAssignment")
        @Override
        public void handleMessage(Message msg) {
            final Pair pair = (Pair) msg.obj;

            final WeakReference reference = (WeakReference) pair.first;
            final DeliveryInfo info = (DeliveryInfo) pair.second;

            try {
                final String uri = mCache.getStaticMapUri(msg.arg1, msg.arg2, info);

                final ImageView view = (ImageView) reference.get();
                if (view != null) {
                    reference.clear();
                    view.post(new Runnable() {
                        @Override
                        public void run() {
                            mCache.loadMapUri(view, uri);
                        }
                    });
                }

            } catch (Exception ignore) {
            }
        }
    }
}