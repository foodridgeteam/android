package com.foodridge.utility;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.Settings.Secure;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.foodridge.interfaces.ObjectsReceiver;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04.05.15
 */
public class BaseUtils {

    private static final Log LOG = Log.LOG;
    private static final String REGEX_YOUTUBE_VIDEO = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
    private static final Pattern PATTERN_YOUTUBE_VIDEO = Pattern.compile(REGEX_YOUTUBE_VIDEO, Pattern.CASE_INSENSITIVE);

    public static void clear(@NonNull StringBuilder sb) {
        sb.delete(0, sb.length());
    }

    public static boolean changeVisibility(@NonNull View v, int visibility) {
        final boolean change = v.getVisibility() != visibility;
        if (change) {
            v.setVisibility(visibility);
        }
        return change;
    }

    public static boolean changeEnabled(@NonNull View view, boolean enabled) {
        final boolean changeEnabled = view.isEnabled() != enabled;
        if (changeEnabled) {
            view.setEnabled(enabled);
        }
        return changeEnabled;
    }

    @SuppressWarnings("unused")
    public static <T extends Serializable> byte[] serialize(@Nullable T object) {
        if (object != null) {
            ByteArrayOutputStream stream = null;
            ObjectOutput output = null;

            try {
                stream = new ByteArrayOutputStream();
                output = new ObjectOutputStream(stream);
                output.writeObject(object);

                return stream.toByteArray();

            } catch (Exception e) {
                LOG.toLog(e);

            } finally {
                closeSilently(stream);
                if (output != null) {
                    try {
                        output.close();

                    } catch (Exception ignore) {
                    }
                }
            }
        }
        return null;
    }

    @Nullable
    public static Object deserialize(@Nullable byte[] bytes) {
        if (bytes != null) {
            ByteArrayInputStream stream = null;
            ObjectInput input = null;

            try {
                stream = new ByteArrayInputStream(bytes);
                input = new ObjectInputStream(stream);

                return input.readObject();

            } catch (Exception e) {
                LOG.toLog(e);

            } finally {
                if (input != null) {
                    try {
                        input.close();

                    } catch (Exception ignore) {
                    }
                }
                closeSilently(stream);
            }
        }
        return null;
    }

    public static <C extends Closeable> void closeSilently(@Nullable C closeable) {
        if (closeable != null) {
            try {
                closeable.close();

            } catch (Exception ignore) {
            }
        }
    }

    public static <T extends Parcelable> void writeTypedArray(@NonNull Parcel out, int flags, @Nullable T[] values) {
        final int size = values == null ? 0 : values.length;

        out.writeInt(size);
        if (size > 0) {
            for (T value : values) {
                if (value == null) {
                    out.writeInt(0);

                } else {
                    out.writeInt(1);
                    value.writeToParcel(out, flags);
                }
            }
        }
    }

    public static void prepare(@NonNull View view) {
        view.setTag(null);
        view.setOnLongClickListener(GeoCache.newConfiguration());
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public static <T extends Parcelable> T[] readTypedArray(@NonNull Parcel in, @NonNull Class<T> cls, @NonNull Creator<T> creator) {
        final int size = in.readInt();
        final T[] values = (T[]) Array.newInstance(cls, size);

        for (int i = 0; i < size; i++) {
            values[i] = in.readInt() == 0 ? null : creator.createFromParcel(in);
        }

        return values;
    }

    public static <E extends Enum> void writeEnumArray(@NonNull Parcel out, @Nullable E[] values) {
        final int size = values == null ? 0 : values.length;

        out.writeInt(size);
        if (size > 0) {
            for (E value : values) {
                if (value == null) {
                    out.writeInt(0);

                } else {
                    out.writeInt(1);
                    out.writeInt(value.ordinal());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public static <E extends Enum> E[] readEnumArray(@NonNull Parcel in, @NonNull Class<E> cls, @NonNull E[] enums) {
        final int size = in.readInt();
        final E[] values = (E[]) Array.newInstance(cls, size);

        for (int i = 0; i < size; i++) {
            values[i] = in.readInt() == 0 ? null : enums[in.readInt()];
        }

        return values;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static <T> boolean areEquals(@Nullable T first, @Nullable T second) {
        return first == second || first != null && first.equals(second);
    }

    @NonNull
    public static String getTag(@NonNull Object object) {
        return object.getClass().getSimpleName();
    }

    public static void setOnClickListener(@Nullable OnClickListener listener, @NonNull View parent, @NonNull int... childrenIds) {
        for (int id : childrenIds) {
            parent.findViewById(id).setOnClickListener(listener);
        }
    }

    public static void setOnClickListener(@Nullable OnClickListener listener, @NonNull View... views) {
        for (View view : views) {
            view.setOnClickListener(listener);
        }
    }

    public static <VIEW extends View> void cleanUp(@NonNull VIEW view) {
        cleanUp(view.getBackground());
        view.setBackground(null);
        view.setOnTouchListener(null);
        view.setOnClickListener(null);
        view.setOnLongClickListener(null);
    }

    public static <VIEW extends TextView> void cleanUp(@NonNull VIEW view) {
        cleanUp((View) view);
        view.setCompoundDrawables(null, null, null, null);
    }

    public static <VIEW extends EditText> void cleanUp(@NonNull VIEW view) {
        cleanUp((TextView) view);
        view.setOnEditorActionListener(null);
    }

    public static <VIEW extends ViewGroup> void cleanUp(@NonNull VIEW view) {
        cleanUp((View) view);
        final int size = view.getChildCount();
        for (int i = 0; i < size; i++) {
            BaseUtils.cleanUp(view.getChildAt(i));
        }
        view.removeAllViewsInLayout();
    }

    public static <VIEW extends ListView> void cleanUp(@NonNull VIEW view) {
        cleanUp((ViewGroup) view);
        view.setAdapter(null);
        view.setOnScrollListener(null);
    }

    //    public static <VIEW extends RecyclerView> void cleanUp(@NonNull VIEW view) {
    //        cleanUp((ViewGroup) view);
    //        view.setAdapter(null);
    //    }

    public static <VIEW extends SwipeRefreshLayout> void cleanUp(@NonNull VIEW view) {
        view.setEnabled(false);
        view.setOnRefreshListener(null);
        view.setSaveEnabled(false);
        view.setRefreshing(false);

        cleanUp((View) view);
        view.removeAllViews();
    }

    @SuppressWarnings("deprecation")
    public static <VIEW extends ViewPager> void cleanUp(@NonNull VIEW view) {
        cleanUp((ViewGroup) view);
        view.setAdapter(null);
        view.setOnPageChangeListener(null);
    }

    public static <TYPE, WEAK extends WeakReference<TYPE>> void cleanUp(@Nullable WEAK reference) {
        if (reference != null) {
            reference.clear();
        }
    }

    public static <DRAWABLE extends Drawable> void cleanUp(@Nullable DRAWABLE drawable) {
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }

    public static <BITMAP extends Bitmap> void cleanUp(@Nullable BITMAP bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    public static <VIEW extends ImageView> void cleanUp(@NonNull VIEW view) {
        cleanUp((View) view);

        cleanUp(view.getDrawable());
        view.setImageDrawable(null);
    }

    public static void cleanUp(@NonNull SeekBar view) {
        cleanUp((View) view);

        BaseUtils.cleanUp(view.getIndeterminateDrawable());
        BaseUtils.cleanUp(view.getProgressDrawable());
        BaseUtils.cleanUp(view.getThumb());
    }

    @SuppressWarnings({"deprecation", "UnusedParameters"})
    public static void setImageDrawable(@NonNull SparseArray<WeakReference<Drawable>> drawables,
                                        @NonNull ImageView view, @DrawableRes int drawableRes) {
        //        final WeakReference<Drawable> weakReference = drawables.get(drawableRes);
        //        Drawable drawable = weakReference == null ? null : weakReference.get();
        //        if (drawable == null) {
        //            drawable = view.getResources().getDrawable(drawableRes);
        //            drawables.put(drawableRes, new WeakReference<>(drawable));
        //
        //        } else {
        //            drawable.setCallback(null);
        //        }
        //        view.setImageDrawable(drawable);
        view.setImageResource(drawableRes);
    }

    /**
     * @param l just for positive
     */
    @NonNull
    public static String appendZero(long l) {
        return l < 10 ? (SharedStrings.ZERO + l) : String.valueOf(l);
    }

    /**
     * @param year       must be positive
     * @param month      must be between [0...11]
     * @param dayOfMonth must be between [1...31]
     */
    public static int toDateInt(int year, int month, int dayOfMonth) {
        return year * 1_00_00 + month * 1_00 + dayOfMonth;
    }

    public static int getYearFromDateInt(int dateInt) {
        return dateInt / 1_00_00;
    }

    public static int getMonthFromDateInt(int dateInt) {
        final int year = getYearFromDateInt(dateInt);
        return (dateInt - year * 1_00_00) / 1_00;
    }

    public static int getDayOfMonthFromDateInt(int dateInt) {
        final int year = getYearFromDateInt(dateInt);
        final int month = getMonthFromDateInt(dateInt);
        return dateInt - year * 1_00_00 - month * 1_00;
    }

    /**
     * @param hour    must be between [0...23]
     * @param minutes must be between [0...59]
     */
    public static int toMinutesInt(int hour, int minutes) {
        return hour * 60 + minutes;
    }

    public static int getHourFromMinutesInt(int minutesInt) {
        return minutesInt / 60;
    }

    public static int getMinutesFromMinutesInt(int minutesInt) {
        return minutesInt % 60;
    }

    @SuppressWarnings("deprecation")
    @NonNull
    public static Point getDeviceScreenSize(@NonNull Context context) {
        final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        return size;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public static <VALUE> VALUE[] toArray(@Nullable List<VALUE> values, @NonNull Class<VALUE> cls) {
        if (values != null && values.size() > 0) {
            final VALUE[] array = (VALUE[]) Array.newInstance(cls, values.size());
            int count = 0;

            for (VALUE value : values) {
                array[count++] = value;
            }

            return array;
        }
        return (VALUE[]) Array.newInstance(cls, 0);
    }

    @SuppressWarnings("unused")
    @NonNull
    public static CharSequence randomWords(@NonNull Random random, int wordsCount) {
        if (wordsCount <= 0) {
            wordsCount = random.nextInt(13) + 1;
        }
        final StringBuilder sb = new StringBuilder(wordsCount * 14);
        int wordCharsCount;

        for (int wordIndex = 0; wordIndex < wordsCount; wordIndex++) {
            wordCharsCount = random.nextInt(13) + 1;
            for (int charIndex = 0; charIndex < wordCharsCount; charIndex++) {
                sb.append((char) ('a' + random.nextInt(26)));
            }
            sb.append(SharedStrings.SPACE_C);
        }
        return sb.deleteCharAt(sb.length() - 1);
    }

    private static final String[] IMAGE_URLS = new String[]{
            "http://www.color-hex.com/palettes/3686.png",
            "https://openlybeautiful.files.wordpress.com/2015/04/yellows.jpg",
            "http://www.sensationalcolor.com/wp-content/uploads/2009/12/green350x350.jpg",
            "https://upload.wikimedia.org/wikipedia/commons/2/2e/CyanIcon.png",
            "http://40.media.tumblr.com/bacff277f7d26ee96dad526cb24071e9/tumblr_mjk998Rvyh1qbh26io1_1280.png",
            "https://upload.wikimedia.org/wikipedia/commons/a/a7/MagentaIcon.png"
    };

    @Nullable
    public static String randomImage(@NonNull Random random) {
        return random.nextInt(3) == 2 ? null : IMAGE_URLS[random.nextInt(IMAGE_URLS.length)];
    }

    public static int toHashCode(long id) {
        if (id >= Integer.MIN_VALUE && id <= Integer.MAX_VALUE) {
            return (int) id;
        }
        return String.valueOf(id).hashCode();
    }

    public static boolean startIntent(@NonNull Context context, @NonNull Intent intent) {
        final boolean startIntent = intent.resolveActivity(context.getPackageManager()) != null;
        if (startIntent) {
            context.startActivity(intent);
        }
        return startIntent;
    }

    public static void recursiveSetClipChildren(@NonNull View view, boolean value) {
        final ViewParent parent = view.getParent();
        if (parent != null && parent instanceof ViewGroup) {
            final ViewGroup group = (ViewGroup) parent;
            group.setClipChildren(value);
            group.setClipToPadding(value);
            recursiveSetClipChildren(group, value);
        }
    }

    @NonNull
    public static String toStringHeaders(@Nullable Map<String, List<String>> map) {
        if (map == null || map.isEmpty()) {
            return SharedStrings.NULL;
        }
        final StringBuilder sb = new StringBuilder();

        for (Entry<String, List<String>> entry : map.entrySet()) {
            sb.append("\n\t\t");
            if (!TextUtils.isEmpty(entry.getKey())) {
                sb.append(entry.getKey()).append(" : ");
            }
            for (String value : entry.getValue()) {
                sb.append(value).append(", ");
            }
            sb.delete(sb.length() - 2, sb.length());
        }

        return sb.toString();
    }

    public static int compareFromLowToHigh(long leftL, @NonNull String leftS, long rightL, @NonNull String rightS) {
        if (leftL == rightL) {
            return leftS.compareTo(rightS);
        }
        return leftL > rightL ? 1 : -1;
    }

    @NonNull
    public static String getUniqueId(@NonNull Context context) {
        final long mostSigBits;
        final String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        if (TextUtils.isEmpty(androidId)) {
            mostSigBits = (SharedStrings.EMPTY + Build.SERIAL + Build.DEVICE + Build.MODEL + Build.PRODUCT + VERSION.CODENAME).hashCode();

        } else {
            return androidId;
        }
        final long leastSigBits;
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm == null || !isTelephonyManagerAvailable(tm)) {
            leastSigBits = mostSigBits;

        } else {
            leastSigBits = ((long) (SharedStrings.EMPTY + tm.getDeviceId()).hashCode() << 32) | (SharedStrings.EMPTY + tm.getSimSerialNumber()).hashCode();
        }
        return new UUID(mostSigBits, leastSigBits).toString();
    }

    private static boolean isTelephonyManagerAvailable(@NonNull TelephonyManager manager) {
        try {
            manager.getDeviceId();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public static void requestLayout(int width, int height, @NonNull View... views) {
        LayoutParams params;
        for (View view : views) {
            params = view.getLayoutParams();
            params.width = width;
            params.height = height;
            view.requestLayout();
        }
    }

    public static void requestLayout(@NonNull View... views) {
        LayoutParams params;
        for (View view : views) {
            params = view.getLayoutParams();
            params.width = view.getWidth();
            params.height = view.getHeight();
            view.requestLayout();
        }
    }

    public static void setAlpha(float alpha, @NonNull View... views) {
        for (View view : views) {
            view.animate().alpha(alpha).start();
        }
    }

    @SuppressWarnings("unused")
    public static int getSampleSize(float inWidth, float inHeight, float outWidth, float outHeight) {
        float sampleSize = 1F;
        if (inWidth > outWidth || inHeight > outHeight) {
            sampleSize = 2F;

            while ((inWidth / sampleSize) > outWidth || (inHeight / sampleSize) > outHeight) {
                sampleSize += 1F;
            }
        }
        return (int) sampleSize;
    }

    @Nullable
    public static Drawable getDrawable(@NonNull Context context, @DrawableRes int drawableRes) {
        return getDrawable(context.getResources(), drawableRes);
    }

    @SuppressWarnings("deprecation")
    @Nullable
    public static Drawable getDrawable(@NonNull Resources resources, @DrawableRes int drawableRes) {
        return resources.getDrawable(drawableRes);
    }

    @NonNull
    public static CharSequence merge(@NonNull String... strings) {
        final StringBuilder sb = new StringBuilder();

        for (String string : strings) {
            if (!TextUtils.isEmpty(string)) {
                sb.append(string).append(SharedStrings.SPACE_C);
            }
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb;
    }

    public static boolean receiveObjects(@Nullable WeakReference<ObjectsReceiver> weakReceiver, @IdRes int code, @NonNull Object... objects) {
        final ObjectsReceiver receiver = weakReceiver == null ? null : weakReceiver.get();
        final boolean received = receiver != null;
        if (received) {
            receiver.onObjectsReceive(code, objects);
        }
        return received;
    }

    public static boolean notZero(int... values) {
        for (int i : values) {
            if (i != 0) {
                return true;
            }
        }
        return false;
    }

    //    public static void showSnackBar(@Nullable View view, @NonNull String text) {
    //        if (view != null && view.getParent() != null) {
    //            Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
    //        }
    //    }

    @Nullable
    public static String getYoutubeVideoId(@NonNull String youtubeUrl) {
        final Matcher matcher = PATTERN_YOUTUBE_VIDEO.matcher(youtubeUrl);
        return matcher.find() ? matcher.group(1) : null;
    }

    @Nullable
    public static Uri getYoutubeVideoThumb(@NonNull String youtubeUrl, boolean hd) {
        final Matcher matcher = PATTERN_YOUTUBE_VIDEO.matcher(youtubeUrl);
        if (matcher.find()) {
            final StringBuilder sb = new StringBuilder()
                    .append("http://img.youtube.com/vi/")
                    .append(matcher.group(1));
            if (hd) {
                sb.append("/hqdefault.jpg");

            } else {
                sb.append("/mqdefault.jpg");
            }
            return Uri.parse(sb.toString());
        }
        return null;
    }

    public static void setOnCheckedChangeListener(@Nullable OnCheckedChangeListener listener, @NonNull CompoundButton... buttons) {
        for (CompoundButton button : buttons) {
            button.setOnCheckedChangeListener(listener);
        }
    }

    public static int getColor(int i, int divider) {
        if (divider <= 0) {
            divider = 6;
        }
        switch (i % divider) {
            case 0:
                return Color.WHITE;

            case 1:
                return Color.RED;

            case 2:
                return Color.YELLOW;

            case 3:
                return Color.GREEN;

            case 4:
                return Color.BLUE;

            case 5:
                return Color.MAGENTA;

            default:
                return Color.BLACK;
        }
    }

    @SuppressWarnings("ManualArrayToCollectionCopy")
    public static <V> List<V> toList(@Nullable V[] array) {
        if (array == null || array.length == 0) {
            return new ArrayList<>(0);
        }
        final ArrayList<V> list = new ArrayList<>(array.length);
        for (V v : array) {
            list.add(v);
        }
        return list;
    }

    @SuppressWarnings({"ManualArrayToCollectionCopy", "unchecked"})
    public static ArrayList<Integer> toList(@Nullable int[] values) {
        if (values == null) {
            return new ArrayList<>(0);
        }
        final ArrayList<Integer> list = new ArrayList<>(values.length);
        for (int i : values) {
            list.add(i);
        }
        return list;
    }

    @NonNull
    public static Looper newLooper(@NonNull String name, int priority) {
        final HandlerThread thread = new HandlerThread(name);
        thread.setPriority(priority);
        thread.start();

        return thread.getLooper();
    }

    @NonNull
    public static String toString(@NonNull InputStream stream, boolean closeStream) throws Exception {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(stream));

            final StringBuilder builder = new StringBuilder();
            String string;
            while ((string = reader.readLine()) != null) {
                builder.append(string);
            }
            return builder.toString();

        } finally {
            closeSilently(reader);
            if (closeStream) {
                closeSilently(stream);
            }
        }
    }
}