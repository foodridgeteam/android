package com.foodridge.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView.OnScrollListener;

import com.foodridge.application.AConstant;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04.05.15
 */
@SuppressWarnings("unused")
public class Log {

    /**
     * Default log instance: "adb logcat -s tLog"
     */
    public static final Log LOG = new Log("aLog");
    private static final int MAX_MESSAGE_LENGTH = 3 * 1024;

    //BASE
    private final String mLogTag;
    private final FileLogHandler mHandler;

    public Log(@NonNull String logTag) {
        this(logTag, null);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public Log(@NonNull String logTag, @Nullable Context context) {
        mLogTag = logTag;
        if (context == null) {
            mHandler = null;

        } else {
            final File file = new File(context.getExternalCacheDir(), logTag);
            LOG.toLog("log file path: " + file);
            final File parent = file.getParentFile();

            if (parent != null) {
                file.getParentFile().mkdirs();

                final HandlerThread thread = new HandlerThread(logTag);
                thread.start();
                mHandler = new FileLogHandler(thread.getLooper(), file);

            } else {
                mHandler = null;
            }
        }
    }

    public boolean deleteFile() {
        return mHandler != null && mHandler.getFile().delete();
    }

    public void shareWithEmail(@NonNull Activity activity) {
        if (mHandler != null) {
            mHandler.share(activity);
        }
    }

    public void toLog(@Nullable String s) {
        if (AConstant.DEBUG) {
            if (s == null) {
                android.util.Log.d(mLogTag, SharedStrings.NULL);

            } else {
                final int length = s.length();
                if (length > MAX_MESSAGE_LENGTH) {
                    toLog("\ts\t-\t-\t-\t-\tpart start\t-\t-\t-\t-\ts");
                    for (int start = 0, end = MAX_MESSAGE_LENGTH;
                         start < length;
                         start += MAX_MESSAGE_LENGTH, end += MAX_MESSAGE_LENGTH) {
                        if (end > length) {
                            end = length;
                        }
                        toLog(s.substring(start, end));
                    }
                    toLog("\te\t-\t-\t-\t-\tpart end  \t-\t-\t-\t-\te");

                } else {
                    android.util.Log.d(mLogTag, s);
                    if (mHandler != null) {
                        mHandler.write(s);
                    }
                }
            }
        }
    }

    public void toLog(@Nullable Object o) {
        toLog(o == null ? null : o.toString());
    }

    public void toLog(@NonNull PointF p) {
        toLog("PointF\tx = " + p.x + "\ty = " + p.y);
    }

    public void toLog(@NonNull Throwable e) {
        final StringBuilder sb = new StringBuilder();

        sb.append(e.getMessage());
        sb.append(SharedStrings.NEW_LINE_C);
        sb.append(e.getClass().getName());

        for (StackTraceElement s : e.getStackTrace()) {
            sb.append(SharedStrings.NEW_LINE_C);
            sb.append(s.toString());
        }

        toLog(sb);
    }

    public void toLog(@NonNull Cursor c) {
        final StringBuilder sb = new StringBuilder();

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            BaseUtils.clear(sb);

            sb.append("< < < < < < <\n");
            for (int i = 0; i < c.getColumnCount(); i++) {
                BaseUtils.clear(sb);

                final String columnName = c.getColumnName(i);

                sb.append(columnName);
                sb.append(SharedStrings.TAB_C);
                sb.append(c.getString(c.getColumnIndex(columnName)));
                sb.append(SharedStrings.NEW_LINE_C);

                toLog(sb);
            }
            sb.append(" > > >\n \n");
        }
    }

    public void toLog(@Nullable Intent intent) {
        final StringBuilder sb = new StringBuilder().append("Intent:");
        if (intent == null) {
            sb.append("null");

        } else {
            sb.append("\n\taction: ").append(intent.getAction());
            sb.append("\n\tdata: ").append(intent.getData());
        }
        toLog(sb);
        if (intent != null) {
            toLog(intent.getExtras());
        }
    }

    public static void logMaster(@NonNull Context context) {
        BaseUtils.startIntent(context, Utils.gainRam());
    }

    public void toLog(@Nullable Bundle b) {
        final StringBuilder sb = new StringBuilder("Bundle:");
        if (b != null) {
            for (String key : b.keySet()) {
                sb.append(SharedStrings.NEW_LINE_C).append(key).append(SharedStrings.TAB_C).append(b.get(key));
            }
        }
        toLog(sb);
    }

    public void toLog(@NonNull MotionEvent event) {
        final String action;
        switch (event.getAction()) {
            case MotionEvent.ACTION_MASK:
                action = "ACTION_MASK";
                break;

            case MotionEvent.ACTION_DOWN:
                action = "ACTION_DOWN";
                break;

            case MotionEvent.ACTION_UP:
                action = "ACTION_UP";
                break;

            case MotionEvent.ACTION_MOVE:
                action = "ACTION_MOVE";
                break;

            case MotionEvent.ACTION_CANCEL:
                action = "ACTION_CANCEL";
                break;

            case MotionEvent.ACTION_OUTSIDE:
                action = "ACTION_OUTSIDE";
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                action = "ACTION_POINTER_DOWN";
                break;

            case MotionEvent.ACTION_POINTER_UP:
                action = "ACTION_POINTER_UP";
                break;

            case MotionEvent.ACTION_HOVER_MOVE:
                action = "ACTION_HOVER_MOVE";
                break;

            case MotionEvent.ACTION_HOVER_ENTER:
                action = "ACTION_HOVER_ENTER";
                break;

            case MotionEvent.ACTION_HOVER_EXIT:
                action = "ACTION_HOVER_EXIT";
                break;

            case MotionEvent.ACTION_POINTER_INDEX_MASK:
                action = "ACTION_POINTER_INDEX_MASK";
                break;

            case MotionEvent.ACTION_POINTER_INDEX_SHIFT:
                action = "ACTION_POINTER_INDEX_SHIFT";
                break;

            default:
                action = event.toString();
                break;
        }
        toLog("MotionEvent: " + action + "\t x: " + event.getX() + "\t y: " + event.getY());
    }

    public <T> void toLog(@Nullable Set<T> set) {
        if (set != null) {
            final StringBuilder sb = new StringBuilder();
            for (T value : set) {
                sb.append(value).append(SharedStrings.NEW_LINE_C);
            }
            toLog(sb);
        }
    }

    public void toLogScrollState(int scrollState) {
        final String string;
        switch (scrollState) {
            case OnScrollListener.SCROLL_STATE_FLING://завершает
                string = "SCROLL_STATE_FLING";
                break;

            case OnScrollListener.SCROLL_STATE_IDLE://не скролится
                string = "SCROLL_STATE_IDLE";
                break;

            case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL://скролится
                string = "SCROLL_STATE_TOUCH_SCROLL";
                break;

            default:
                string = String.valueOf(scrollState);
                break;
        }
        toLog("scrollState: " + string);
    }

    public void toLogStackTrace(int traceSize) {
        final StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (elements.length < traceSize) {
            traceSize = elements.length;
        }
        if (elements.length < 4) {
            return;
        }
        final StringBuilder sb = new StringBuilder("StackTrace:");
        for (int i = 3; i < traceSize; i++) {
            sb.append("\n\t").append(elements[i].toString());
        }
        toLog(sb);
    }

    public <VALUE, LIST extends List<VALUE>> void toLog(@NonNull LIST list) {
        final StringBuilder sb = new StringBuilder();
        sb.append(list.getClass().getName()).append(SharedStrings.TAB_C).append("size: ").append(list.size()).append(SharedStrings.NEW_LINE_C);
        int count = 0;

        for (VALUE value : list) {
            sb.append(count++).append(SharedStrings.TAB_C).append(value).append(SharedStrings.NEW_LINE_C);
        }
        sb.deleteCharAt(sb.length() - 1);

        toLog(sb.toString());
    }

    public void toLog(@NonNull View v) {
        toLog(v.getClass().getSimpleName()
                + SharedStrings.NEW_LINE_C + SharedStrings.TAB_C
                + "x: " + v.getX() + SharedStrings.TAB_C
                + "y: " + v.getY() + SharedStrings.TAB_C
                + "width: " + v.getWidth() + SharedStrings.TAB_C
                + "height: " + v.getHeight());
    }

    public <T> T throughLog(T value) {
        toLog(value);
        return value;
    }

    private static final class FileLogHandler extends Handler {

        private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z\n", Locale.ENGLISH);

        private static final String TEXT_PLAIN = "text/plain";
        private static final String TEXT_SUBJECT = "%1$s log file: %2$s";
        private static final String APPEND_START = "\n______ { ";
        private static final String APPEND_END = "\n______ }\n";
        private static final String FILE_EXTENSION = ".zip";

        private static final int WRITE = 0;
        private static final int SHARE = 1;

        private final File mFile;
        private final Date mDate;

        public FileLogHandler(@NonNull Looper looper, @NonNull File file) {
            super(looper);

            mFile = file;
            mDate = new Date();
        }

        @SuppressWarnings("unchecked")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WRITE:
                    final String string = (String) msg.obj;

                    Writer writer = null;
                    try {
                        writer = new FileWriter(mFile, true);

                        writer.append(APPEND_START);

                        mDate.setTime(System.currentTimeMillis());
                        writer.append(SDF.format(mDate));
                        writer.append(string);

                        writer.append(APPEND_END);

                    } catch (Exception e) {
                        LOG.toLog(e);

                    } finally {
                        if (writer != null) {
                            try {
                                writer.close();

                            } catch (Exception e) {
                                LOG.toLog(e);
                            }
                        }
                    }
                    break;

                case SHARE:
                    final File zip = new File(mFile.getParent(), mFile.getName() + FILE_EXTENSION);
                    //noinspection ResultOfMethodCallIgnored
                    zip.delete();

                    ZipOutputStream zos = null;
                    try {
                        zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zip)));

                        zos.putNextEntry(new ZipEntry(mFile.getName()));

                        FileInputStream is = null;
                        try {
                            is = new FileInputStream(mFile);

                            final byte[] buffer = new byte[1024];
                            int count;

                            while ((count = is.read(buffer)) != -1) {
                                zos.write(buffer, 0, count);
                            }

                        } finally {
                            zos.closeEntry();
                            if (is != null) {
                                is.close();
                            }
                        }

                    } catch (Exception e) {
                        LOG.toLog(e);

                    } finally {
                        if (zos != null) {
                            try {
                                zos.close();

                            } catch (IOException e) {
                                LOG.toLog(e);
                            }
                        }
                    }

                    if (zip.exists() && zip.length() > 0) {
                        final Activity activity = ((WeakReference<Activity>) msg.obj).get();
                        if (activity != null) {
                            final ArrayList<Uri> extra = new ArrayList<>(1);
                            extra.add(Uri.fromFile(zip));

                            final Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE)
                                    .setType(TEXT_PLAIN)
                                    .putExtra(Intent.EXTRA_SUBJECT, String.format(TEXT_SUBJECT, "Log file", mFile.getName()))
                                    .putParcelableArrayListExtra(Intent.EXTRA_STREAM, extra);

                            if (intent.resolveActivity(activity.getPackageManager()) != null) {
                                activity.startActivity(intent);
                            }
                        }
                    }
                    break;

                default:
                    super.handleMessage(msg);
                    break;
            }
        }

        public void write(@NonNull String string) {
            final Message message = obtainMessage();
            message.what = WRITE;
            message.obj = string;

            sendMessage(message);
        }

        public void share(@NonNull Activity activity) {
            final Message message = obtainMessage();
            message.what = SHARE;
            message.obj = new WeakReference<>(activity);

            sendMessage(message);
        }

        @NonNull
        public File getFile() {
            return mFile;
        }
    }
}