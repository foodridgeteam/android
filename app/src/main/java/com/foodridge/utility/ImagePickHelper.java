package com.foodridge.utility;

import android.Manifest.permission;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;

import com.foodridge.R;
import com.foodridge.application.AContext;
import com.foodridge.fragment.BaseFragment;
import com.foodridge.interfaces.ObjectsReceiver;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
public class ImagePickHelper {

    public static final int CODE = R.id.code_image_pick_helper;

    private static final int REQUEST_CAMERA = 0x6;
    private static final int REQUEST_GALLERY = 0x7;

    private static final int REQUEST_PERMISSION_CAMERA = 0x8;
    private static final int REQUEST_PERMISSION_GALLERY = 0x9;

    private static final String EXTRA_1 = "EXTRA_1234567890";//mResourceUri
    private static final String AUTHORITY = "com.foodridge.cache";
    private static final String PATH = "camera/";

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.ENGLISH);

    private final WeakReference<ObjectsReceiver> mReceiver;
    private final Handler mHandler;
    private Uri mResourceUri;

    public ImagePickHelper(@NonNull ObjectsReceiver receiver, @Nullable Bundle b) {
        mReceiver = new WeakReference<>(receiver);
        mHandler = new Handler();
        if (b != null) {
            mResourceUri = b.getParcelable(EXTRA_1);
        }
    }

    public void onSaveInstanceState(@NonNull Bundle b) {
        b.putParcelable(EXTRA_1, mResourceUri);
    }

    public void start(@NonNull final BaseFragment fragment) {
        if (new Intent(MediaStore.ACTION_IMAGE_CAPTURE).resolveActivity(fragment.getActivity().getPackageManager()) == null) {
            startGallery(fragment);
            return;
        }
        final OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    final Activity activity = fragment.getActivity();
                    if (ContextCompat.checkSelfPermission(activity, permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(activity, permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        startCamera(fragment);

                    } else {
                        fragment.requestPermissions(new String[]{permission.READ_EXTERNAL_STORAGE, permission.CAMERA}, REQUEST_PERMISSION_CAMERA);
                    }

                } else {
                    startGallery(fragment);
                }
            }
        };
        new AlertDialog.Builder(new ContextThemeWrapper(fragment.getActivity(), R.style.AppTheme_AlertDialogLight))
                .setTitle(R.string.Image_source)
                .setItems(R.array.Image_source, listener)
                .show();
    }

    @SuppressWarnings({"ConstantConditions", "ResultOfMethodCallIgnored"})
    private void startCamera(@NonNull BaseFragment fragment) {
        try {
            final Context context = fragment.getContext();

            final String prefix = SDF.format(new Date()) + "_foodridge";
            final File directory = new File(context.getFilesDir(), PATH);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            final File file = File.createTempFile(prefix, ".jpg", directory);
            file.deleteOnExit();

            mResourceUri = FileProvider.getUriForFile(context, AUTHORITY, file);
            final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra(MediaStore.EXTRA_OUTPUT, mResourceUri);

            grantUriPermissions(context, intent, mResourceUri);
            fragment.startActivityForResult(intent, REQUEST_CAMERA);

        } catch (Exception e) {
            Log.LOG.toLog(e);
            Snackbar.make(fragment.getView(), R.string.Access_denied, Snackbar.LENGTH_LONG).show();
        }
    }

    private void startGallery(@NonNull BaseFragment fragment) {
        fragment.startActivityForResult(
                Intent.createChooser(new Intent().setType("image/jpeg").setAction(Intent.ACTION_GET_CONTENT), fragment.getString(R.string.Get_image)
                ), REQUEST_GALLERY);
    }

    public void onActivityResult(@NonNull BaseFragment fragment
            , int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK != resultCode) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CAMERA:
                saveResource();
                break;

            case REQUEST_GALLERY:
                mResourceUri = data.getData();
                if (ContextCompat.checkSelfPermission(fragment.getActivity(), permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    saveResource();

                } else {
                    fragment.requestPermissions(new String[]{permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_GALLERY);
                }
                break;

            default:
                break;
        }
    }

    @SuppressWarnings("ConstantConditions")
    public void onRequestPermissionsResult(@NonNull BaseFragment fragment
            , int requestCode, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CAMERA:
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    startCamera(fragment);

                } else {
                    Snackbar.make(fragment.getView(), R.string.Access_denied, Snackbar.LENGTH_LONG).show();
                }
                break;

            case REQUEST_PERMISSION_GALLERY:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveResource();

                } else {
                    Snackbar.make(fragment.getView(), R.string.Access_denied, Snackbar.LENGTH_LONG).show();
                }
                break;

            default:
                break;
        }
    }

    private void saveResource() {
        if (mResourceUri != null) {
            new InnerThread(this, mResourceUri).start();
        }
    }

    private void receive(@Nullable final File file) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Utils.receiveObjects(mReceiver, CODE, file);
            }
        });
    }

    private static void grantUriPermissions(@NonNull Context context, @NonNull Intent intent, @NonNull Uri uri) {
        final List<ResolveInfo> resolveInfo = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo info : resolveInfo) {
            context.grantUriPermission(info.activityInfo.packageName, uri
                    , Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    private static final class InnerThread extends Thread {

        private final WeakReference<ImagePickHelper> mHelper;
        private final Uri mResourceUri;

        public InnerThread(@NonNull ImagePickHelper helper, @NonNull Uri resourceUri) {
            super.setPriority(MAX_PRIORITY);

            mHelper = new WeakReference<>(helper);
            mResourceUri = resourceUri;
        }

        @SuppressWarnings({"ConstantConditions", "UnusedAssignment", "ResultOfMethodCallIgnored"})
        @Override
        public void run() {
            boolean allOk = false;
            File file = null;

            InputStream is = null;
            OutputStream os = null;
            try {
                if (AUTHORITY.equals(mResourceUri.getAuthority())) {
                    final ExifInterface exif = new ExifInterface(AContext.getApp().getFilesDir().getPath() + "/" + PATH + mResourceUri.getLastPathSegment());
                    final int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                    final boolean rotate = orientation == ExifInterface.ORIENTATION_ROTATE_90
                            || orientation == ExifInterface.ORIENTATION_ROTATE_180
                            || orientation == ExifInterface.ORIENTATION_ROTATE_270;

                    if (rotate) {
                        final BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;

                        final AssetFileDescriptor descriptor = AContext.getContentResolver().openAssetFileDescriptor(mResourceUri, "r");
                        BitmapFactory.decodeFileDescriptor(descriptor.getFileDescriptor(), null, options);
                        options.inSampleSize = Utils.getSampleSize(options.outWidth, options.outHeight, 1280, 1280);
                        options.inJustDecodeBounds = false;

                        Bitmap bmp = BitmapFactory.decodeFileDescriptor(descriptor.getFileDescriptor(), null, options);

                        Bitmap newBmp = getRotateBitmap(bmp, orientation);
                        if (newBmp != bmp) {
                            bmp.recycle();
                            bmp = null;
                        }
                        final String fileName = "bmp_" + mResourceUri.getLastPathSegment();
                        file = new File(AContext.getApp().getCacheDir(), fileName);

                        os = new BufferedOutputStream(new FileOutputStream(file));

                        newBmp.compress(CompressFormat.JPEG, 90, os);
                        newBmp.recycle();
                        newBmp = null;

                        allOk = true;
                        return;
                    }
                }
                is = AContext.getContentResolver().openInputStream(mResourceUri);

                final String fileName = "image_" + mResourceUri.hashCode() + "_" + is.available();
                file = new File(AContext.getApp().getCacheDir(), fileName);
                if (file.exists()) {
                    if (file.length() == is.available()) {
                        allOk = true;
                        return;

                    } else {
                        file.delete();
                        file.createNewFile();
                    }
                }
                os = new BufferedOutputStream(new FileOutputStream(file));

                byte[] buffer = new byte[8 * 1024];
                int bytesRead;

                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                allOk = true;

            } catch (Exception e) {
                Log.LOG.toLog(e);

            } finally {
                Utils.closeSilently(os);
                Utils.closeSilently(is);

                if (file != null) {
                    file.deleteOnExit();
                    if (!allOk) {
                        file.delete();
                        file = null;
                    }
                }
                final ImagePickHelper helper = mHelper.get();
                if (helper != null) {
                    helper.receive(file);
                }
            }
        }
    }

    @NonNull
    public static Bitmap getRotateBitmap(@NonNull Bitmap bitmap, final int orientation) {
        final Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;

            default:
                return bitmap;
        }
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}