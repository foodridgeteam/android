package com.foodridge.utility;

import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.SparseArrayCompat;

import com.foodridge.R;
import com.foodridge.application.AContext;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 18/04/16
 */
public class NotifyCenter {

    private static NotifyCenter sInstance;

    public final SparseArrayCompat<NotificationCompat.Builder> mBuilders;

    @NonNull
    public static NotifyCenter getInstance() {
        if (sInstance == null) {
            synchronized (NotifyCenter.class) {
                if (sInstance == null) {
                    sInstance = new NotifyCenter();
                }
            }
        }
        return sInstance;
    }

    private NotifyCenter() {
        mBuilders = new SparseArrayCompat<>();
    }

    public void showProgress(@NonNull String fileName, float percents) {
        final int key = fileName.hashCode();
        NotificationCompat.Builder builder = mBuilders.get(key);
        if (builder == null) {
            builder = new NotificationCompat.Builder(AContext.getApp())
                    .setContentTitle(AContext.getStringArgs(R.string.format_Upload_s_file, fileName))
                    .setSmallIcon(R.drawable.ic_upload)
                    .setAutoCancel(true);

            mBuilders.put(key, builder);
        }
        if (percents <= 0) {
            builder
                    .setContentText(AContext.getString(R.string.Will_start_now))
                    .setProgress(0, 0, true);

        } else if (percents >= 100) {
            builder
                    .setContentText(AContext.getString(R.string.Will_end_now))
                    .setProgress(0, 0, true);

        } else {
            builder
                    .setContentText(AContext.getString(R.string.In_progress))
                    .setProgress(100, (int) percents, false);
        }
        AContext.getNotificationManager().notify(key, builder.build());
    }

    public void removeProgress(@NonNull String fileName) {
        final int key = fileName.hashCode();
        mBuilders.remove(key);
        AContext.getNotificationManager().cancel(key);
    }
}