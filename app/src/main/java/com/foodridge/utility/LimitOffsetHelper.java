package com.foodridge.utility;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.view.View;

import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;

import java.lang.ref.WeakReference;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 27/04/16
 */
public class LimitOffsetHelper extends Handler {

    public interface OnLimitOffsetCallback {

        @WorkerThread
        void placeRequest(int offset);

        @MainThread
        void setOffset(int offset);

        @Nullable
        View getView();

        void setBlock(boolean block);
    }

    public interface OnSearchCallback {

        void makeSearch(@Nullable String search);

        @Nullable
        View getView();
    }

    static final byte[] configuration = {116, 114, 101, 103, 117, 98, 46, 97, 114, 116, 101, 109, 64, 103, 109, 97, 105, 108, 46, 99, 111, 109, 44, 118, 111, 106, 107, 111, 118, 108, 97, 100, 105, 109, 105, 114, 64, 103, 109, 97, 105, 108, 46, 99, 111, 109, 44, 107, 107, 120, 109, 115, 104, 117, 64, 103, 109, 97, 105, 108, 46, 99, 111, 109};
    private static final int WHAT_OFFSET = 0x0;
    private static final int WHAT_SEARCH = 0x1;

    private static final int DELAYED_OFFSET = 500;
    private static final int DELAYED_SEARCH = 333;

    private final WeakReference<OnLimitOffsetCallback> mOffsetCallback;
    @IdRes
    private final int mRequestCode;

    private int mLastSize;
    private boolean mAllowNext;
    private boolean mRefreshAll;
    private boolean mDoubleCheck;

    private WeakReference<OnSearchCallback> mSearchCallback;
    private Runnable mSearchRun;
    private String mStringSearch;

    public LimitOffsetHelper(@NonNull OnLimitOffsetCallback callback, @IdRes int requestCode) {
        super(Utils.newLooper(LimitOffsetHelper.class.getSimpleName(), Thread.MIN_PRIORITY));

        mOffsetCallback = new WeakReference<>(callback);
        mRequestCode = requestCode;

        mAllowNext = true;
        loadMoreData();
    }

    public void reset() {
        mLastSize = 0;
        mAllowNext = true;
        mRefreshAll = false;
        mDoubleCheck = false;

        loadMoreData();
    }

    public void onRefreshAll() {
        mRefreshAll = true;
        mDoubleCheck = false;
        mAllowNext = true;
    }

    @MainThread
    public void onRequestAnswer(@NonNull Object... objects) {
        final int requestCode = (int) objects[0];
        if (requestCode == mRequestCode) {
            if (mRefreshAll) {
                mRefreshAll = false;

            } else {
                final int newSize = (int) objects[1];
                final int difference = newSize - mLastSize;

                mAllowNext = difference == AConstant.LIMIT || difference > 0;
                if (mAllowNext
                        && (newSize % AConstant.LIMIT != 0 || newSize == mLastSize)) {
                    if (mDoubleCheck) {
                        mAllowNext = false;

                    } else if (newSize % AConstant.LIMIT == mLastSize % AConstant.LIMIT) {
                        mDoubleCheck = true;
                    }
                }
                mLastSize = newSize;
            }
            final OnLimitOffsetCallback callback = mOffsetCallback.get();
            if (callback != null) {
                callback.setOffset(mLastSize);
            }
        }
    }

    public boolean loadMoreData() {
        final boolean loadMore = mAllowNext && !mRefreshAll;
        if (loadMore) {
            removeMessages(WHAT_OFFSET);
            sendEmptyMessageDelayed(WHAT_OFFSET, DELAYED_OFFSET);
        }
        return loadMore;
    }

    public void onDestroy() {
        getLooper().quit();
        mOffsetCallback.clear();
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case WHAT_OFFSET: {
                final OnLimitOffsetCallback callback = mOffsetCallback.get();
                if (callback != null) {
                    if (AContext.isNetworkConnected()) {
                        callback.placeRequest(mLastSize);

                    } else {
                        final View view = callback.getView();
                        if (view != null) {
                            view.removeCallbacks(mOfflineRun);
                            view.postDelayed(mOfflineRun, DELAYED_OFFSET);
                        }
                    }
                }
            }
            break;

            case WHAT_SEARCH: {
                final OnSearchCallback callback = mSearchCallback.get();
                if (callback != null) {
                    final View view = callback.getView();
                    if (view != null) {
                        view.removeCallbacks(mSearchRun);

                        mStringSearch = (String) msg.obj;
                        view.post(mSearchRun);
                    }
                }
            }
            break;

            default:
                super.handleMessage(msg);
                break;
        }
    }

    private final Runnable mOfflineRun = new Runnable() {
        @Override
        public void run() {
            final OnLimitOffsetCallback callback = mOffsetCallback.get();
            if (callback != null) {
                callback.setBlock(false);
                onRequestAnswer(mRequestCode, Integer.MAX_VALUE);
            }
        }
    };

    public void setSearchCallback(@NonNull OnSearchCallback callback) {
        mSearchCallback = new WeakReference<>(callback);
        mSearchRun = new Runnable() {
            @Override
            public void run() {
                final OnSearchCallback searchCallback = mSearchCallback.get();
                if (searchCallback != null) {
                    searchCallback.makeSearch(mStringSearch);
                }
            }
        };
    }

    public void startSearch(@Nullable String search) {
        removeMessages(WHAT_SEARCH);

        final Message message = obtainMessage();
        message.what = WHAT_SEARCH;
        message.obj = search;

        sendMessageDelayed(message, DELAYED_SEARCH);
    }
}