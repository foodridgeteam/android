package com.foodridge.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.foodridge.api.model.Chef;
import com.foodridge.api.model.Meal;
import com.foodridge.interfaces.AdapterCursorEntity;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.MealContract;
import com.foodridge.utility.Utils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 18/04/16
 */
@DatabaseTable(tableName = MealContract.TABLE_NAME)
@DefaultContentUri(authority = FoodridgeMetaData.AUTHORITY, path = MealContract.TABLE_NAME)
@DefaultContentMimeTypeVnd(name = FoodridgeMetaData.AUTHORITY_PROVIDER, type = MealContract.TABLE_NAME)
public class DbMeal
        implements AdapterCursorEntity<Meal> {

    public static final String SEARCH_SELF = DbMeal.class.getName().toLowerCase();

    @SuppressWarnings("FieldCanBeLocal")
    @DatabaseField(columnName = MealContract._ID, dataType = DataType.STRING, id = true)
    private String mId;

    @DatabaseField(columnName = MealContract.ENTITY_ID, dataType = DataType.STRING)
    private String mEntityId;

    @DefaultSortOrder
    @DatabaseField(columnName = MealContract.ORDER, dataType = DataType.LONG)
    private long mOrder;

    @DatabaseField(columnName = MealContract.ENTITY, dataType = DataType.SERIALIZABLE)
    private Meal mMeal;

    @DatabaseField(columnName = MealContract.CHEF_ID, dataType = DataType.STRING)
    private String mChefId;

    @DatabaseField(columnName = MealContract.CHEF, dataType = DataType.SERIALIZABLE)
    private Chef mChef;

    @DatabaseField(columnName = MealContract.SEARCH, dataType = DataType.STRING)
    private String mSearch;

    private static int[] sColumns;

    public DbMeal() {
    }

    @NonNull
    public ContentValues toContentValues(long order, @NonNull Meal meal, @Nullable String search) {
        mEntityId = meal.getId();
        mOrder = order;
        mMeal = meal;
        mChef = meal.getChef();
        mChefId = mChef.getId();
        mSearch = search;

        return toContentValues();
    }

    @Override
    public void fromCursor(@NonNull Cursor cursor) {
        if (sColumns == null) {
            sColumns = new int[7];

//            sColumns[0] = cursor.getColumnIndex(MealContract._ID);
            sColumns[1] = cursor.getColumnIndex(MealContract.ENTITY_ID);
            sColumns[2] = cursor.getColumnIndex(MealContract.ORDER);
            sColumns[3] = cursor.getColumnIndex(MealContract.ENTITY);
            sColumns[4] = cursor.getColumnIndex(MealContract.CHEF_ID);
            sColumns[5] = cursor.getColumnIndex(MealContract.CHEF);
            sColumns[6] = cursor.getColumnIndex(MealContract.SEARCH);
        }
        mEntityId = cursor.getString(sColumns[1]);
        mOrder = cursor.getLong(sColumns[2]);
        mMeal = Utils.deserialize(cursor.getBlob(sColumns[3]), Meal.class);
        mChefId = cursor.getString(sColumns[4]);
        mChef = Utils.deserialize(cursor.getBlob(sColumns[5]), Chef.class);
        mSearch = cursor.getString(sColumns[6]);

        mMeal.setChef(mChef);
    }

    @NonNull
    @Override
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues(7);

        mId = mEntityId + mSearch;
        mMeal.setChef(null);

        cv.put(MealContract._ID, mId);
        cv.put(MealContract.ENTITY_ID, mEntityId);
        if (mOrder != Integer.MIN_VALUE) {
            cv.put(MealContract.ORDER, mOrder);
        }
        cv.put(MealContract.ENTITY, Utils.serialize(mMeal));
        cv.put(MealContract.CHEF_ID, mChefId);
        cv.put(MealContract.CHEF, Utils.serialize(mChef));
        cv.put(MealContract.SEARCH, mSearch);

        return cv;
    }

    @NonNull
    @Override
    public Meal getEntity() {
        return mMeal;
    }
}