package com.foodridge.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.foodridge.api.model.Comment;
import com.foodridge.interfaces.AdapterCursorEntity;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.CommentContract;
import com.foodridge.utility.Utils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 18/04/16
 */
@DatabaseTable(tableName = CommentContract.TABLE_NAME)
@DefaultContentUri(authority = FoodridgeMetaData.AUTHORITY, path = CommentContract.TABLE_NAME)
@DefaultContentMimeTypeVnd(name = FoodridgeMetaData.AUTHORITY_PROVIDER, type = CommentContract.TABLE_NAME)
public class DbComment
        implements AdapterCursorEntity<Comment> {

    @DatabaseField(columnName = CommentContract._ID, dataType = DataType.STRING, id = true)
    private String mId;

    @DefaultSortOrder
    @DatabaseField(columnName = CommentContract.ORDER, dataType = DataType.LONG)
    private long mOrder;

    @DatabaseField(columnName = CommentContract.ENTITY, dataType = DataType.SERIALIZABLE)
    private Comment mEntity;

    @DatabaseField(columnName = CommentContract.MEAL_ID, dataType = DataType.STRING)
    private String mMealId;

    private static int[] sColumns;

    public DbComment() {
    }

    @NonNull
    public ContentValues toContentValues(long order, @NonNull Comment entity) {
        mId = entity.getOrderId();
        mOrder = order;
        mEntity = entity;
        mMealId = entity.getMealId();

        return toContentValues();
    }

    @Override
    public void fromCursor(@NonNull Cursor cursor) {
        if (sColumns == null) {
            sColumns = new int[4];

            sColumns[0] = cursor.getColumnIndex(CommentContract._ID);
            sColumns[1] = cursor.getColumnIndex(CommentContract.ORDER);
            sColumns[2] = cursor.getColumnIndex(CommentContract.ENTITY);
            sColumns[3] = cursor.getColumnIndex(CommentContract.MEAL_ID);
        }
        mId = cursor.getString(sColumns[0]);
        mOrder = cursor.getLong(sColumns[1]);
        mEntity = Utils.deserialize(cursor.getBlob(sColumns[2]), Comment.class);
        mMealId = cursor.getString(sColumns[3]);
    }

    @NonNull
    @Override
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues(4);

        cv.put(CommentContract._ID, mId);
        if (mOrder != Integer.MIN_VALUE) {
            cv.put(CommentContract.ORDER, mOrder);
        }
        cv.put(CommentContract.ENTITY, Utils.serialize(mEntity));
        cv.put(CommentContract.MEAL_ID, mMealId);

        return cv;
    }

    @NonNull
    @Override
    public Comment getEntity() {
        return mEntity;
    }
}