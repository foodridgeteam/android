package com.foodridge.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.foodridge.api.model.OrderMessage;
import com.foodridge.interfaces.AdapterCursorEntity;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.MessageContract;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.SortOrder;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 18/04/16
 */
@DatabaseTable(tableName = MessageContract.TABLE_NAME)
@DefaultContentUri(authority = FoodridgeMetaData.AUTHORITY, path = MessageContract.TABLE_NAME)
@DefaultContentMimeTypeVnd(name = FoodridgeMetaData.AUTHORITY_PROVIDER, type = MessageContract.TABLE_NAME)
public class DbMessage
        implements AdapterCursorEntity<OrderMessage> {

    @DatabaseField(columnName = MessageContract._ID, dataType = DataType.STRING, id = true)
    private String mId;

    @DefaultSortOrder(order = SortOrder.DESC)
    @DatabaseField(columnName = MessageContract.ORDER, dataType = DataType.LONG)
    private long mOrder;

    @DatabaseField(columnName = MessageContract.ENTITY, dataType = DataType.SERIALIZABLE)
    private OrderMessage mEntity;

    @DatabaseField(columnName = MessageContract.ORDER_ID, dataType = DataType.STRING)
    private String mOrderId;

    private static int[] sColumns;

    public DbMessage() {
    }

    @NonNull
    public ContentValues toContentValues(@NonNull String orderId, @NonNull OrderMessage entity) {
        mId = entity.getId();
        mOrder = TimeCache.getInstance().getTime(entity.getCreated());
        mEntity = entity;
        mOrderId = orderId;

        mEntity.setCreatedMillis(mOrder);

        return toContentValues();
    }

    @Override
    public void fromCursor(@NonNull Cursor cursor) {
        if (sColumns == null) {
            sColumns = new int[4];

//            sColumns[0] = cursor.getColumnIndex(MessageContract._ID);
            sColumns[1] = cursor.getColumnIndex(MessageContract.ORDER);
            sColumns[2] = cursor.getColumnIndex(MessageContract.ENTITY);
            sColumns[3] = cursor.getColumnIndex(MessageContract.ORDER_ID);
        }
//        mId = cursor.getString(sColumns[0]);
        mOrder = cursor.getLong(sColumns[1]);
        mEntity = Utils.deserialize(cursor.getBlob(sColumns[2]), OrderMessage.class);
        //noinspection ConstantConditions
        mEntity.setCreatedMillis(mOrder);
        mOrderId = cursor.getString(sColumns[3]);
    }

    @NonNull
    @Override
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues(4);

        cv.put(MessageContract._ID, mId);
        cv.put(MessageContract.ORDER, mOrder);
        cv.put(MessageContract.ENTITY, Utils.serialize(mEntity));
        cv.put(MessageContract.ORDER_ID, mOrderId);

        return cv;
    }

    @NonNull
    @Override
    public OrderMessage getEntity() {
        return mEntity;
    }

    public String getOrderId() {
        return mOrderId;
    }
}