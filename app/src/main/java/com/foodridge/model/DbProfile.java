package com.foodridge.model;

import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 15/04/16
 */
@DatabaseTable(tableName = ProfileContract.TABLE_NAME)
@DefaultContentUri(authority = FoodridgeMetaData.AUTHORITY, path = ProfileContract.TABLE_NAME)
@DefaultContentMimeTypeVnd(name = FoodridgeMetaData.AUTHORITY_PROVIDER, type = ProfileContract.TABLE_NAME)
public class DbProfile {

    @DatabaseField(columnName = ProfileContract._ID, dataType = DataType.LONG, generatedId = true)
    private long mId;
}