package com.foodridge.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.foodridge.api.model.Chef;
import com.foodridge.interfaces.AdapterCursorEntity;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.ChefContract;
import com.foodridge.utility.Utils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 18/04/16
 */
@DatabaseTable(tableName = ChefContract.TABLE_NAME)
@DefaultContentUri(authority = FoodridgeMetaData.AUTHORITY, path = ChefContract.TABLE_NAME)
@DefaultContentMimeTypeVnd(name = FoodridgeMetaData.AUTHORITY_PROVIDER, type = ChefContract.TABLE_NAME)
public class DbChef
        implements AdapterCursorEntity<Chef> {

    public static final String SEARCH_SELF = DbChef.class.getName().toLowerCase();

    @SuppressWarnings("FieldCanBeLocal")
    @DatabaseField(columnName = ChefContract._ID, dataType = DataType.STRING, id = true)
    private String mId;

    @DatabaseField(columnName = ChefContract.ENTITY_ID, dataType = DataType.STRING)
    private String mEntityId;

    @DefaultSortOrder
    @DatabaseField(columnName = ChefContract.ORDER, dataType = DataType.LONG)
    private long mOrder;

    @DatabaseField(columnName = ChefContract.ENTITY, dataType = DataType.SERIALIZABLE)
    private Chef mChef;

    @DatabaseField(columnName = ChefContract.SEARCH, dataType = DataType.STRING)
    private String mSearch;

    private static int[] sColumns;

    public DbChef() {
    }

    public DbChef(@NonNull Cursor cursor) {
        fromCursor(cursor);
    }

    @NonNull
    public ContentValues toContentValues(long order, @NonNull Chef chef, @Nullable String search) {
        mEntityId = chef.getId();
        mOrder = order;
        mChef = chef;
        mSearch = search;

        return toContentValues();
    }

    @Override
    public void fromCursor(@NonNull Cursor cursor) {
        if (sColumns == null) {
            sColumns = new int[5];

//            sColumns[0] = cursor.getColumnIndex(ChefContract._ID);
            sColumns[1] = cursor.getColumnIndex(ChefContract.ENTITY_ID);
            sColumns[2] = cursor.getColumnIndex(ChefContract.ORDER);
            sColumns[3] = cursor.getColumnIndex(ChefContract.ENTITY);
            sColumns[4] = cursor.getColumnIndex(ChefContract.SEARCH);
        }
        mEntityId = cursor.getString(sColumns[1]);
        mOrder = cursor.getLong(sColumns[2]);
        mChef = Utils.deserialize(cursor.getBlob(sColumns[3]), Chef.class);
        mSearch = cursor.getString(sColumns[4]);
    }

    @NonNull
    @Override
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues(5);

        mId = mEntityId + mSearch;

        cv.put(ChefContract._ID, mId);
        cv.put(ChefContract.ENTITY_ID, mEntityId);
        cv.put(ChefContract.ORDER, mOrder);
        cv.put(ChefContract.ENTITY, Utils.serialize(mChef));
        cv.put(ChefContract.SEARCH, mSearch);

        return cv;
    }

    @NonNull
    @Override
    public Chef getEntity() {
        return mChef;
    }
}