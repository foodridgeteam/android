package com.foodridge.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.foodridge.api.request.BaseRequestEntity;
import com.foodridge.interfaces.CursorEntity;
import com.foodridge.provider.ContentHelper;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.RequestEntityContract;
import com.foodridge.type.RequestEntityStatus;
import com.foodridge.utility.Utils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19.03.2016
 */
@DatabaseTable(tableName = RequestEntityContract.TABLE_NAME)
@DefaultContentUri(authority = FoodridgeMetaData.AUTHORITY, path = RequestEntityContract.TABLE_NAME)
@DefaultContentMimeTypeVnd(name = FoodridgeMetaData.AUTHORITY_PROVIDER, type = RequestEntityContract.TABLE_NAME)
public class DbRequestEntity
        implements CursorEntity {

    @DatabaseField(columnName = RequestEntityContract._ID, dataType = DataType.INTEGER, generatedId = true)
    private int mId;

    @DatabaseField(columnName = RequestEntityContract.RECEIVER_CODE, dataType = DataType.INTEGER)
    private int mReceiverCode;

    @DatabaseField(columnName = RequestEntityContract.ENTITY, dataType = DataType.SERIALIZABLE)
    private BaseRequestEntity mEntity;

    @DatabaseField(columnName = RequestEntityContract.STATUS, dataType = DataType.INTEGER)
    @RequestEntityStatus
    private int mStatus;

    private static int[] sColumns;

    @NonNull
    public static ContentValues convert(@NonNull BaseRequestEntity entity) {
        final ContentValues cv = new ContentValues(3);

        cv.put(RequestEntityContract.RECEIVER_CODE, entity.getReceiverCode());
        cv.put(RequestEntityContract.ENTITY, Utils.serialize(entity));
        cv.put(RequestEntityContract.STATUS, RequestEntityStatus.PLACED_IN_DB);

        return cv;
    }

    public DbRequestEntity() {
    }

    @Override
    public void fromCursor(@NonNull Cursor cursor) {
        if (sColumns == null) {
            sColumns = new int[4];

            sColumns[0] = cursor.getColumnIndex(RequestEntityContract._ID);
            sColumns[1] = cursor.getColumnIndex(RequestEntityContract.ENTITY);
            sColumns[2] = cursor.getColumnIndex(RequestEntityContract.STATUS);
            sColumns[3] = cursor.getColumnIndex(RequestEntityContract.RECEIVER_CODE);
        }
        mId = cursor.getInt(sColumns[0]);
        mEntity = Utils.deserialize(cursor.getBlob(sColumns[1]), BaseRequestEntity.class);
        if (mEntity == null) {
            mReceiverCode = cursor.getInt(sColumns[3]);

        } else {
            mEntity.setDbId(mId);
            mReceiverCode = mEntity.getReceiverCode();
        }
        //noinspection ResourceType
        mStatus = cursor.getInt(sColumns[2]);
    }

    @NonNull
    @Override
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues(4);

        if (mId > 0) {
            cv.put(RequestEntityContract._ID, mId);
        }
        cv.put(RequestEntityContract.RECEIVER_CODE, mReceiverCode);
        cv.put(RequestEntityContract.ENTITY, Utils.serialize(mEntity));
        cv.put(RequestEntityContract.STATUS, mStatus);

        return cv;
    }

    public int getId() {
        return mId;
    }

    public int getReceiverCode() {
        return mReceiverCode;
    }

    public BaseRequestEntity getEntity() {
        return mEntity;
    }

    @RequestEntityStatus
    public int getStatus() {
        return mStatus;
    }

    public void setStatus(@RequestEntityStatus int status) {
        mStatus = status;
    }

    @Override
    public String toString() {
        return "id: " + mId
                + "\tstatus: " + mStatus
                + "\tentity: " + mEntity;
    }

    public static void updateStatus(@NonNull ContentResolver resolver, int dbId, @RequestEntityStatus int status) {
        final ContentValues cv = new ContentValues(1);
        cv.put(RequestEntityContract.STATUS, status);

        resolver.update(RequestEntityContract.CONTENT_URI, cv, ContentHelper.eq(RequestEntityContract._ID, dbId), null);
    }
}