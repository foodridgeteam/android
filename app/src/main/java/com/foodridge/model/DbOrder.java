package com.foodridge.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.foodridge.api.model.Order;
import com.foodridge.interfaces.AdapterCursorEntity;
import com.foodridge.provider.FoodridgeMetaData;
import com.foodridge.provider.FoodridgeMetaData.OrderContract;
import com.foodridge.type.OrderStatus;
import com.foodridge.utility.Utils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 18/04/16
 */
@DatabaseTable(tableName = OrderContract.TABLE_NAME)
@DefaultContentUri(authority = FoodridgeMetaData.AUTHORITY, path = OrderContract.TABLE_NAME)
@DefaultContentMimeTypeVnd(name = FoodridgeMetaData.AUTHORITY_PROVIDER, type = OrderContract.TABLE_NAME)
public class DbOrder
        implements AdapterCursorEntity<Order> {

    @DatabaseField(columnName = OrderContract._ID, dataType = DataType.STRING, id = true)
    private String mId;

    @DefaultSortOrder
    @DatabaseField(columnName = OrderContract.ORDER, dataType = DataType.LONG)
    private long mOrder;

    @DatabaseField(columnName = OrderContract.ENTITY, dataType = DataType.SERIALIZABLE)
    private Order mEntity;

    @DatabaseField(columnName = OrderContract.STATUS, dataType = DataType.INTEGER)
    @OrderStatus
    private int mStatus;

    @DatabaseField(columnName = OrderContract.CHEF_ID, dataType = DataType.STRING)
    private String mChefId;

    @DatabaseField(columnName = OrderContract.CLIENT_ID, dataType = DataType.STRING)
    private String mClientId;

    private static int[] sColumns;

    public DbOrder() {
    }

    @NonNull
    public ContentValues toContentValues(long order, @NonNull Order entity) {
        mId = entity.getId();
        mOrder = order;
        mEntity = entity;
        mStatus = entity.getStatus();
        mChefId = entity.getMeal().getChef().getId();
        mClientId = entity.getClient().getId();

        return toContentValues();
    }

    @Override
    public void fromCursor(@NonNull Cursor cursor) {
        if (sColumns == null) {
            sColumns = new int[6];

            sColumns[0] = cursor.getColumnIndex(OrderContract._ID);
            sColumns[1] = cursor.getColumnIndex(OrderContract.ORDER);
            sColumns[2] = cursor.getColumnIndex(OrderContract.ENTITY);
            sColumns[3] = cursor.getColumnIndex(OrderContract.STATUS);
            sColumns[4] = cursor.getColumnIndex(OrderContract.CHEF_ID);
            sColumns[5] = cursor.getColumnIndex(OrderContract.CLIENT_ID);
        }
        mId = cursor.getString(sColumns[0]);
        mOrder = cursor.getLong(sColumns[1]);
        mEntity = Utils.deserialize(cursor.getBlob(sColumns[2]), Order.class);
        //noinspection WrongConstant
        mStatus = cursor.getInt(sColumns[3]);
        mChefId = cursor.getString(sColumns[4]);
        mClientId = cursor.getString(sColumns[5]);
    }

    @NonNull
    @Override
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues(6);

        cv.put(OrderContract._ID, mId);
        if (mOrder != Integer.MIN_VALUE) {
            cv.put(OrderContract.ORDER, mOrder);
        }
        cv.put(OrderContract.ENTITY, Utils.serialize(mEntity));
        cv.put(OrderContract.STATUS, mStatus);
        cv.put(OrderContract.CHEF_ID, mChefId);
        cv.put(OrderContract.CLIENT_ID, mClientId);

        return cv;
    }

    @NonNull
    @Override
    public Order getEntity() {
        return mEntity;
    }
}