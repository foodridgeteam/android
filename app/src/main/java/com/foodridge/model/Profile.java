package com.foodridge.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foodridge.api.ApiKeys;
import com.foodridge.api.model.Auth;
import com.foodridge.api.model.ChefProfile;
import com.foodridge.api.model.UserProfile;
import com.foodridge.api.request.ANetworkRequest;
import com.foodridge.api.request.ChefsRequest;
import com.foodridge.api.request.GetProfileChefRequest;
import com.foodridge.api.request.MealsRequest;
import com.foodridge.api.request.OrdersRequest;
import com.foodridge.api.request.PushIdRequest;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.provider.FoodridgeMetaData.ProfileContract;
import com.foodridge.utility.Log;
import com.foodridge.utility.SharedStrings;

import java.io.IOException;

import io.branch.referral.Branch;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public class Profile extends UserProfile {

    private static final String KEY_PREFERENCES = "profile.KEY_PREFERENCES";

    private static final String KEY_PROFILE = "profile.KEY_PROFILE";
    private static final String KEY_FILE_AVATAR = "profile.KEY_FILE_AVATAR";
    private static final String KEY_PROFILE_CHEF = "profile.KEY_PROFILE_CHEF";

    //SINGLETON
    private static Profile sInstance;

    //VALUE's
    private SharedPreferences mPreferences;

    @JsonProperty(ApiKeys.TOKEN)
    private String mToken;

    @JsonProperty(KEY_FILE_AVATAR)
    private String mFileAvatar;

    @JsonProperty(KEY_PROFILE_CHEF)
    private ChefProfile mChefProfile;

    private boolean mMakeLogout;

    public static void init(@NonNull Context context) {
        final SharedPreferences preferences = context.getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
        final Profile profile = new Profile();
        final String rawJson = preferences.getString(KEY_PROFILE, null);

        if (rawJson != null) {
            try {
                final Profile savedProfile = new ObjectMapper().readValue(rawJson, Profile.class);
                if (savedProfile != null) {
                    profile.mToken = savedProfile.mToken;
                    profile.mFileAvatar = savedProfile.mFileAvatar;
                    profile.mChefProfile = savedProfile.mChefProfile;

                    profile.set(savedProfile);
                }

            } catch (IOException e) {
                Log.LOG.toLog(e);
            }
        }
        profile.mPreferences = preferences;
        sInstance = profile;
    }

    @NonNull
    public static Profile getInstance() {
        return sInstance;
    }

    private Profile() {
    }

    @Override
    public String getId() {
        return mId == null ? SharedStrings.EMPTY : mId;
    }

    public String getToken() {
        return mToken;
    }

    @Override
    public String getAvatar() {
        return mAvatar == null ? mFileAvatar : mAvatar;
    }

    @NonNull
    public ChefProfile getChefProfile() {
        return mChefProfile;
    }

    public boolean isChef() {
        return mChefProfile.getBusinessInfo() != null;
    }

    public void setChefProfile(@NonNull ChefProfile chefProfile) {
        mChefProfile = chefProfile;
    }

    public void set(@NonNull Auth auth) {
        mToken = auth.getToken();
        set(auth.getProfile());
    }

    public boolean isSameAvatar(@Nullable Uri uri) {
        if (uri != null) {
            final String string = uri.toString();
            return string.equals(mFileAvatar) || string.equals(mAvatar);
        }
        return false;
    }

    public void setFileAvatar(@Nullable String fileAvatar) {
        mFileAvatar = fileAvatar;
    }

    public boolean isMakeLogout() {
        return mMakeLogout;
    }

    @WorkerThread
    public synchronized void makeLogout() {
        mMakeLogout = true;
        commit();

        AContext.notifyChange(ProfileContract.CONTENT_URI);
    }

    @WorkerThread
    public synchronized void commit() {
        try {
            final String rawJson = new ObjectMapper().writeValueAsString(this);

            mPreferences.edit()
                    .putString(KEY_PROFILE, rawJson)
                    .apply();

        } catch (JsonProcessingException e) {
            Log.LOG.toLog(e);
        }
    }

    @WorkerThread
    public synchronized void logout() {
        Branch.getInstance(AContext.getApp()).logout();
        Answers.getInstance().logLogin(new LoginEvent()
                .putMethod("logout")
                .putCustomAttribute("email", mEmail)
                .putCustomAttribute("deviceId", Secure.getString(AContext.getContentResolver(), Secure.ANDROID_ID))
        );
        mToken = null;
        mFileAvatar = null;
        mChefProfile = new ChefProfile();
        mMakeLogout = false;
        set(new UserProfile());

        commit();
    }

    public synchronized void showTermsConditions() {
        mAgreeTerms = false;
        AContext.notifyChange(ProfileContract.CONTENT_URI);
    }

    @WorkerThread
    public synchronized void agreeTerms() throws Exception {
        mAgreeTerms = true;
        commit();

        loadForLogin();
    }

    public static boolean isAuthorized() {
        return sInstance.mToken != null;
    }

    @WorkerThread
    public static void login(@NonNull Auth auth) throws Exception {
        final Profile profile = sInstance;
        profile.setChefProfile(new ChefProfile());
        profile.set(auth);
        profile.commit();

        if (profile.mAgreeTerms) {
            loadForLogin();
        }
        Branch.getInstance(AContext.getApp()).setIdentity(profile.getId());
        Answers.getInstance().logLogin(new LoginEvent()
                .putMethod("login")
                .putCustomAttribute("email", sInstance.mEmail)
                .putCustomAttribute("deviceId", Secure.getString(AContext.getContentResolver(), Secure.ANDROID_ID))
        );
    }

    private static void loadForLogin() throws Exception {
        ASettings.loadStatic();
        PushIdRequest.send();
        new ANetworkRequest(new GetProfileChefRequest()).loadDataFromNetwork();
        new ANetworkRequest(new MealsRequest(0, null, null)).execute();

        new ANetworkRequest(new OrdersRequest(null)).oneShot(Thread.MIN_PRIORITY);
        new ANetworkRequest(new MealsRequest(0, sInstance.mId, DbMeal.SEARCH_SELF)).oneShot(Thread.MIN_PRIORITY);
        new ANetworkRequest(new ChefsRequest()).oneShot(Thread.MIN_PRIORITY);
    }
}