package com.foodridge.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 11/05/16
 */
public class ListEmptyView extends LinearLayoutCompat {

    //VIEW's
    private final ImageView mImageView;
    private final TextView mTextView;
    private final TextView mButton;

    @NonNull
    public static ListEmptyView attach(@NonNull ViewGroup group, @NonNull ListView view) {
        final ListEmptyView emptyView = new ListEmptyView(group.getContext());

        group.addView(emptyView);
        view.setEmptyView(emptyView);

        return emptyView;
    }

    public ListEmptyView(Context context) {
        this(context, null);
    }

    public ListEmptyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ListEmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_list_empty, this);

            super.setOrientation(VERTICAL);
            super.setGravity(Gravity.CENTER);
        }
        mImageView = (ImageView) findViewById(R.id.iv_empty_content);
        mTextView = (TextView) findViewById(R.id.tv_empty_content);
        mButton = (TextView) findViewById(R.id.btn_empty_content);

        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    public void setData(@DrawableRes int drawableRes, @StringRes int textStringRes, @StringRes int buttonStringRes, @Nullable OnClickListener listener) {
//        if (drawableRes > 0) {
//            Utils.changeVisibility(mImageView, VISIBLE);
//            mImageView.setImageResource(drawableRes);
//
//        } else {
        Utils.changeVisibility(mImageView, GONE);
//        }
        mTextView.setText(textStringRes);
        if (buttonStringRes > 0) {
            Utils.changeVisibility(mButton, VISIBLE);
            mButton.setText(buttonStringRes);
            mButton.setOnClickListener(listener);

        } else {
            Utils.changeVisibility(mButton, GONE);
        }
    }
}