package com.foodridge.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.foodridge.R;
import com.foodridge.api.model.Chef;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19/04/16
 */
public class ChefShortView extends RelativeLayout {

    //VIEW's
    private final ImageView mImageView;
    private final TextView mTvName;
    private final RatingView mRatingView;
    private final TextView mTvVisited;

    //VALUE's
    private final RequestManager mGlide;
    private final TimeCache mTimeCache;
    private final Drawable mPlaceholder;

    public ChefShortView(@NonNull Context context) {
        this(context, null);
    }

    public ChefShortView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChefShortView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_chef_short, this);

            super.setBackgroundResource(R.drawable.sel_gray_background_header);
            final int padding = getResources().getDimensionPixelSize(R.dimen.spacing);
            super.setPadding(padding * 2, padding, padding * 2, padding);
        }
        mImageView = (ImageView) findViewById(R.id.iv_chef_avatar);
        mTvName = (TextView) findViewById(R.id.tv_chef_name);
        mRatingView = (RatingView) findViewById(R.id.chef_rating);
        mTvVisited = (TextView) findViewById(R.id.tv_visited);

        mGlide = isInEditMode() ? null : Glide.with(context);
        mTimeCache = isInEditMode() ? null : TimeCache.getInstance();
        mPlaceholder = ContextCompat.getDrawable(context, R.drawable.svg_user_placeholder);
    }

    public void setData(@NonNull Chef chef) {
        Utils.loadInto(mGlide, chef.getAvatar(), mPlaceholder, mImageView);

//        mTvName.setText(getResources().getString(R.string.format_Chef_s, chef.getName()));
        mTvName.setText(chef.getName());
        mRatingView.setRating(chef.getRating());
        mTvVisited.setText(mTimeCache.getVisitedAt(chef.getLastVisit()));
    }
}