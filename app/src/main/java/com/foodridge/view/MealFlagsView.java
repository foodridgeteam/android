package com.foodridge.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.MealFlags;
import com.foodridge.application.AContext;
import com.foodridge.type.MealFlag;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 15.04.16.
 */
public class MealFlagsView extends LinearLayoutCompat
        implements OnCheckedChangeListener {

    private static final int EDITABLE_DIVIDERS = SHOW_DIVIDER_MIDDLE | SHOW_DIVIDER_END | SHOW_DIVIDER_BEGINNING;

    //VIEW's
    private final TextView[] mFlagsViews;

    // VALUE's
    private final Drawable mDrawableOn;
    private final Drawable mDrawableOff;

    @ColorInt
    private final int mColorEvenFlag;
    @ColorInt
    private final int mColorOddFlag;
    private boolean mEditable;

    public MealFlagsView(@NonNull Context context) {
        this(context, null);
    }

    public MealFlagsView(@NonNull Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MealFlagsView(@NonNull Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        {
            super.setOrientation(VERTICAL);
            super.setDividerDrawable(ContextCompat.getDrawable(context, R.drawable.divider_gray_light));
        }
        mDrawableOn = ContextCompat.getDrawable(context, R.drawable.svg_v_teal);
        mDrawableOff = ContextCompat.getDrawable(context, R.drawable.svg_x_red);

        mFlagsViews = new TextView[MealFlag.values.length];

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MealFlagsView, defStyle, 0);

        mEditable = a.getBoolean(R.styleable.MealFlagsView_mfv_editable, false);
        if (!mEditable) {
            super.setShowDividers(SHOW_DIVIDER_MIDDLE | SHOW_DIVIDER_END | SHOW_DIVIDER_BEGINNING);
        }
        mColorEvenFlag = a.getColor(R.styleable.MealFlagsView_mfv_even_flag, Color.TRANSPARENT);
        mColorOddFlag = a.getColor(R.styleable.MealFlagsView_mfv_odd_flag, Color.TRANSPARENT);

        a.recycle();

        if (!isInEditMode()) {
            populateMealFlags();
        }
    }

    public void setData(@NonNull MealFlags flags, boolean editable) {
        if (mEditable != editable) {
            mEditable = editable;
            super.setShowDividers(mEditable ? EDITABLE_DIVIDERS : SHOW_DIVIDER_NONE);
            populateMealFlags();
        }
        setFlagEnabled(mFlagsViews[0], flags.isBreakfast());
        setFlagEnabled(mFlagsViews[1], flags.isLunch());
        setFlagEnabled(mFlagsViews[2], flags.isDinner());
        setFlagEnabled(mFlagsViews[3], flags.isBakery());
        setFlagEnabled(mFlagsViews[4], flags.isVegetarian());
        setFlagEnabled(mFlagsViews[5], flags.isEgg());
        setFlagEnabled(mFlagsViews[6], flags.isGlutenFree());
        setFlagEnabled(mFlagsViews[7], flags.isTracesNuts());
        setFlagEnabled(mFlagsViews[8], flags.isGarlic());
        setFlagEnabled(mFlagsViews[9], flags.isGinger());
    }

    public void fillData(@NonNull MealFlags flags) {
        flags.set(((Checkable) mFlagsViews[0]).isChecked()
                , ((Checkable) mFlagsViews[1]).isChecked()
                , ((Checkable) mFlagsViews[2]).isChecked()
                , ((Checkable) mFlagsViews[3]).isChecked()
                , ((Checkable) mFlagsViews[4]).isChecked()
                , ((Checkable) mFlagsViews[5]).isChecked()
                , ((Checkable) mFlagsViews[6]).isChecked()
                , ((Checkable) mFlagsViews[7]).isChecked()
                , ((Checkable) mFlagsViews[8]).isChecked()
                , ((Checkable) mFlagsViews[9]).isChecked());
    }

    private void populateMealFlags() {
        if (getChildCount() > 0) {
            removeAllViews();
        }
        final Integer[] flags = MealFlag.values;
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        @LayoutRes
        final int layoutRes = mEditable ? R.layout.view_meal_flag_editable : R.layout.view_meal_flag;

        TextView view;
        for (int i = 0; i < flags.length; i++) {
            view = (TextView) inflater.inflate(layoutRes, this, false);
            view.setText(flags[i]);
            if (mEditable) {
                view.setBackgroundColor(i % 2 == 0 ? mColorEvenFlag : mColorOddFlag);
            }
            setFlagEnabled(view, false);
            if (mEditable) {
                ((CompoundButton) view).setOnCheckedChangeListener(this);
            }
            mFlagsViews[i] = view;
            addView(view, i);
        }
    }

    private void setFlagEnabled(@NonNull TextView view, boolean isEnabled) {
        if (mEditable) {
            ((CompoundButton) view).setChecked(isEnabled);

        } else {
            final Drawable drawable = isEnabled ? mDrawableOn : mDrawableOff;
            view.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawable, null);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        AContext.hideInputKeyboard(buttonView);
    }
}