package com.foodridge.view;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.WeekWorkingTime;
import com.foodridge.api.model.WorkingTime;
import com.foodridge.type.WeekDay;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
class WorkingTimeView extends LinearLayoutCompat {

    //VIEW's
    private final TextView mTvWeekDay;
    private final TextView mTvHours;
    private final View mIvEdit;

    //VALUE's
    private final TimeCache mTimeCache;
    @WeekDay
    private int mWeekDay;
    private WorkingTime mWorkingTime;
    private boolean mEditable;

    public WorkingTimeView(Context context) {
        this(context, null);
    }

    public WorkingTimeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WorkingTimeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_operating_hour, this);
            super.setOrientation(HORIZONTAL);
        }
        mTvWeekDay = (TextView) findViewById(R.id.tv_week_day);
        mTvHours = (TextView) findViewById(R.id.tv_hours);
        mIvEdit = findViewById(R.id.iv_edit);

        mTimeCache = isInEditMode() ? null : TimeCache.getInstance();
    }

    public void setData(@WeekDay int weekDay, @Nullable WorkingTime workingTime, boolean editable) {
        mWeekDay = weekDay;
        mWorkingTime = workingTime;
        mEditable = editable;

        mTvWeekDay.setText(mWeekDay);
        if (mWorkingTime == null) {
            mTvHours.setText(null);

        } else {
            final String startString;
            final String endString;
            if (mTimeCache.is24HourCached()) {
                startString = mWorkingTime.getStart();
                endString = mWorkingTime.getEnd();

            } else {
                final Point start = mWorkingTime.getStartTime();
                final Point end = mWorkingTime.getEndTime();

                startString = mTimeCache.format(TimeCache.HH_MM, start.x, start.y);
                endString = mTimeCache.format(TimeCache.HH_MM, end.x, end.y);
            }
            mTvHours.setText(getResources().getString(R.string.format_s_line_s, startString, endString));
        }
        Utils.changeVisibility(mIvEdit, mEditable ? VISIBLE : GONE);
    }

    public void changeTime(@NonNull String start, @NonNull String end) {
        if (mWorkingTime == null) {
            mWorkingTime = new WorkingTime();
        }
        mWorkingTime.setStart(start);
        mWorkingTime.setEnd(end);

        setData(mWeekDay, mWorkingTime, mEditable);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mIvEdit.setOnClickListener(l);
    }

    @Override
    public void setTag(Object tag) {
        final WeekWorkingTime time = (WeekWorkingTime) tag;
        switch (mWeekDay) {
            case WeekDay.MON:
                time.setMon(mWorkingTime);
                break;

            case WeekDay.TUE:
                time.setTue(mWorkingTime);
                break;

            case WeekDay.WED:
                time.setWed(mWorkingTime);
                break;

            case WeekDay.THU:
                time.setThu(mWorkingTime);
                break;

            case WeekDay.FRI:
                time.setFri(mWorkingTime);
                break;

            case WeekDay.SAT:
                time.setSat(mWorkingTime);
                break;

            case WeekDay.SUN:
                time.setSun(mWorkingTime);
                break;

            default:
                throw new IllegalStateException();
        }
    }

    @Nullable
    public WorkingTime getWorkingTime() {
        return mWorkingTime;
    }

    @WeekDay
    public int getWeekDay() {
        return mWeekDay;
    }

    public boolean isEditable() {
        return mEditable;
    }
}