package com.foodridge.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Chef;
import com.foodridge.model.Profile;
import com.foodridge.utility.Utils;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 26.04.16
 */
public class FollowersFollowView extends FrameLayout {

    //VIEW's
    private final TextView mTvFollow;
    private final TextView mTvFollowers;

    //VALUE
    private final String mProfileId;

    public FollowersFollowView(Context context) {
        this(context, null);
    }

    public FollowersFollowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FollowersFollowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_followers, this);
        }
        mTvFollow = (TextView) findViewById(R.id.btn_follow);
        mTvFollowers = (TextView) findViewById(R.id.tv_followers);

        mProfileId = isInEditMode() ? null : Profile.getInstance().getId();
    }

    public void setData(@NonNull Chef chef) {
        if (mProfileId.equals(chef.getId())) {
            Utils.changeVisibility(mTvFollow, INVISIBLE);

        } else {
            Utils.changeVisibility(mTvFollow, VISIBLE);
            mTvFollow.setText(chef.isFollow() ? R.string.Unfollow : R.string.Follow);
        }
        final int followers = chef.getFollowers();
        if (followers <= 0) {
            Utils.changeVisibility(mTvFollowers, INVISIBLE);

        } else {
            Utils.changeVisibility(mTvFollowers, VISIBLE);
            @StringRes final int stringRes = followers == 1 ? R.string.format_d_Follower : R.string.format_d_Followers;
            mTvFollowers.setText(Html.fromHtml(getContext().getString(stringRes, followers)));
        }
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mTvFollow.setOnClickListener(l);
    }
}