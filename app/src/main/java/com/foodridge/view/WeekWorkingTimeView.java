package com.foodridge.view;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;

import com.foodridge.R;
import com.foodridge.api.model.WeekWorkingTime;
import com.foodridge.api.model.WorkingTime;
import com.foodridge.dialog.WorkingTimeDialog;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.type.WeekDay;
import com.foodridge.utility.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 15/04/16
 */
public class WeekWorkingTimeView extends LinearLayoutCompat
        implements ObjectsReceiver, OnClickListener {

    public static final int CODE = R.id.code_week_working_time;

    //VIEW's
    private final WorkingTimeView[] mViews;
    private final WorkingTimeDialog mTimeDialog;

    //VALUE's
    private WeekWorkingTime mWorkingTime;
    private WeakReference<ObjectsReceiver> mReceiver;

    public WeekWorkingTimeView(Context context) {
        this(context, null);
    }

    public WeekWorkingTimeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeekWorkingTimeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            super.setOrientation(VERTICAL);
            super.setShowDividers(SHOW_DIVIDER_MIDDLE | SHOW_DIVIDER_END);
            super.setDividerDrawable(ContextCompat.getDrawable(context, R.drawable.divider_gray));
        }
        mViews = new WorkingTimeView[WeekDay.size];
        final LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        for (int i = 0; i < WeekDay.size; i++) {
            mViews[i] = new WorkingTimeView(getContext());
            mViews[i].setOnClickListener(this);

            super.addView(mViews[i], params);
        }
        mTimeDialog = isInEditMode() ? null : new WorkingTimeDialog(context, this);
    }

    public void setData(@NonNull WeekWorkingTime workingTime, boolean editable) {
        mWorkingTime = workingTime;

        final List<WorkingTime> time = new ArrayList<>(7);
        final List<Integer> weekDay = Utils.toList(Utils.WEEK_DAYS);

        time.add(workingTime.getMon());
        time.add(workingTime.getTue());
        time.add(workingTime.getWed());
        time.add(workingTime.getThu());
        time.add(workingTime.getFri());
        time.add(workingTime.getSat());
        time.add(workingTime.getSun());

//        if (!editable) {
//            for (Iterator<?> i1 = time.iterator(), i2 = weekDay.iterator(); i1.hasNext() && i2.hasNext(); ) {
//                i2.next();
//                if (i1.next() == null) {
//                    i1.remove();
//                    i2.remove();
//                }
//            }
//        }
//        super.removeAllViews();

        for (int i = 0, size = time.size(); i < size; i++) {
            //noinspection WrongConstant
            mViews[i].setData(weekDay.get(i), time.get(i), editable);
        }
    }

    @NonNull
    public WeekWorkingTime getEditedWorkingTime() {
        for (int i = 0, size = getChildCount(); i < size; i++) {
            getChildAt(i).setTag(mWorkingTime);
        }
        return mWorkingTime;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_edit:
                final WorkingTimeView view = (WorkingTimeView) v.getParent();
                mTimeDialog.show(view.getWorkingTime(), view.getWeekDay(), view);
                break;

            default:
                break;
        }
    }

    public void setReceiver(@NonNull ObjectsReceiver receiver) {
        mReceiver = new WeakReference<>(receiver);
    }

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        final WorkingTimeView view = (WorkingTimeView) objects[0];
        if (objects.length > 1) {
            view.changeTime((String) objects[1], (String) objects[2]);

        } else {
            view.setData(view.getWeekDay(), null, view.isEditable());
        }
        Utils.receiveObjects(mReceiver, CODE);
    }
}