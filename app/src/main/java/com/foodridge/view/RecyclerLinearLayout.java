package com.foodridge.view;

import android.content.Context;
import android.database.Observable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Stack;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 19.04.16.
 */
public class RecyclerLinearLayout extends LinearLayoutCompat {

    public static final int NO_POSITION = -1;

    private final ArrayList<ViewHolder> mHolders;
    private final Stack<ViewHolder> mCachedViews;
    private final RecyclerLinearLayoutDataObserver mObserver;
    private Adapter<ViewHolder> mAdapter;

    public RecyclerLinearLayout(@NonNull Context context) {
        this(context, null);
    }

    public RecyclerLinearLayout(@NonNull Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecyclerLinearLayout(@NonNull Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mHolders = new ArrayList<>();
        mCachedViews = new Stack<>();
        mObserver = new RecyclerLinearLayoutDataObserver();
    }

    @SuppressWarnings("unchecked")
    public void setAdapter(@Nullable Adapter adapter) {
        if (mAdapter == adapter) {
            return;

        } else if (mAdapter != null) {
            mAdapter.unregisterAdapterDataObserver(mObserver);
        }
        mAdapter = adapter;
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mObserver);
        }
        mCachedViews.clear();
        recycle();
    }

    public Adapter<ViewHolder> getAdapter() {
        return mAdapter;
    }

    private void recycle() {
        if (mAdapter == null) {
            mHolders.clear();
            removeAllViews();
            return;
        }
        if (mHolders.size() != mAdapter.getItemCount()) {
            if (mHolders.size() < mAdapter.getItemCount()) {
                for (int i = mHolders.size(); i < mAdapter.getItemCount(); i++) {
                    mHolders.add(getViewHolder());
                    addView(mHolders.get(i).mItemView, i);
                }
            }

            if (mHolders.size() > mAdapter.getItemCount()) {
                for (int i = mHolders.size() - 1; i > mAdapter.getItemCount() - 1; i--) {
                    cacheViewHolder(mHolders.remove(i));
                    removeViewAt(i);
                }
            }
        }
        for (int i = 0; i < mAdapter.getItemCount(); i++) {
            mAdapter.bindViewHolder(mHolders.get(i), i);
        }
    }

    @NonNull
    private ViewHolder getViewHolder() {
        return mCachedViews.size() > 0 ? mCachedViews.pop() : mAdapter.createViewHolder(this);
    }

    private void cacheViewHolder(@NonNull ViewHolder holder) {
        mCachedViews.push(holder);
    }

    @Nullable
    public ViewHolder getChildViewHolder(int position) {
        if (position < 0 || position >= mHolders.size())
            return null;

        return mHolders.get(position);
    }

    public int getChildAdapterPosition(View v) {
        final ViewHolder holder = (ViewHolder) v.getTag();
        if (holder != null) {
            return holder.mPosition;
        }

        return NO_POSITION;
    }

    private class RecyclerLinearLayoutDataObserver extends AdapterDataObserver {
        @Override
        public void onChanged() {
            recycle();
        }
    }

    public static abstract class AdapterDataObserver {
        public void onChanged() {
            // Do nothing
        }
    }

    static class AdapterDataObservable extends Observable<AdapterDataObserver> {
        public void notifyChanged() {
            for (int i = mObservers.size() - 1; i >= 0; i--) {
                mObservers.get(i).onChanged();
            }
        }
    }

    public static abstract class ViewHolder<VIEW extends View> {

        public final VIEW mItemView;
        int mPosition = NO_POSITION;

        public ViewHolder(VIEW itemView) {
            mItemView = itemView;
        }
    }

    public static abstract class Adapter<VH extends ViewHolder> {

        private static final int CACHED_VIEW_HOLDERS_CAPACITY = 0;

        private final AdapterDataObservable mObservable = new AdapterDataObservable();

        public abstract int getItemCount();

        private VH createViewHolder(ViewGroup parent) {
            return onCreateViewHolder(parent);
        }

        public abstract VH onCreateViewHolder(ViewGroup parent);

        public void bindViewHolder(VH holder, int position) {
            holder.mPosition = position;
            holder.mItemView.setTag(holder);
            onBindViewHolder(holder, position);
        }

        public abstract void onBindViewHolder(VH viewHolder, int position);

        public final void notifyDataSetChanged() {
            mObservable.notifyChanged();
        }

        public void unregisterAdapterDataObserver(RecyclerLinearLayoutDataObserver observer) {
            mObservable.unregisterObserver(observer);
        }

        public void registerAdapterDataObserver(RecyclerLinearLayoutDataObserver observer) {
            mObservable.registerObserver(observer);
        }

        private int getCachedViewHoldersCapacity() {
            return CACHED_VIEW_HOLDERS_CAPACITY;
        }
    }
}