package com.foodridge.view;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.utility.BaseUtils;
import com.foodridge.utility.GeoCache;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 15/04/16
 */
public class DeliveryAddressView extends RelativeLayout {

    //VIEW's
    private final ImageView mIvMap;
    private final TextView mTvTitle;
    private final TextView mTvStreet;
    private final TextView mTvCity;
    private final TextView mTvState;
    private final View mBtnEdit;

    //VALUE's
    private static Integer sLayoutHeight;
    private static Point sMapSize;

    private final GeoCache mGeoCache;

    public DeliveryAddressView(Context context) {
        this(context, null);
    }

    public DeliveryAddressView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DeliveryAddressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_delivery_address, this);

            super.setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.spacing));
        }
        mIvMap = (ImageView) findViewById(R.id.iv_map);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvStreet = (TextView) findViewById(R.id.tv_street);
        mTvCity = (TextView) findViewById(R.id.tv_city);
        mTvState = (TextView) findViewById(R.id.tv_state);
        mBtnEdit = findViewById(R.id.btn_delivery_address);

        BaseUtils.prepare(mBtnEdit);
        if (sMapSize == null || sLayoutHeight == null) {
            final Point screenSize = isInEditMode() ? new Point(1000, 500) : AContext.getScreenSize();

            mTvStreet.measure(screenSize.x, screenSize.y);
            mTvCity.measure(screenSize.x, screenSize.y);
            mTvState.measure(screenSize.x, screenSize.y);

            sLayoutHeight = mTvStreet.getMeasuredHeight()
                    + mTvCity.getMeasuredHeight()
                    + mTvState.getMeasuredHeight()
                    + getResources().getDimensionPixelSize(R.dimen.stroke) * 2;

            final int height = sLayoutHeight + getResources().getDimensionPixelSize(R.dimen.spacing);

            sMapSize = new Point(screenSize.x, height);
        }
        findViewById(R.id.layout_content).getLayoutParams().height = sLayoutHeight;
        findViewById(R.id.view_gradient).getLayoutParams().width = (int) (sMapSize.x / 1.33F);

        final ViewGroup.LayoutParams params = mIvMap.getLayoutParams();
        params.width = sMapSize.x;
        params.height = sMapSize.y;

        if (isInEditMode() || !AConstant.DEBUG) {
            mGeoCache = null;
            mIvMap.setImageResource(R.drawable.background_address);

        } else {
            mGeoCache = GeoCache.getInstance();
            mGeoCache.loadMapUri(mIvMap, null);
        }
    }

    public void setData(@Nullable DeliveryInfo info, boolean editable) {
        if (info == null) {
            mTvStreet.setText(null);
            mTvCity.setText(null);
            mTvState.setText(null);

            if (AConstant.DEBUG) {
                mGeoCache.loadMapUri(mIvMap, null);
            }

        } else {
            mTvStreet.setText(info.getStreet());
            mTvCity.setText(info.getCity());
            mTvState.setText(info.getCountry());

            if (AConstant.DEBUG) {
                mGeoCache.loadStaticMap(mIvMap, sMapSize, info);
            }
        }
        Utils.changeVisibility(mBtnEdit, editable ? VISIBLE : GONE);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mBtnEdit.setOnClickListener(l);
    }

    public void setTitle(@StringRes int stringRes, @ColorInt int textColor) {
        mTvTitle.setText(stringRes);
        mTvTitle.setTextColor(textColor);
    }

    public void setTitle(@StringRes int stringRes) {
        mTvTitle.setText(stringRes);
    }

    /**
     * default - true & bold
     */
    public void setTitleParams(boolean allCaps, int typeFace, int paddingTop) {
        mTvTitle.setAllCaps(allCaps);
        mTvTitle.setTypeface(null, typeFace);
        mTvTitle.setPadding(mTvTitle.getPaddingLeft(), paddingTop, mTvTitle.getPaddingRight(), mTvTitle.getPaddingBottom());
    }
}