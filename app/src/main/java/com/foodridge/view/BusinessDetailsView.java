package com.foodridge.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.BusinessInfo;
import com.foodridge.utility.Utils;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 18.04.16
 */
public class BusinessDetailsView extends RelativeLayout {

    //VIEW's
    private final TextView mTvTitle;
    private final TextView mTvName;
    private final TextView mTvNumber;
    private final TextView mTvPhone;
    private final TextView mTvLink;
    private final View mBtnEdit;

    public BusinessDetailsView(Context context) {
        this(context, null);
    }

    public BusinessDetailsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BusinessDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_business_details, this);
        }
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvNumber = (TextView) findViewById(R.id.tv_number);
        mTvPhone = (TextView) findViewById(R.id.tv_phone);
        mTvLink = (TextView) findViewById(R.id.tv_link);
        mBtnEdit = findViewById(R.id.btn_business_details);

        mTvLink.setOnClickListener(mLinkClickListener);
    }

    public void setData(@Nullable BusinessInfo businessInfo, boolean editable) {
        if (businessInfo == null) {
            mTvName.setText(null);
            mTvNumber.setText(null);
            mTvPhone.setText(null);
            mTvLink.setText(null);

        } else {
            mTvName.setText(businessInfo.getName());
            mTvNumber.setText(businessInfo.getRegistrationId());
            mTvPhone.setText(businessInfo.getPhone());
            mTvLink.setText(businessInfo.getSite());
        }
        Utils.changeVisibility(mBtnEdit, editable ? VISIBLE : GONE);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mBtnEdit.setOnClickListener(l);
    }

    public void setTitle(@StringRes int stringRes, @ColorInt int textColor) {
        mTvTitle.setText(stringRes);
        mTvTitle.setTextColor(textColor);
    }

    private final OnClickListener mLinkClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String url = mTvLink.getText().toString();
            if (url.startsWith("www.")) {
                url = "http://" + url;
            }
            if (url.length() == 0 || !Utils.startIntent(v.getContext(), new Intent(Intent.ACTION_VIEW, Uri.parse(url)))) {
                Snackbar.make(v, R.string.Web_site_wrong, Snackbar.LENGTH_LONG).show();
            }
        }
    };
}