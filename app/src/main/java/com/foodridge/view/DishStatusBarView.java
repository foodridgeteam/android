package com.foodridge.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Meal;
import com.foodridge.type.DeliveryType;
import com.foodridge.utility.Utils;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 25.04.16
 */
public class DishStatusBarView extends LinearLayoutCompat {

    // VIEW's
    private final AppCompatImageView mIvPickUp;
    private final AppCompatImageView mIvDelivery;
    private final AppCompatImageView mIvServings;
    private final TextView mTvServingsCounter;
    private final AppCompatImageView mIvRatings;
    private final TextView mTvRatingsCounter;

    //VALUE's
    private final String mMoreThan;

    public DishStatusBarView(Context context) {
        this(context, null);
    }

    public DishStatusBarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DishStatusBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_dish_actions, this);

            super.setOrientation(HORIZONTAL);
            super.setBackgroundColor(Color.WHITE);

            final int padding = getResources().getDimensionPixelSize(R.dimen.spacing);
            super.setPadding(padding * 2, padding, padding * 2, padding);
        }
        mIvPickUp = (AppCompatImageView) findViewById(R.id.iv_pick_up);
        mIvDelivery = (AppCompatImageView) findViewById(R.id.iv_delivery);

        mIvServings = (AppCompatImageView) findViewById(R.id.iv_servings);
        mTvServingsCounter = (TextView) findViewById(R.id.tv_servings_counter);
        mIvRatings = (AppCompatImageView) findViewById(R.id.iv_ratings);
        mTvRatingsCounter = (TextView) findViewById(R.id.tv_ratings_counter);

        mMoreThan = isInEditMode() ? null : getResources().getString(R.string.more_than_99);
    }

    public void setData(@NonNull Meal meal) {
        final boolean pickupAvailable;
        final boolean deliveryAvailable;
        switch (meal.getDeliveryType()) {
            case DeliveryType.PICKUP_AND_DELIVERY:
                pickupAvailable = true;
                deliveryAvailable = true;
                break;

            case DeliveryType.DELIVERY:
                pickupAvailable = false;
                deliveryAvailable = true;
                break;

            default:
            case DeliveryType.PICKUP:
                pickupAvailable = true;
                deliveryAvailable = false;
                break;
        }
        Utils.setChecked(mIvPickUp, pickupAvailable);
        Utils.setChecked(mIvDelivery, deliveryAvailable);

        if (meal.getPortionsAvailable() > 0) {
            Utils.setChecked(mIvServings, true);

            Utils.changeVisibility(mTvServingsCounter, VISIBLE);
            setTextCounter(mTvServingsCounter, meal.getPortionsAvailable());

        } else {
            Utils.setChecked(mIvServings, false);
            Utils.changeVisibility(mTvServingsCounter, GONE);
        }
        if (meal.getVotesCount() > 0) {
            Utils.setChecked(mIvRatings, true);

            Utils.changeVisibility(mTvRatingsCounter, VISIBLE);
            setTextCounter(mTvRatingsCounter, meal.getVotesCount());

        } else {
            Utils.setChecked(mIvRatings, false);
            Utils.changeVisibility(mTvRatingsCounter, GONE);
        }
    }

    private void setTextCounter(@NonNull TextView view, int counter) {
        view.setText(counter < 100 ? String.valueOf(counter) : mMoreThan);
    }
}