package com.foodridge.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.IdName;
import com.foodridge.application.ASettings;
import com.foodridge.utility.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 15.04.16.
 */
public class IngredientsView extends LinearLayoutCompat {

    //VIEW's
    private final List<TextView> mIngredients;

    //VALUE's
    private final int mPadding;
    private final int mTextSize;
    private final int mTextColor;
    private final LayoutParams mParams;

    private final List<IdName> mListIngredients;

    public IngredientsView(@NonNull Context context) {
        this(context, null);
    }

    public IngredientsView(@NonNull Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IngredientsView(@NonNull Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            super.setOrientation(VERTICAL);
            super.setShowDividers(SHOW_DIVIDER_MIDDLE | SHOW_DIVIDER_END | SHOW_DIVIDER_BEGINNING);
            super.setDividerDrawable(ContextCompat.getDrawable(context, R.drawable.divider_gray_light));
        }
        mIngredients = new ArrayList<>();
        mPadding = getResources().getDimensionPixelSize(R.dimen.spacing);
        mTextSize = getResources().getDimensionPixelSize(R.dimen.text_normal);
        mTextColor = ContextCompat.getColor(context, R.color.gray);
        mParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        mListIngredients = new ArrayList<>(ASettings.getInstance().getIngredients());
        Collections.sort(mListIngredients);
    }

    public void setData(@Nullable int... ingredients) {
        if (ingredients == null || ingredients.length == 0) {
            mIngredients.clear();
            super.removeAllViews();

            mIngredients.add(null);

            final TextView view = new TextView(getContext());
            view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
            view.setPadding(0, mPadding, 0, mPadding);
            view.setText(R.string.Nothing_here);
            view.setTextColor(mTextColor);

            super.addView(view, 0, mParams);

        } else {
            final int newSize = mIngredients.size();

            if (ingredients.length > mIngredients.size()) {
                TextView view;
                for (int i = newSize; i < ingredients.length; i++) {
                    view = new TextView(getContext());
                    view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
                    view.setPadding(0, mPadding, 0, mPadding);
                    view.setTextColor(mTextColor);

                    mIngredients.add(i, view);
                    super.addView(view, i, mParams);
                }

            } else {
                for (int i = mIngredients.size() - 1; i >= mIngredients.size(); i--) {
                    mIngredients.remove(i);
                    super.removeViewAt(i);
                }
            }
            final String formatIngredient = getContext().getString(R.string.format_ingredient);
            final List<Integer> list = Utils.toList(ingredients);

            int count = 0;
            int id;

            for (IdName idName : mListIngredients) {
                if (list.size() == 0) {
                    break;
                }
                id = idName.getId();
                for (Iterator<Integer> iterator = list.iterator(); iterator.hasNext(); ) {
                    if (id == iterator.next()) {
                        mIngredients.get(count++).setText(String.format(formatIngredient, idName.getName()));
                        iterator.remove();
                        break;
                    }
                }
            }
        }
    }
}