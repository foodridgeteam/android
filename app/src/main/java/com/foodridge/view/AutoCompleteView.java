package com.foodridge.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.IdName;
import com.tokenautocomplete.TokenCompleteTextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 13.04.16
 */
public class AutoCompleteView extends TokenCompleteTextView<Object> {

    private final List<IdName> mAvailableOptions;

    public AutoCompleteView(Context context) {
        this(context, null);
    }

    public AutoCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mAvailableOptions = new ArrayList<>();

        super.allowCollapse(false);
        super.setSaveEnabled(false);
        super.allowDuplicates(false);
        super.setTokenClickStyle(TokenClickStyle.Delete);
    }

    public void setAvailableOptions(@NonNull Collection<IdName> values) {
        mAvailableOptions.clear();
        mAvailableOptions.addAll(values);

        final String[] strings = new String[mAvailableOptions.size()];
        int count = 0;
        for (IdName value : mAvailableOptions) {
            strings[count++] = value.getName();
        }
        setAvailableOptions(strings);
    }

    public void setAvailableOptions(@NonNull String... strings) {
        super.setAdapter(new ArrayAdapter<>(getContext(), R.layout.view_item_autocomplete_popup, android.R.id.text1, strings));
    }

    public void setSelectedOptions(@NonNull Collection<Integer> values) {
        final ArrayList<Integer> selected = new ArrayList<>(values);
        for (IdName value : mAvailableOptions) {
            if (selected.size() == 0) {
                break;
            }
            for (Iterator<Integer> iterator = selected.iterator(); iterator.hasNext(); ) {
                if (iterator.next() == value.getId()) {
                    iterator.remove();
                    addObject(value);
                    break;
                }
            }
        }
    }

    @Override
    protected View getViewForObject(Object object) {
        final View view = inflate(getContext(), R.layout.view_item_autocomplete_dish, null);
        ((TextView) view.findViewById(R.id.text_view)).setText(object.toString());

        return view;
    }

    @Override
    protected IdName defaultObject(@NonNull String string) {
        for (IdName value : mAvailableOptions) {
            if (value.getName().equals(string)) {
                return value;
            }
        }
        return null;
    }

    public void getObjects(@NonNull ArrayList<Integer> list, @NonNull ArrayMap<String, IdName> map) {
        list.clear();
        for (Object obj : getObjects()) {
            if (obj instanceof IdName) {
                list.add(obj.hashCode());

            } else {
                final IdName idName = map.get(obj);
                if (idName != null) {
                    list.add(idName.getId());
                }
            }
        }
    }
}