package com.foodridge.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.ViewPropertyAnimation;
import com.bumptech.glide.request.animation.ViewPropertyAnimation.Animator;
import com.bumptech.glide.request.target.Target;
import com.foodridge.application.AConstant;
import com.foodridge.interfaces.CleaningEntity;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 06/04/16
 */
public class ProgressImageView extends FrameLayout
        implements RequestListener, CleaningEntity {

    //VIEW's
    private final ProgressBar mProgressBar;
    private final ImageView mImageView;

    //VALUE's
    private final RequestManager mGlide;
    private final String mPackageName;
    private final ViewPropertyAnimation.Animator mAnimator;

    private boolean mProgressBarShowing;
    private Drawable mPlaceholder;

    public ProgressImageView(@NonNull Context context) {
        this(context, null);
    }

    public ProgressImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            mProgressBar = new ProgressBar(context);
            super.addView(mProgressBar, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        }
        {
            mImageView = new ImageView(context);
            mImageView.setScaleType(ScaleType.CENTER_CROP);

            super.addView(mImageView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        }
        mGlide = isInEditMode() ? null : Glide.with(context);
        mPackageName = context.getPackageName();
        mAnimator = new Animator() {
            @Override
            public void animate(View view) {
                view.setAlpha(0F);
                view.animate().alpha(1F).setDuration(AConstant.ANIMATION_DURATION).start();
            }
        };
        mProgressBarShowing = true;
    }

    public void setPlaceholder(@DrawableRes int drawableResId) {
        setPlaceholder(drawableResId <= 0 ? null : ContextCompat.getDrawable(getContext(), drawableResId));
    }

    public void setPlaceholder(Drawable placeholder) {
        mPlaceholder = placeholder;
    }

    public void show(@DrawableRes int drawableResId) {
        show(drawableResId <= 0 ? null : Uri.parse("android.resource://" + mPackageName + "/" + drawableResId));
    }

    public void show(@Nullable String string) {
        show(string == null ? null : Uri.parse(string));
    }

    @SuppressWarnings("unchecked")
    public void show(@Nullable Uri uri) {
        if (mProgressBarShowing) {
            Utils.changeVisibility(mProgressBar, VISIBLE);
        }
        mGlide.load(uri).asBitmap().animate(mAnimator).centerCrop().placeholder(mPlaceholder).listener(this).into(mImageView);
    }

    @Override
    public boolean onException(Exception e, Object model, Target target, boolean isFirstResource) {
        Utils.changeVisibility(mProgressBar, INVISIBLE);
        return false;
    }

    @Override
    public boolean onResourceReady(Object resource, Object model, Target target, boolean isFromMemoryCache, boolean isFirstResource) {
        Utils.changeVisibility(mProgressBar, INVISIBLE);
        return false;
    }

    @Override
    public void onCleanUp() throws Exception {
        Utils.cleanUp(mPlaceholder);
        Utils.cleanUp(mProgressBar);
        Utils.cleanUp(mImageView);
    }
}