package com.foodridge.view.creditcard;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 27.04.16.
 */
public class CreditCardTextWatcher implements TextWatcher {

    private final EditText mEditText;
    private final TextWatcherListener mListener;

    public interface TextWatcherListener {
        void onTextChanged(@NonNull EditText view, String text);
    }

    public CreditCardTextWatcher(@NonNull EditText editText, @NonNull TextWatcherListener listener) {
        mEditText = editText;
        mListener = listener;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mListener.onTextChanged(mEditText, s.toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}