package com.foodridge.view.creditcard;

import android.support.annotation.Nullable;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 27.04.16.
 */
public class StringUtil {
    private static final int INDEX_NOT_FOUND = -1;

    @Nullable
    public static String difference(@Nullable String str1, @Nullable String str2) {
        if (str1 == null) {
            return str2;
        }
        if (str2 == null) {
            return str1;
        }
        int at = indexOfDifference(str1, str2);
        if (at == INDEX_NOT_FOUND) {
            return "";
        }
        return str2.substring(at);
    }

    public static int indexOfDifference(@Nullable CharSequence cs1, @Nullable CharSequence cs2) {
        if (cs1 == cs2) {
            return INDEX_NOT_FOUND;
        }
        if (cs1 == null || cs2 == null) {
            return 0;
        }

        int i;
        for (i = 0; i < cs1.length() && i < cs2.length(); ++i) {
            if (cs1.charAt(i) != cs2.charAt(i)) {
                break;
            }
        }
        if (i < cs2.length() || i < cs1.length()) {
            return i;
        }

        return INDEX_NOT_FOUND;
    }
}
