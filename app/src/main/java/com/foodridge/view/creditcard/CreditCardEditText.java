package com.foodridge.view.creditcard;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.view.creditcard.CreditCardTextWatcher.TextWatcherListener;

import java.util.ArrayList;
import java.util.List;

import static android.support.v4.content.ContextCompat.getDrawable;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 27.04.16.
 */
public class CreditCardEditText extends AppCompatEditText
        implements TextWatcherListener {

    private static final String SEPARATOR = "-";

    /**
     * Default minimum and maximum card length.
     */
    private static final int MINIMUM_CREDIT_CARD_LENGTH = 13;
    private static final int MAXIMUM_CREDIT_CARD_LENGTH = 19;

    /**
     * List of CreditCard objects containing the image to display
     * and the regex for pattern matching.
     */
    private List<CreditCard> mListOfCreditCardChecks;

    /**
     * This drawable is shown by default and when no match is found
     */
    private Drawable mNoMatchFoundDrawable;
    private CreditCardTextWatcher mTextWatcher;

    private int mMinimumCreditCardLength, mMaximumCreditCardLength;
    private String mPreviousText;

    public CreditCardEditText(@NonNull Context context) {
        super(context);
        init();
    }

    public CreditCardEditText(@NonNull Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CreditCardEditText(@NonNull Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mMinimumCreditCardLength = MINIMUM_CREDIT_CARD_LENGTH;
        mMaximumCreditCardLength = MAXIMUM_CREDIT_CARD_LENGTH;
        mNoMatchFoundDrawable = getDrawable(getContext(), R.drawable.svg_card_gray);
        mNoMatchFoundDrawable.setBounds(0, 0, mNoMatchFoundDrawable.getIntrinsicWidth(), mNoMatchFoundDrawable.getIntrinsicHeight());
        {
            super.setInputType(InputType.TYPE_CLASS_NUMBER);
            super.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mMaximumCreditCardLength)});
            setCardMatchDrawable(mNoMatchFoundDrawable);
        }

        mListOfCreditCardChecks = CreditCard.DEFAULT_CARDS;

        mTextWatcher = new CreditCardTextWatcher(this, this);
        addTextChangedListener(mTextWatcher);
    }

    public Drawable getNoMatchFoundDrawable() {
        return mNoMatchFoundDrawable;
    }

    public void setNoMatchFoundDrawable(Drawable noMatchFoundDrawable) {
        if (noMatchFoundDrawable != null) {
            mNoMatchFoundDrawable = noMatchFoundDrawable;
            mNoMatchFoundDrawable.setBounds(0, 0, mNoMatchFoundDrawable.getIntrinsicWidth(), mNoMatchFoundDrawable.getIntrinsicHeight());
        }
    }

    public int getMaximumCreditCardLength() {
        return mMaximumCreditCardLength;
    }

    public void setMaximumCreditCardLength(int maximumCreditCardLength) {
        mMaximumCreditCardLength = maximumCreditCardLength;
    }

    public int getMinimumCreditCardLength() {
        return mMinimumCreditCardLength;
    }

    public void setMinimumCreditCardLength(int minimumCreditCardLength) {
        mMinimumCreditCardLength = minimumCreditCardLength;
    }

    @Nullable
    public String getCreditCardNumber() {
        String creditCardNumber = getText().toString().replace(SEPARATOR, "");
        if (creditCardNumber.length() >= mMinimumCreditCardLength && creditCardNumber.length() <= mMaximumCreditCardLength) {
            return creditCardNumber;
        }

        return null;
    }

    @Override
    public void onTextChanged(@NonNull EditText view, String text) {
        matchRegexPatternsWithText(text.replace(SEPARATOR, ""));

        if (!SEPARATOR.equals(StringUtil.difference(text, mPreviousText))) {
            addSeparatorToText();
        }
        mPreviousText = text;
    }

    private void addSeparatorToText() {
        String text = getText().toString();
        text = text.replace(SEPARATOR, "");
        if (text.length() > 16) {
            return;
        }
        int interval = 4;
        char separator = SEPARATOR.charAt(0);

        StringBuilder stringBuilder = new StringBuilder(text);
        for (int i = 0; i < text.length() / interval; i++) {
            stringBuilder.insert(((i + 1) * interval) + i, separator);
        }
        removeTextChangedListener(mTextWatcher);
        setText(stringBuilder.toString());
        setSelection(getText().length());
        addTextChangedListener(mTextWatcher);
    }

    private void matchRegexPatternsWithText(String text) {
        if (mListOfCreditCardChecks != null && mListOfCreditCardChecks.size() > 0) {
            Drawable drawable = null;
            for (CreditCard creditCard : mListOfCreditCardChecks) {
                String regex = creditCard.getRegexPattern();
                if (text.matches(regex)) {
                    drawable = ContextCompat.getDrawable(getContext(), creditCard.getDrawableRes());
                    break;
                }
            }
            showRightDrawable(drawable);
        }
    }

    private void showRightDrawable(@Nullable Drawable drawable) {
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            setCardMatchDrawable(drawable);
        } else {
            setCardMatchDrawable(mNoMatchFoundDrawable);
        }
    }

    private void setCardMatchDrawable(@Nullable Drawable drawable) {
        setCompoundDrawables(drawable, getCompoundDrawables()[1], getCompoundDrawables()[2], getCompoundDrawables()[3]);
    }

    public static class CreditCard {
        public static final CreditCard VISA;
        public static final CreditCard MASTER_CARD;
        public static final CreditCard AMERICAN_EXPRESS;

        public static final List<CreditCard> DEFAULT_CARDS;

        private String mRegexPattern;
        private int mDrawableRes;

        static {
            // TODO - fix cards regex
            VISA = new CreditCard("^4[0-9]{0,15}$", R.drawable.svg_visa);
            MASTER_CARD = new CreditCard("^5[1-5][0-9]{0,19}$", R.drawable.svg_mastercard);
            AMERICAN_EXPRESS = new CreditCard("^3[47][0-9]{0,14}$", R.drawable.svg_amex);

            DEFAULT_CARDS = new ArrayList<>(3);
            DEFAULT_CARDS.add(VISA);
            DEFAULT_CARDS.add(MASTER_CARD);
            DEFAULT_CARDS.add(AMERICAN_EXPRESS);
        }

        public CreditCard(String regexPattern, @DrawableRes int drawableRes) {
            if (regexPattern == null || drawableRes <= 0) {
                throw new IllegalArgumentException();
            }
            mRegexPattern = regexPattern;
            mDrawableRes = drawableRes;
        }

        public String getRegexPattern() {
            return mRegexPattern;
        }

        @DrawableRes
        public int getDrawableRes() {
            return mDrawableRes;
        }
    }
}
