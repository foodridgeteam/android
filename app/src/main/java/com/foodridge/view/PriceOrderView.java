package com.foodridge.view;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Meal;
import com.foodridge.model.Profile;
import com.foodridge.utility.Utils;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 26.04.16
 */
public class PriceOrderView extends RelativeLayout {

    //VIEW's
    private final TextView mTvPrice;
    private final TextView mTvPriceRest;
    private final TextView mBtnOrder;

    //VALUE
    private final String mProfileId;

    public PriceOrderView(Context context) {
        this(context, null);
    }

    public PriceOrderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PriceOrderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        {
            inflate(context, R.layout.view_dish_price_layout, this);
        }
        mTvPrice = (TextView) findViewById(R.id.tv_price);
        mTvPriceRest = (TextView) findViewById(R.id.tv_price_rest);
        mBtnOrder = (TextView) findViewById(R.id.btn_order);

        mProfileId = isInEditMode() ? null : Profile.getInstance().getId();
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mBtnOrder.setOnClickListener(l);
    }

    public void setTextColor(@ColorInt int color) {
        mTvPrice.setTextColor(color);
        mTvPriceRest.setTextColor(color);
    }

    public void setData(@NonNull Meal meal) {
        final Float price = meal.getPortionPrice();
        final int rest = (int) ((price - price.intValue()) * 100);

        mTvPrice.setText(getContext().getString(R.string.format_dollar_d, price.intValue()));
        mTvPriceRest.setText(getContext().getString(R.string.format_02d, rest));

        final boolean currentUser = mProfileId.equals(meal.getChef().getId());
        final boolean orderEnabled = meal.getPortionsAvailable() > 0;
        if (currentUser) {
            Utils.changeVisibility(mBtnOrder, INVISIBLE);

        } else {
            Utils.changeVisibility(mBtnOrder, VISIBLE);
            mBtnOrder.setText(orderEnabled ? R.string.Order_now : R.string.Sold_out);
        }
        mBtnOrder.setEnabled(orderEnabled || currentUser);
    }
}