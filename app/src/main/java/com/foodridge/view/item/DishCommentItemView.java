package com.foodridge.view.item;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.foodridge.R;
import com.foodridge.api.model.Comment;
import com.foodridge.api.model.User;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;
import com.foodridge.view.RatingView;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 18.04.16.
 */
public class DishCommentItemView extends FrameLayout
        implements DataEntity<DishCommentItemView, Comment> {

    // VIEW's
    private final ImageView mIvAvatar;
    private final TextView mTvName;
    private final TextView mTvDate;
    private final View mDivider;
    private final TextView mTvText;
    private final RatingView mRating;

    // VALUE's
    private final RequestManager mGlide;
    private final TimeCache mTimeCache;
    private final Drawable mPlaceholder;

    public DishCommentItemView(@NonNull Context context) {
        super(context);
        {
            inflate(context, R.layout.view_item_comment, this);
        }
        mIvAvatar = (ImageView) findViewById(R.id.iv_avatar);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvDate = (TextView) findViewById(R.id.tv_date);
        mDivider = findViewById(R.id.divider);
        mTvText = (TextView) findViewById(R.id.tv_text);
        mRating = (RatingView) findViewById(R.id.rating);

        mGlide = Glide.with(context);
        mTimeCache = TimeCache.getInstance();
        mPlaceholder = ContextCompat.getDrawable(context, R.drawable.svg_user_placeholder);
    }

    @Override
    public void setData(@Nullable Comment comment, @NonNull Object... objects) {
        if (comment == null) {
            throw new IllegalStateException();
        }
        final User user = comment.getUser();

        Utils.loadInto(mGlide, user.getAvatar(), mPlaceholder, mIvAvatar);
        mTvName.setText(user.getName());
        mTvDate.setText(mTimeCache.getCuteTime(TimeCache.DD_MMM_YYYY, comment.getCreated()));
        mRating.setRating(comment.getRating());

        final int visibilityText;
        if (TextUtils.isEmpty(comment.getMessage())) {
            visibilityText = GONE;

        } else {
            visibilityText = VISIBLE;
            mTvText.setText(comment.getMessage());
        }
        Utils.changeVisibility(mDivider, visibilityText);
        Utils.changeVisibility(mTvText, visibilityText);
    }

    @Nullable
    @Override
    public Comment getData() {
        return null;
    }

    @NonNull
    @Override
    public DishCommentItemView getSelf() {
        return this;
    }
}