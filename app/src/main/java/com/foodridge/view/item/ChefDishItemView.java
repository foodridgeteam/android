package com.foodridge.view.item;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.foodridge.R;
import com.foodridge.api.model.Meal;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;
import com.foodridge.view.PriceOrderView;
import com.foodridge.view.RatingView;

import java.lang.ref.WeakReference;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 13.04.16.
 */
@SuppressLint("ViewConstructor")
public class ChefDishItemView extends FrameLayout
        implements DataEntity<ChefDishItemView, Meal>, OnClickListener {

    public static final int CODE = R.id.code_dish_item;

    // VIEW's
    private final ImageView mIvPhoto;
    private final TextView mTvName;
    private final RatingView mRating;

    private final PriceOrderView mPriceView;

    //VALUE's
    private final WeakReference<ObjectsReceiver> mReceiver;

    private final RequestManager mGlide;
    private final Drawable mHolder;

    public ChefDishItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        super(context);
        {
            inflate(context, R.layout.view_item_chef_dish, this);
            super.setBackground(null);
        }
        mIvPhoto = (ImageView) findViewById(R.id.iv_photo);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mRating = (RatingView) findViewById(R.id.rating);

        mPriceView = (PriceOrderView) findViewById(R.id.view_dish_price);

        mPriceView.setTextColor(ContextCompat.getColor(context, R.color.gray));

        mReceiver = receiver;

        mGlide = Glide.with(context);
        mHolder = ContextCompat.getDrawable(context, R.drawable.svg_logo_small_placeholder);

        super.setOnClickListener(this);
        mPriceView.setOnClickListener(this);
    }

    @Override
    public void setData(@Nullable Meal meal, @NonNull Object... objects) {
        if (meal == null) {
            throw new IllegalStateException();
        }
        setTag(meal);

        Utils.loadInto(mGlide, meal.getFirstPhoto(), mHolder, mIvPhoto);
        mTvName.setText(meal.getName());
        mRating.setRating(meal.getRating());

        mPriceView.setData(meal);
    }

    @Nullable
    @Override
    public Meal getData() {
        return null;
    }

    @NonNull
    @Override
    public ChefDishItemView getSelf() {
        return this;
    }

    @Override
    public void onClick(View v) {
        Utils.receiveObjects(mReceiver, CODE, v.getId() == R.id.btn_order, getTag());
    }
}