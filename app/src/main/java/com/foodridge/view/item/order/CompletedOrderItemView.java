package com.foodridge.view.item.order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Comment;
import com.foodridge.api.model.Order;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.type.OrderStatus;
import com.foodridge.type.OrderType;
import com.foodridge.utility.Utils;
import com.foodridge.view.RatingView;

import java.lang.ref.WeakReference;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 12.04.16.
 */
@SuppressLint("ViewConstructor")
public class CompletedOrderItemView extends BaseOrderItemView {

    // VIEW's
    private final View mRateChefLayout;
    private final View mBtnRate;

    private final View mRatedChefLayout;
    private final View mDivider;
    private final TextView mTvComment;
    private final RatingView mRating;

    public CompletedOrderItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver, @OrderType int type) {
        super(context, receiver, type);
        {
            inflate(context, R.layout.view_order_rate_chef, mRootLayout);
            inflate(context, R.layout.view_order_rated_chef, mRootLayout);
        }
        mRateChefLayout = findViewById(R.id.rate_chef_layout);
        mBtnRate = mRateChefLayout.findViewById(R.id.btn_rate);

        mRatedChefLayout = findViewById(R.id.rated_chef_layout);
        mDivider = mRatedChefLayout.findViewById(R.id.mid_divider);
        mTvComment = (TextView) mRatedChefLayout.findViewById(R.id.tv_comment);
        mRating = (RatingView) mRatedChefLayout.findViewById(R.id.rating);

        mTvOrderTime.setVisibility(INVISIBLE);
        mBtnRate.setOnClickListener(this);

        mLayoutUser.setEnabled(false);
        mLayoutUser.setOnClickListener(null);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void setData(@Nullable Order order, @NonNull Object... objects) {
        super.setData(order, objects);

        final Comment comment = order.getComment();
        if (comment == null) {
            Utils.changeVisibility(mRatedChefLayout, GONE);
            Utils.changeVisibility(mRateChefLayout, isClient() && order.getStatus() == OrderStatus.COMPLETED ? VISIBLE : GONE);
            mBtnRate.setEnabled(!isBlockedOrder());

        } else {
            Utils.changeVisibility(mRatedChefLayout, VISIBLE);
            Utils.changeVisibility(mRateChefLayout, GONE);
            {
                final int visibility = TextUtils.isEmpty(comment.getMessage()) ? GONE : VISIBLE;
                Utils.changeVisibility(mTvComment, visibility);
                Utils.changeVisibility(mDivider, visibility);
            }
            mTvComment.setText(comment.getMessage());
            mRating.setRating(comment.getRating());
        }
    }
}