package com.foodridge.view.item;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Meal;
import com.foodridge.application.ASettings;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;
import com.foodridge.view.RatingView;

import java.lang.ref.WeakReference;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 15.04.16
 */
@SuppressLint("ViewConstructor")
public class MyDishItemView extends FrameLayout
        implements DataEntity<MyDishItemView, Meal>, OnClickListener, OnCheckedChangeListener {

    public static final int CODE = R.id.code_my_dish_item;

    // VIEW's
    private final TextView mTvName;
    private final TextView mTvCuisine;
    private final TextView mTvPortions;
    private final RatingView mRating;
    private final TextView mTvComments;
    private final SwitchCompat mScListed;

    //VALUE's
    private final WeakReference<ObjectsReceiver> mReceiver;
    private final ASettings mSettings;

    public MyDishItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        super(context);
        {
            inflate(context, R.layout.view_item_my_dish, this);
        }
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvCuisine = (TextView) findViewById(R.id.tv_cuisine);
        mTvPortions = (TextView) findViewById(R.id.tv_portions);
        mRating = (RatingView) findViewById(R.id.rating);
        mTvComments = (TextView) findViewById(R.id.tv_comments);
        mScListed = (SwitchCompat) findViewById(R.id.switch_listed);

        mReceiver = receiver;
        mSettings = ASettings.getInstance();

        mScListed.setOnCheckedChangeListener(this);
        Utils.setOnClickListener(this, this, R.id.btn_edit, R.id.btn_share);
    }

    @Override
    public void setData(@Nullable Meal meal, @NonNull Object... objects) {
        if (meal == null) {
            throw new IllegalStateException();
        }
        setTag(meal);

        mTvName.setText(meal.getName());
        mTvCuisine.setText(Utils.findAll(meal.getCuisines(), mSettings.getCuisines(), false));
        mTvPortions.setText(Html.fromHtml(getResources().getString(R.string.format_Portions_sold_d, meal.getPortionsSold())));
        mRating.setRating(meal.getRating());
        mTvComments.setText(String.valueOf(meal.getVotesCount()));

        mScListed.setOnCheckedChangeListener(null);
        mScListed.setChecked(meal.isListed());
        mScListed.setOnCheckedChangeListener(this);
    }

    @Nullable
    @Override
    public Meal getData() {
        return null;
    }

    @NonNull
    @Override
    public MyDishItemView getSelf() {
        return this;
    }

    @Override
    public void onClick(View v) {
        Utils.receiveObjects(mReceiver, CODE, getTag(), v.getId() == R.id.btn_edit);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Utils.receiveObjects(mReceiver, CODE, getTag(), null);
    }
}