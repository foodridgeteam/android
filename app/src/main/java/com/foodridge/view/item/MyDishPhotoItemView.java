package com.foodridge.view.item;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.foodridge.R;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;
import com.foodridge.view.ProgressImageView;

import java.lang.ref.WeakReference;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 20/04/16
 */
@SuppressLint("ViewConstructor")
public class MyDishPhotoItemView extends ProgressImageView
        implements OnClickListener {

    public static final int CODE = R.id.code_dish_photo;

    private final WeakReference<ObjectsReceiver> mReceiver;
    private final Point mSize;

    public MyDishPhotoItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        super(context);

        mReceiver = receiver;

        final ImageView view = new ImageView(context);
        view.setImageResource(R.drawable.svg_circle_x);
        view.setScaleType(ScaleType.CENTER_INSIDE);

        final int dimen = getResources().getDimensionPixelSize(R.dimen.stroke_x3);

        final LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.TOP | Gravity.START);
        params.setMargins(dimen , dimen , dimen , dimen );

        view.setOnClickListener(this);

        super.addView(view, params);

        final int height = getResources().getDimensionPixelSize(R.dimen.add_dish_height) - dimen * 2;
        final int width = (int) (height * 1.333F);
        mSize = new Point(width, height);
    }

    @Override
    public void setLayoutParams(ViewGroup.LayoutParams params) {
        params.width = mSize.x;
        params.height = mSize.y;

        super.setLayoutParams(params);
    }

    public void setData(@NonNull Uri uri) {
        setTag(uri);
        super.show(uri);
    }

    @Override
    public void onClick(View v) {
        Utils.receiveObjects(mReceiver, CODE, getTag());
    }
}