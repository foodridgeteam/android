package com.foodridge.view.item.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.text.Html;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Order;
import com.foodridge.api.model.OrderMessage;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.type.OrderStatus;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;

import java.text.DateFormat;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 19.04.16.
 */
public class OrderTimelineItemView extends RelativeLayout
        implements DataEntity<OrderTimelineItemView, OrderMessage> {

    //VIEW's
    private final TextView mTvTime;
    private final TextView mTvName;
    private final TextView mTvMessage;
    private final AppCompatImageView mIvIcon;

    //VALUE's
    private final int mColorClient;
    private final int mColorChef;
    private final TimeCache mTimeCache;
    private final DateFormat mDateFormat;

    public OrderTimelineItemView(@NonNull Context context) {
        super(context);
        {
            inflate(context, R.layout.view_item_order_timeline, this);
        }
        mIvIcon = (AppCompatImageView) findViewById(R.id.iv_icon);
        mTvTime = (TextView) findViewById(R.id.tv_time);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvMessage = (TextView) findViewById(R.id.tv_message);

        mColorClient = ContextCompat.getColor(context, R.color.teal);
        mColorChef = ContextCompat.getColor(context, R.color.gray);
        mTimeCache = TimeCache.getInstance();
        mDateFormat = mTimeCache.is24HourCached() ? TimeCache.HTML_HH_MM24 : TimeCache.HTML_HH_MM;
    }

    @Override
    public void setData(@Nullable OrderMessage message, @NonNull Object... objects) {
        if (message == null) {
            throw new IllegalStateException();
        }
        final Order order = (Order) objects[0];

        final CharSequence messageText;
        switch (message.getStatus()) {
            case OrderStatus.ACCEPTED:
            case OrderStatus.CHEF_CANCEL:
            case OrderStatus.COMPLETED:
            case OrderStatus.CREATED:
            case OrderStatus.PAYED:
            case OrderStatus.PREPARED:
            case OrderStatus.RECEIVED:
            case OrderStatus.SENT:
            case OrderStatus.SYSTEM_CANCEL:
            case OrderStatus.USER_CANCEL:
                messageText = getResources().getString(Utils.getOrderStatusStringRes(message.getStatus()));
                break;

            default:
                messageText = message.getMessage();
                break;
        }
        mTvTime.setText(Html.fromHtml(mTimeCache.getCuteTime(mDateFormat, message.getCreated())));
        mTvMessage.setText(messageText);
        mIvIcon.setImageResource(Utils.getOrderStatusDrawableRes(message.getStatus()));

        if (order.getClient().getId().equals(message.getUserId())) {
            mIvIcon.setColorFilter(mColorClient);
            mTvName.setTextColor(mColorClient);
            mTvName.setText(order.getClient().getName());

        } else {
            mIvIcon.setColorFilter(mColorChef);
            mTvName.setTextColor(mColorChef);
//            mTvName.setText(getResources().getString(R.string.format_Chef_s, order.getMeal().getChef().getName()));
            mTvName.setText(order.getMeal().getChef().getName());
        }
    }

    @Nullable
    @Override
    public OrderMessage getData() {
        return null;
    }

    @NonNull
    @Override
    public OrderTimelineItemView getSelf() {
        return this;
    }
}