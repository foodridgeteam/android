package com.foodridge.view.item;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Meal;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;
import com.foodridge.view.ChefShortView;
import com.foodridge.view.DishStatusBarView;
import com.foodridge.view.PriceOrderView;
import com.foodridge.view.ProgressImageView;

import java.lang.ref.WeakReference;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 07.04.16.
 */
@SuppressLint("ViewConstructor")
public class DishItemView extends CardView
        implements DataEntity<DishItemView, Meal>, OnClickListener {

    public static final int CODE = R.id.code_dish_item;

    //VIEW's
    private final ProgressImageView mIvPhoto;
    private final TextView mTvName;

    private final PriceOrderView mDishPriceView;
    private final DishStatusBarView mStatusBarView;
    private final ChefShortView mChefShortView;

    //VALUE's
    private final WeakReference<ObjectsReceiver> mReceiver;

    public DishItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        super(new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight));
        {
            inflate(context, R.layout.view_item_dish, this);
            super.setRadius(context.getResources().getDimensionPixelSize(R.dimen.cardview_default_radius));
        }
        mIvPhoto = (ProgressImageView) findViewById(R.id.iv_photo);
        mTvName = (TextView) findViewById(R.id.tv_name);

        mDishPriceView = (PriceOrderView) findViewById(R.id.view_dish_price);
        mStatusBarView = (DishStatusBarView) findViewById(R.id.view_status_bar);
        mChefShortView = (ChefShortView) findViewById(R.id.view_chef_short);

        mIvPhoto.setBackgroundColor(ContextCompat.getColor(context, R.color.black));
        mIvPhoto.setPlaceholder(R.drawable.svg_logo_placeholder);

        mIvPhoto.getLayoutParams().height = (int) (AContext.getScreenSize().x / 1.25F);
        mIvPhoto.requestLayout();

        mReceiver = receiver;

        super.setOnClickListener(this);
        mDishPriceView.setOnClickListener(this);
        mChefShortView.setOnClickListener(this);
    }

    @Override
    public void setData(@Nullable Meal meal, @NonNull Object... objects) {
        if (meal == null) {
            throw new IllegalStateException();
        }
        setTag(meal);

        mIvPhoto.show(meal.getFirstPhoto());
        mTvName.setText(meal.getName());

        mDishPriceView.setData(meal);
        mStatusBarView.setData(meal);
        mChefShortView.setData(meal.getChef());
    }

    @Nullable
    @Override
    public Meal getData() {
        return null;
    }

    @NonNull
    @Override
    public DishItemView getSelf() {
        return this;
    }

    @Override
    public void onClick(View v) {
        final Boolean order;
        switch (v.getId()) {
            case R.id.btn_order:
                order = true;
                break;

            case R.id.view_chef_short:
                order = false;
                break;

            default:
                order = null;
                break;
        }
        Utils.receiveObjects(mReceiver, CODE, order, getTag());
    }
}