package com.foodridge.view.item;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.api.model.Chef;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.model.Profile;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;
import com.foodridge.view.FollowersFollowView;
import com.foodridge.view.ProgressImageView;
import com.foodridge.view.RatingView;

import java.lang.ref.WeakReference;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 07.04.16.
 */
@SuppressLint("ViewConstructor")
public class ChefItemView extends CardView
        implements DataEntity<ChefItemView, Chef>, OnClickListener {

    public static final int CODE = R.id.code_chef_item;

    // VIEW's
    private final ProgressImageView mIvChefAvatar;
    private final TextView mTvName;
    private final TextView mTvVisited;
    private final View mIvCuisines;
    private final TextView mTvCuisines;
    private final RatingView mRating;

    private final FollowersFollowView mFollowersView;

    //VALUE's
    private final WeakReference<ObjectsReceiver> mReceiver;
    private final TimeCache mTimeCache;
    private final ASettings mSettings;
    private final String mProfileId;

    public ChefItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver) {
        super(new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight));
        {
            inflate(context, R.layout.view_item_chef, this);
            super.setRadius(context.getResources().getDimensionPixelSize(R.dimen.cardview_default_radius));
        }
        mIvChefAvatar = (ProgressImageView) findViewById(R.id.iv_chef_avatar);
        mTvName = (TextView) findViewById(R.id.tv_chef_name);
        mTvVisited = (TextView) findViewById(R.id.tv_visited);
        mIvCuisines = findViewById(R.id.iv_cuisines);
        mTvCuisines = (TextView) findViewById(R.id.tv_cuisines);
        mRating = (RatingView) findViewById(R.id.rating);

        mFollowersView = (FollowersFollowView) findViewById(R.id.view_followers);

        mIvChefAvatar.setBackgroundColor(ContextCompat.getColor(context, R.color.black));
        mIvChefAvatar.setPlaceholder(R.drawable.svg_logo_placeholder);

        mIvChefAvatar.getLayoutParams().height = (int) (AContext.getScreenSize().x / 1.25F);
        mIvChefAvatar.requestLayout();

        mReceiver = receiver;
        mTimeCache = TimeCache.getInstance();
        mSettings = ASettings.getInstance();

        setOnClickListener(this);
        mFollowersView.setOnClickListener(this);
        mProfileId = Profile.getInstance().getId();
    }

    @Override
    public void setData(@Nullable Chef chef, @NonNull Object... objects) {
        if (chef == null) {
            throw new IllegalStateException();
        }
        setTag(chef);

        mIvChefAvatar.show(chef.getAvatar());
//        mTvName.setText(getContext().getString(R.string.format_Chef_s, chef.getName()));
        mTvName.setText(chef.getName());
        mFollowersView.setData(chef);
        mTvVisited.setText(mTimeCache.getVisitedAt(chef.getLastVisit()));
        {
            final int cuisinesVisibility;
            if (mProfileId.equals(chef.getId())) {
                cuisinesVisibility = INVISIBLE;

            } else {
                final CharSequence cuisines = Utils.findAll(chef.getCuisines(), mSettings.getCuisines(), true);
                cuisinesVisibility = cuisines != null && cuisines.length() > 0 ? VISIBLE : INVISIBLE;
                mTvCuisines.setText(cuisines);
            }
            Utils.changeVisibility(mIvCuisines, cuisinesVisibility);
            Utils.changeVisibility(mTvCuisines, cuisinesVisibility);
        }
        mRating.setRating(chef.getRating());
    }

    @Nullable
    @Override
    public Chef getData() {
        return null;
    }

    @NonNull
    @Override
    public ChefItemView getSelf() {
        return this;
    }

    @Override
    public void onClick(View v) {
        Utils.receiveObjects(mReceiver, CODE, v.getId() == R.id.btn_follow, getTag());
    }
}