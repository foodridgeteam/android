package com.foodridge.view.item.order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodridge.R;
import com.foodridge.adapter.TimelineAdapter;
import com.foodridge.api.model.Chef;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.api.model.Order;
import com.foodridge.api.model.OrderMessage;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.type.DeliveryType;
import com.foodridge.type.OrderStatus;
import com.foodridge.type.OrderType;
import com.foodridge.utility.OrderMessageCache;
import com.foodridge.utility.Utils;
import com.foodridge.view.DeliveryAddressView;
import com.foodridge.view.RecyclerLinearLayout;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 12.04.16.
 */
@SuppressLint("ViewConstructor")
public class OrdinalOrderItemView extends BaseOrderItemView {

    //VIEW's
    private final DeliveryAddressView mAddressView;

    private final View mBtnAccept;
    private final ImageView mIvAccept;
    private final TextView mTvAccept;

    private final View mBtnCancel;
    private final ImageView mIvCancel;
    private final TextView mTvCancel;

    private final View mBtnDelivered;
    private final ImageView mIvDelivered;
    private final TextView mTvDelivered;

    private final View mTimelineDivider;
    private final RecyclerLinearLayout mRllTimeline;

    //VALUE's
    private final OrderMessageCache mMessageCache;
    private final Drawable mDrawableSent;
    private final Drawable mDrawableReceived;
    private final int mColorDisabled;

    //ADAPTER
    private final TimelineAdapter mAdapter;

    public OrdinalOrderItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver, @OrderType int type) {
        super(context, receiver, type);
        {
            mAddressView = new DeliveryAddressView(context);

            final int margin = getResources().getDimensionPixelSize(R.dimen.spacing);
            final LinearLayoutCompat.LayoutParams params = new LinearLayoutCompat.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            params.setMarginStart(margin);
            params.setMarginEnd(margin);
            params.bottomMargin = margin;

            mRootLayout.addView(mAddressView, params);

            inflate(context, R.layout.view_timeline_list, mRootLayout);
            inflate(context, R.layout.view_order_bottom_bar_ordinal, mRootLayout);
        }
        mAddressView.setTitle(R.string.Delivery_Address, Color.BLACK);
        mAddressView.setTitleParams(false, Typeface.NORMAL, 0);

        mBtnAccept = findViewById(R.id.btn_accept_edit);
        mIvAccept = (ImageView) findViewById(R.id.iv_accept_edit);
        mTvAccept = (TextView) findViewById(R.id.tv_accept_edit);

        mBtnCancel = findViewById(R.id.btn_cancel);
        mIvCancel = (ImageView) findViewById(R.id.iv_cancel);
        mTvCancel = (TextView) findViewById(R.id.tv_cancel);

        mBtnDelivered = findViewById(R.id.btn_delivered_received);
        mIvDelivered = (ImageView) findViewById(R.id.iv_delivered_received);
        mTvDelivered = (TextView) findViewById(R.id.tv_delivered_received);

        mAdapter = new TimelineAdapter();

        mTimelineDivider = findViewById(R.id.divider_timeline);
        mRllTimeline = (RecyclerLinearLayout) findViewById(R.id.recycler_layout);
        mRllTimeline.setAdapter(mAdapter);

        mBtnAccept.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        findViewById(R.id.btn_message).setOnClickListener(this);
        mBtnDelivered.setOnClickListener(this);

        mMessageCache = OrderMessageCache.getInstance();
        mDrawableSent = ContextCompat.getDrawable(context, R.drawable.svg_circle_arrow_up);
        mDrawableReceived = ContextCompat.getDrawable(context, R.drawable.svg_circle_arrow_down);
        mColorDisabled = ContextCompat.getColor(context, R.color.gray_unchecked);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void setData(@Nullable Order order, @NonNull Object... objects) {
        super.setData(order, objects);

        final boolean pickup = order.getDeliveryType() == DeliveryType.PICKUP;
        final Chef chef = order.getMeal().getChef();

        final DeliveryInfo deliveryInfo = pickup ? chef.getDeliveryInfo() : order.getClientDeliveryInfo();
        if (deliveryInfo == null) {
            Utils.changeVisibility(mAddressView, GONE);

        } else {
            Utils.changeVisibility(mAddressView, VISIBLE);
            mAddressView.setTitle(pickup ? R.string.Pickup_Address : R.string.Delivery_Address);
            mAddressView.setData(deliveryInfo, false);
        }
        final ArrayList<OrderMessage> messages = mMessageCache.find(order.getId());
        final int size = messages == null ? 0 : messages.size();
        final int visibility;
        if (size > 0) {
            visibility = VISIBLE;
            setOrderTime(messages.get(size - 1).getCreated());

            mAdapter.setData(order, messages);
            mAdapter.notifyDataSetChanged();

        } else {
            visibility = GONE;
        }
        Utils.changeVisibility(mTimelineDivider, visibility);
        Utils.changeVisibility(mRllTimeline, visibility);

        final boolean isClient = isClient();
        @OrderStatus final int status = order.getStatus();

        if (isClient) {
            mBtnAccept.setEnabled(false);
            mBtnAccept.setTag(null);

            mTvAccept.setEnabled(false);
            mTvAccept.setText(R.string.Accept);

        } else {
            final boolean enabled = status < OrderStatus.PREPARED;

            mBtnAccept.setEnabled(enabled);
            @OrderStatus final int acceptOperation = status == OrderStatus.CREATED ? OrderStatus.ACCEPTED : OrderStatus.PREPARED;
            mBtnAccept.setTag(acceptOperation);

            mTvAccept.setEnabled(enabled);
            mTvAccept.setText(acceptOperation == OrderStatus.ACCEPTED ? R.string.Accept : R.string.Prepared);
        }
        final boolean cancelEnabled = status < OrderStatus.ACCEPTED || status < OrderStatus.SENT && mProfileId.equals(chef.getId());
        mBtnCancel.setEnabled(cancelEnabled);
        mTvCancel.setEnabled(cancelEnabled);
        {
            final boolean enabled = status == (isClient ? OrderStatus.SENT : OrderStatus.PREPARED);

            @OrderStatus final int deliveryOperation = isClient ? OrderStatus.RECEIVED : OrderStatus.SENT;
            mBtnDelivered.setEnabled(enabled);
            mBtnDelivered.setTag(deliveryOperation);

            mIvDelivered.setImageDrawable(isClient ? mDrawableReceived : mDrawableSent);

            mTvDelivered.setEnabled(enabled);
            mTvDelivered.setText(isClient ? R.string.Received : R.string.Sent);
        }
        if (isBlockedOrder()) {
            mBtnAccept.setEnabled(false);
            mTvAccept.setEnabled(false);

            mBtnCancel.setEnabled(false);
            mTvCancel.setEnabled(false);

            mBtnDelivered.setEnabled(false);
            mTvDelivered.setEnabled(false);
        }
        setChecked(mIvAccept, mBtnAccept.isEnabled());
        setChecked(mIvCancel, mBtnCancel.isEnabled());
        setChecked(mIvDelivered, mBtnDelivered.isEnabled());
    }

    private void setChecked(@NonNull ImageView view, boolean enabled) {
        if (enabled) {
            view.setColorFilter(null);

        } else {
            view.setColorFilter(mColorDisabled);
        }
    }
}