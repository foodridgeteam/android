package com.foodridge.view.item.order;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.CallSuper;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.foodridge.R;
import com.foodridge.api.model.Meal;
import com.foodridge.api.model.Order;
import com.foodridge.interfaces.DataEntity;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.interfaces.UserInterface;
import com.foodridge.model.Profile;
import com.foodridge.type.OrderStatus;
import com.foodridge.type.OrderType;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.text.DateFormat;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 11.04.16.
 */
public abstract class BaseOrderItemView extends CardView
        implements DataEntity<BaseOrderItemView, Order>, OnClickListener {

    @IntDef({OrderItemClick.ORDER, OrderItemClick.USER, OrderItemClick.ACCEPT_EDIT, OrderItemClick.CANCEL
            , OrderItemClick.MESSAGE, OrderItemClick.DELIVERED_RECEIVED, OrderItemClick.RATE, OrderItemClick.MEAL})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface OrderItemClick {
        int ORDER = 0;
        int USER = 1;
        int ACCEPT_EDIT = 2;
        int CANCEL = 3;
        int MESSAGE = 4;
        int DELIVERED_RECEIVED = 5;
        int RATE = 6;
        int MEAL = 7;
    }

    public static final int CODE = R.id.code_order_item;

    // VIEW's
    protected final LinearLayoutCompat mRootLayout;

    private final View mViewTopIndicator;

    private final TextView mTvOrderNumber;
    private final TextView mTvOrderDate;
    protected final TextView mTvOrderTime;

    protected final View mLayoutUser;
    private final ImageView mIvAvatar;
    private final TextView mTvName;
    private final TextView mTvDishQuantity;
    private final TextView mTvDishesPrice;

    //VALUE's
    protected final WeakReference<ObjectsReceiver> mReceiver;
    @OrderType
    protected final int mOrderType;

    protected final RequestManager mGlide;
    protected final TimeCache mTimeCache;
    protected final Drawable mPlaceholder;
    private final DateFormat mDateFormat;

    protected final String mProfileId;
    private boolean mIsClient;
    private boolean mBlockedOrder;

    private final int mColorTeal;
    private final int mColorRed;

    public BaseOrderItemView(@NonNull Context context, @NonNull WeakReference<ObjectsReceiver> receiver, @OrderType int type) {
        super(new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight));
        {
            inflate(context, R.layout.view_item_order_base, this);
            super.setRadius(context.getResources().getDimensionPixelSize(R.dimen.cardview_default_radius));
        }
        mRootLayout = (LinearLayoutCompat) findViewById(R.id.root_layout);

        mViewTopIndicator = findViewById(R.id.view_top_indicator);

        mTvOrderNumber = (TextView) findViewById(R.id.tv_order_number);
        mTvOrderDate = (TextView) findViewById(R.id.tv_order_date);
        mTvOrderTime = (TextView) findViewById(R.id.tv_order_time);

        mLayoutUser = findViewById(R.id.layout_user);
        mIvAvatar = (ImageView) findViewById(R.id.iv_avatar);
        mTvName = (TextView) findViewById(R.id.tv_name);

        mTvDishQuantity = (TextView) findViewById(R.id.tv_dish_quantity);
        mTvDishesPrice = (TextView) findViewById(R.id.tv_dishes_price);

        mReceiver = receiver;
        mOrderType = type;

        mGlide = Glide.with(context);
        mTimeCache = TimeCache.getInstance();
        mPlaceholder = ContextCompat.getDrawable(context, R.drawable.svg_user_placeholder);
        mProfileId = Profile.getInstance().getId();
        mDateFormat = mTimeCache.is24HourCached() ? TimeCache.DD_MMM_HH_MM24 : TimeCache.DD_MMM_HH_MM;

        mColorTeal = ContextCompat.getColor(context, R.color.teal);
        mColorRed = ContextCompat.getColor(context, R.color.red_google);

        super.setOnClickListener(this);
        mLayoutUser.setOnClickListener(this);
        findViewById(R.id.layout_meal).setOnClickListener(this);
    }

    @Override
    @CallSuper
    public void setData(@Nullable Order order, @NonNull Object... objects) {
        if (order == null) {
            throw new IllegalStateException();
        }
        setTag(order);

        mBlockedOrder = (boolean) objects[0];
        mIsClient = mProfileId.equals(order.getClient().getId());

        final Meal meal = order.getMeal();
        final UserInterface user = mIsClient ? meal.getChef() : order.getClient();

        Utils.loadInto(mGlide, user.getAvatar(), mPlaceholder, mIvAvatar);
        mTvName.setText(user.getName());

        mTvOrderNumber.setText(getContext().getString(R.string.format_order_number, order.getId()));
        mTvOrderDate.setText(Html.fromHtml(mTimeCache.getCuteTime(mDateFormat, order.getCreated())));
        if (mOrderType == OrderType.COMPLETED) {
            final boolean completedCorrectly = order.getStatus() == OrderStatus.COMPLETED;

            if (mTvOrderNumber.getTag() == null || !mTvOrderNumber.getTag().equals(completedCorrectly)) {
                mTvOrderNumber.setTag(completedCorrectly);

                final int color = completedCorrectly ? mColorTeal : mColorRed;
                mViewTopIndicator.setBackgroundColor(color);
                mTvOrderNumber.setTextColor(color);
            }

        } else {
            setOrderTime(order.getCreated());
        }
        mTvDishQuantity.setText(getContext().getString(R.string.format_dish_quantity, order.getQuantity(), meal.getName()));
        mTvDishesPrice.setText(getContext().getString(R.string.format_dollar_2f, order.getTotalSum()));
    }

    protected final void setOrderTime(@Nullable String time) {
        mTvOrderTime.setText(mTimeCache.getCuteTime(mTimeCache.is24HourCached() ? TimeCache.HH_MM24 : TimeCache.HH_MM, time));
    }

    @Nullable
    @Override
    public Order getData() {
        return null;
    }

    @NonNull
    @Override
    public BaseOrderItemView getSelf() {
        return this;
    }

    protected final boolean isClient() {
        return mIsClient;
    }

    protected final boolean isBlockedOrder() {
        return mBlockedOrder;
    }

    @Override
    public void onClick(View v) {
        @OrderItemClick final int click;
        switch (v.getId()) {
            case R.id.btn_accept_edit:
                click = OrderItemClick.ACCEPT_EDIT;
                break;

            case R.id.btn_cancel:
                click = OrderItemClick.CANCEL;
                break;

            case R.id.btn_message:
                click = OrderItemClick.MESSAGE;
                break;

            case R.id.btn_delivered_received:
                click = OrderItemClick.DELIVERED_RECEIVED;
                break;

            case R.id.layout_user:
                click = OrderItemClick.USER;
                break;

            case R.id.btn_rate:
                click = OrderItemClick.RATE;
                break;

            case R.id.layout_meal:
                click = OrderItemClick.MEAL;
                break;

            default:
                click = OrderItemClick.ORDER;
                break;
        }
        Utils.receiveObjects(mReceiver, CODE, click, getTag(), v.getTag());
    }
}