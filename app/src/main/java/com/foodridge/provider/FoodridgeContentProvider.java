package com.foodridge.provider;

import android.content.ContentUris;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.foodridge.utility.SharedStrings;
import com.tojc.ormlite.android.OrmLiteSimpleContentProvider;
import com.tojc.ormlite.android.framework.MatcherController;
import com.tojc.ormlite.android.framework.MatcherPattern;
import com.tojc.ormlite.android.framework.MimeTypeVnd.SubType;
import com.tojc.ormlite.android.framework.OperationParameters.InsertParameters;

/**
 * @author Cheakshov R.(email:roman_woland@mail.ru)
 * @since 11.01.16
 */
public class FoodridgeContentProvider extends OrmLiteSimpleContentProvider<DbHelper> {

    @Override
    protected Class<DbHelper> getHelperClass() {
        return DbHelper.class;
    }

    @Override
    public boolean onCreate() {
        final MatcherController matcher = new MatcherController();

        int i = 0;
        for (Class<?> cls : FoodridgeMetaData.CLASSES) {
            i = addToMatcher(matcher, cls, i);
        }

        setMatcherController(matcher);
        return true;
    }

    private int addToMatcher(MatcherController matcher, Class<?> cls, int i) {
        matcher.add(cls, SubType.DIRECTORY, SharedStrings.EMPTY, ++i);
        matcher.add(cls, SubType.ITEM, SharedStrings.SHARP, ++i);

        return i;
    }

    @Override
    public Uri onBulkInsert(DbHelper helper, SQLiteDatabase db, MatcherPattern target, InsertParameters parameter) {
        return super.onBulkInsert(helper, db, target, parameter);
    }

    @Override
    public Uri onInsert(DbHelper helper, SQLiteDatabase db, MatcherPattern target, InsertParameters parameter) {
        final long id = db.insertWithOnConflict(target.getTableInfo().getName(), null, parameter.getValues(), SQLiteDatabase.CONFLICT_REPLACE);
        if (id >= 0) {
            return ContentUris.withAppendedId(target.getContentUriPattern(), id);
        }
        throw new SQLException("Failed to insert row into : " + parameter.getUri());

    }
}