package com.foodridge.provider;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.foodridge.api.model.Chef;
import com.foodridge.api.model.Comment;
import com.foodridge.api.model.Meal;
import com.foodridge.api.model.Order;
import com.foodridge.api.model.OrderMessage;
import com.foodridge.application.AContext;
import com.foodridge.model.DbChef;
import com.foodridge.model.DbMeal;
import com.foodridge.model.DbMessage;
import com.foodridge.model.DbOrder;
import com.foodridge.provider.FoodridgeMetaData.ChefContract;
import com.foodridge.provider.FoodridgeMetaData.MealContract;
import com.foodridge.provider.FoodridgeMetaData.MessageContract;
import com.foodridge.provider.FoodridgeMetaData.OrderContract;
import com.foodridge.type.OrderStatus;
import com.foodridge.utility.SharedStrings;
import com.foodridge.utility.Utils;

import java.util.ArrayList;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/01/16
 */
public class ContentHelper extends BaseContentHelper {

    public static void insert(@NonNull Meal meal, boolean onlyUpdate) throws RemoteException, OperationApplicationException {
        AContext.getContentResolver().applyBatch(FoodridgeMetaData.AUTHORITY
                , insertOperations(new DbMeal(), onlyUpdate ? Integer.MIN_VALUE : -1, meal, DbMeal.SEARCH_SELF, onlyUpdate));
    }

    public static void insert(@NonNull Chef chef) throws RemoteException, OperationApplicationException {
        AContext.getContentResolver().applyBatch(FoodridgeMetaData.AUTHORITY
                , insertOperations(new DbChef(), Integer.MIN_VALUE, chef, DbChef.SEARCH_SELF));
    }

    public static void insert(@NonNull String orderId, @NonNull OrderMessage message) {
        AContext.getContentResolver().insert(MessageContract.CONTENT_URI
                , new DbMessage().toContentValues(orderId, message));
    }

    public static void update(@NonNull Order order) {
        AContext.getContentResolver().update(OrderContract.CONTENT_URI
                , new DbOrder().toContentValues(Integer.MIN_VALUE, order), eq(OrderContract._ID, order.getId()), null);
    }

    public static void insert(@NonNull Order order) {
        AContext.getContentResolver().insert(OrderContract.CONTENT_URI
                , new DbOrder().toContentValues(-1, order));
    }

    @NonNull
    public static ArrayList<ContentProviderOperation> insertOperations(@NonNull DbMeal dbMeal, long order, @NonNull Meal meal, @Nullable String search, boolean onlyUpdate) {
        final ArrayList<ContentProviderOperation> operations = new ArrayList<>(3);

        operations.add(ContentProviderOperation
                .newDelete(MealContract.CONTENT_URI)
                .withSelection(ContentHelper.and(ContentHelper.eq(MealContract.ORDER, order), ContentHelper.eq(MealContract.SEARCH, search)), null)
                .build());

        final ContentValues insert = dbMeal.toContentValues(order, meal, search);

        final ContentValues update = new ContentValues(1);
        update.put(MealContract.ENTITY, insert.getAsByteArray(MealContract.ENTITY));

        operations.add(ContentProviderOperation
                .newUpdate(MealContract.CONTENT_URI)
                .withSelection(ContentHelper.eq(MealContract.ENTITY_ID, meal.getId()), null)
                .withValues(update)
                .build());

        if (!onlyUpdate) {
            operations.add(ContentProviderOperation
                    .newInsert(MealContract.CONTENT_URI)
                    .withValues(insert)
                    .build());
        }
        return operations;
    }

    @NonNull
    public static ArrayList<ContentProviderOperation> insertOperations(@NonNull DbChef dbChef, long order, @NonNull Chef chef, @Nullable String search) {
        final ArrayList<ContentProviderOperation> operations = new ArrayList<>(4);

        operations.add(ContentProviderOperation
                .newDelete(ChefContract.CONTENT_URI)
                .withSelection(ContentHelper.and(ContentHelper.eq(ChefContract.ORDER, order), ContentHelper.eq(ChefContract.SEARCH, search)), null)
                .build());

        final ContentValues insert = dbChef.toContentValues(order, chef, search);

        final byte[] chefBlob = insert.getAsByteArray(ChefContract.ENTITY);

        final ContentValues updateEntity = new ContentValues(1);
        updateEntity.put(ChefContract.ENTITY, chefBlob);

        operations.add(ContentProviderOperation
                .newUpdate(ChefContract.CONTENT_URI)
                .withSelection(ContentHelper.eq(ChefContract.ENTITY_ID, chef.getId()), null)
                .withValues(updateEntity)
                .build());

        final ContentValues updateChef = new ContentValues(1);
        updateChef.put(MealContract.CHEF, chefBlob);

        operations.add(ContentProviderOperation
                .newUpdate(MealContract.CONTENT_URI)
                .withSelection(ContentHelper.eq(MealContract.CHEF_ID, chef.getId()), null)
                .withValues(updateChef)
                .build());

        operations.add(ContentProviderOperation
                .newInsert(ChefContract.CONTENT_URI)
                .withValues(insert)
                .build());

        return operations;
    }

    @NonNull
    public static String orderStatus(boolean active) {
        final StringBuilder sb = new StringBuilder()
                .append(OrderContract.STATUS)
                .append(active ? SharedStrings.NOT_IN : SharedStrings.IN)
                .append(SharedStrings.BRACKET_OPEN_C);
        final int[] statuses = {OrderStatus.USER_CANCEL, OrderStatus.CHEF_CANCEL, OrderStatus.SYSTEM_CANCEL, OrderStatus.COMPLETED};

        for (int status : statuses) {
            sb.append(status).append(SharedStrings.COMMA_C);
        }
        sb.deleteCharAt(sb.length() - 1);

        return sb
                .append(SharedStrings.BRACKET_CLOSE_C)
                .toString();
    }

    public static void update(@NonNull Comment comment, @NonNull String orderId) {
        final String selection = eq(OrderContract._ID, orderId);
        final DbOrder order;

        Cursor c = null;
        try {
            c = AContext.getContentResolver().query(OrderContract.CONTENT_URI, null, selection, null, null);
            if (c != null && c.moveToFirst()) {
                order = new DbOrder();
                order.fromCursor(c);
                order.getEntity().setComment(comment);

            } else {
                return;
            }

        } finally {
            Utils.closeSilently(c);
        }
        AContext.getContentResolver().update(OrderContract.CONTENT_URI, order.toContentValues(Integer.MIN_VALUE, order.getEntity()), selection, null);
    }
}