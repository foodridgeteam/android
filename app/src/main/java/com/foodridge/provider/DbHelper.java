package com.foodridge.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.foodridge.utility.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 27.08.14
 */
public class DbHelper extends OrmLiteSqliteOpenHelper {

    private static DbHelper sInstance;

    @NonNull
    public static DbHelper getInstance(@NonNull Context context) {
        if (sInstance == null) {
            synchronized (DbHelper.class) {
                if (sInstance == null) {
                    sInstance = new DbHelper(context);
                }
            }
        }
        return sInstance;
    }

    public DbHelper(@NonNull Context context) {
        super(context, FoodridgeMetaData.DATABASE_NAME, null, FoodridgeMetaData.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource cs) {
        for (Class<?> cls : FoodridgeMetaData.CLASSES) {
            try {
                TableUtils.createTableIfNotExists(cs, cls);

            } catch (SQLException e) {
                Log.LOG.toLog(e);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource cs, int oldVersion, int newVersion) {
        for (Class<?> cls : FoodridgeMetaData.CLASSES) {
            try {
                TableUtils.clearTable(cs, cls);

            } catch (SQLException e) {
                Log.LOG.toLog(e);
            }
        }
        onCreate(db, cs);
    }
}