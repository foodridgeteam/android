package com.foodridge.provider;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
@IntDef({DatabaseVersion.FIRST})
@Retention(RetentionPolicy.RUNTIME)
@interface DatabaseVersion {
    int FIRST = 2;
}