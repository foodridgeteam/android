package com.foodridge.provider;

import android.content.ContentValues;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;

import com.foodridge.application.AContext;
import com.foodridge.interfaces.CursorEntity;
import com.foodridge.utility.SharedStrings;

import java.util.List;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/01/16
 */
abstract class BaseContentHelper {

    protected static <V extends CursorEntity> void insert(@NonNull Uri uri, @NonNull V[] values) {
        final ContentValues[] cvs = new ContentValues[values.length];
        int count = 0;
        for (V value : values) {
            cvs[count++] = value.toContentValues();
        }
        AContext.getContentResolver().bulkInsert(uri, cvs);
    }

    protected static <V extends CursorEntity> void insert(@NonNull Uri uri, @NonNull V value) {
        AContext.getContentResolver().insert(uri, value.toContentValues());
    }

    @NonNull
    public static String eq(@NonNull String column, @Nullable String value) {
        if (value == null) {
            return column + SharedStrings.IS_NULL;
        }
        return column + SharedStrings.EQUALS + SharedStrings.QUOTE + value + SharedStrings.QUOTE;
    }

    @NonNull
    public static String eq(@NonNull String column, @Nullable Object value) {
        if (value == null) {
            return column + SharedStrings.IS_NULL;
        }
        return column + SharedStrings.EQUALS + SharedStrings.QUOTE + value + SharedStrings.QUOTE;
    }

    @NonNull
    public static String eq(@NonNull String column, long value, boolean equals) {
        return equals ? (column + SharedStrings.EQUALS + value) : (column + SharedStrings.NOT_EQUALS + value);
    }

    @NonNull
    public static String is(@NonNull String column, boolean value) {
        if (value) {
            return column + SharedStrings.EQUALS + SharedStrings.ONE;
        }
        return column + SharedStrings.EQUALS + SharedStrings.ZERO;
    }

    @NonNull
    public static <N extends Number> String in(@NonNull String columnName, @NonNull @Size(min = 1) List<N> ids) {
        final StringBuilder sb = new StringBuilder();

        sb
                .append(columnName).append(SharedStrings.IN)
                .append(SharedStrings.BRACKET_OPEN_C);
        for (N id : ids) {
            sb.append(id).append(SharedStrings.COMMA_C);
        }
        sb
                .deleteCharAt(sb.length() - 1)
                .append(SharedStrings.BRACKET_CLOSE_C);

        return sb.toString();
    }

    @NonNull
    public static <N extends Number> String moreEq(@NonNull String columnName, boolean moreEq, @NonNull N value) {
        return moreEq ? columnName + ">=" + value : columnName + "<=" + value;
    }

    @NonNull
    public static String and(@NonNull String... whats) {
        final StringBuilder sb = new StringBuilder();

        for (String what : whats) {
            sb.append(what).append(SharedStrings.AND);
        }
        sb.delete(sb.length() - 5, sb.length());

        return sb.toString();
    }
}