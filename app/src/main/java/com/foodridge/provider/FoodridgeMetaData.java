package com.foodridge.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import com.foodridge.model.DbChef;
import com.foodridge.model.DbComment;
import com.foodridge.model.DbMeal;
import com.foodridge.model.DbMessage;
import com.foodridge.model.DbOrder;
import com.foodridge.model.DbProfile;
import com.foodridge.model.DbRequestEntity;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/01/16
 */
public class FoodridgeMetaData {

    static final Class<?>[] CLASSES = {DbRequestEntity.class, DbProfile.class, DbMeal.class
            , DbChef.class, DbOrder.class, DbComment.class, DbMessage.class};

    static final int DATABASE_VERSION = DatabaseVersion.FIRST;
    static final String DATABASE_NAME = "foodridge.SQLite";
    public static final String AUTHORITY = "com.foodridge";
    public static final String AUTHORITY_PROVIDER = AUTHORITY + ".provider";

    public static final String TABLE_REQUEST_ENTITY = "requestEntity";
    public static final String TABLE_PROFILE = "foodridgeProfile";
    public static final String TABLE_MEAL = "foodridgeMeal";
    public static final String TABLE_CHEF = "foodridgeChef";
    public static final String TABLE_ORDER = "foodridgeOrder";
    public static final String TABLE_COMMENT = "foodridgeComment";
    public static final String TABLE_MESSAGE = "foodridgeOrderMessage";

    @NonNull
    private static Uri toContentUri(@NonNull String tableName) {
        return new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(AUTHORITY)
                .appendPath(tableName)
                .build();
    }

    public static final class RequestEntityContract implements BaseColumns {

        public static final String TABLE_NAME = TABLE_REQUEST_ENTITY;
        public static final Uri CONTENT_URI = toContentUri(TABLE_NAME);

        public static final String RECEIVER_CODE = "receiverCode";
        public static final String ENTITY = "entity";
        public static final String STATUS = "status";
    }

    public static final class ProfileContract implements BaseColumns {

        public static final String TABLE_NAME = TABLE_PROFILE;
        public static final Uri CONTENT_URI = toContentUri(TABLE_NAME);
    }

    public abstract static class EntityColumns implements BaseColumns {

        public static final String ENTITY_ID = "_entityId";
        public static final String ORDER = "_order";
        public static final String ENTITY = "_entity";
    }

    public abstract static class SearchColumns extends EntityColumns {
        public static final String SEARCH = "search";
    }

    public static final class MealContract extends SearchColumns {

        public static final String TABLE_NAME = TABLE_MEAL;
        public static final Uri CONTENT_URI = toContentUri(TABLE_NAME);

        public static final String CHEF_ID = "chefId";
        public static final String CHEF = "chef";
    }

    public static final class ChefContract extends SearchColumns {

        public static final String TABLE_NAME = TABLE_CHEF;
        public static final Uri CONTENT_URI = toContentUri(TABLE_NAME);
    }

    public static final class OrderContract extends EntityColumns {

        public static final String TABLE_NAME = TABLE_ORDER;
        public static final Uri CONTENT_URI = toContentUri(TABLE_NAME);

        @SuppressWarnings("unused")
        @Deprecated
        public static final String ENTITY_ID = "_entityId";

        public static final String STATUS = "status";
        public static final String CHEF_ID = "chefId";
        public static final String CLIENT_ID = "clientId";
    }

    public static final class CommentContract extends EntityColumns {

        public static final String TABLE_NAME = TABLE_COMMENT;
        public static final Uri CONTENT_URI = toContentUri(TABLE_NAME);

        @SuppressWarnings("unused")
        @Deprecated
        public static final String ENTITY_ID = "_entityId";

        public static final String MEAL_ID = "mealId";
    }

    public static final class MessageContract extends EntityColumns {

        public static final String TABLE_NAME = TABLE_MESSAGE;
        public static final Uri CONTENT_URI = toContentUri(TABLE_NAME);

        @SuppressWarnings("unused")
        @Deprecated
        public static final String ENTITY_ID = "_entityId";

        public static final String ORDER_ID = "orderId";
    }
}