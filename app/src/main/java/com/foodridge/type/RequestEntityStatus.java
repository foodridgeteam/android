package com.foodridge.type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19.03.2016
 */
@IntDef({RequestEntityStatus.PLACED_IN_DB, RequestEntityStatus.SENT_TO_EXECUTION
        , RequestEntityStatus.EXECUTED, RequestEntityStatus.RECEIVED, RequestEntityStatus.CANCELED})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestEntityStatus {

    int PLACED_IN_DB = 0;
    int SENT_TO_EXECUTION = 1;
    int EXECUTED = 2;
    int RECEIVED = 3;
    int CANCELED = 4;
}