package com.foodridge.type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
@IntDef({DeliveryType.PICKUP_AND_DELIVERY, DeliveryType.PICKUP, DeliveryType.DELIVERY,})
@Retention(RetentionPolicy.RUNTIME)
public @interface DeliveryType {

    int PICKUP_AND_DELIVERY = 1;
    int PICKUP = 2;
    int DELIVERY = 3;
}