package com.foodridge.type;

import android.support.annotation.IntDef;
import android.support.annotation.StringRes;

import com.foodridge.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
@IntDef({WeekDay.MON, WeekDay.TUE, WeekDay.WED, WeekDay.THU, WeekDay.FRI, WeekDay.SAT, WeekDay.SUN})
@Retention(RetentionPolicy.RUNTIME)
public @interface WeekDay {

    @StringRes
    int MON = R.string.Monday;
    @StringRes
    int TUE = R.string.Tuesday;
    @StringRes
    int WED = R.string.Wednesday;
    @StringRes
    int THU = R.string.Thursday;
    @StringRes
    int FRI = R.string.Friday;
    @StringRes
    int SAT = R.string.Saturday;
    @StringRes
    int SUN = R.string.Sunday;

    int size = 7;
}