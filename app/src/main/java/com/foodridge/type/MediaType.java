package com.foodridge.type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
@IntDef({MediaType.AVATAR, MediaType.MEAL})
@Retention(RetentionPolicy.RUNTIME)
public @interface MediaType {

    int AVATAR = 0;
    int MEAL = 1;
}