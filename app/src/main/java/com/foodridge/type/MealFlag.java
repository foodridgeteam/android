package com.foodridge.type;

import android.support.annotation.IntDef;
import android.support.annotation.StringRes;

import com.foodridge.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13/04/16
 */
@IntDef({MealFlag.BREAKFAST, MealFlag.LUNCH, MealFlag.DINNER, MealFlag.BAKERY, MealFlag.VEGETARIAN,
        MealFlag.EGG, MealFlag.GLUTEN_FREE, MealFlag.TRACES_NUTS, MealFlag.GARLIC, MealFlag.GINGER})
@Retention(RetentionPolicy.RUNTIME)
public @interface MealFlag {

    @StringRes
    int BREAKFAST = R.string.Breakfast;

    @StringRes
    int LUNCH = R.string.Lunch;

    @StringRes
    int DINNER = R.string.Dinner;

    @StringRes
    int BAKERY = R.string.Bakery;

    @StringRes
    int VEGETARIAN = R.string.Vegetarian;

    @StringRes
    int EGG = R.string.Egg;

    @StringRes
    int GLUTEN_FREE = R.string.Gluten_free;

    @StringRes
    int TRACES_NUTS = R.string.Traces_nuts;

    @StringRes
    int GARLIC = R.string.Garlic;

    @StringRes
    int GINGER = R.string.Ginger;

    Integer[] values = {MealFlag.BREAKFAST, MealFlag.LUNCH, MealFlag.DINNER, MealFlag.BAKERY,
            MealFlag.VEGETARIAN, MealFlag.EGG, MealFlag.GLUTEN_FREE, MealFlag.TRACES_NUTS,
            MealFlag.GARLIC, MealFlag.GINGER};
}