package com.foodridge.type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
@IntDef({OrderStatus.CREATED, OrderStatus.ACCEPTED, OrderStatus.PREPARED, OrderStatus.SENT
        , OrderStatus.RECEIVED, OrderStatus.PAYED, OrderStatus.USER_CANCEL, OrderStatus.CHEF_CANCEL
        , OrderStatus.SYSTEM_CANCEL, OrderStatus.COMPLETED})
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderStatus {

    int CREATED = 1;
    int ACCEPTED = 2;
    int PREPARED = 3;
    int SENT = 4;
    int RECEIVED = 5;
    int PAYED = 6;
    int USER_CANCEL = 7;
    int CHEF_CANCEL = 8;
    int SYSTEM_CANCEL = 9;
    int COMPLETED = 10;
}