package com.foodridge.type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Vojko Vladimir vojkovladimir@gmail.com
 * @since 06.04.16.
 */
@IntDef({SocialType.FACEBOOK, SocialType.GOOGLE_PLUS})
@Retention(RetentionPolicy.RUNTIME)
public @interface SocialType {

    int FACEBOOK = 1;
    int GOOGLE_PLUS = 2;
}