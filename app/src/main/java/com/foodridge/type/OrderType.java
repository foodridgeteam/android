package com.foodridge.type;

import android.support.annotation.IntDef;
import android.support.annotation.StringRes;

import com.foodridge.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 02/05/16
 */
@IntDef({OrderType.RECEIVED, OrderType.PLACED, OrderType.COMPLETED})
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderType {
    @StringRes
    int RECEIVED = R.string.Received;
    @StringRes
    int PLACED = R.string.Placed;
    @StringRes
    int COMPLETED = R.string.Completed;
}