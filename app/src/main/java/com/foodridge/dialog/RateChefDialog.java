package com.foodridge.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.api.model.Order;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;
import com.foodridge.view.RatingView;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 28.04.16
 */
public class RateChefDialog extends BaseAlertDialog
        implements DialogInterface.OnClickListener {

    public static final int CODE = R.id.code_rate_chef;

    private final AlertDialog mDialog;

    private final RatingView mRatingView;
    private final EditText mEditText;

    private Order mOrder;

    public RateChefDialog(@NonNull Context context, @Nullable ObjectsReceiver receiver) {
        super(context, receiver);

        context = new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight);

        @SuppressLint("InflateParams")
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_rate_chef, null);

        mRatingView = (RatingView) view.findViewById(R.id.rating);
        mEditText = (EditText) view.findViewById(R.id.edit_text);

        mRatingView.setMinimum(1);

        mDialog = new Builder(context)
                .setView(view)
                .setNegativeButton(R.string.Cancel, this)
                .setPositiveButton(R.string.Submit, this)
                .setCancelable(false)
                .create();
    }

    @Override
    public void show(@NonNull Object... objects) {
        mOrder = (Order) objects[0];

        mRatingView.setRating(5);
        mEditText.setText(null);

        mDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final int rating = (int) mRatingView.getRating();
        final String comment = Utils.checkEmpty(mEditText.getText().toString().trim());

        Utils.receiveObjects(mReceiver, CODE, which == DialogInterface.BUTTON_POSITIVE ? mOrder : null, rating, comment);
    }
}