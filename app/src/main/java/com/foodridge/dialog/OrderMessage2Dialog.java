package com.foodridge.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.api.model.Order;
import com.foodridge.api.request.SendMessageRequest;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.request.RequestCenter;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class OrderMessage2Dialog extends BaseAlertDialog
        implements OnClickListener, TextWatcher, OnShowListener
        , OnDismissListener, View.OnClickListener {

    private final AlertDialog mDialog;

    private final EditText mEditText;
    private View mBtnPositive;

    private Order mOrder;

    public OrderMessage2Dialog(@NonNull Context context, @NonNull ObjectsReceiver receiver) {
        super(context, receiver);

        context = new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight);

        @SuppressLint("InflateParams")
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_order_message, null);

        mEditText = (EditText) view.findViewById(R.id.edit_text);

        Utils.setOnClickListener(this, view, R.id.tv_tip_pick_up_now, R.id.tv_tip_delivery_on_way);

        mDialog = new Builder(context)
                .setView(view)
                .setNegativeButton(R.string.Cancel, null)
                .setPositiveButton(R.string.Send, this)
                .setCancelable(false)
                .create();

        mDialog.setOnShowListener(this);
        mDialog.setOnDismissListener(this);
    }

    @Override
    public void show(@NonNull Object... objects) {
        mOrder = (Order) objects[0];
        mEditText.setText(null);

        mDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final String string = mEditText.getText().toString().trim();
        final ObjectsReceiver receiver = mReceiver.get();

        if (receiver != null) {
            receiver.onObjectsReceive(R.id.code_block, true);
            RequestCenter.place(new SendMessageRequest(mOrder, string), receiver);
        }
        mOrder = null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mBtnPositive != null) {
            mBtnPositive.setEnabled(!isEmptyShowRequired(mEditText));
        }
    }

    @Override
    public void onShow(DialogInterface dialog) {
        mBtnPositive = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mBtnPositive.setEnabled(false);

        mEditText.addTextChangedListener(this);
        AContext.showInputKeyboard(mEditText);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        AContext.hideInputKeyboard(mEditText);
        mEditText.removeTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        mEditText.setText((CharSequence) v.getTag());
        mEditText.setSelection(mEditText.length());
    }
}