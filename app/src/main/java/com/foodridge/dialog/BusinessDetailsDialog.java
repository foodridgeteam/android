package com.foodridge.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.api.model.BusinessInfo;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.application.ASettings;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;
import com.foodridge.utility.Utils.SymbolsFilter;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class BusinessDetailsDialog extends BaseAlertDialog
        implements OnClickListener, TextWatcher, OnShowListener, OnDismissListener {

    public static final int CODE = R.id.code_business_details;

    private final AlertDialog mDialog;

    private EditText mEtName;
    private EditText mEtRegistration;
    private EditText mEtPhone;
    private EditText mEtSite;

    private View mBtnPositive;

    public BusinessDetailsDialog(@NonNull Activity activity, @NonNull ObjectsReceiver receiver) {
        super(activity, receiver);

        final ContextThemeWrapper context = new ContextThemeWrapper(activity, R.style.AppTheme_AlertDialogLight);

        @SuppressLint("InflateParams")
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_business_details, null);

        mEtName = (EditText) view.findViewById(R.id.et_business_name);
        mEtRegistration = (EditText) view.findViewById(R.id.et_registration_number);
        mEtPhone = (EditText) view.findViewById(R.id.et_phone_number);
        mEtSite = (EditText) view.findViewById(R.id.et_web_site);

        Utils.addFilter(mEtPhone, new SymbolsFilter(AConstant.PHONE_NOT_ALLOWED));

        mDialog = new Builder(context)
                .setTitle(R.string.Business_details)
                .setView(view)
                .setNegativeButton(R.string.Cancel, null)
                .setPositiveButton(R.string.Submit, null)
                .create();

        mDialog.setOnShowListener(this);
        mDialog.setOnDismissListener(this);
    }

    @Override
    public void show(@NonNull Object... objects) {
        mDialog.show();

        final BusinessInfo info = (BusinessInfo) objects[0];

        mEtName.setText(info.getName());
        mEtRegistration.setText(info.getRegistrationId());
        mEtPhone.setText(info.getPhone());
        mEtSite.setText(info.getSite());

        mEtName.setSelection(mEtName.length());
        mEtRegistration.setSelection(mEtRegistration.length());
        mEtPhone.setSelection(mEtPhone.length());
        mEtSite.setSelection(mEtSite.length());

        if (mEtPhone.length() == 0) {
            mEtPhone.setText(ASettings.getInstance().getCountryCode());
        }
    }

    @Override
    public void onClick(View v) {
        final String name = mEtName.getText().toString().trim();
        final String phone = mEtPhone.getText().toString().trim();
        String registration = mEtRegistration.getText().toString().trim();
        String site = mEtSite.getText().toString().trim();

        if (registration.length() == 0) {
            registration = null;
        }
        if (site.length() == 0) {
            site = null;
        }
        Utils.receiveObjects(mReceiver, CODE, name, registration, phone, site);
        mDialog.dismiss();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mBtnPositive != null) {
            boolean isEmpty = false;

            if (isEmptyShowRequired(mEtName)) {
                isEmpty = true;
            }
            if (isEmptyShowRequired(mEtPhone)) {
                isEmpty = true;
            }

            mBtnPositive.setEnabled(!isEmpty);
        }
    }

    @Override
    public void onShow(DialogInterface dialog) {
        mBtnPositive = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);

        mBtnPositive.setOnClickListener(this);
        mEtName.addTextChangedListener(this);
        mEtRegistration.addTextChangedListener(this);
        mEtPhone.addTextChangedListener(this);
        mEtSite.addTextChangedListener(this);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        AContext.hideInputKeyboard(mEtName);

        mEtName.removeTextChangedListener(this);
        mEtRegistration.removeTextChangedListener(this);
        mEtPhone.removeTextChangedListener(this);
        mEtSite.removeTextChangedListener(this);
    }
}