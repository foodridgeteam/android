package com.foodridge.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.application.AConstant;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class DeliveryAddressDialog extends BaseAlertDialog
        implements OnClickListener, TextWatcher, OnShowListener, OnDismissListener {

    public static final int CODE = R.id.code_delivery_address;

    private final AlertDialog mDialog;

    private EditText mEtStreet;
    private EditText mEtCity;
    private EditText mEtCountry;

    private View mBtnPositive;

    public DeliveryAddressDialog(@NonNull Activity activity, @StringRes int titleStringRes, @NonNull ObjectsReceiver receiver) {
        super(activity, receiver);

        final ContextThemeWrapper context = new ContextThemeWrapper(activity, R.style.AppTheme_AlertDialogLight);

        @SuppressLint("InflateParams")
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_delivery_address, null);

        mEtStreet = (EditText) view.findViewById(R.id.et_street);
        mEtCity = (EditText) view.findViewById(R.id.et_city);
        mEtCountry = (EditText) view.findViewById(R.id.et_country);

        final Builder builder = new Builder(context)
                .setTitle(titleStringRes)
                .setView(view)
                .setNegativeButton(R.string.Cancel, null)
                .setPositiveButton(R.string.Submit, this);

        if (AConstant.DEBUG) {
            builder.setNeutralButton(R.string.Via_Google, this);
        }
        mDialog = builder.create();
        mDialog.setOnShowListener(this);
        mDialog.setOnDismissListener(this);
    }

    @Override
    public void show(@NonNull Object... objects) {
        mDialog.show();

        final DeliveryInfo info = (DeliveryInfo) objects[0];

//        if (AConstant.DEBUG && info.hasEmptyField()) {
//            info.set("Street", "City", "Country", null);
//        }
        mEtStreet.setError(null);
        mEtCity.setError(null);
        mEtCountry.setError(null);

        mEtStreet.setText(info.getStreet());
        mEtCity.setText(info.getCity());
        mEtCountry.setText(info.getCountry());

        mEtStreet.setSelection(mEtStreet.length());
        mEtCity.setSelection(mEtCity.length());
        mEtCountry.setSelection(mEtCountry.length());
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final String street = mEtStreet.getText().toString().trim();
        final String city = mEtCity.getText().toString().trim();
        final String country = mEtCountry.getText().toString().trim();

        if (which == DialogInterface.BUTTON_NEUTRAL) {
            final DeliveryInfo info = new DeliveryInfo();
            info.set(street, city, country, null);

            new DeliveryAddressGoogleDialog(mContext, mReceiver.get()).show(info);
            return;
        }
        Utils.receiveObjects(mReceiver, CODE, street, city, country, null);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mBtnPositive != null) {
            boolean isEmpty = false;

            if (isEmptyShowRequired(mEtCountry)) {
                isEmpty = true;
            }
            if (isEmptyShowRequired(mEtCity)) {
                isEmpty = true;
            }
            if (isEmptyShowRequired(mEtStreet)) {
                isEmpty = true;
            }

            mBtnPositive.setEnabled(!isEmpty);
        }
    }

    @Override
    public void onShow(DialogInterface dialog) {
        mBtnPositive = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);

        mEtStreet.addTextChangedListener(this);
        mEtCity.addTextChangedListener(this);
        mEtCountry.addTextChangedListener(this);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        AContext.hideInputKeyboard(mEtStreet);

        mEtStreet.removeTextChangedListener(this);
        mEtCity.removeTextChangedListener(this);
        mEtCountry.removeTextChangedListener(this);
    }
}