package com.foodridge.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.interfaces.ObjectsReceiver;

import java.lang.ref.WeakReference;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
abstract class BaseAlertDialog {

    protected final Context mContext;
    protected final WeakReference<ObjectsReceiver> mReceiver;

    public BaseAlertDialog(@NonNull Context context, @Nullable ObjectsReceiver receiver) {
        mContext = context;
        mReceiver = receiver == null ? null : new WeakReference<>(receiver);
    }

    public abstract void show(@NonNull Object... objects);

    protected boolean isEmptyShowRequired(@NonNull EditText view) {
        final String text = view.getText().toString().trim();
        final boolean isEmpty = text.length() == 0;

        if (isEmpty && !view.hasFocus() && view.getError() == null) {
            view.setError(mContext.getString(R.string.Field_required));
        }
        return isEmpty;
    }
}