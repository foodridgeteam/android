package com.foodridge.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout.LayoutParams;

import com.foodridge.R;
import com.foodridge.api.ApiConstant;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class TermsPolicyDialog extends BaseAlertDialog
        implements OnClickListener, OnShowListener {

    public static final int CODE = R.id.code_terms_policy;

    private final AlertDialog mDialog;
    private final RefreshLayout mLayout;
    private final WebView mView;

    private View mBtnPositive;
    private boolean mLoaded;

    public TermsPolicyDialog(@NonNull Activity activity, @NonNull ObjectsReceiver receiver) {
        super(activity, receiver);

        mLayout = new RefreshLayout(activity);
        Utils.setStyle(mLayout);

        mView = new WebView(activity);

        mView.getSettings().setAppCacheEnabled(true);
        mView.setWebViewClient(new Client());
        mView.loadUrl(ApiConstant.HTML_TERMS_CONDITION_USER);
        {
            final int padding = activity.getResources().getDimensionPixelSize(R.dimen.spacing);
            mView.setPadding(padding, padding, padding, padding);
        }
        mLayout.addView(mView, new RefreshLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        mDialog = new Builder(activity)
                .setTitle(R.string.Terms_Conditions)
                .setView(mLayout)
                .setNegativeButton(R.string.Disagree, this)
                .setPositiveButton(R.string.Agree, this)
                .setCancelable(false)
                .create();

        mDialog.setOnShowListener(this);
    }

    @Override
    public void show(@NonNull Object... objects) {
        if (AContext.isNetworkConnected() || mLoaded) {
            if (mLoaded) {
                mView.post(new Runnable() {
                    @Override
                    public void run() {
                        mView.scrollTo(0, 0);
                    }
                });

            } else {
                mView.loadUrl(ApiConstant.HTML_TERMS_CONDITION_USER);
            }
            mDialog.show();
        }
        if (mBtnPositive != null) {
            mBtnPositive.setEnabled(mLoaded);
        }
    }

    public boolean isShowing() {
        return mDialog.isShowing();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Utils.receiveObjects(mReceiver, CODE, which == DialogInterface.BUTTON_POSITIVE);
    }

    @Override
    public void onShow(DialogInterface dialog) {
        if (mBtnPositive == null) {
            mBtnPositive = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            mBtnPositive.setEnabled(mLoaded);
        }
    }

    private final class Client extends WebViewClient {

        private Object mError;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mError = null;
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            mError = error;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            mLayout.setRefreshing(false);
            mLoaded = mError == null;
            if (mBtnPositive != null) {
                mBtnPositive.setEnabled(mLoaded);
            }
        }
    }

    private final class RefreshLayout extends SwipeRefreshLayout
            implements OnRefreshListener {

        public RefreshLayout(Context context) {
            super(context);
            super.setOnRefreshListener(this);
        }

        @Override
        public boolean canChildScrollUp() {
            return mView.getScrollY() > 0;
        }

        @Override
        public void onRefresh() {
            if (mLoaded && !AContext.isNetworkConnected()) {
                setRefreshing(false);

            } else {
                mView.loadUrl(ApiConstant.HTML_TERMS_CONDITION_USER);
            }
        }
    }
}