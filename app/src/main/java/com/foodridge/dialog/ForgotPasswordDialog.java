package com.foodridge.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;

import com.foodridge.R;
import com.foodridge.api.request.RestorePasswordRequest;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.request.RequestCenter;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class ForgotPasswordDialog extends BaseAlertDialog
        implements OnClickListener, TextWatcher, OnShowListener, OnDismissListener {

    private final AlertDialog mDialog;

    private final EditText mEditText;
    private View mBtnPositive;

    public ForgotPasswordDialog(@NonNull Activity activity, @NonNull ObjectsReceiver receiver) {
        super(activity, receiver);

        mEditText = new EditText(activity);
        mEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimensionPixelSize(R.dimen.text_big));
        mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        mEditText.setHintTextColor(ContextCompat.getColor(activity, R.color.gray_hint));
        mEditText.setTextColor(Color.WHITE);
        mEditText.setHint(R.string.Email);
        {
            final int padding = activity.getResources().getDimensionPixelSize(R.dimen.spacing);
            mEditText.setPadding(padding * 2, padding, padding * 2, padding);
        }
        mDialog = new Builder(activity)
                .setTitle(R.string.Reset_Password)
                .setMessage(R.string.Please_enter_email)
                .setView(mEditText)
                .setNegativeButton(R.string.Cancel, null)
                .setPositiveButton(R.string.Submit, this)
                .setCancelable(false)
                .create();

        mDialog.setOnShowListener(this);
        mDialog.setOnDismissListener(this);
    }

    @Override
    public void show(@NonNull Object... objects) {
        mDialog.show();

        mEditText.setText((CharSequence) objects[0]);
        mEditText.setSelection(mEditText.length());
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        AContext.hideInputKeyboard(mEditText);

        final String email = mEditText.getText().toString().trim();
        final ObjectsReceiver receiver = mReceiver.get();

        if (receiver != null) {
            receiver.onObjectsReceive(R.id.code_block, true);
            RequestCenter.place(new RestorePasswordRequest(email), receiver);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mBtnPositive != null) {
            final boolean emailCorrect = Patterns.EMAIL_ADDRESS.matcher(s).matches();
            mBtnPositive.setEnabled(emailCorrect);
            if (!emailCorrect) {
                mEditText.setError(mContext.getString(TextUtils.isEmpty(s) ? R.string.Field_required : R.string.Email_wrong));
            }
        }
    }

    @Override
    public void onShow(DialogInterface dialog) {
        mBtnPositive = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mBtnPositive.setEnabled(mEditText.length() > 0);

        mEditText.addTextChangedListener(this);
        AContext.showInputKeyboard(mEditText);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        AContext.hideInputKeyboard(mEditText);
        mEditText.removeTextChangedListener(this);
    }
}