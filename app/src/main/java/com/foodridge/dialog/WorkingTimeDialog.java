package com.foodridge.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.foodridge.R;
import com.foodridge.api.model.WorkingTime;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.type.WeekDay;
import com.foodridge.utility.Log;
import com.foodridge.utility.TimeCache;
import com.foodridge.utility.Utils;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 07/04/16
 */
public class WorkingTimeDialog extends BaseAlertDialog
        implements DialogInterface.OnClickListener, OnDismissListener, OnShowListener, View.OnClickListener {

    public static final int CODE = R.id.code_working_time;

    private static final Point DEFAULT_OPEN = new Point(10, 0);
    private static final Point DEFAULT_CLOSE = new Point(20, 0);

    //VIEW's
    private final AlertDialog mDialog;

    private final TextView mTvOpen;
    private final TextView mTvClose;
    private final TimePicker mPickerOpen;
    private final TimePicker mPickerClose;

    //VALUE's
    private final TimeCache mTimeCache;
    private final Point mPointOpen;
    private final Point mPointClose;

    private Object mTag;

    public WorkingTimeDialog(@NonNull Context context, @NonNull ObjectsReceiver receiver) {
        super(context, receiver);

        final ContextThemeWrapper wrapper = new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight);

        @SuppressLint("InflateParams")
        final View view = LayoutInflater.from(wrapper).inflate(R.layout.dialog_working_time, null);

        mTvOpen = (TextView) view.findViewById(R.id.tv_open);
        mTvClose = (TextView) view.findViewById(R.id.tv_close);
        mPickerOpen = (TimePicker) view.findViewById(R.id.picker_open);
        mPickerClose = (TimePicker) view.findViewById(R.id.picker_close);

        Utils.setTime(mPickerOpen, DEFAULT_OPEN);
        Utils.setTime(mPickerClose, DEFAULT_CLOSE);

        mTvOpen.setOnClickListener(this);
        mTvClose.setOnClickListener(this);

        mPickerOpen.setOnTimeChangedListener(mOpenTimeListener);
        mPickerClose.setOnTimeChangedListener(mCloseTimeListener);

        mPickerOpen.setTag(mOpenTimeListener);
        mPickerClose.setTag(mCloseTimeListener);

        mDialog = new Builder(wrapper)
                .setView(view)
                .setNegativeButton(R.string.Cancel, null)
                .setNeutralButton(R.string.Closed, this)
                .setPositiveButton(R.string.Save, this)
                .setCancelable(false)
                .create();

        mDialog.setOnDismissListener(this);
        mDialog.setOnShowListener(this);

        mTimeCache = TimeCache.getInstance();
        mPointOpen = new Point();
        mPointClose = new Point();
    }

    @Override
    public void show(@NonNull Object... objects) {
        final WorkingTime time = (WorkingTime) objects[0];
        @WeekDay final int weekDay = (int) objects[1];
        mTag = objects[2];

        final boolean is24Hour = mTimeCache.is24HourCached();
        mPickerOpen.setIs24HourView(is24Hour);
        mPickerClose.setIs24HourView(is24Hour);

        mTvOpen.setTag(null);
        mTvClose.setTag(null);

        mTvOpen.setText(null);
        mTvClose.setText(null);

        if (time == null) {
            mPointOpen.set(DEFAULT_OPEN.x, DEFAULT_OPEN.y);
            mPointClose.set(DEFAULT_CLOSE.x, DEFAULT_CLOSE.y);

        } else {
            final Point start = time.getStartTime();
            final Point end = time.getEndTime();

            mPointOpen.set(start.x, start.y);
            mPointClose.set(end.x, end.y);
        }
        Utils.setTime(mPickerOpen, mPointOpen);
        Utils.setTime(mPickerClose, mPointClose);

        if (mTvOpen.length() == 0 || mTvClose.length() == 0) {
            mOpenTimeListener.onTimeChanged(null, mPointOpen.x, mPointOpen.y);
            mCloseTimeListener.onTimeChanged(null, mPointClose.x, mPointClose.y);
        }
        mDialog.setTitle(weekDay);
        mDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            final String open = mContext.getString(R.string.format_hour_minutes, mPointOpen.x, mPointOpen.y);
            final String close = mContext.getString(R.string.format_hour_minutes, mPointClose.x, mPointClose.y);

            Utils.receiveObjects(mReceiver, CODE, mTag, open, close);

        } else {
            Utils.receiveObjects(mReceiver, CODE, mTag);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        mTag = null;

        mPickerOpen.setVisibility(View.GONE);
        mPickerClose.setVisibility(View.GONE);
    }

    private final OnTimeChangedListener mOpenTimeListener = new OnTimeChangedListener() {
        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            mPointOpen.set(hourOfDay, minute);
            mTvOpen.setText(toTimeString(hourOfDay, minute));
        }
    };

    private final OnTimeChangedListener mCloseTimeListener = new OnTimeChangedListener() {
        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            mPointClose.set(hourOfDay, minute);
            mTvClose.setText(toTimeString(hourOfDay, minute));
        }
    };

    @NonNull
    private CharSequence toTimeString(int hourOfDay, int minute) {
        Log.LOG.toLog(hourOfDay + ":" + minute);
        if (mTimeCache.is24HourCached()) {
            return mContext.getString(R.string.format_hour_minutes, hourOfDay, minute);
        }
        return Log.LOG.throughLog(mTimeCache.format(TimeCache.HH_MM, hourOfDay, minute));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_open:
                if (v.getTag() == null) {
                    v.setTag(true);
                    mPickerOpen.setVisibility(View.VISIBLE);

                    mTvClose.setTag(null);
                    Utils.changeVisibility(mPickerClose, View.GONE);

                } else {
                    v.setTag(null);
                    mPickerOpen.setVisibility(View.GONE);
                }
                break;

            case R.id.tv_close:
                if (v.getTag() == null) {
                    v.setTag(true);
                    mPickerClose.setVisibility(View.VISIBLE);

                    mTvOpen.setTag(null);
                    Utils.changeVisibility(mPickerOpen, View.GONE);

                } else {
                    v.setTag(null);
                    mPickerClose.setVisibility(View.GONE);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onShow(DialogInterface dialog) {
    }
}