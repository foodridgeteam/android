package com.foodridge.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ContextThemeWrapper;
import android.widget.FrameLayout;
import android.widget.NumberPicker;

import com.foodridge.R;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.Utils;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 26.04.16
 */
public class QuantityDialog extends BaseAlertDialog
        implements DialogInterface.OnClickListener {

    public static final int CODE = R.id.code_order_quantity;

    private final AlertDialog mDialog;

    private final NumberPicker mPicker;

    public QuantityDialog(@NonNull Activity activity, @Nullable ObjectsReceiver receiver) {
        super(activity, receiver);

        final ContextThemeWrapper context = new ContextThemeWrapper(activity, R.style.AppTheme_AlertDialogLight);

        final FrameLayout layout = new FrameLayout(context);
        final int spacing = context.getResources().getDimensionPixelSize(R.dimen.spacing);
        layout.setPadding(spacing * 2, spacing, spacing * 2, spacing);

        mPicker = new NumberPicker(context);
        mPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mPicker.setMinValue(1);

        layout.addView(mPicker);

        mDialog = new Builder(context)
                .setTitle(R.string.Quantity)
                .setView(layout)
                .setNegativeButton(R.string.Cancel, null)
                .setPositiveButton(R.string.Submit, this)
                .create();
    }

    @Override
    public void show(@NonNull Object... objects) {
        final int quantity = (int) objects[0];
        final int maxQuantity = (int) objects[1];

        mPicker.setValue(quantity);
        mPicker.setMaxValue(maxQuantity);

        mDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Utils.receiveObjects(mReceiver, CODE, mPicker.getValue());
    }
}