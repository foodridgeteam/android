package com.foodridge.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.foodridge.R;
import com.foodridge.adapter.PlaceAutocompleteAdapter;
import com.foodridge.api.model.DeliveryInfo;
import com.foodridge.application.AContext;
import com.foodridge.interfaces.ObjectsReceiver;
import com.foodridge.utility.GeoCache;
import com.foodridge.utility.Log;
import com.foodridge.utility.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.Locale;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 19.04.16
 */
public class DeliveryAddressGoogleDialog extends BaseAlertDialog
        implements OnClickListener, TextWatcher, OnShowListener, OnDismissListener {

    public static final int CODE = DeliveryAddressDialog.CODE;

    // VIEW's
    private final AutoCompleteTextView mEditText;
    private View mBtnPositive;

    private final AlertDialog mDialog;

    //VALUE's
    private final GoogleApiClient mClient;
    private final Geocoder mGeocoder;

    private Address mAddress;
    private DeliveryInfo mInfo;

    // ADAPTER
    private final PlaceAutocompleteAdapter mAdapter;

    public DeliveryAddressGoogleDialog(@NonNull Context context, @NonNull ObjectsReceiver receiver) {
        super(context, receiver);

        mClient = new GoogleApiClient.Builder(AContext.getApp())
                .addApi(Places.GEO_DATA_API)
                .build();
        mGeocoder = new Geocoder(AContext.getApp(), Locale.ENGLISH);

        final ContextThemeWrapper wrapper = new ContextThemeWrapper(context, R.style.AppTheme_AlertDialogLight);
        @SuppressLint("InflateParams")
        final View view = LayoutInflater.from(wrapper).inflate(R.layout.dialog_delivery_address_ac, null);

        final AutocompleteFilter filter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();
        mAdapter = new PlaceAutocompleteAdapter(AContext.getApp(), mClient, filter);

        mEditText = (AutoCompleteTextView) view.findViewById(R.id.edit_text);
        mEditText.setAdapter(mAdapter);

        mEditText.setOnItemClickListener(mItemClickListener);

        mDialog = new Builder(wrapper)
                .setTitle(R.string.Delivery_Address)
                .setView(view)
                .setNegativeButton(R.string.Cancel, null)
                .setPositiveButton(R.string.Submit, this)
                .create();
        mDialog.setOnShowListener(this);
        mDialog.setOnDismissListener(this);
    }

    @Override
    public void show(@NonNull Object... objects) {
        mInfo = (DeliveryInfo) objects[0];

        mAddress = null;
        mEditText.setText(GeoCache.toString(mInfo));
        AContext.showInputKeyboard(mEditText);
        mDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final String street = mInfo.getStreet();
        final String city = mInfo.getCity();
        final String country = mInfo.getCountry();
        final String more = mInfo.getMore();

        Utils.receiveObjects(mReceiver, CODE, street, city, country, more);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mBtnPositive != null) {
            mBtnPositive.setEnabled(!isEmptyShowRequired(mEditText) && mAddress != null);
        }
    }

    @Override
    public void onShow(DialogInterface dialog) {
        mBtnPositive = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mBtnPositive.setEnabled(false);

        mClient.connect();
        mEditText.addTextChangedListener(this);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        AContext.hideInputKeyboard(mEditText);

        mClient.disconnect();
        mEditText.removeTextChangedListener(this);
    }

    private final AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();

            Places.GeoDataApi.getPlaceById(mClient, placeId).setResultCallback(mCallback);
        }
    };

    private final ResultCallback<PlaceBuffer> mCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (places.getStatus().isSuccess()) {
                try {
                    final LatLng latLng = places.get(0).getLatLng();
                    mAddress = mGeocoder.getFromLocation(latLng.latitude, latLng.longitude, 1).get(0);

                    mInfo.set(mAddress.getAddressLine(0), mAddress.getLocality(), mAddress.getCountryName(), GeoCache.toString(latLng.latitude, latLng.longitude));

                } catch (IOException e) {
                    Log.LOG.toLog(e);
                    mAddress = null;
                }
                if (mBtnPositive != null) {
                    mBtnPositive.setEnabled(mAddress != null);
                }
            }
            places.release();
        }
    };
}