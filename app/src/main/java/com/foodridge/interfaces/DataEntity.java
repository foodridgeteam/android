package com.foodridge.interfaces;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 13.08.15
 */
public interface DataEntity<SELF, DATA> {

    void setData(@Nullable DATA data, @NonNull Object... objects);

    @Nullable
    DATA getData();

    @NonNull
    SELF getSelf();
}