package com.foodridge.interfaces;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 10.08.15
 */
public interface ObjectsReceiver {
    void onObjectsReceive(@IdRes int code, @NonNull Object... objects);
}