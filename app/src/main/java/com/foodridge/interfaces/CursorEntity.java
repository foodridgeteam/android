package com.foodridge.interfaces;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04.05.15
 */
public interface CursorEntity extends Serializable {

    void fromCursor(@NonNull Cursor cursor);

    @NonNull
    ContentValues toContentValues();
}