package com.foodridge.interfaces;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 04/04/16
 */
public interface CleaningEntity {
    /**
     * Release resources method
     */
    void onCleanUp() throws Exception;
}