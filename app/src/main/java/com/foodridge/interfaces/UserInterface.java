package com.foodridge.interfaces;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 02/05/16
 */
public interface UserInterface {

    String getName();

    String getAvatar();
}