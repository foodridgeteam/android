package com.foodridge.interfaces;

import android.support.annotation.NonNull;

/**
 * @author Tregub Artem tregub.artem@gmail.com
 * @since 19/04/16
 */
public interface AdapterCursorEntity<ENTITY> extends CursorEntity {

    @NonNull
    ENTITY getEntity();
}